package com.blazingseollc.qa.abstracts;

import com.blazingseollc.qa.env.rb.RebillyAdmin;
import com.blazingseollc.qa.env.rb.DashboardV2;
import com.blazingseollc.qa.env.rb.OMAD;
import org.junit.jupiter.api.BeforeEach;
import org.vitmush.qa.selenium_frame.base.SceneManager;
import org.vitmush.qa.selenium_frame.exceptions.URLIsImproper;
import java.lang.reflect.InvocationTargetException;

public class AbstractRebillyTest extends AbstractAnyTest {

    protected AbstractRebillyTest(){ this(false); }
    protected AbstractRebillyTest(boolean doBenchmark){
        super(doBenchmark);
    }

    public OMAD Omad = null;
    public RebillyAdmin Rebilly = null;
    public DashboardV2 Dashboard = null;

    protected AbstractAnyTest getSingle() throws InvocationTargetException, URLIsImproper, NoSuchMethodException, InstantiationException, IllegalAccessException {
        refreshScene();
        return this;
    }

    protected void refreshScene() throws InvocationTargetException, URLIsImproper, NoSuchMethodException, InstantiationException, IllegalAccessException {
        SceneManager.quit();
        before();
    }

    @BeforeEach
    public void before() throws URLIsImproper, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Omad = new OMAD();
        Rebilly = new RebillyAdmin();
        Dashboard = new DashboardV2();
    }

}
