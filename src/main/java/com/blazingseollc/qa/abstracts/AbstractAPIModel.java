package com.blazingseollc.qa.abstracts;

import org.json.simple.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class AbstractAPIModel {

    protected static <T> void required(String name, T param){
        if(param == null)
            throw new NullPointerException("%s is required".formatted(name));
    }

    protected static String jsonBody(String firstKey, Object firstValue, Object... keyAndValues){
        var json = new JSONObject();

        if(firstValue != null)
            json.put(firstKey, firstValue);

        for (int i = 0; i < keyAndValues.length; i+=2) {
            if(keyAndValues[i+1] != null)
                json.put(keyAndValues[i], keyAndValues[i+1]);
        }

        return json.toJSONString();
    }

    protected static Map<String, Object> params(String firstKey, Object firstValue, Object... keyAndValues){
        var map = new HashMap<String, Object>();

        if(firstValue != null)
            map.put(firstKey, firstValue);

        for (int i = 0; i < keyAndValues.length; i+=2) {
            if(keyAndValues[i+1] != null)
                map.put((String) keyAndValues[i], keyAndValues[i+1]);
        }

        return map;
    }

}
