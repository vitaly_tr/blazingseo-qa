package com.blazingseollc.qa.tools;

import com.blazingseollc.qa.base.Config;
import com.jcraft.jsch.JSchException;
import org.vitmush.qa.selenium_frame.base.Debug;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DB {

    public enum Host {Proxy_Backend, WHMCS, v2_Backend, Proxy_Creds };

    final String driver = "com.mysql.cj.jdbc.Driver";
    static final int sql_port = 3306;

    Connection con;
    SSH.ForwardedPortSession ssh_session;
    static DBConfig[] dbConfigs;
    static DB[] sessions = new DB[Host.values().length];

    static {
        dbConfigs = new DBConfig[Host.values().length];

        dbConfigs[Host.Proxy_Backend.ordinal()] = new DBConfig(SSH.Host.Proxy_Backend_DB,
                Config.db.proxy_backend.dbName, Config.db.proxy_backend.dbUsername, Config.db.proxy_backend.dbPassword,
                Config.db.proxy_backend.forwardingDestination);

        dbConfigs[Host.WHMCS.ordinal()] = new DBConfig(SSH.Host.WHMCS_DB,
                Config.db.whmcs.dbName, Config.db.whmcs.dbUsername, Config.db.whmcs.dbPassword,
                Config.db.whmcs.forwardingDestination);

        dbConfigs[Host.v2_Backend.ordinal()] = new DBConfig(SSH.Host.v2_Backend_DB,
                Config.db.v2_backend.dbName, Config.db.v2_backend.dbUsername, Config.db.v2_backend.dbPassword,
                Config.db.v2_backend.forwardingDestination);

        dbConfigs[Host.Proxy_Creds.ordinal()] = new DBConfig(SSH.Host.API,
                Config.db.api.proxy_creds.dbName, Config.db.api.proxy_creds.dbUsername, Config.db.api.proxy_creds.dbPassword,
                Config.db.api.proxy_creds.forwardingDestination);
    }

    public static class quick{

        public static class proxy_backend {

            public static String getTFACode(String email) throws SQLException, JSchException, ClassNotFoundException {
                //TODO check expiration once issue on production is resolved
                String sql = String.format("" +
                        "SELECT code FROM `user_mta_otp` \n" +
                        "WHERE (user_id in (select id from proxy_users where email = '%s') or user_key = '%s') \n" + //and expiration > now()\n" +
                        "order by id desc\n" +
                        "limit 1", email, email);
                return getSession(Host.Proxy_Backend).select(sql).first().getString(1);
            }

            public static List<Map<String, String>> getIPv4Packages(String email) throws JSchException, SQLException, ClassNotFoundException {
                String sql = String.format("SELECT pup.country, pup.category, pup.ports amount FROM proxy_user_packages pup \n" +
                        "JOIN proxy_users pu on pu.email = '%s' and pu.id = pup.user_id \n" +
                        "ORDER BY pup.id", email);
                return getSession(Host.Proxy_Backend).select(sql).fetchAll();
                /*var list = new ArrayList<Map<String, String>>();
                while(r.set.next()){
                    list.add(Map.of("country", r.set.getString(1), "category", r.set.getString(2),
                            "amount", r.set.getString(3)));
                }
                return list;*/
            }


            public static <T> List<Map<String, String>> getProxyInfo(List<T> ids) throws JSchException, SQLException, ClassNotFoundException {
                //transform lsit of integers to string and pass as list of ids to query
                String sql = String.format("select * from proxies_ipv4 where id in (%s)",
                        ids.stream().map(Object::toString).collect(Collectors.joining(",")));
                return getSession(Host.Proxy_Backend).select(sql).fetchAll();
            }

            public static <T> Map<String, String> getSourceInfo(T id) throws JSchException, SQLException, ClassNotFoundException {
                String sql = String.format("select * from proxy_source where id = %s", id.toString());
                return getSession(Host.Proxy_Backend).select(sql).fetchFirst();
            }

            public static <T> Map<String, String> getRegionInfo(T id) throws JSchException, SQLException, ClassNotFoundException {
                String sql = String.format("select * from proxy_regions where id = %s", id.toString());
                return getSession(Host.Proxy_Backend).select(sql).fetchFirst();
            }

            public static List<Map<String, String>> getRandomProxies(int limit) throws JSchException, SQLException, ClassNotFoundException {
                return getRandomProxies(limit, "1");
            }

            public static List<Map<String, String>> getRandomProxies(int limit, String whereQueryPart) throws JSchException, SQLException, ClassNotFoundException {
                String sql = String.format("select * from proxies_ipv4 where %s order by rand() limit %d", whereQueryPart, limit);
                return getSession(Host.Proxy_Backend).select(sql).fetchAll();
            }

            public static <T> void updateProxies(String updateQueryPart, List<T> ids) throws JSchException, SQLException, ClassNotFoundException {
                String sql = String.format("update proxies_ipv4 set %s where id in (%s)", updateQueryPart,
                        ids.stream().map(Object::toString).collect(Collectors.joining(",")));
                getSession(Host.Proxy_Backend).update(sql);
            }

            /**
             * @return all fields from proxy_users
             */
            public static Map<String, String> getUserInfo(String email) throws JSchException, SQLException, ClassNotFoundException {
                return getSession(Host.Proxy_Backend).select("select * from proxy_users where email = '%s'".formatted(email))
                        .fetchFirst();
            }
        }

        public static class v2_backend{

            /**
             *
             * @return all fields from users
             */
            public static Map<String, String> getClientInfo(String email) throws JSchException, SQLException, ClassNotFoundException {
                String sql = String.format("SELECT * from users where email = '%s'", email);
                return getSession(Host.v2_Backend).select(sql).fetchFirst();
            }

            /**
             *
             * @return []{status: status of subscription, type: subscription's, equals 'traffic' by default, value: amount of traffic}
             */
            public static List<Map<String, String>> getSubscriptionsBandwidth(String email) throws JSchException, SQLException, ClassNotFoundException {
                String sql = "SELECT s.status, s.type, b.value FROM `subscriptions` s\n" +
                        "join users u on u.id = s.user_id\n" +
                        "join user_balances b on b.subscription_id = s.id\n" +
                        "where email = '%s'".formatted(email);
                return getSession(Host.v2_Backend).select(sql).fetchAll();
            }

            public static void addBandwidth(String email, float mbytes) throws JSchException, SQLException, ClassNotFoundException {
                int bytes = (int) Math.floor(mbytes * Math.pow(1024, 2));
                getSession(Host.v2_Backend).update("insert into bandwidths " +
                        "select null, 0, %d, '0000-00-00', id, now(), now() from users where email = '%s'".formatted(bytes, email));
            }
        }

        public static class api {
            public static class proxy_creds{
                public static void removeCollectionsWithProxyIDs(List<Integer> proxyIDs) throws JSchException, SQLException, ClassNotFoundException {
                    var sql = "delete cpr, cc, ci, cp from creds_proxies cpr \n" +
                            "left join creds_collections cc on cc.id = cpr.collection_id \n" +
                            "left join creds_ip ci on ci.collection_id = cc.id \n" +
                            "left join creds_pwd cp on cp.collection_id = cc.id \n" +
                            "where cpr.proxy_id in (%s)".formatted(proxyIDs.stream().map(Object::toString).collect(Collectors.joining(", ")));
                    getSession(Host.Proxy_Creds).update(sql);
                }
            }
        }
    }



    public DB(String host, String port, String dbName, String dbUsername, String dbPassword) throws SQLException {
        String url = "jdbc:mysql://" + host +":" + port + "/";
        con = DriverManager.getConnection(url+dbName, dbUsername, dbPassword);
    }

    protected DB(Host host) throws JSchException, SQLException, ClassNotFoundException {
        int rport = sql_port;
        DBConfig config = dbConfigs[host.ordinal()];

        ssh_session = SSH.forwardPort(config.ssh_host, config.destination, rport);
        String dbUrl = "jdbc:mysql://localhost:%s/%s".formatted(ssh_session.lport, config.db);
        con = DriverManager.getConnection(dbUrl, config.username, config.password);
    }

    public static DB getSession(Host host) throws ClassNotFoundException, JSchException, SQLException {
        int ordinal = host.ordinal();
        if(sessions[ordinal] == null){
            sessions[ordinal] = new DB(host);
        }

        return sessions[ordinal];
    }



    public static class SelectResult{
        Statement st;
        public ResultSet set;

        public SelectResult(Statement st, ResultSet set){
            this.st = st;
            this.set = set;
        }

        public ResultSet first() throws SQLException {
            if(! set.first())
                throw new SQLException("Expected first result, but not found");
            return set;
        }

        public Map<String, String> fetchFirst() throws SQLException {
            set.first();
            return fetchCurrentRow(getColumnNames());
        }

        public List<Map<String, String>> fetchAll() throws SQLException {
            set.beforeFirst();
            var columns = getColumnNames();
            var list = new ArrayList<Map<String, String>>();
            while(set.next()){
                list.add(fetchCurrentRow(columns));
            }
            return list;
        }

        private Map<String, String> fetchCurrentRow(List<String> columns) throws SQLException {
            var map = new HashMap<String, String>();
            for(String column : columns){
                map.put(column, set.getString(column));
            }
            return map;
        }

        private List<String> getColumnNames() throws SQLException {
            var meta = set.getMetaData();
            int columnCount = meta.getColumnCount();
            var columnNames = new ArrayList<String>();
            for(int c = 1; c <= columnCount; c++){
                columnNames.add(meta.getColumnLabel(c));
            }
            return columnNames;
        }
    }

    public SelectResult select(String sql) throws SQLException {
        Statement st = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        ResultSet set = st.executeQuery(sql);
        return new SelectResult(st, set);
    }

    public void update(String sql) throws SQLException {
        Statement st = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
        st.executeUpdate(sql);
    }




    private static class DBConfig{
        SSH.Host ssh_host;
        String db;
        String username;
        String password;
        String destination;

        DBConfig(SSH.Host ssh_host, String db, String username, String password, String destination){
            this.ssh_host = ssh_host;
            this.db = db;
            this.username = username;
            this.password = password;
            this.destination = destination;
        }
    }


}
