package com.blazingseollc.qa.tools;

import com.blazingseollc.qa.base.Config;
import com.mysql.cj.exceptions.WrongArgumentException;
import org.eclipse.jetty.http.HttpStatus;
import org.vitmush.qa.selenium_frame.base.Debug;
import org.vitmush.qa.selenium_frame.exceptions.WrongValueException;

import javax.net.ssl.*;
import java.io.IOException;
import java.io.InputStream;
import java.net.*;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Comparator;
import java.util.regex.Pattern;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ProxyChecker {
    private ProxyChecker() { }

    public static float downloadWithISPRotating(String authCreds, int mbytes) throws InterruptedException, RuntimeException {

        Debug.Log("Downloading with ISP Rotating: " + mbytes + "mb");

        resetTunnelingProperty();

        String username;
        String password;
        String proxyIp;
        int proxyPort;

        try {
            String[] authPart = authCreds.split(":|@");
            username = authPart[0];
            password = authPart[1];
            proxyIp = authPart[2];
            proxyPort = Integer.parseInt(authPart[3]);
        } catch (ArrayIndexOutOfBoundsException ex){
            throw new WrongArgumentException("Argument authCreds expected to be isp rotating proxies auth credentials: username:password@ip:port; value provided instead: '" + authCreds + "'");
        }

        int max_threads = Config.misc.proxy_checker.download_threads;
        int max_tries = Config.misc.proxy_checker.retries_on_fail + 1;
        int threads = Math.min(max_threads, mbytes);
        int mbytes_per_thread = (int) Math.floor(mbytes / (float) threads);
        //all those not be downloaded because of rounding in mbytes_per_thread
        int extra_thread_mbytes = mbytes - mbytes_per_thread * threads;


        class DownloadLog {
            private final int goal;
            private final int step;
            public int downloaded_bytes = 0;
            public float downloaded_mbytes = 0;

            public DownloadLog(int downloadGoal){
                goal = downloadGoal;
                if(goal > 20000)
                    step = 5000;
                else if(goal >= 10000)
                    step = 2000;
                else if (goal >= 1000)
                    step = 200;
                else if (goal >= 100)
                    step = 50;
                else if (goal >= 50)
                    step = 10;
                else step = Math.min(goal, 5);
            }

            public void push(int bytes){
                if(bytes == 0)
                    return;

                downloaded_bytes += bytes;

                float prev_mbytes = downloaded_mbytes;
                downloaded_mbytes = (float) downloaded_bytes / 1024 / 1024;

                if((Math.floor(downloaded_mbytes / step) > Math.floor(prev_mbytes / step)) || downloaded_mbytes >= goal){
                    Debug.Log(String.format("Downloaded %fmb / %dmb", downloaded_mbytes, goal));
                }
            }
        }

        class Download implements Runnable {

            private int mbytes_left;
            private final DownloadLog log;
            //private final Lock fileLock = new ReentrantLock();

            public Download(int mbytes, DownloadLog log){
                this.mbytes_left = mbytes;
                this.log = log;
            }

            @Override
            public void run() {

                //tries count will reduce if 0 bytes downloaded, and exit if tries reached 0 left
                int tries_left = max_tries;

                while(this.mbytes_left > 0) {

                    //let's find biggest file to download in compare with mbytes_left
                    Integer mb_to_download = Arrays.asList(
                            new Integer[]{/*500, 200, 100, 50, 20,*/ 10, 5, 2, 1})
                            .stream().sorted(Comparator.reverseOrder())
                            .filter(n -> {
                        try {
                            //select biggest number if there's such file in configured in Config
                            return (this.mbytes_left >= n || n == 1)
                                    && null != Config.misc.proxy_checker.class.getField("mbyte_file_url_"+String.valueOf(n)).get(null);
                        } catch (Exception e) {  }
                        return false;
                    }).findFirst().get();

                    int downloaded_bytes = 0;

                    try {

                        String file = (String) Config.misc.proxy_checker.class.getField("mbyte_file_url_" + String.valueOf(mb_to_download)).get(null);

                        //Connect to HTTP server, and get response
                        HttpRequest request = HttpRequest.newBuilder(URI.create(file))
                                .version(HttpClient.Version.HTTP_2)
                                .build();
                        HttpResponse<InputStream> response = getHttpClient(proxyIp, proxyPort, true, username, password, true)
                                .send(request, HttpResponse.BodyHandlers.ofInputStream());
                        int statusCode = response.statusCode();
                        if (statusCode != 200)
                            throw new IOException(String.format("Response Status Code: %d %s", statusCode, HttpStatus.getMessage(statusCode)));

                        //Download file into 100mb buffer until it's fully downloaded
                        InputStream stream = response.body();
                        //1mb buffer, originally it loads only 16kb per turn
                        byte[] buffer = new byte[1024 * 1024];

                        int bytes = 0;
                        while (true) {
                            bytes = stream.read(buffer);
                            if (bytes != -1)
                                downloaded_bytes += bytes;
                            else break;
                        }

                    } catch (IOException | InterruptedException | NoSuchFieldException | IllegalAccessException e) {
                        //e.printStackTrace();
                        if(downloaded_bytes != 0) {
                            Debug.Log(String.format("%s\tTries Left: %d / %d (downloaded: %d = %fmb)",
                                    e.toString(), tries_left, max_tries, downloaded_bytes, (float) downloaded_bytes / 1024 / 1024));
                        }
                    }

                    //Get amount of downloaded MB, floored - we can download only mbytes, while due errors during download we may not get perfectly rounded amount of mbytes
                    log.push(downloaded_bytes);
                    this.mbytes_left -= Math.floor((float) downloaded_bytes / 1024 / 1024);

                    //Debug.Log(String.format("bytes: %d   downloading: %d   left: %d", downloaded_bytes, mb_to_download, mbytes_left));

                    //let's exit, probably failed tries was caused by something not be fixed anyway (like exhausted traffic)
                    if(tries_left-- <= 0)
                        break;

                    //reset amount of tries if at least something downloaded
                    if(downloaded_bytes > 0)
                        tries_left = max_tries;

                }
            }
        }

        Thread[] downloads = new Thread[threads+1];

        DownloadLog log = new DownloadLog(mbytes);
        IntStream.range(0, threads).forEach(t -> {
            downloads[t] = new Thread(new Download(mbytes_per_thread, log));
            downloads[t].start();
        });
        downloads[threads] = new Thread(new Download(extra_thread_mbytes, log));
        downloads[threads].start();

        for(Thread thread : downloads){
            thread.join();
        }

        //Debug.Log("Total downloaded: " + log.downloaded);

        if(log.downloaded_mbytes == 0)
            throw new RuntimeException("Nothing downloaded through Rotating Residential proxy");

        //Debug.Log(String.format("#bytes total: %d", log.downloaded_bytes));

        return log.downloaded_mbytes;
    }


    public static String getConnectionIp() throws IOException, InterruptedException {
        return getConnectionIp(getHttpClient());
    }

    public static String getConnectionIp(HttpClient client) throws IOException, InterruptedException {
		//TODO use https://check-host.net/ip instead
        String host = "https://www.myip.com/";

        HttpResponse<Stream<String>> response = client
                .send(HttpRequest.newBuilder(URI.create(host)).version(HttpClient.Version.HTTP_2).build(),
                        HttpResponse.BodyHandlers.ofLines());
        int statusCode = response.statusCode();
        if (statusCode != 200)
            throw new IOException(String.format("Response Status Code: %d %s", statusCode, HttpStatus.getMessage(statusCode)));

        String ipLine = response.body().filter(l -> l.contains("<span id=\"ip\">")).findFirst().get();
        var match = Pattern.compile("[0-9]{1,3}"+"\\.[0-9]{1,3}".repeat(3)).matcher(ipLine);
        match.find();
        return match.group(0);
    }

    public static void testIPv4Connection(String proxy) throws WrongValueException, IOException, InterruptedException {
        testIPv4Connection(proxy, proxy.split(":")[0]);
    }

    public static void testIPv4Connection(String proxy, String expectedIp) throws IOException, WrongValueException, InterruptedException {

        resetTunnelingProperty();

        String[] split = proxy.split(":");
        boolean pwdType = false;
        String proxyIp = split[0];
        int proxyPort = Integer.parseInt(split[1]);
        String username = "";
        String password = "";
        if(split.length == 4) {
            pwdType = true;
            username = split[2];
            password = split[3];

        }
        else if(split.length != 2)
            throw new WrongValueException("?", "IP or PWD", "proxy creds", "testIPv4Connection");

        String connectedIp = getConnectionIp(getHttpClient(proxyIp, proxyPort, pwdType, username, password, false));

        /*var proxyInfo = new Proxy();
        proxyInfo.setHttpProxy(proxyHost);
        proxyInfo.setSslProxy(proxyHost);
        if(pwdType){
            proxyInfo.setSocksUsername(username);
            proxyInfo.setSocksPassword(password);
        }
        ChromeOptions options = new ChromeOptions();
        options.setCapability("proxy", proxyInfo);*/

        /*String tabName = "ipv4_proxytest";
        SceneManager.Scene scene = SceneManager.add(tabName, new ChromeDriver(options));

        try {
            scene.a().get("https://www.myip.com/");
            scene.tab().waitLoaded();
            if(pwdType){
                //Helper.sleepSeconds(60);
                scene.a().switchTo().activeElement().sendKeys(tabName);
                Helper.sleepSeconds(10);
                scene.a().switchTo().alert().sendKeys(tabName);
            }
            connectedIp = scene.a().findElement(By.id("ip")).getText();
        } catch (NoSuchElementException ex) {
            throw new ConnectException("Couldn't check proxy ip. Seems proxy connection didn't work");
        } catch (PageTimeoutException ex) {
            throw new ConnectException("Timeout. Seems proxy connection didn't work");
        } finally {
            SceneManager.close(tabName);
        }*/

        if(!expectedIp.equals(connectedIp))
            throw new RuntimeException("Tried to connect through proxy ip %s:%d, ".formatted(proxyIp, proxyPort)
                    + (expectedIp.equals(proxyIp) ? "" : "and have gateway ip %s, ".formatted(expectedIp))
                    + "but gateway ip is %s instead".formatted(connectedIp));
            //throw new WrongValueException(connectedIp, expectedIp, "proxy", "ProxyChecker.testIPv4Connection");
    }

    private static HttpClient getHttpClient(){
        return getHttpClient(null, null, false,null, null, false);
    }

    private static HttpClient getHttpClient(String proxyIp, int proxyPort){
        return getHttpClient(proxyIp, proxyPort, null, null);
    }

    private static HttpClient getHttpClient(String proxyIp, int proxyPort, String authUsername, String authPassword){
        return getHttpClient(proxyIp, proxyPort, authUsername, authPassword, false);
    }

    private static HttpClient getHttpClient(String proxyIp, int proxyPort, String authUsername, String authPassword, boolean trustAllCertificates){
        return getHttpClient(proxyIp, proxyPort, true, authUsername, authPassword, trustAllCertificates);
    }

    private static HttpClient getHttpClient(String proxyIp, Integer proxyPort, boolean authorizeWithPassword, String authUsername, String authPassword, boolean trustAllCertificates){
        var sslParam = new SSLParameters();
        sslParam.setEnableRetransmissions(true);

        HttpClient.Builder builder = HttpClient.newBuilder();

        //if proxy ip and port provided
        if(proxyIp != null && proxyPort != null){
            builder.proxy(ProxySelector.of(new InetSocketAddress(proxyIp, proxyPort))).sslParameters(sslParam);
        }
        if(authorizeWithPassword) {
            builder.authenticator(new Authenticator() {
                @Override
                public PasswordAuthentication getPasswordAuthentication() {
                    return (new PasswordAuthentication(authUsername, authPassword.toCharArray()));
                }
            });
        }

        if(trustAllCertificates) {
            TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return null;
                        }
                        public void checkClientTrusted(
                                java.security.cert.X509Certificate[] certs, String authType) {
                        }
                        public void checkServerTrusted(
                                java.security.cert.X509Certificate[] certs, String authType) {
                        }
                    }
            };

            SSLContext sslContext = null;
            try {
                sslContext = SSLContext.getInstance("TLS");
                sslContext.init(null, trustAllCerts, new SecureRandom());
            } catch (NoSuchAlgorithmException | KeyManagementException e) {
                e.printStackTrace();
            }

            builder.sslContext(sslContext);
        }

        return builder.build();
    }

    private static final String tunnelingPropertyName = "jdk.http.auth.tunneling.disabledSchemes";
    private static String tunellingProperty;

    private static void resetTunnelingProperty(){
        System.setProperty(tunnelingPropertyName, "");
    }

    /*private static void saveTunellingProperty(){
        tunellingProperty = System.getProperty(tunnelingPropertyName);
        System.setProperty(tunnelingPropertyName, "");

        //String proxyingPropertyName = "jdk.http.auth.proxying.disabledSchemes";
        //String prevProxying = System.getProperty(proxyingPropertyName);
        //System.setProperty(proxyingPropertyName, "");
    }

    private static void revertTunellingProperty(){
        //revert tunneling property
        if(tunellingProperty == null)
            System.clearProperty(tunellingProperty);
        else System.setProperty(tunnelingPropertyName, tunellingProperty);
    }*/

}
