package com.blazingseollc.qa.tools;

import org.vitmush.qa.selenium_frame.base.Helper;

import java.util.Date;

public class Wait extends Helper.Wait {
    public Wait(int seconds) {
        super(seconds);
    }

    public Wait(Date untilDate) {
        super(untilDate);
    }
}
