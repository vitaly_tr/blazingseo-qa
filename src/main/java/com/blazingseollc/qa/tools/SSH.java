package com.blazingseollc.qa.tools;

import com.blazingseollc.qa.base.Config;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

public class SSH {

    public enum Host {Proxy_Backend_DB, WHMCS_DB, v2_Backend_DB, API};

    static final int ssh_port = 22;
    static final int port_from = 4320;
    static final int maximum_sessions = 100;
    static String ssh_dir;
    static String knownHosts;
    static Creds[] sshCreds;
    static ForwardedPortSession[] sessions = new ForwardedPortSession[Host.values().length];

    static {
        ssh_dir = Config.ssh.ssh_creds_folder;
        knownHosts = ssh_dir + "known_hosts";

        sshCreds = new Creds[Host.values().length];
        sshCreds[Host.Proxy_Backend_DB.ordinal()] = new Creds(Config.ssh.proxy_backend.host, Config.ssh.proxy_backend.username, Config.ssh.proxy_backend.privateKey);
        sshCreds[Host.WHMCS_DB.ordinal()] = new Creds(Config.ssh.whmcs.host, Config.ssh.whmcs.username, Config.ssh.whmcs.privateKey);
        sshCreds[Host.v2_Backend_DB.ordinal()] = new Creds(Config.ssh.v2_backend.host, Config.ssh.v2_backend.username, Config.ssh.v2_backend.privateKey);
        sshCreds[Host.API.ordinal()] = new Creds(Config.ssh.api.host, Config.ssh.api.username, Config.ssh.api.privateKey);
    }

    private static class Creds {
        public String host;
        public String user;
        public String password;
        public String privateKey;

        public Creds (String host, String user, String privateKeyName){
            this.host = host;
            this.user = user;
            this.privateKey = ssh_dir + privateKeyName;
        }
    }

    public static class ForwardedPortSession{
        public Session session;
        public int lport;

        ForwardedPortSession(Session session, int lport){
            this.session = session;
            this.lport = lport;
        }
    }

    public static ForwardedPortSession forwardPort (Host host, String destination, int rport) throws JSchException {

        int ordinal = host.ordinal();
        if(sessions[ordinal] != null){
            return sessions[ordinal];
        } else {

            Creds creds = sshCreds[ordinal];
            int lport = port_from + ordinal;

            JSch jsch = new JSch();
            jsch.setKnownHosts(knownHosts);
            jsch.addIdentity(creds.privateKey);

            Session session = jsch.getSession(creds.user, creds.host, ssh_port);
            session.setConfig("StrictHostKeyChecking", "no");
            ;
            session.connect();

            //For multiple sessions - find boundable port
            int maximal_port = port_from + maximum_sessions * sshCreds.length;
            while(true) {
                try {
                    session.setPortForwardingL(lport, destination, rport);
                    break;
                } catch(JSchException e){
                    //workaround only specific message, and only if amount of maximal sessions is reached
                    if(e.getMessage().matches("PortForwardingL: local port [0-9:\\.]+ cannot be bound.")
                            && lport <= maximal_port - sshCreds.length)
                        lport += sshCreds.length;
                    else throw e;
                }
            }

            sessions[ordinal] = new ForwardedPortSession(session, lport);
        }

        return sessions[ordinal];
    }

}
