package com.blazingseollc.qa.tools;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.GmailScopes;
import com.google.api.services.gmail.model.Message;
import com.google.api.services.gmail.model.MessagePartHeader;

import java.io.*;
import java.security.GeneralSecurityException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.*;
import java.util.stream.Collectors;

public class MailChecker {

    public static class Mail {
        public final Date date;
        public final String to;
        public final String from_email;
        public final String from_name;
        public final String subject;
        public final String body;

        private Mail(Date date, String to, String from, String subject, String body){
            this.date = date;
            this.to = to.replaceAll("[<>]", "").trim();
            this.from_email = from.replaceAll(".*<|>", "").trim();
            this.from_name = from.replaceAll("<.*", "").trim();
            this.subject = subject;
            this.body = body;
        }
    }

    private static final String APPLICATION_NAME = "BlazingSEO QA";
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static final String TOKENS_DIRECTORY_PATH = "tokens";

    /**
     * Global instance of the scopes required by this quickstart.
     * If modifying these scopes, delete your previously saved tokens/ folder.
     */
    private static final List<String> SCOPES = List.of(GmailScopes.GMAIL_READONLY);
    private static final String CREDENTIALS_FILE_PATH = ".\\credentials.json";
    private static final String USER = "me";
    private static Gmail gmail = null;


    public static List<Mail> getMailList(String email) throws GeneralSecurityException, IOException, ParseException {
        return getMailList(email, new Date(0));
    }

    public static List<Mail> getMailList(String email, int lastN) throws GeneralSecurityException, IOException, ParseException {
        return getMailList(email, new Date(0)).stream().limit(lastN).toList();
    }

    public static List<Mail> getMailList(String email, Duration newerThan) throws GeneralSecurityException, IOException, ParseException {
        return getMailList(email, new Date(new Date().getTime() - newerThan.toMillis()));
    }

    /**
     *
     * @param email email to which message is received
     * @param newerThan date to look for emails newer than date
     * @return list of mail, newer goes first, older goes last
     */
    public static List<Mail> getMailList(String email, Date newerThan) throws GeneralSecurityException, IOException, ParseException {

        // Build a new authorized API client service.
        if(gmail == null) {
            NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            gmail = new Gmail.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                    .setApplicationName(APPLICATION_NAME).build();
        }

        var messages = gmail.users().messages().list(USER)
                .setQ("to:%s".formatted(email)).execute().getMessages();

        List<Mail> mailList = new ArrayList<>();
        if (messages != null) {
            var messageIds = messages.stream().map(Message::getId).toList();
            for(var id : messageIds){
                var message = gmail.users().messages().get(USER, id).execute().getPayload();
                var headers = message.getHeaders().stream()
                        .filter(v -> List.of("From", "Subject", "Content-Type", "To", "Date").contains(v.getName()))
                        .collect(Collectors.toMap(MessagePartHeader::getName, MessagePartHeader::getValue, (v1, v2) -> v1));

                var dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy kk:mm:ss Z");
                var date = dateFormat.parse(headers.get("Date"));

                if(date.getTime() >= newerThan.getTime()){
                    var body = new String(message.getBody().decodeData()); //BaseEncoding.base64Url().decode(message.getBody().getData())
                    mailList.add(new Mail(
                            date, headers.get("To"), headers.get("From"),
                            headers.get("Subject"), body));
                }
            }
        }

        return mailList;
    }

    /**
     * Creates an authorized Credential object.
     * @param HTTP_TRANSPORT The network HTTP Transport.
     * @return An authorized Credential object.
     * @throws IOException If the credentials.json file cannot be found.
     */
    private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
        // Load client secrets.
        InputStream in = new FileInputStream(new File(CREDENTIALS_FILE_PATH));
        if (in == null) {
            throw new FileNotFoundException("Credentials file not found: " + CREDENTIALS_FILE_PATH);
        }
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

        // Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
                .setAccessType("offline")
                .build();
        LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();
        return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
    }


}
