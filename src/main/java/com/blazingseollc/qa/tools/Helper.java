package com.blazingseollc.qa.tools;

import com.blazingseollc.qa.base.Config;
import org.vitmush.qa.selenium_frame.base.Debug;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Calendar;

public class Helper {

    public static String lastGeneratedUniqueEmail;

    public static String generateUniqueEmail(){
        String email = String.format(Config.env.email_tokenFormat,
                (new SimpleDateFormat("yyyyMMdd_HHmmss")).format(Calendar.getInstance().getTime())
        );
        Debug.Templates.TestDebug("Unique email generated: " + email);
        lastGeneratedUniqueEmail = email;
        return email;
    }

    public static void sleep(int seconds){
        org.vitmush.qa.selenium_frame.base.Helper.sleep(seconds * 1000);
    }

    public static void sleep(Duration duration){
        org.vitmush.qa.selenium_frame.base.Helper.sleep(duration.toMillis());
    }

    //TODO fix and use
    public static double getPrice (String priceText) {
        String number = new String("");
        int separators = 0;
        for(char c : priceText.toCharArray()) {
            if(c == ',') c = '.';
            if(Character.isDigit(c)) {
                number += c;
            }
            else if (c == '.') {
                if(++separators >= 2) {
                    break;
                }
                number += c;
            }
        }
        try {
            return Double.parseDouble(number);
        } catch (NumberFormatException ex) {
            Debug.Error(String.format("Cannot parse price string %s (raw %s) in Price constructor", number, priceText));
            return 0;
        }
    }

}
