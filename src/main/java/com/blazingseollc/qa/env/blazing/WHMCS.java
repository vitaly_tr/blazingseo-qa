package com.blazingseollc.qa.env.blazing;

import com.blazingseollc.qa.base.Config;
import org.vitmush.qa.selenium_frame.abstracts.AbstractSite;
import org.vitmush.qa.selenium_frame.base.SceneManager;
import org.vitmush.qa.selenium_frame.exceptions.URLIsImproper;

public class WHMCS extends AbstractSite {

    public WHMCS() throws URLIsImproper {
        this(SceneManager.getDefault().tab("whmcs"));
    }

    public WHMCS(SceneManager.Scene.TabHandle tab) throws URLIsImproper {
        super(tab, Config.env.whmcs.url_whmcs);
    }
/*
    public Admin admin = new Admin();

    public class Admin {

        public Admin() throws URLIsImproper {  }

        public Any any = new Any();


        public class Any extends AbstractPage {

            Any() throws URLIsImproper {
                super("any_page", "/admin", true, false);
            }

            PageElement signin_button = new PageElement("signin-button", TokenType.LinkText, "Sign In");
            PageElement signup_button = new PageElement("signup-button", TokenType.LinkText, "Sign Up");


            public Admin open() {
                openAndWait(this.path, "whmcs admin area");
                return admin;
            }

            public boolean isAuthorized() throws MalformedURLException, WrongPageException {
                isSubPageOpened(path);
                try {
                    login.isOpened();
                } catch (WrongPageException ex) {
                    //then, it's not login page
                    return true;
                }

                return false;
            }

            public Admin authorize() throws MalformedURLException, WrongPageException, CredentialException, ConditionTimeoutException, NoSuchElementException {
                openAndWait(path, "open whmcs admin");
                if (!isAuthorized()) {
                    login.signIn(Config.env.whmcs.admin.username, Config.env.whmcs.admin.password);
                    waitOrWarn("whmcs admin signIn");
                }
                if (!isAuthorized()) {
                    throw new CredentialException("credentials from config seems didn't work on whmcs admin panel during signIn");
                }

                return this;
            }

        }


        public Login login = new Login();

        public class Login {
            protected Login() {
            }

            public final PageElement username_field = new PageElement("admin-login-username-field", TokenType.Name, "username");
            public final PageElement password_field = new PageElement("admin-password-username-field", TokenType.Name, "password");
            public final PageElement remember_checkbox = new PageElement("admin-remember-checkbox", TokenType.Name, "rememberme");
            public final PageElement submit_button = new PageElement("admin-submit-button", TokenType.CssSelector, "input[type=submit]");

            public final String path = "/admin/login.php";

            public Admin up() {
                return d();
            }

            public Login open() throws TimeoutException {
                openAndWait(this.path, "whmcs admin area login");
                return this;
            }

            public Login isOpened() throws WrongPageException, MalformedURLException {
                isPageOpened(path);
                return this;
            }

            public TabHandle signIn(String username, String password) throws ConditionTimeoutException, NoSuchElementException {
                signIn(username, password, true);

                return t;
            }

            public TabHandle signIn(String username, String password, boolean remember) throws ConditionTimeoutException, NoSuchElementException {
                input(username_field, username);
                input(password_field, password);
                click(remember_checkbox);
                click(submit_button);

                return t;
            }

        }
    }
    */
}