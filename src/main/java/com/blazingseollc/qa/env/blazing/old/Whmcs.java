package com.blazingseollc.qa.env.blazing.old;

import java.net.MalformedURLException;
import java.util.concurrent.TimeoutException;

import javax.security.auth.login.CredentialException;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;

import org.vitmush.qa.selenium_frame.base.SceneManager.Scene.TabHandle;

import com.blazingseollc.qa.base.Config;
import org.vitmush.qa.selenium_frame.types.PageElement;
import org.vitmush.qa.selenium_frame.types.PageElement.Token.TokenType;
import org.vitmush.qa.selenium_frame.exceptions.ConditionTimeoutException;
import org.vitmush.qa.selenium_frame.exceptions.NoSuchElementException;
import org.vitmush.qa.selenium_frame.exceptions.PageTimeoutException;
import org.vitmush.qa.selenium_frame.exceptions.WrongPageException;
import org.vitmush.qa.selenium_frame.exceptions.WrongValueException;
import org.vitmush.qa.selenium_frame.abstracts.Abstract;

public class Whmcs extends Abstract {

	public Whmcs(TabHandle tab, String baseUrl) {
		super(tab, baseUrl);
	}
	
	protected Whmcs d () {
		return this;
	}
	
	
	public OAuth oauth = new OAuth();
	public class OAuth {
		protected OAuth() {}
		
		public String path = "/oauth/authorize.php";
		
		public final PageElement confirm_button = new PageElement("oauth-confirm", TokenType.Id, "userAuthorizationAccepted");
		public final PageElement decline_button = new PageElement("oauth-decline", TokenType.Id, "userAuthorizationDeclined");
			
		public Whmcs up() {
			return d();
		}
		
		public OAuth isOpened() throws WrongPageException, MalformedURLException {
			isPageOpened(path);
			return this;
		}
		
		public TabHandle confirm() throws ConditionTimeoutException, NoSuchElementException {
			click(confirm_button);
			return t;
		}
		
		public TabHandle decline() throws ConditionTimeoutException, NoSuchElementException {
			click(decline_button);
			return t;
		}
	}
	
	
	public Client client = new Client();
	public class Client{
		protected Client(){}
		
		public final PageElement signin_button = new PageElement("signin-button", TokenType.LinkText, "Sign In");
		public final PageElement signup_button = new PageElement("signup-button", TokenType.LinkText, "Sign Up");
		
		public final String path = "/";
		
		public Whmcs up() {
			return Whmcs.this.d();
		}
		
		protected Client d() {
			return this;
		}
		
		public Client open() throws TimeoutException {
			openAndWait(this.path, "open home");
			return this;
		}
		
		public Client isOpened() throws WrongPageException, MalformedURLException {
			isPageOpened(path);
			return this;
		}
		
		//do some validation here
		public Client validate() {
			return this;
		}
		
		public Home home = new Home();
		public class Home {
			protected Home() { }
			
			public final PageElement signin_proxy_button = new PageElement("client-signin-proxy-button", TokenType.Id, "Secondary_Sidebar-Client_Shortcuts-login-to-proxy-dashboard");
			
			public final String path = "/clientarea.php";
			
			public Client up() {
				return d();
			}
			
			public Home open() throws TimeoutException {
				openAndWait(this.path, "open whmcs client home");
				return this;
			}
			
			public Home isOpened() throws WrongPageException, MalformedURLException {
				isPageOpened(path);
				return this;
			}
			
			public TabHandle pressLoginProxyDashboard() throws ConditionTimeoutException, NoSuchElementException {
				click(signin_proxy_button);
				return t;
			}
		}
		
		
		public Invoice invoice = new Invoice();
		public class Invoice {
			protected Invoice() { }
			
			public final PageElement get_proxies_button = new PageElement("client-signin-proxy-button", 
					TokenType.CssSelector, "div.text-center > a.btn.btn-success.btn-lg.col-sm-6");
			
			public final String path = "/viewinvoice.php";
			
			public Client up() {
				return d();
			}
			
			public Invoice open(int id) throws TimeoutException {
				openAndWait(this.path+"?id="+String.valueOf(id), "open whmcs client invoice");
				return this;
			}
			
			public Invoice isOpened() throws WrongPageException, MalformedURLException {
				isPageOpened(path);
				return this;
			}
			
			public TabHandle pressGetProxies() throws ConditionTimeoutException, NoSuchElementException {
				click(get_proxies_button);
				
				return t;
			}
		}
		
	}
	
	
	
	
	
	public Admin admin = new Admin();
	public class Admin {
		protected Admin(){}
		
		public final PageElement signin_button = new PageElement("signin-button", TokenType.LinkText, "Sign In");
		public final PageElement signup_button = new PageElement("signup-button", TokenType.LinkText, "Sign Up");
		
		public final String path = "/admin";
		
		public Whmcs up() {
			return Whmcs.this.d();
		}
		
		public Admin d() {
			return this;
		}
		
		public Admin open() throws TimeoutException {
			openAndWait(this.path, "whmcs admin area");
			return this;
		}
		
		public Admin isOpened() throws WrongPageException, MalformedURLException {
			isPageOpened(path);
			return this;
		}
		
		public boolean isAuthorized() throws MalformedURLException, WrongPageException {
			isSubPageOpened(path);
			try {
				login.isOpened();
			}
			catch(WrongPageException ex) {
				//then, it's not login page
				return true;
			}
			
			return false;
		}
		
		public Admin authorize() throws MalformedURLException, WrongPageException, CredentialException, ConditionTimeoutException, NoSuchElementException {
			openAndWait(path, "open whmcs admin");
			if(!isAuthorized()) {
				login.signIn(Config.env.whmcs.admin.username, Config.env.whmcs.admin.password);
				waitOrWarn("whmcs admin signIn");
			}
			if(!isAuthorized()) {
				throw new CredentialException("credentials from config seems didn't work on whmcs admin panel during signIn");
			}
			
			return this;
		}
		
		
		
		public Login login = new Login();
		public class Login {
			protected Login(){}
			
			public final PageElement username_field = new PageElement("admin-login-username-field", TokenType.Name, "username");
			public final PageElement password_field = new PageElement("admin-password-username-field", TokenType.Name, "password");
			public final PageElement remember_checkbox = new PageElement("admin-remember-checkbox", TokenType.Name, "rememberme");
			public final PageElement submit_button = new PageElement("admin-submit-button", TokenType.CssSelector, "input[type=submit]");
			
			public final String path = "/admin/login.php";
			
			public Admin up() {
				return d();
			}
			
			public Login open() throws TimeoutException {
				openAndWait(this.path, "whmcs admin area login");
				return this;
			}
			
			public Login isOpened() throws WrongPageException, MalformedURLException {
				isPageOpened(path);
				return this;
			}
			
			public TabHandle signIn(String username, String password) throws ConditionTimeoutException, NoSuchElementException {
				signIn(username, password, true);
				
				return t;
			}
			
			public TabHandle signIn(String username, String password, boolean remember) throws ConditionTimeoutException, NoSuchElementException {
				input(username_field, username);
				input(password_field, password);
				click(remember_checkbox);
				click(submit_button);
				
				return t;
			}
			
		}
		
		
		
		public Clients clients = new Clients();
		public class Clients{
			protected Clients(){}
			
			public final PageElement filter_tab = new PageElement("admin-clients-filter-tab", TokenType.Id, "tabLink1");
			public final PageElement filter_tab_active_state = new PageElement("admin-clients-filter-tab-active-state", 
					TokenType.CssSelector, "#tab1.active");
			public final PageElement filter_tab_inactive_state = new PageElement("admin-clients-filter-tab-inactive-state", 
					TokenType.CssSelector, "#tab1:not(.active)");
			public final PageElement email_field = new PageElement("admin-password-username-field", TokenType.Name, "email");
			public final PageElement submit_button = new PageElement("admin-submit-button", TokenType.Id, "search-clients");
			
			public final String path = "/admin/clients.php";
			
			public Admin up() {
				return d();
			}
			
			public Clients open() throws TimeoutException {
				openAndWait(this.path, "whmcs admin area clients search");
				return this;
			}
			
			public Clients isOpened() throws WrongPageException, MalformedURLException {
				isPageOpened(path);
				return this;
			}
			
			public Client.Summary findClient(String email) throws NoSuchElementException, MalformedURLException, WrongPageException, WrongValueException, ConditionTimeoutException {
				try {
					findOne(filter_tab_inactive_state);
					click(filter_tab);
				} catch (NoSuchElementException ex) {
					findOne(filter_tab_active_state);
				}
				
				input(email_field, email);
				click(submit_button);
				waitOrWarn("searching whmcs client");
				
				try {
					isOpened();
				}
				catch (WrongPageException ex) {
					client.summary.isOpened();
					return client.summary;
				}
				
				throw new WrongValueException(email, "", "email search", "Whmcs/Admin/Clients");
			}
		}
		
		
		
		public Client client = new Client();
		public class Client{
			protected Client(){}
			
			public Summary summary = new Summary();
			public class Summary{
				
				protected Summary(){}
				public final PageElement client_id = new PageElement("admin-client-summary-clientId", TokenType.Id, "userId");
				public final PageElement delete_client_button = new PageElement("admin-client-summary-delete", TokenType.CssSelector, "a[onclick='deleteClient();return false'");
				public final PageElement orders_link = new PageElement("admin-client-summary-orders-link", TokenType.LinkText, "View Orders");
				public final PageElement profile_link = new PageElement("admin-client-summary-profile-link", TokenType.Id, "clientTab-2");
				public final PageElement login_client_link = new PageElement("admin-client-summary-login", TokenType.Id, "summary-login-as-client");
				
				public final String path = "/admin/clientssummary.php";
				
				public Admin up() {
					return d();
				}
				
				public Summary open(int userid) throws TimeoutException {
					openAndWait(this.path+"?userid="+String.valueOf(userid), "whmcs admin open client");
					return this;
				}
				
				public Summary isOpened() throws WrongPageException, MalformedURLException {
					isPageOpened(path);
					return this;
				}
				
				public int getId() throws NumberFormatException {
					return Integer.parseUnsignedInt(t.a().findElement(client_id.by()).getText());
				}
				
				public TabHandle openOrders() throws MalformedURLException, WrongPageException, ConditionTimeoutException, NoSuchElementException {
					click(orders_link);
					//waitOrWarn("open orders on client summary");
					
					return t;
				}
				
				public Profile openProfile() throws MalformedURLException, WrongPageException, ConditionTimeoutException, NoSuchElementException {
					click(profile_link);
					waitOrWarn("open pfofile on client summary");
					profile.isOpened();
					
					return profile;
				}
				
				public TabHandle deleteAccount() throws ConditionTimeoutException, NoSuchElementException {
					click(delete_client_button);
					waitAlert().accept();
					
					return t;
				}
			
				
				public TabHandle pressLoginAsClient() throws ConditionTimeoutException, NoSuchElementException {
					click(login_client_link);
					
					return t;
				}
			}
			
			
			
			public Profile profile = new Profile();
			public class Profile{
				
				protected Profile(){}
				
				public final String path = "/admin/clientsprofile.php";
				
				public final PageElement email_field = new PageElement("admin-profile-email-field", TokenType.Name, "email");
				public final PageElement submit_button = new PageElement("admin-profile-submit-button", TokenType.CssSelector, "#contentarea .btn-container input[type=submit]");
				public final PageElement state_select = new PageElement("admin-profile-state-select", TokenType.Id, "stateselect");
				
				
				public Admin up() {
					return d();
				}
				
				public Profile open(int userid) throws TimeoutException {
					openAndWait(this.path+"?userid="+String.valueOf(userid), "whmcs admin - profile");
					return this;
				}
				
				public Profile isOpened() throws WrongPageException, MalformedURLException {
					isPageOpened(path);
					return this;
				}
				
				
				public Profile updateEmail(String newEmail) throws PageTimeoutException, ConditionTimeoutException, NoSuchElementException {
					input(email_field, newEmail);
					click(submit_button);
					t.waitLoaded();
					
					return this;
				}
				
				public Profile updateState() throws PageTimeoutException, ConditionTimeoutException, NoSuchElementException {
					select(state_select, 1);
					click(submit_button);
					t.waitLoaded();
					
					return this;
				}
				
				
				
			}
			
			
			
			public Orders orders = new Orders();
			public class Orders{
				
				protected Orders(){}
				
				public final String path = "/admin/orders.php";

				public final PageElement order_multiple_form  = new PageElement("admin-orders-multiple_form", TokenType.Name, "massaccept");
				public final PageElement filter_tab = new PageElement("admin-orders-filter-tab", TokenType.Id, "tabLink1");
				public final PageElement filter_tab_active_state = new PageElement("admin-orders-filter-tab-active-state", 
						TokenType.CssSelector, "#tab1.active");
				public final PageElement filter_tab_inactive_state = new PageElement("admin-orders-filter-tab-inactive-state", 
						TokenType.CssSelector, "#tab1:not(.active)");
				public final PageElement client_select_selected_state = new PageElement("admin-orders-client-selected", 
						TokenType.CssSelector, "#selectClientid > option:not([value=''])");
				
				public final PageElement select_all_checkbox = new PageElement("admin-orders-select-all", 
						TokenType.Id, "checkall0");
				public final PageElement cancel_button = new PageElement("admin-orders-cancel", 
						TokenType.Name, "masscancel");
				public final PageElement delete_button = new PageElement("admin-orders-delete", 
						TokenType.Name, "massdelete");
				public String js_get_total_orders = "return document.querySelector('form td[align=left]').innerText.split(' ')[0]";
				
				public Admin up() {
					return d();
				}
				
				public Orders open(int userid) throws TimeoutException {
					openAndWait(this.path+"?clientid="+String.valueOf(userid), "whmcs admin - client orders");
					return this;
				}
				
				public Orders isOpened() throws WrongPageException, MalformedURLException {
					isPageOpened(path);
					try {
						findOne(order_multiple_form);
					} catch (NoSuchElementException ex) {
						throw(new WrongPageException(t.a().getCurrentUrl(), siteUrl +path, "whmcs/admin/orders-multiple"));
					}
					return this;
				}
				
				public Orders validate() throws NoSuchElementException, ConditionTimeoutException {
					try {
						findOne(filter_tab_inactive_state);
						click(filter_tab);
					} catch (NoSuchElementException ex) {
						findOne(filter_tab_active_state);
					}
					
					findOne(client_select_selected_state);
					click(filter_tab);
					
					return this;
				}
				
				public int getTotalOrders() {
					return Integer.parseInt((String) ((JavascriptExecutor)t.a()).executeScript(js_get_total_orders));
				}
				
				public TabHandle deleteAll() throws MalformedURLException, ConditionTimeoutException, NoSuchElementException {
					
					click(select_all_checkbox);
					click(cancel_button);
					waitAlert().accept();
					waitOrWarn("wait for orders cancelled");
					
					click(select_all_checkbox);
					click(delete_button);
					waitAlert().accept();
					waitOrWarn("wait for orders removed");
					
					//TODO delete logic for removing all order on all pages, 
					//TODO ..add more validation steps (like client Name) before removing
					
					return t;
				}
				
			}
			
			
			public Order order = new Order();
			public class Order{
				
				protected Order(){}
				
				public final String path = "/admin/orders.php";
				
				public final PageElement single_order_form = new PageElement("admin-orders-single-form", 
						TokenType.Id, "frmWhois");
				public final PageElement order_cancel = new PageElement("admin-orders-single-order-cancel", 
						TokenType.CssSelector, "input[onclick='cancelOrder()']");
				public final PageElement order_cancel_disabled_state = new PageElement("admin-orders-single-order-cancel-disabled", 
						TokenType.CssSelector, "input[onclick='cancelOrder()'][disabled=disabled]");
				public final PageElement order_cancel_enabled_state = new PageElement("admin-orders-single-order-cancel-enabled", 
						TokenType.CssSelector, "input[onclick='cancelOrder()']:not([disabled=disabled])");
				public final PageElement order_delete = new PageElement("admin-orders-single-order-delete", 
						TokenType.CssSelector, "input[onclick='deleteOrder()']");
				
				
				public Admin up() {
					return d();
				}
				
				public Order open(int orderid) throws TimeoutException {
					openAndWait(this.path+"?action=view&id="+String.valueOf(orderid), "whmcs admin - order");
					return this;
				}
				
				public Order isOpened() throws WrongPageException, MalformedURLException, NoSuchElementException {
					isPageOpened(path);
					try {
						findOne(single_order_form);
					} catch (NoSuchElementException ex) {
						throw(new WrongPageException(t.a().getCurrentUrl(), siteUrl +path, "whmcs/admin/orders-multiple"));
					}
					return this;
				}
				
				public TabHandle delete() throws ConditionTimeoutException, NoSuchElementException {
					try {
						findOne(order_cancel_enabled_state);
						click(order_cancel);
						waitAlert().accept();
						waitOrWarn("wait order cancelled");
					} catch(NoSuchElementException ex) {
						//then it's aready cancelled, let's just delte order then
					}
					
					click(order_delete);
					waitAlert().accept();
					waitOrWarn("wait order deleted");
					
					return t;
				}
				
			}
			
		}
		
		
		public ConfigGeneral configGeneral = new ConfigGeneral();
		public class ConfigGeneral {
		
		protected ConfigGeneral() { }
		
		public final String path = "/admin/configgeneral.php";
		
		public final PageElement password_field = new PageElement("admin-config-password-field", 
			TokenType.Id, "inputConfirmPassword");
		public final PageElement password_submit_button = new PageElement("admin-config-password-submit", 
			TokenType.CssSelector, "form[method=post] > button[type=submit]");
		
		public final PageElement security_tab = new PageElement("admin-config-security-tab", 
			TokenType.LinkText, "Security");
		public final PageElement captcha_off_radio = new PageElement("admin-config-captcha-off", 
			TokenType.Id, "captcha-setting-alwaysoff");
		public final PageElement captcha_on_radio = new PageElement("admin-config-captcha-on", 
			TokenType.CssSelector, "input[name=captchasetting][value=on]");
		public final PageElement settings_submit_button = new PageElement("admin-config-submit", 
			TokenType.Id, "saveChanges");
		
		
		public Admin up() {
			return d();
		}
		
		public ConfigGeneral open() throws TimeoutException {
			openAndWait(this.path, "whmcs admin - config");
			return this;
		}
		
		public ConfigGeneral isOpened() throws WrongPageException, MalformedURLException, NoSuchElementException {
			isPageOpened(path);
			return this;
		}
		
		public ConfigGeneral auth() throws ConditionTimeoutException, NoSuchElementException {
			try {
				findOne(password_field);
			} catch (NoSuchElementException ex) {
				//probably already authorized
				return this;
			}
			input(password_field, Config.env.whmcs.admin.password);
			click(password_submit_button);
			waitOrWarn("authorizing in admin's config");
			return this;
		}
		
		public ConfigGeneral setCaptcha(boolean enabled) throws ConditionTimeoutException, NoSuchElementException {
			auth();
			openAndWait(path+"?tab=10", "openening security tab");
			waitElement(ExpectedConditions.presenceOfElementLocated(captcha_on_radio.by()));
			
			if(enabled) {
				click(captcha_on_radio);
			} else {
				click(captcha_off_radio);
			}
			click(settings_submit_button);
			
			waitOrWarn("saving settings");
			return this;
		}
		}


		public class Invoice {
			Invoice(){

			}
		}
			
	}

}
