package com.blazingseollc.qa.env.blazing;

import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.util.concurrent.TimeoutException;

import com.blazingseollc.qa.tools.Helper;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import org.vitmush.qa.selenium_frame.base.Debug;
import org.vitmush.qa.selenium_frame.base.SceneManager.Scene;

import org.vitmush.qa.selenium_frame.abstracts.AbstractSite;
import org.vitmush.qa.selenium_frame.exceptions.*;
import org.vitmush.qa.selenium_frame.types.PageElement;
import org.vitmush.qa.selenium_frame.types.PageElement.Token.TokenType;

public class ProxyDashboard extends AbstractSite {
	
	public ProxyDashboard(Scene.TabHandle tab, String baseUrl) throws URLIsImproper {
		super(tab, baseUrl);
	}
	
	private ProxyDashboard d () {
		return this;
	}

	public Home home = new Home();
	public Login login = new Login();
	public Signup signup = new Signup();
	public Dashboard dashboard = new Dashboard();
	public Purchase purchase = new Purchase();
	public Upgrade upgrade = new Upgrade();


	public class Home {
		
		protected Home(){}
				
		static final String signin_button_class = ".btn.btn-primary.btn-lg";
		static final String signin_button_text = "Sign In";
		static final String signup_button_class = ".btn.btn-success.btn-lg";
		static final String signup_button_text = "Sign Up";
		
		final String path = "/dashboard";
		
		public ProxyDashboard up() {
			return d();
		}
		
		public Home open() throws TimeoutException {
			openAndWait(this.path, "open home");
			return this;
		}
		
		public Home isOpened() throws WrongPageException, MalformedURLException {
			isPageOpened(path);
			return this;
		}
		
		//do some validation here
		public Home validate() {
			return this;
		}
		
		public Signup pressSignup() {
			//s.driver.findElement(By.className(signup_button_class)).click();
			t.a().findElement(By.linkText(signup_button_text)).click();
			waitOrWarn("pressSignup");
			return signup;
		}
		
		public Login pressSignin() {
			t.a().findElement(By.className(signup_button_class)).click();
			waitOrWarn("pressSignin");
			return login;
		}
	}

	public class Login {
		protected Login(){}
		
		final String path = "/dashboard/login-type";
		
		ForgotPassword forgot_password = new ForgotPassword();
		public class ForgotPassword {
			protected ForgotPassword(){}
			
			public Pre pre = new Pre();
			public class Pre {
				Pre(){}

			}
			
			public Post post = new Post();
			public class Post {
				Post(){}
				
			}
		}
	}

	public class Signup {
		Signup(){}
		
		final String path = "/purchase";
		
		public ProxyDashboard up () {
			return d();
		}
		
		public Signup open() {
			openAndWait(this.path, "signup");
			return this;
		}
		
		public Signup isOpened() throws MalformedURLException, WrongPageException {
			isPageOpened(this.path);
			return this;
		}
		
		public class PE extends PageElement{
			public PE(String name, TokenType type, String value) {
				super("dashboard-signup-" + name, type, value);
			}	
		}
		
		public final PE lose_focus = new PE("lose_focus", TokenType.Class, "content-heading");
		public final PE country_select = new PE("country-select", TokenType.Name, "plan[country][]");
		public final PE category_select = new PE("category-select", TokenType.Name, "plan[category][]");
		public final PE amount_field = new PE("amount-field", TokenType.Name, "plan[amount][]");
		public final PE warning_button = new PE("warning_button", TokenType.Class, "btn-warning");
		public final PE promo_field = new PE("promo-field", TokenType.Name, "details[promocode]");
		public final PE promo_apply = new PE("promo-apply", TokenType.LinkText, "Apply Promo Code");
		public final PE promo_discount = new PE("promo_discount", TokenType.Class, "discount-total");
		public final PE total = new PE("total", TokenType.Id, "total");
		public final PE continue_button = new PE("continue-button", TokenType.CssSelector, "#step1-body > .panel-body > .btn-continue-step1");
		public final PE continue_button_disabled_state = 
				new PE("continue-button-disabled", TokenType.CssSelector, "#step1-body > .panel-body > .btn-continue-step1.disabled");
		public final PE continue_button_enabled_state = 
				new PE("continue-button-enabled", TokenType.CssSelector, "#step1-body > .panel-body > .btn-continue-step1:not(.disabled)");
		public final PE email_field = new PE("email-field", TokenType.Id, "email");
		public final PE password_field = new PE("password-field", TokenType.Id, "password");
		public final PE password_confirm_field = new PE("password_confirm-field", TokenType.CssSelector, "[data-match=\"#password\"]");
		public final PE first_name_field = new PE("first-name-field", TokenType.Name, "firstname");
		public final PE last_name_field = new PE("last-name-field", TokenType.Name, "lastname");
		public final PE phone_number_field = new PE("phone-number-field", TokenType.Name, "phone");
		public final PE birthday_field = new PE("birthday-field", TokenType.Id, "birthday");
		public final PE company_name_field = new PE("company-name-field", TokenType.Name, "company");
		public final PE address_field = new PE("address-field", TokenType.Name, "address");
		public final PE city_field = new PE("city-field", TokenType.Name, "city");
		public final PE state_field = new PE("state-field", TokenType.Name, "state");
		public final PE postcode_field = new PE("postcode-field", TokenType.Name, "postcode");
		public final PE country_list = new PE("country-list", TokenType.Name, "country");
		public final PE tos_checkbox = new PE("terms-of-service-checkbox", TokenType.Id, "checkbox-tos");
		public final PE aup_checkbox = new PE("acceptable-use-policy-checkbox", TokenType.Id, "checkbox-aup");
		public final PE pp_checkbox= new PE("privacy-policy-checkbox", TokenType.Id, "checkbox-pp");
		public final PE captcha_frame = new PE("captcha-iframe", TokenType.CssSelector, "iframe[role=presentation][scrolling=no]");
		public final PE captcha_checkbox = new PE("captcha-checkbox", TokenType.Id, "recaptcha-anchor");
		public final PE captcha_solved_state = new PE("captcha-solved-state", TokenType.CssSelector, ".recaptcha-checkbox[aria-checked=\"true\"]");
		public final PE captcha_loading_state = new PE("captcha-loading-state", TokenType.Class, "recaptcha-checkbox-loading");
		public final PE captcha_disabled_state = new PE("captcha-disabled-state", TokenType.Class, "recaptcha-checkbox-disabled");
		public final PE captcha_timeout_state = new PE("captcha-timeout-state", TokenType.Class, "recaptcha-checkbox-expired");
		public final PE complete_button = new PE("complete-button", TokenType.CssSelector, "[type=\"submit\"].disabled");
		public final PE complete_button_disabled_state = 
				new PE("complete-button-disabled-state", TokenType.CssSelector, "[type=\"submit\"].disabled");
		public final PE complete_button_enabled_state = 
				new PE("complete-button-enabled-state", TokenType.CssSelector, "[type=\"submit\"]:not(.disabled)");
		
		public class SignupForm {
			String plan_country, plan_category, plan_amount, promo, email, password, confirmPassword, firstName, lastName,
			phoneNumber, birthday, address, city, state, postcode, country;
			
			public SignupForm(){}
			public SignupForm(String plan_country, String plan_category, String plan_amount, String promo, 
					String email, String password, String confirmPassword, String firstName, String lastName,
			String phoneNumber, String birthday, String address, String city, String state, String postcode, String country) {
				String[] params = {plan_country, plan_category, plan_amount, promo, email, password, confirmPassword, firstName, lastName,
				phoneNumber, address, city, state, postcode, country};
				Field[] fields = SignupForm.class.getDeclaredFields();
				for(int i = 0; i < fields.length; ++i) {
					try {
						fields[i].set(this, params[i]);
					} catch (Exception e) {
						Debug.Error("Cannot assign field on SignupForm constructor");
					}
				}
			}
		}
		
		public void signUp(String plan_country, String plan_category, String plan_amount, String promo, 
				String email, String password, String confirmPassword, String firstName, String lastName,
		String phoneNumber, String birthday, String address, String city, String state, String postcode, String country) throws NoSuchElementException, ConditionTimeoutException {
			
			fillForm(plan_country, plan_category, plan_amount, promo, email, password, confirmPassword, 
					firstName, lastName, phoneNumber, birthday, address, city, state, postcode, country);
			
			fillTOS();
			fillCaptcha();
			submit();
		}
		
		public Signup fillForm(SignupForm f) throws ConditionTimeoutException, NoSuchElementException {
			return fillForm(f.plan_country, f.plan_category, f.plan_amount, f.promo, f.email, f.password,
					f.confirmPassword, f.firstName, f.lastName, f.phoneNumber, f.birthday, f.address, f.city, f.state,
					f.postcode, f.country);
		}
		
		public Signup fillForm(String plan_country, String plan_category, String plan_amount, String promo, 
				String email, String password, String confirmPassword, String firstName, String lastName,
		String phoneNumber, String birthday, String address, String city, String state, String postcode, String country) throws ConditionTimeoutException, NoSuchElementException {
			
			select(country_select, plan_country);
			select(category_select, plan_category);
			try {
				click(warning_button);
			} catch(ElementNotInteractableException ex) { }
			
			input(amount_field, plan_amount);
			click(lose_focus);
			
			if(!promo.isEmpty()) {
				input(promo_field, promo);
				click(promo_apply);
			}

			//waitElement(ExpectedConditions.presenceOfElementLocated(continue_button_enabled_state.by()));
			click(continue_button);
		
			input(email_field, email);
			input(password_field, password);
			input(password_confirm_field, confirmPassword);
			input(first_name_field, firstName);
			input(last_name_field, lastName);
			input(phone_number_field, phoneNumber);
			input(birthday_field, birthday);
			input(address_field, address);
			input(city_field, city);
			input(state_field, state);
			input(postcode_field, postcode);
			select(country_list, country);
			
			return this;
		}
		
		public Signup fillTOS() throws ConditionTimeoutException, NoSuchElementException {
			click(tos_checkbox);
			click(aup_checkbox);
			click(pp_checkbox);
			
			return this;
		}
		
		public Signup fillCaptcha() throws NoSuchElementException, ConditionTimeoutException {
			WebDriver captcha = t.a().switchTo().frame(findOne(captcha_frame));
			captcha.findElement(captcha_checkbox.by()).click();
			waitElement(ExpectedConditions.presenceOfElementLocated(captcha_solved_state.by()));
			
			return this;
		}
		
		public void submit() throws ConditionTimeoutException, NoSuchElementException {
			//waitElement(ExpectedConditions.presenceOfElementLocated(complete_button_enabled_state.by()));
			click(complete_button);
		}
		
		public double total() {
			return Helper.getPrice(t.a().findElement(total.by()).getText());
		}
		
		public Signup validateTotal(double expect) throws WrongValueException {
			var total = total();
			if(total != expect)
				throw new WrongValueException(String.valueOf(total), String.valueOf(expect), this.total.name, "Dashboard/Signup");
			
			return this;
		}
	}

	public class Dashboard {
		Dashboard(){}
	}

	public class Purchase {
		Purchase(){}
	}

	public class Upgrade {
		Upgrade(){}
	}
}
