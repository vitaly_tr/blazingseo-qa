package com.blazingseollc.qa.env.blazing;

import com.blazingseollc.qa.env.rb.DashboardV2;
import com.blazingseollc.qa.tools.DB;
import com.blazingseollc.qa.tools.Helper;
import com.blazingseollc.qa.base.ProxyType;
import com.jcraft.jsch.JSchException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.vitmush.qa.selenium_frame.base.Debug;
import org.vitmush.qa.selenium_frame.base.SceneManager;
import org.vitmush.qa.selenium_frame.base.SceneManager.Scene.TabHandle;
import org.vitmush.qa.selenium_frame.abstracts.AbstractSite;
import org.vitmush.qa.selenium_frame.types.PageElement.Token.TokenType;
import org.vitmush.qa.selenium_frame.exceptions.*;

import com.blazingseollc.qa.base.Config;

import java.sql.SQLException;
import java.util.ArrayList;

public class BDR extends AbstractSite {

	public BDR() throws URLIsImproper { this(SceneManager.getDefault().tab("bdr")); }

	public BDR(TabHandle tab) throws URLIsImproper {
		super(tab, Config.env.whmcs.url_bdr);
	}

	public Signup signup = new Signup();
	public QuickTasks quick = new QuickTasks();

	public class QuickTasks {

		public void createAccount() throws JSchException, PageTimeoutException, SQLException, ConditionTimeoutException, ClassNotFoundException, NoSuchElementException, WrongPageException, WebFormErrorException {
			createAccount(Helper.generateUniqueEmail());
		}

		public void createAccount(String email) throws WrongPageException, NoSuchElementException, PageTimeoutException, ConditionTimeoutException, JSchException, SQLException, ClassNotFoundException, WebFormErrorException {
			createAccount(email, Config.env.password);
		}

		public void createAccount(String email, String password) throws WrongPageException, NoSuchElementException, PageTimeoutException, ConditionTimeoutException, JSchException, SQLException, ClassNotFoundException, WebFormErrorException {
			ProxyType type = new ProxyType(ProxyType.ProxyCategory.Dedicated);
			signup.open().selectProduct(type.category, type.country, type.billingCycle, Tools.getSignupQuantity(type))
					.fillInformation(email, password).setTOS().pressCheckout();
			if(signup.isTFAInquired())
				signup.confirmTFA(DB.quick.proxy_backend.getTFACode(email));
		}

		public void createAccount(String email, ProxyType pendingProduct) throws WrongPageException, NoSuchElementException, PageTimeoutException, ConditionTimeoutException, JSchException, SQLException, ClassNotFoundException, WebFormErrorException {
			ProxyType type = pendingProduct;
			String password = Config.env.password;
			signup.open().selectProduct(type.category, type.country, type.billingCycle, Tools.getSignupQuantity(type))
					.fillInformation(email, password).setTOS().pressCheckout();
			if(signup.isTFAInquired())
				signup.confirmTFA(DB.quick.proxy_backend.getTFACode(email));
		}

		public void createAccountWithProduct(ProxyType type) throws JSchException, PageTimeoutException, SQLException, ConditionTimeoutException, ClassNotFoundException, NoSuchElementException, WrongPageException, WebFormErrorException {
			createAccountWithProduct(type, Tools.getSignupQuantity(type));
		}

		public void createAccountWithProduct(ProxyType type, int quantity) throws JSchException, PageTimeoutException, SQLException, ConditionTimeoutException, ClassNotFoundException, NoSuchElementException, WrongPageException, WebFormErrorException {
			createAccountWithProduct(type, quantity, Helper.generateUniqueEmail());
		}

		public void createAccountWithProduct(ProxyType type, int quantity, String email) throws PageTimeoutException, ConditionTimeoutException, NoSuchElementException, WrongPageException, JSchException, SQLException, ClassNotFoundException, WebFormErrorException {
			signup.open().doFillRequiredFields(type, quantity, email, Config.env.password).setPromo(Config.env.whmcs.promo_100)
					.pressCheckout();
			if(signup.isTFAInquired())
				signup.confirmTFA(DB.quick.proxy_backend.getTFACode(email));
		}

		public QuickTasks signupWithArguments(ProxyType type, int quantity, String query) throws PageTimeoutException, ConditionTimeoutException, NoSuchElementException, WrongPageException, WebFormErrorException, JSchException, SQLException, ClassNotFoundException {
			String email = Helper.generateUniqueEmail();
			openAndWait("%s?%s".formatted(signup.path, query), signup.pathAsUrl);
			signup.doFillRequiredFields(type, quantity, email, Config.env.password).setPromo(Config.env.whmcs.promo_100)
					.pressCheckout();
			if(signup.isTFAInquired())
				signup.confirmTFA(DB.quick.proxy_backend.getTFACode(email));
			Debug.Preserve(String.format("Signup with %s - %s %s", ProxyType.AliasCollection.common.countryName.of(type.country), ProxyType.AliasCollection.common.categoryName.of(type.category), ProxyType.AliasCollection.common.billingCycleName.of(type.billingCycle)), true);
			return this;
		}

		public static class Tools {
			public static int getSignupQuantity(ProxyType type) {
				int amount = 7;
				if (type.category == ProxyType.ProxyCategory.ISP)
					amount = 27;
				return amount;
			}
		}
	}

	public class Signup extends AbstractPage{
		protected Signup() throws URLIsImproper {
			super("BDR Signup", "/purchase");
		}

		protected PageElement name_field = new PageElement("name_field", TokenType.Id, "input-20");
		protected PageElement email_field = new PageElement("email_field", TokenType.Id, "input-26");
		protected PageElement other_categories_checkbox = new PageElement("other_categories", TokenType.CssSelector, ".v-input:nth-child(1) .v-input__slot > .v-label > .h6");
		protected PageElement ipv4_button = new PageElement("ipv4_button", TokenType.XPath, "//span[@class='v-btn__content' and contains(text(), 'IPv4')]/..");
		protected PageElement ipv4_button_selected = new PageElement("ipv4_button_selected", TokenType.XPath, "//span[@class='v-btn__content' and contains(text(), 'IPv4')]/parent::button[contains(@class, 'theme--dark')]");
		protected PageElement ipv6_button = new PageElement("ipv6_button", TokenType.XPath, "//span[@class='v-btn__content' and contains(text(), 'IPv6')]/..");
		protected PageElement ipv6_button_selected = new PageElement("ipv6_button_selected", TokenType.XPath, "//span[@class='v-btn__content' and contains(text(), 'IPv6')]/parent::button[contains(@class, 'theme--dark')]");
		protected PageElement quantity_input = new PageElement("quantity_input", TokenType.CssSelector, ".v-text-field__slot > [type='number']");
		protected PageElement code_input = new PageElement("code_input", TokenType.XPath, "//label[starts-with(text(), 'Please input code')]/parent::div/input");
		protected PageElement code_confirm_button = new PageElement("code_confirm_button", TokenType.XPath, "//span[@class='v-btn__content' and contains(text(), 'Confirm')]/parent::button");
		protected PageElement preloader = new PageElement("preloader", PageElement.Token.TokenType.Class, "v-dialog__content--active");
		protected PageElement promocode_input = new PageElement("promocode_input", TokenType.XPath, "//input[@placeholder='Promo Code']");
		protected PageElement promocode_apply_button = new PageElement("promocode_apply_button", TokenType.XPath, "//span[@class='v-btn__content' and contains(text(), 'Apply')]/parent::button");
		protected PageElement wrong_promocode_alert = new PageElement("wrong_promocode_alert", TokenType.XPath, "//div[@class='v-alert__content' and contains(text(), 'Sorry, that promo code is not valid')]/parent::div/parent::div[not(contains(@style, 'display: none'))]");
		protected GenericPageElement generic_option_selector = new GenericPageElement("generic_option_selector", TokenType.XPath, "//div[text() = '%s']/../parent::div | //span[text() = ' %s ']/parent::button", (s, obj) -> s.formatted(obj[0], obj[0]));
		protected PageElement alert_block = new PageElement("alert_block",  TokenType.XPath, "//div[contains(@class, 'v-toast__item--error')]");
		protected PageElement error_messages = new PageElement("error_messages", TokenType.Class, "v-messages__message");


		public Signup open() {
			return (Signup) _open();
		}

		public Signup doFillRequiredFields(ProxyType proxyType, int amount, String email, String password) throws WrongPageException, ConditionTimeoutException, NoSuchElementException, PageTimeoutException {
			assertPageOpened();
			resetProductSelection().selectProduct(proxyType.category, proxyType.country, proxyType.billingCycle, amount)
					.fillInformation(email, password).setTOS();
			return this;
		}

		public Signup resetProductSelection() throws WrongPageException {
			assertPageOpened();

			try {
				findOne(ipv4_button_selected, true).click();
			} catch (NoSuchElementException e) {
				//Debug.Log("No selected elements found on BDR.resetProductSelection");
			}

			try {
				findOne(ipv6_button_selected, true).click();
			} catch (NoSuchElementException e) { }
			
			return this;
		}

		public Signup selectProduct(boolean trial,ProxyType.ProxyCategory category, ProxyType.ProxyCountry country,
									int quantity, ProxyType.BillingCycle cycle) throws WrongPageException, NoSuchElementException, ConditionTimeoutException {

			assertPageOpened();

			if(trial) {
				click(new PageElement("trial_button", TokenType.CssSelector, ".row:nth-child(2) > .col-md-4"));
			}
			else {
				click(new PageElement("purchase_button", TokenType.CssSelector, ".col-md-4:nth-child(1) .v-btn__content"));
			}

			return selectProduct(category, country, cycle, quantity);
		}

		public Signup selectProduct(ProxyType.ProxyCategory category, ProxyType.ProxyCountry country,
				ProxyType.BillingCycle cycle, int quantity) throws WrongPageException, NoSuchElementException, ConditionTimeoutException {
			assertPageOpened();

			resetProductSelection();

			if(category == ProxyType.ProxyCategory.IPv6) {
				click(ipv6_button);
				click(generic_option_selector.context("category").get(ProxyType.AliasCollection.bdr.categoryName.of(ProxyType.ProxyCategory.Dedicated)));
			} else {
				click(ipv4_button);
				click(generic_option_selector.context("category").get(ProxyType.AliasCollection.bdr.categoryName.of(category)));
				click(generic_option_selector.context("country").get(ProxyType.AliasCollection.common.countryName.of(country)));
			}

			clean(quantity_input);
			input(quantity_input, String.valueOf(quantity));
			click(generic_option_selector.context("billing_cycle").get(ProxyType.AliasCollection.bdr.billingCycleName.of(cycle)));

			return this;
		    
		}
		
		public Signup fillInformation(String email, String password) throws WrongPageException, PageTimeoutException, NoSuchElementException {
			assertPageOpened();
			
			//TODO rewrite for AbstractSite usage 
			
		    input(name_field, email.split("@")[0]);
		    t.a().findElement(By.id("input-23")).sendKeys("---");
		    input(email_field, email);
		    t.a().findElement(By.id("input-35")).sendKeys("456456");
		    t.a().findElement(By.id("input-41")).sendKeys("asdfasdf");
		    t.a().findElement(By.id("input-50")).sendKeys("asdfasdf");
		    t.a().findElement(By.id("input-47")).sendKeys("asdfasdf");
		    t.a().findElement(By.id("input-53")).sendKeys("456456456");
		    t.a().findElement(By.id("input-79")).sendKeys(password);
		    t.a().findElement(By.id("input-76")).sendKeys(password);
		    
		    return this;
		}
		
		public Signup setTOS() throws NoSuchElementException, ConditionTimeoutException {
			click(new PageElement("tos_checkbox", TokenType.CssSelector, ".v-input:nth-child(3) .v-input--selection-controls__ripple"));
			return this;
		}

		public Signup setPromo(String promo) throws NoSuchElementException, ConditionTimeoutException, IllegalArgumentException {
			input(promocode_input, promo);
			click(promocode_apply_button);
			waitDisappeared(preloader);
			try {
				findOne(wrong_promocode_alert, true);
				throw new IllegalArgumentException("Promo didn't added: '%s'".formatted(promo));
			} catch (NoSuchElementException ex){
				//that's ok, there should no be such exception
			}

			return this;
		}
		
		public void pressCheckout() throws NoSuchElementException, PageTimeoutException, WebFormErrorException, ConditionTimeoutException {

			String currentUrl = getUrl();
			click(new PageElement("checkout_button", TokenType.CssSelector, ".pa-3:nth-child(3)"));
			//waitElement(ExpectedConditions.presenceOfElementLocated(code_input.by()));

			boolean tfaIsRequired = isTFAInquired(true);

			if(!tfaIsRequired)
				waitDisappeared(preloader);

			try{
				pageUpdatedSleep();
				//TODO fix this doesn't work on wrong amount input
				String error = findOne(alert_block).getText();
				ArrayList<String> context = new ArrayList<String>();
				for(WebElement message : findAll(error_messages, true)){
					context.add(message.getText());
				}
				throw new WebFormErrorException(error, getPageName(), context);
			} catch (NoSuchElementException ex){
				//Should not appear, continuing
			}

			if(!tfaIsRequired)
				t.waitPageChanged(currentUrl);
		}

		public boolean isTFAInquired(){
			return isTFAInquired(false);
		}

		public boolean isTFAInquired(boolean waitAppearance){
			try {
				findOne(code_input, !waitAppearance);
				return true;
			} catch (NoSuchElementException e) {
				//not inquired then
			}
			return false;
		}

		public void confirmTFA(String code) throws NoSuchElementException, PageTimeoutException {
			input(code_input, code);
			click(code_confirm_button);

			t.waitPageChanged();
		}

	}
	
}
