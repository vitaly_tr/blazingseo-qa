package com.blazingseollc.qa.env.lightproxies;

import org.vitmush.qa.selenium_frame.base.SceneManager.Scene;
import org.vitmush.qa.selenium_frame.base.SceneManager.Scene.TabHandle;

import com.blazingseollc.qa.base.Config;

public class Whmcs extends com.blazingseollc.qa.env.blazing.old.Whmcs {

	public Whmcs(Scene.TabHandle tab){
		super(tab, Config.env.url.lp_whmcs);
	}
	
	public Whmcs(TabHandle tab, String baseUrl) {
		super(tab, baseUrl);
	}
	
}
