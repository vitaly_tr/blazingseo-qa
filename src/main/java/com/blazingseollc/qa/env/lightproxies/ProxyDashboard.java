package com.blazingseollc.qa.env.lightproxies;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import com.blazingseollc.qa.tools.Helper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.vitmush.qa.selenium_frame.base.SceneManager.Scene;
import org.vitmush.qa.selenium_frame.base.SceneManager.Scene.TabHandle;

import com.blazingseollc.qa.base.Config;

import org.vitmush.qa.selenium_frame.abstracts.Abstract;
import org.vitmush.qa.selenium_frame.types.PageElement;
import org.vitmush.qa.selenium_frame.types.PageElement.Token.TokenType;
import org.vitmush.qa.selenium_frame.exceptions.*;


public class ProxyDashboard extends Abstract {
	
	public ProxyDashboard(Scene.TabHandle tab){
		super(tab, Config.env.url.lp_dashboard);
	}
	
	public ProxyDashboard(Scene.TabHandle tab, String baseUrl){
		super(tab, baseUrl);
	}
	
	protected ProxyDashboard d () {
		return this;
	}
	
	public  ProxyDashboard isOpened() throws MalformedURLException, WrongPageException{
		isSubPageOpened("");
		return this;
	}
	
	public enum AuthType { PWD, IP }
	
	public SynonymsClass synonyms = new SynonymsClass();
	public class SynonymsClass {
		
		public final CountryClass country = new CountryClass();
		public class CountryClass {
			public final Synonym usa = new Synonym("USA", "US", "us");
			public final Synonym german = new Synonym("Germany", "DE", "de");
			public final Synonym brazil = new Synonym("Brazil", "Brazil", "br");
			public final Synonym canada = new Synonym("Canada", "Canadian", "ca");
		}
		
		public final CategoryClass category = new CategoryClass();
		public class CategoryClass {
			public final Synonym dedicated = new Synonym("Dedicated", "Dedicated", "dedicated");
			public final Synonym semi = new Synonym("Shared", "Shared", "semi-3");
			public final Synonym rotate = new Synonym("Backconnect", "Backconnect", "rotating");
			public final Synonym sneaker = new Synonym("Sneaker", "Dedicated", "sneaker");
		}
		
		public class Synonym {
			public String listName;
			public String packageName;
			public String robotName;
			
			public Synonym(String listName, String packageName, String robotName){
				this.listName = listName;
				this.packageName= packageName;
				this.robotName = robotName;
			}
			
			public boolean compareList(String str) {
				return listName.equals(str);
			}
			
			public boolean comparePackage(String str) {
				return packageName.equals(str);
			}
			
			public boolean compareRobot(String str) {
				return robotName.equals(str);
			}
		}
	}
	
	public Home home = new Home();
	public class Home {
		
		protected Home(){}
				
		public final PageElement signin_button = new PageElement("signin-button", TokenType.LinkText, "Sign In");
		public final PageElement signup_button = new PageElement("signup-button", TokenType.LinkText, "Sign Up");
		
		protected String path = "";
		
		public ProxyDashboard up() {
			return d();
		}
		
		public Home open() throws TimeoutException {
			openAndWait(this.path, "open home");
			return this;
		}
		
		public Home isOpened() throws WrongPageException, MalformedURLException {
			isPageOpened(path);
			return this;
		}
		
		//do some validation here
		public Home validate() {
			return this;
		}
		
		public Signup pressSignup() throws ConditionTimeoutException, NoSuchElementException {
			click(signup_button);
			waitOrWarn("pressSignup");
			return signup;
		}
		
		public Login pressSignin() throws ConditionTimeoutException, NoSuchElementException {
			click(signin_button);
			waitOrWarn("pressSignin");
			return login;
		}
	}
	
	public Login login = new Login();
	public class Login {
		protected Login(){}
		
		protected String path = "/auth/sign-in.html";
		
		public class PE extends PageElement{
			public PE(String name, TokenType type, String value) {
				super("signin-" + name, type, value);
			}	
		}
		
		public final PE email_field = new PE("email-field", TokenType.Name, "login");
		public final PE pwd_field = new PE("pwd-field", TokenType.Name, "password");
		public final PE submit_button = new PE("submit_button", TokenType.CssSelector, "input[type=submit]");
		
		public ProxyDashboard up() {
			return d();
		}
		
		public Login open() {
			openAndWait(path, "openLoginPage");
			return this;
		}
		
		public Login isOpened() throws MalformedURLException, WrongPageException {
			isPageOpened(path);
			return this;
		}
		
		public TabHandle signIn(String email, String password) throws ConditionTimeoutException, NoSuchElementException {
			input(email_field, email);
			input(pwd_field, password);
			click(submit_button);
			
			return t;
		}
		
		ForgotPassword forgot_password = new ForgotPassword();
		public class ForgotPassword {
			protected ForgotPassword(){}
		}
	}
	
	public Signup signup = new Signup();
	public class Signup {
		protected Signup(){}
		
		protected String path = "/purchase";
		
		public ProxyDashboard up () {
			return d();
		}
		
		public Signup open() {
			openAndWait(this.path, "signup");
			return this;
		}
		
		public Signup isOpened() throws MalformedURLException, WrongPageException {
			isPageOpened(this.path);
			return this;
		}
		
		public class PE extends PageElement{
			public PE(String name, TokenType type, String value) {
				super("signup-" + name, type, value);
			}	
		}
		
		public final PE lose_focus = new PE("lose_focus", TokenType.CssSelector, "h2.m0");
		public final PE country_select = new PE("country-select", TokenType.Name, "plan[country][]");
		public final PE category_select = new PE("category-select", TokenType.Name, "plan[category][]");
		public final PE amount_field = new PE("amount-field", TokenType.Name, "plan[amount][]");
		public final PE promo_field = new PE("promo-field", TokenType.Name, "details[promocode]");
		public final PE promo_apply = new PE("promo-apply", TokenType.LinkText, "Apply Promo Code");
		public final PE promo_discount = new PE("promo_discount", TokenType.Class, "discount-total");
		public final PE total = new PE("total", TokenType.Id, "total");
		public final PE continue_button1 = new PE("continue-button1", TokenType.CssSelector, "#plan a[type=button]");
		public final PE email_field = new PE("email-field", TokenType.Id, "email");
		public final PE password_field = new PE("password-field", TokenType.Id, "password");
		public final PE password_confirm_field = new PE("password_confirm-field", TokenType.CssSelector, "[data-match='#password']");
		public final PE first_name_field = new PE("first-name-field", TokenType.Name, "firstname");
		public final PE last_name_field = new PE("last-name-field", TokenType.Name, "lastname");
		public final PE phone_number_field = new PE("phone-number-field", TokenType.Name, "phone");
		public final PE continue_button2 = new PE("continue-button1", TokenType.CssSelector, "#personal a[type=button]");
		public final PE company_name_field = new PE("company-name-field", TokenType.Name, "company");
		public final PE address_field = new PE("address-field", TokenType.Name, "address");
		public final PE city_field = new PE("city-field", TokenType.Name, "city");
		public final PE state_field = new PE("state-field", TokenType.Name, "state");
		public final PE postcode_field = new PE("postcode-field", TokenType.Name, "postcode");
		public final PE country_list = new PE("country-list", TokenType.Name, "country");
		public final PE tos_checkbox = new PE("terms-of-service-checkbox", TokenType.Id, "checkbox-tos");
		public final PE complete_button = new PE("complete-button", TokenType.CssSelector, "[type=\"submit\"]");
		public final PE complete_button_disabled_state = 
				new PE("complete-button-disabled-state", TokenType.CssSelector, "[type=\"submit\"].disabled");
		public final PE complete_button_enabled_state = 
				new PE("complete-button-enabled-state", TokenType.CssSelector, "[type=\"submit\"]:not(.disabled)");
		public final PE error_text_span = new PE("error-text", TokenType.Class, "alert-flash");

		public TabHandle signUp(String plan_country, String plan_category, String plan_amount, String promo, 
				String email, String password, String confirmPassword, String firstName, String lastName,
		String phoneNumber,String companyName, String address, String city, String state, String postcode, String country) 
				throws Exception {
			
			fillForm(plan_country, plan_category, plan_amount, promo, email, password, confirmPassword, 
					firstName, lastName, phoneNumber, companyName, address, city, state, postcode, country);
			fillTOS();
			submit();
			
			try {
				t.waitLoaded();
				isPageOpened(path+"/process");
				String error = getError();
				throw new Exception("Couldn't sign up. '"+error+"'");
			} catch(WrongPageException ex) {
				//then, user was redirected further
			}
			
			return t;
		}
		
		public Signup fillForm(String plan_country, String plan_category, String plan_amount, String promo, 
				String email, String password, String confirmPassword, String firstName, String lastName,
		String phoneNumber,String companyName, String address, String city, String state, String postcode, String country) throws ConditionTimeoutException, NoSuchElementException {
			
			select(country_select, plan_country);
			select(category_select, plan_category);			
			input(amount_field, plan_amount);
			
			if(!promo.isEmpty()) {
				input(promo_field, promo);
				click(promo_apply);
			}

			click(continue_button1);
		
			input(email_field, email);
			input(password_field, password);
			input(password_confirm_field, confirmPassword);
			input(first_name_field, firstName);
			input(last_name_field, lastName);
			input(phone_number_field, phoneNumber);
			
			click(continue_button2);
			
			input(company_name_field, companyName);
			input(address_field, address);
			input(city_field, city);
			input(state_field, state);
			input(postcode_field, postcode);
			select(country_list, country);
			
			return this;
		}
		
		public Signup fillTOS() throws ConditionTimeoutException, NoSuchElementException {
			click(tos_checkbox);
			
			return this;
		}
		
		public TabHandle submit() throws ConditionTimeoutException, NoSuchElementException {
			//waitElement(ExpectedConditions.presenceOfElementLocated(complete_button_enabled_state.by()));
			click(complete_button);
			
			return t;
		}
		
		public double total() {
			return Helper.getPrice(t.a().findElement(total.by()).getText());
		}
		
		public Signup validateTotal(double expect) throws WrongValueException {
			var total = total();
			if(total != expect)
				throw new WrongValueException(String.valueOf(total), String.valueOf(expect), this.total.name, "Dashboard/Signup");
			
			return this;
		}
		
		public String getError() throws NoSuchElementException {
			return findOne(error_text_span).getText();
		}
	}
	
	public Locations locations = new Locations();
	public class Locations {
		
		protected Locations(){}
		
		protected String path = "/update-locations.html";
		
		
		//f like us_dedicated
		public final PageElement package_tab_button_f = new PageElement("locations-package-button-tab", TokenType.CssSelector, "a[href='#%f_proxies]'");
		//f like us_dedicated
		public final PageElement package_tab_f = new PageElement("locations-package-tab", TokenType.Id, "%f_proxies");
		public final PageElement save_locations_button = new PageElement("locations-save", TokenType.CssSelector, "input.location_save[type=submit]");
		public final PageElement save_locations_button_disabled_state = new PageElement("locations-save-disabled", TokenType.CssSelector, "input.location_save[type=submit].disabled");
		public final PageElement save_locations_button_enabled_state = new PageElement("locations-save-enabled", TokenType.CssSelector, "input.location_save[type=submit]:not(.disabled)");
		
		
		public ProxyDashboard up() {
			return d();
		}
		
		public Locations open() throws TimeoutException {
			openAndWait(this.path, "open home");
			return this;
		}
		
		public Locations isOpened() throws WrongPageException, MalformedURLException {
			isPageOpened(path);
			return this;
		}
		
		public Dashboard saveAllMixedOnFirst() throws TimeoutException, WrongPageException, MalformedURLException, NoSuchElementException, ConditionTimeoutException {
			WebElement firstLocationHandle = findOne(new PageElement("first-mixed-handle", TokenType.Class, "noUi-origin"));
			WebElement mostRightElement = findOne(new PageElement("static-2", TokenType.LinkText, "Logout"));
			
			build().clickAndHold(firstLocationHandle).moveToElement(mostRightElement).release(firstLocationHandle).perform();
		
			saveLocations();
			
			try {
				t.waitLoaded();
			} catch(PageTimeoutException ex) {
				dashboard.open();
			}
			
			waitOrWarn("open dashboard");
			
			return dashboard.isOpened();
		}
		
		public TabHandle saveLocations() throws TimeoutException, ConditionTimeoutException, NoSuchElementException {
			//waitElement(ExpectedConditions.presenceOfElementLocated(save_locations_button_enabled_state.by()));
			click(save_locations_button);
			
			return t;
		}
	}
	
	public Dashboard dashboard = new Dashboard();
	public class Dashboard {
		protected Dashboard(){}
		
		protected String path = "/overview.html";
		
		public final PageElement proxies_tab = new PageElement("dashboard-proxies_tab", TokenType.CssSelector, "a[href='#proxies']");
		public final PageElement auth_tab = new PageElement("dashboard-auth-tab", TokenType.CssSelector, "a[href='#authtype']");
		public final PageElement ip_auth_radio = new PageElement("dashboard-ip-auth-radio", TokenType.Id, "format_ip");
		public final PageElement pwd_auth_radio = new PageElement("dashboard-pwd-auth-radio", TokenType.Id, "format_pw");
		public final PageElement export_modal = new PageElement("dashboard-export-modal-button", TokenType.Class, "modal-content");
		public final PageElement export_modal_button = new PageElement("dashboard-export-modal-button", TokenType.Class, "ion-ios-download-outline");
		public final PageElement modal_export_all_link = new PageElement("dashboard-pwd-auth-radio", TokenType.CssSelector, "a[href='/export/all']");
		public final PageElement modal_close_button = new PageElement("dashboard-pwd-auth-radio", TokenType.CssSelector, "button[data-dismiss='modal']");
		public final PageElement add_ip_button = new PageElement("dashboard-auth-add-ip-button", TokenType.Class, "add-new-ip");
		public final PageElement auth_ip_el = new PageElement("dashboard-auth-add-ip-listelement", TokenType.CssSelector, "tr[data-ip-id]:not(.template)");
		
		
		public ProxyDashboard up() {
			return d();
		}
		
		public Dashboard open() throws TimeoutException {
			openAndWait(this.path, "open dashboard");
			return this;
		}
		
		public Dashboard isOpened() throws WrongPageException, MalformedURLException {
			isPageOpened(path);
			return this;
		}
		
		public Dashboard openProxiesTab() throws ConditionTimeoutException, NoSuchElementException {
			click(proxies_tab);
			sleep();
			return this;
		}
		
		public Dashboard openAuthTab() throws ConditionTimeoutException, NoSuchElementException {
			click(auth_tab);
			sleep();
			return this;
		}
		
		
		public Dashboard setAuthType(AuthType type) throws ConditionTimeoutException, NoSuchElementException {
			openAuthTab();
			if(type == AuthType.IP) {
				click(ip_auth_radio);
			}
			else {
				click(pwd_auth_radio);
			}
			sleep();
			return this;
		}
		
		public Dashboard setCurrentAuthIP() throws ConditionTimeoutException, org.vitmush.qa.selenium_frame.exceptions.NoSuchElementException {
			setAuthType(AuthType.IP);
			openAuthTab();
			click(add_ip_button);
			waitElement(ExpectedConditions.presenceOfElementLocated(auth_ip_el.by()));
			
			return this;
		}
		
		public Dashboard validateIpAdded() throws WrongValueException, org.vitmush.qa.selenium_frame.exceptions.NoSuchElementException, ConditionTimeoutException {
			openAuthTab();
			if(findAll(auth_ip_el).size() > 0) {
				return this;
			}
			else {
				throw new WrongValueException("", "", auth_ip_el.name, "Dashboard:validateIpAdded");
			}
		}
		
		public Dashboard closeExportModal() throws ConditionTimeoutException, NoSuchElementException {
			click(modal_close_button);
			waitElement(ExpectedConditions.invisibilityOfElementLocated(export_modal.by()));
			
			return this;
		}
		
		public Scene openExportAll() throws ConditionTimeoutException, NoSuchElementException, TabAlreadySavedException {
			openProxiesTab();
			click(export_modal_button);
			waitElement(ExpectedConditions.elementToBeClickable(modal_export_all_link.by()));
			click(modal_export_all_link);
			closeExportModal();
			
			t.scene().saveRecentTab();
			return t.scene();
		}
		
		public void openExportPackage(String country_packageName, String category_packageName) throws ConditionTimeoutException, NoSuchElementException, TabAlreadySavedException {
			openProxiesTab();
			click(export_modal_button);
			PageElement package_export_link = new PageElement("export-package-"+country_packageName+"_"+category_packageName, 
					TokenType.LinkText, country_packageName + " " + category_packageName + " Proxies");
			waitElement(ExpectedConditions.elementToBeClickable(package_export_link.by()));
			click(package_export_link);
			closeExportModal();
			
			t.scene().saveRecentTab();
		}
		
		public Export export = new Export();
		public class Export {
			
			protected Export(){}
				
			protected String path = "/export";
			
			public final PageElement ips_list = new PageElement("general_export_api_list", TokenType.Tag, "pre");

			public ProxyDashboard up() {
				return d();
			}
			
			public Export open_all() throws TimeoutException {
				openAndWait(this.path+"/all", "open export all");
				return this;
			}
			
			public Export isOpened() throws WrongPageException, MalformedURLException {
				isSubPageOpened(path);
				
				return this;
			}
			
			public String getList() {
				return t.a().findElement(ips_list.by()).getText();
			}
			
		}
	}
	
	
	public Sidebar sidebar = new Sidebar();
	public class Sidebar {
		
		protected Sidebar(){}
			
		protected String path = "/";
		
		public final PageElement sidebar = new PageElement("sidebar", TokenType.Class, "sidebar-nav");
		public final PageElement checkout_link = new PageElement("sidebar_checkout", TokenType.LinkText, "Checkout");
		
		
		public ProxyDashboard up() {
			return d();
		}
		
		public Sidebar isFound() throws WrongPageException, MalformedURLException, ConditionTimeoutException {
			waitElement(ExpectedConditions.presenceOfElementLocated(sidebar.by()));
			return this;
		}
		
		public Checkout pressCheckout() throws ConditionTimeoutException, NoSuchElementException {
			click(checkout_link);
			waitOrWarn("open checkout from sidebar");
			
			return checkout;
		}

	}
	
	
	public Checkout checkout = new Checkout();
	public class Checkout {
		
		protected Checkout(){}
			
		protected String path = "/checkout.html";
		
		public class PE extends PageElement{
			public PE(String name, TokenType type, String value) {
				super("checkout-" + name, type, value);
			}	
		}
		
		
		public final PE lose_focus = new PE("lose_focus", TokenType.CssSelector, "h2.m0");
		public final PE upgrade_tab_button = new PE("upgrade-tab-button", TokenType.CssSelector, "a[href='#home']");
		public final PE purchase_tab_button = new PE("purchase-tab-button", TokenType.CssSelector, "a[href='#profile']");
		public final PE country_select = new PE("country-select", TokenType.CssSelector, "select[name='plan[country][]']");
		public final PE category_select = new PE("category-select", TokenType.CssSelector, "select[name='plan[category][]']");
		public final PE amount_field = new PE("amount-field", TokenType.CssSelector, "input.amount[name='plan[amount][]']");
		public final PE promo_field = new PE("promo-field", TokenType.Name, "details[promocode]");
		public final PE promo_apply = new PE("promo-apply", TokenType.LinkText, "Apply Promo Code");
		public final PE promo_discount = new PE("promo_discount", TokenType.Class, "discount-total");
		public final PE total = new PE("total", TokenType.Id, "total");
		public final PE tos_checkbox = new PE("terms-of-service-checkbox", TokenType.Id, "checkbox-tos");
		public final PE complete_button = new PE("complete-button", TokenType.Id, "submit");
		public final PE complete_button_disabled_state = 
				new PE("complete-button-disabled-state", TokenType.CssSelector, "#submit.disabled");
		public final PE complete_button_enabled_state = 
				new PE("complete-button-enabled-state", TokenType.CssSelector, "#submit:not(.disabled)");
		public final PE package_countries_list = 
				new PE("active-package-countries", TokenType.CssSelector, "input.country");
		public final PE package_categories_list = 
				new PE("active-package-categories", TokenType.CssSelector, "input.category");
		
		
		public ProxyDashboard up() {
			return d();
		}
		
		public Checkout open() throws TimeoutException {
			openAndWait(this.path, "open checkout");
			return this;
		}
		
		public Checkout isOpened() throws WrongPageException, MalformedURLException {
			isPageOpened(path);
			return this;
		}
		
		public Checkout openPurchaseTab() throws ConditionTimeoutException, NoSuchElementException {
			click(purchase_tab_button);
			
			return this;
		}
		
		public Checkout openUpgradeTab() throws ConditionTimeoutException, NoSuchElementException {
			click(upgrade_tab_button);
			
			return this;
		}
		
		public TabHandle purchasePackage(String plan_country, String plan_category, String plan_amount, String promo) throws ConditionTimeoutException, NoSuchElementException {
			openPurchaseTab();
			fillPurchaseForm(plan_country, plan_category, plan_amount, promo);
			fillTOS();
			submit();
			
			return t;
		}
		
		public Checkout fillPurchaseForm(String plan_country, String plan_category, String plan_amount, String promo) throws ConditionTimeoutException, NoSuchElementException {
			
			select(country_select, plan_country);
			select(category_select, plan_category);			
			input(amount_field, plan_amount);
			
			if(!promo.isEmpty()) {
				input(promo_field, promo);
				waitElement(ExpectedConditions.elementToBeClickable(promo_apply.by()));
				click(promo_apply);
			}
			
			return this;
		}
		
		public Checkout fillTOS() throws ConditionTimeoutException, NoSuchElementException {
			click(tos_checkbox);
			
			return this;
		}
		
		public TabHandle submit() throws ConditionTimeoutException, NoSuchElementException {
			waitElement(ExpectedConditions.presenceOfElementLocated(complete_button_enabled_state.by()));
			click(complete_button);
			
			return t;
		}
		
		public double total() {
			return Helper.getPrice(t.a().findElement(total.by()).getText());
		}
		
		public Checkout validateTotal(double expect) throws WrongValueException {
			var total = total();
			if(total != expect)
				throw new WrongValueException(String.valueOf(total), String.valueOf(expect), this.total.name, "Dashboard/Signup");
			
			return this;
		}
		
		public List<String> getPackagesList() throws NoSuchElementException, ConditionTimeoutException{
			try {
				openUpgradeTab();
			} catch(NoSuchElementException ex) {
				return new ArrayList<String>();
			}
				
			List<WebElement> countries = findAll(package_countries_list);
			List<WebElement> categories = findAll(package_categories_list);
			
			ArrayList<String> packages = new ArrayList<String>();
			for(int i = 0; i < countries.size(); i++) {
				packages.add(countries.get(i).getAttribute("value")+"_"+categories.get(i).getAttribute("value"));
			}
			
			return packages;
		}
		
		public boolean hasPackage(String country_robot, String category_robot) throws NoSuchElementException, ConditionTimeoutException {
			return hasPackage(country_robot.toLowerCase()+"_"+category_robot.toLowerCase());
		}
		
		public boolean hasPackage(String packageName_robot) throws NoSuchElementException, ConditionTimeoutException {
			List<String> list = getPackagesList();
			for(String v : list) {
				if(v.equals(packageName_robot))
					return true;
			}
			
			return false;
		}
		
	}
	
}
