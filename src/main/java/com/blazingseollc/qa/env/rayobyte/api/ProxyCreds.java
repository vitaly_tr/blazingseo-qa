package com.blazingseollc.qa.env.rayobyte.api;

import com.blazingseollc.qa.abstracts.AbstractAPIModel;
import com.blazingseollc.qa.base.Config;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class ProxyCreds extends AbstractAPIModel {

    public static ResponseSpecification successSpec;
    public static ResponseSpecification failSpec;
    //TODO add success spec for every method

    private static RequestSpecification commonRequestSpec;
    private static ResponseSpecification commonResponseSpec;

    static {
        successSpec = new ResponseSpecBuilder().
                expectStatusCode(200).
                expectBody("$", hasKey("reason")).
                expectBody("reason", equalTo("SUCCESS")).
                expectBody("$", hasKey("message")).
                //expectBody("message.length()", greaterThan(0)).
                expectBody("$", hasKey("data")).
                build();

        commonRequestSpec = new RequestSpecBuilder().
                setContentType(ContentType.JSON).
                setAccept(ContentType.JSON).
                addHeader("Auth-Token", Config.env.rayobyte.api.proxy_creds.authToken).
                build();
        commonResponseSpec = new ResponseSpecBuilder().
                expectContentType(ContentType.JSON).
                build();
    }

    private ProxyCreds() {}

    public static CollectionList makeCollections(){
        return new CollectionList();
    }

    public static CredentialList makeCredentials(){
        return new CredentialList();
    }

    public static CredentialList addCredentials(){
        return makeCredentials();
    }

    public static class Collections{

        private Collections() {}

        public static Response searchByID(Integer id){
            required("id", id);

            return
                given().
                    spec(ProxyCreds.commonRequestSpec).
                    log().ifValidationFails().
                    pathParam("id", id).
                when().
                    baseUri(Config.env.rayobyte.api.proxy_creds.url).
                    basePath(Config.env.rayobyte.api.proxy_creds.path).
                    get(Config.env.rayobyte.api.proxy_creds.collectionSearchByIDMethod).
                then().
                    log().ifValidationFails().
                    log().ifError().
                    spec(ProxyCreds.commonResponseSpec).
                extract().response();
        }

        public static <ID, IP, LN> Response search(List<ID> proxyId, List<IP> credsIp, List<LN> credsLogin){
            return search(proxyId, credsIp, credsLogin, null, null, null, null, null, null);
        }

        public static <ID, IP, LN> Response search(List<ID> proxyId, List<IP> credsIp, List<LN> credsLogin, Integer limit, Integer offset){
            return search(proxyId, credsIp, credsLogin, null, null, null, null, limit, offset);
        }

        public static <ID, IP, LN> Response search(List<ID> proxyId, List<IP> credsIP, List<LN> credsLogin,
                                      String createTimeFrom, String createTimeTo, String updateTimeFrom, String updateTimeTo,
                                      Integer limit, Integer offset)
        {

            var json = jsonBody("proxyId", proxyId, "credsIP", credsIP, "credsLogin", credsLogin,
                    "createTimeFrom", createTimeFrom, "createTimeTo", createTimeTo, "updateTimeFrom", updateTimeFrom, "updateTimeTo", updateTimeTo,
                    "limit", limit, "offset", offset);

            return
                given().
                    spec(ProxyCreds.commonRequestSpec).
                    log().ifValidationFails().
                    body(json).
                when().
                    baseUri(Config.env.rayobyte.api.proxy_creds.url).
                    basePath(Config.env.rayobyte.api.proxy_creds.path).
                    get(Config.env.rayobyte.api.proxy_creds.collectionSearchMethod).
                then().
                    log().ifValidationFails().
                    log().ifError().
                    spec(ProxyCreds.commonResponseSpec).
                extract().response();
        }

        public static Response add(CollectionList collectionList){
            return add(collectionList.getList());
        }

        private static <T> Response add(List<T> collectionList){
            required("collectionList", collectionList);

            var json = jsonBody("collectionList", collectionList);

            return
                    given().
                        spec(ProxyCreds.commonRequestSpec).
                        log().ifValidationFails().
                        body(json).
                    when().
                        baseUri(Config.env.rayobyte.api.proxy_creds.url).
                        basePath(Config.env.rayobyte.api.proxy_creds.path).
                        post(Config.env.rayobyte.api.proxy_creds.collectionAddMethod).
                    then().
                        log().ifValidationFails().
                        log().ifError().
                        spec(ProxyCreds.commonResponseSpec).
                    extract().response();
        }

        public static <T> Response update(Integer id, List<T> proxyIds, CredentialList credentials){
            return update(id, proxyIds, credentials.getList());
        }

        private static <T, T2> Response update(Integer id, List<T> proxyIds, List<T2> credentials){
            required("id", id);

            var json = jsonBody("proxyId", proxyIds, "credentials", credentials);

            return
                    given().
                        spec(ProxyCreds.commonRequestSpec).
                        log().ifValidationFails().
                        pathParam("id", id).
                        body(json).
                    when().
                        baseUri(Config.env.rayobyte.api.proxy_creds.url).
                        basePath(Config.env.rayobyte.api.proxy_creds.path).
                        put(Config.env.rayobyte.api.proxy_creds.collectionUpdateMethod).
                    then().
                        log().ifValidationFails().
                        log().ifError().
                        spec(ProxyCreds.commonResponseSpec).
                    extract().response();
        }

        public static <T> Response remove(List<T> ids){

            var json = jsonBody("ids", ids);

            return
                    given().
                        spec(ProxyCreds.commonRequestSpec).
                        log().ifValidationFails().
                        body(json).
                    when().
                        baseUri(Config.env.rayobyte.api.proxy_creds.url).
                        basePath(Config.env.rayobyte.api.proxy_creds.path).
                        delete(Config.env.rayobyte.api.proxy_creds.collectionRemoveMethod).
                    then().
                        log().ifValidationFails().
                        log().ifError().
                        spec(ProxyCreds.commonResponseSpec).
                    extract().response();
        }

        public static Response removeByID(Integer id){
            required("id", id);

            return
                    given().
                        spec(ProxyCreds.commonRequestSpec).
                        log().ifValidationFails().
                        pathParam("id", id).
                    when().
                        baseUri(Config.env.rayobyte.api.proxy_creds.url).
                        basePath(Config.env.rayobyte.api.proxy_creds.path).
                        delete(Config.env.rayobyte.api.proxy_creds.collectionRemoveByIDMethod).
                    then().
                        log().ifValidationFails().
                        log().ifError().
                        spec(ProxyCreds.commonResponseSpec).
                    extract().response();
        }
    }


    public static class Credentials{

        private Credentials() {}

        public static Response add(Integer collectionId, CredentialList credentialList){
            return add(collectionId, credentialList.getList());
        }

        private static <T> Response add(Integer collectionId, List<T> credentialList){
            required("collectionId", collectionId);

            var json = jsonBody("credentialList", credentialList);

            return
                    given().
                        spec(ProxyCreds.commonRequestSpec).
                        log().ifValidationFails().
                        pathParam("collectionId", collectionId).
                        body(json).
                    when().
                        baseUri(Config.env.rayobyte.api.proxy_creds.url).
                        basePath(Config.env.rayobyte.api.proxy_creds.path).
                        post(Config.env.rayobyte.api.proxy_creds.credentialAddMethod).
                    then().
                        log().ifValidationFails().
                        log().ifError().
                        spec(ProxyCreds.commonResponseSpec).
                    extract().response();
        }


        public static Response update(Integer collectionId, Integer id, String ip) {
            return update(collectionId, id, "ip", ip, null, null);
        }

        public static Response update(Integer collectionId, Integer id, String login, String password) {
            return update(collectionId, id, "pwd", null, login, password);
        }

        public static Response update(Integer collectionId, Integer id, String type, String ip, String login, String password){
            required("collectionId", collectionId);
            required("id", id);

            var json = jsonBody("type", type, "ip", ip, "login", login, "password", password);

            return
                    given().
                        spec(ProxyCreds.commonRequestSpec).
                        log().ifValidationFails().
                        pathParam("collectionId", collectionId).
                        pathParam("id", id).
                        body(json).
                    when().
                        baseUri(Config.env.rayobyte.api.proxy_creds.url).
                        basePath(Config.env.rayobyte.api.proxy_creds.path).
                        put(Config.env.rayobyte.api.proxy_creds.credentialUpdateMethod).
                    then().
                        log().ifValidationFails().
                        log().ifError().
                        spec(ProxyCreds.commonResponseSpec).
                    extract().response();
        }

        public static <T> Response remove(Integer collectionId, List<T> credentialId){
            required("collectionId", collectionId);

            var json = jsonBody("credentialId", credentialId);

            return
                    given().
                        spec(ProxyCreds.commonRequestSpec).
                        log().ifValidationFails().
                        pathParam("collectionId", collectionId).
                        body(json).
                    when().
                        baseUri(Config.env.rayobyte.api.proxy_creds.url).
                        basePath(Config.env.rayobyte.api.proxy_creds.path).
                        delete(Config.env.rayobyte.api.proxy_creds.credentialRemoveMethod).
                    then().
                        log().ifValidationFails().
                        log().ifError().
                        spec(ProxyCreds.commonResponseSpec).
                    extract().response();
        }

        public static Response removeByID(Integer collectionId, Integer id){
            required("collectionId", collectionId);
            required("id", id);

            return
                    given().
                        spec(ProxyCreds.commonRequestSpec).
                        log().ifValidationFails().
                        pathParam("collectionId", collectionId).
                        pathParam("id", id).
                    when().
                        baseUri(Config.env.rayobyte.api.proxy_creds.url).
                        basePath(Config.env.rayobyte.api.proxy_creds.path).
                        delete(Config.env.rayobyte.api.proxy_creds.credentialRemoveByIDMethod).
                    then().
                        log().ifValidationFails().
                        log().ifError().
                        spec(ProxyCreds.commonResponseSpec).
                    extract().response();
        }

    }





    public static class CredentialList{

        private List<Map<String, String>> credentials;

        private CredentialList(){
            credentials = new ArrayList<>();
        }

        public CredentialList addIP(String ip){
            credentials.add(Map.of("type", "ip", "ip", ip));
            return this;
        }

        public CredentialList addIPs(String firstIP, String... ips){
            addIP(firstIP);
            for(var ip : ips)
                addIP(ip);

            return this;
        }

        public CredentialList addIPs(List<String> ips){
            for(var ip : ips)
                addIP(ip);

            return this;
        }

        public CredentialList addPWD(String login, String password){
            credentials.add(Map.of("type", "pwd", "login", login, "password", password));
            return this;
        }

        public CredentialList addPWDs(Map<String, String> creds){
            for(var login : creds.keySet()) {
                addPWD(login, creds.get(login));
            }
            return this;
        }

        public CredentialList addPWDs(List<Map<String, String>> credsList){
            for(var creds : credsList){
                addPWDs(creds);
            }
            return this;
        }

        public CredentialList addPWDs(String login, String password, String... loginsAndPasswords){
            addPWD(login, password);
            for(int i = 0; i < loginsAndPasswords.length; i+=2){
                addPWD(loginsAndPasswords[i], loginsAndPasswords[i+1]);
            }
            return this;
        }

        public List<Map<String, String>>  getList(){
            return credentials;
        }
    }

    public static class CollectionList {

        private List<Map<String, Object>> collections;

        private CollectionList(){
            collections = new ArrayList<>();
        }

        public <T> CollectionList collection(List<T> proxyIDs, CredentialList credentials){
            return add(proxyIDs, credentials);
        }

        public <T> CollectionList add(List<T> proxyIDs, CredentialList credentials){
            collections.add(Map.of("proxyId", proxyIDs, "credentials", credentials.credentials));
            return this;
        }

        public List<Map<String, Object>> getList(){
            return collections;
        }
    }

}
