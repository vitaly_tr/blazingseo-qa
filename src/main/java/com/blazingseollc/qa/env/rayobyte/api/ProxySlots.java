package com.blazingseollc.qa.env.rayobyte.api;

import com.blazingseollc.qa.abstracts.AbstractAPIModel;
import com.blazingseollc.qa.base.Config;
import io.restassured.authentication.AuthenticationScheme;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.vitmush.qa.selenium_frame.base.Debug;

import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class ProxySlots extends AbstractAPIModel {

    public static ResponseSpecification successSpec;
    public static ResponseSpecification failSpec;
    //TODO add success spec for every method

    private static RequestSpecification commonRequestSpec;
    private static ResponseSpecification commonResponseSpec;

    static {
        successSpec = new ResponseSpecBuilder().
                expectStatusCode(200).
                expectBody("$", hasKey("status")).
                expectBody("status", equalTo("success")).
                expectBody("$", hasKey("message")).
                expectBody("message.length()", greaterThan(0)).
                expectBody("$", hasKey("data")).
                build();

        commonRequestSpec = new RequestSpecBuilder().
                setContentType(ContentType.JSON).
                setAccept(ContentType.JSON).
                addHeader("Authorization", "Bearer %s".formatted(Config.env.rayobyte.api.proxy_slots.authToken)).
                build();
        commonResponseSpec = new ResponseSpecBuilder().
                expectContentType(ContentType.JSON).
                build();
    }

    private ProxySlots() {}


    public static <S, P, U> Response getSlots(List<S> slotID, String type, String region, List<P> proxyID, List<U> userID) {

        var params = params("slotId", slotID, "type", type, "region", region, "proxyId", proxyID, "userId", userID);

        return
                given().
                    spec(ProxySlots.commonRequestSpec).
                    log().ifValidationFails().
                    queryParams(params).
                when().
                    baseUri(Config.env.rayobyte.api.proxy_slots.url).
                    basePath(Config.env.rayobyte.api.proxy_slots.path).
                    post(Config.env.rayobyte.api.proxy_slots.getMethod).
                then().
                    log().ifValidationFails().
                    log().ifError().
                    spec(ProxySlots.commonResponseSpec).
                extract().response();
    }


    public static <P, U> Response createSlot(String type, String region, P proxyID, U userID) {

        var params = params("type", type, "region", region, "proxyId", proxyID, "userId", userID);

        return
                given().
                    spec(ProxySlots.commonRequestSpec).
                    log().ifValidationFails().

                    //formParams(params).
                        formParam("type", 1).
                    formParam("proxyId", 1).

                    formParam("region", 1).
                    formParam("userId", 1).
                    log().all().
                when().
                    baseUri(Config.env.rayobyte.api.proxy_slots.url).
                    basePath(Config.env.rayobyte.api.proxy_slots.path).
                    post(Config.env.rayobyte.api.proxy_slots.createMethod).
                then().
                    log().ifValidationFails().
                    log().ifError().
                    spec(ProxySlots.commonResponseSpec).
                extract().response();
    }

    public static <S, P, U> Response updateSlot(S slotID, String type, String region, List<P> proxyID, List<U> userID) {

        var params = params("type", type, "region", region, "proxyId", proxyID, "userId", userID);

        return
                given().
                    spec(ProxySlots.commonRequestSpec).
                    log().ifValidationFails().
                    pathParams(params("slotId", slotID)).
                    formParams(params).
                when().
                    baseUri(Config.env.rayobyte.api.proxy_slots.url).
                    basePath(Config.env.rayobyte.api.proxy_slots.path).
                    patch(Config.env.rayobyte.api.proxy_slots.updateMethod).
                then().
                    log().ifValidationFails().
                    log().ifError().
                    spec(ProxySlots.commonResponseSpec).
                extract().response();
    }

    public static <S> Response deleteSlot(S slotID) {

        return
                given().
                    spec(ProxySlots.commonRequestSpec).
                    log().ifValidationFails().
                    pathParam("slotId", slotID).
                when().
                    baseUri(Config.env.rayobyte.api.proxy_slots.url).
                    basePath(Config.env.rayobyte.api.proxy_slots.path).
                        delete(Config.env.rayobyte.api.proxy_slots.deleteSingleMethod).
                then().
                    log().ifValidationFails().
                    log().ifError().
                    spec(ProxySlots.commonResponseSpec).
                extract().response();
    }

    public static <S, P, U> Response deleteSlots(List<S> slotID, String type, String region, List<P> proxyID, List<U> userID) {

        var params = params("slotId", slotID, "type", type, "region", region, "proxyId", proxyID, "userId", userID);

        return
                given().
                    spec(ProxySlots.commonRequestSpec).
                    log().ifValidationFails().
                    queryParams(params).
                when().
                    baseUri(Config.env.rayobyte.api.proxy_slots.url).
                    basePath(Config.env.rayobyte.api.proxy_slots.path).
                    delete(Config.env.rayobyte.api.proxy_slots.deleteMultipleMethod).
                then().
                    log().ifValidationFails().
                    log().ifError().
                    spec(ProxySlots.commonResponseSpec).
                extract().response();
    }

}
