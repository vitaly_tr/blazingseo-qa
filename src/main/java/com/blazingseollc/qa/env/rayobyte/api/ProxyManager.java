package com.blazingseollc.qa.env.rayobyte.api;

import com.blazingseollc.qa.abstracts.AbstractAPIModel;
import com.blazingseollc.qa.base.Config;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.greaterThan;

public class ProxyManager extends AbstractAPIModel {

    public static ResponseSpecification successSearchSpec;
    public static ResponseSpecification successIPsSpec;

    private static RequestSpecification commonRequestSpec;
    private static ResponseSpecification commonResponseSpec;

    static {
        successSearchSpec = new ResponseSpecBuilder().
                expectStatusCode(200).
                expectBody("$", hasKey("data")).
                build();
        successIPsSpec = new ResponseSpecBuilder().
                expectStatusCode(200).
                expectBody("$", hasKey("status")).
                expectBody("status", equalTo("success")).
                expectBody("$", hasKey("data")).
                expectBody("data", hasKey("updatedCount")).
                build();

        commonRequestSpec = new RequestSpecBuilder().
                setContentType(ContentType.JSON).
                setAccept(ContentType.JSON).
                addHeader("x-api-key", Config.env.rayobyte.api.proxy_manager.staticAdminToken).
                build();
        commonResponseSpec = new ResponseSpecBuilder().
                expectContentType(ContentType.JSON).
                build();
    }

    private ProxyManager(){ }

    public static Response search(){
        return search(null, null, null, null);
    }

    public static <ID, IP> Response search(List<ID> id, List<IP> ip){
        return search(id, ip, null, null);
    }

    public static <ID, IP> Response search (List<ID> id, List<IP> ip, Integer limit, Integer offset){

        var json = jsonBody("id", id, "ip", ip, "limit", limit, "offset", offset);

        //TODO add schema validation support
        //.body(matchesJsonSchemaInClasspath("./products-schema.json"))
        return
            given().
                spec(ProxyManager.commonRequestSpec).
                body(json).
                log().ifValidationFails().
            when().
                baseUri(Config.env.rayobyte.api.proxy_manager.url).
                basePath(Config.env.rayobyte.api.proxy_manager.path).
                post(Config.env.rayobyte.api.proxy_manager.searchMethod).
            then().
                log().ifValidationFails().
                log().ifError().
                spec(ProxyManager.commonResponseSpec).
            extract().response();
    }

    public static <ID, T> Response ips(List<ID> ids, T assignable) throws NullPointerException {

        var json = jsonBody("id", ids, "assignable", assignable);

        return
                given().
                    spec(ProxyManager.commonRequestSpec).
                    body(json).
                    log().ifValidationFails().
                when().
                    baseUri(Config.env.rayobyte.api.proxy_manager.url).
                    basePath(Config.env.rayobyte.api.proxy_manager.path).
                    patch(Config.env.rayobyte.api.proxy_manager.ipsMethod).
                then().
                    log().ifValidationFails().
                    spec(ProxyManager.commonResponseSpec).
                extract().response();
    }

    public static ResponseSpecification invalidInputSpec(String param){
        //Creates specification for verifying invalid input for specified param
        return new ResponseSpecBuilder().
                expectStatusCode(422).
                expectBody("$", hasKey("reason")).
                expectBody("$", hasKey("message")).
                expectBody("$", hasKey("data")).
                expectBody("reason", equalTo("INPUT_INVALID")).
                expectBody("message.length()", greaterThan(0)).
                expectBody("data", hasKey(param)).
                expectBody("data.%s[0].length()".formatted(param), greaterThan(0)).
                build();
    }

}
