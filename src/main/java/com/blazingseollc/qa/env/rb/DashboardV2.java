package com.blazingseollc.qa.env.rb;

import com.blazingseollc.qa.base.Config;
import com.blazingseollc.qa.tools.DB;
import com.blazingseollc.qa.tools.Helper;
import com.blazingseollc.qa.base.ProxyType;
import com.blazingseollc.qa.base.ProxyType.*;
import com.jcraft.jsch.JSchException;
import org.apache.http.annotation.Obsolete;
import org.openqa.selenium.WebElement;
import org.vitmush.qa.selenium_frame.base.Debug;
import org.vitmush.qa.selenium_frame.base.SceneManager;
import org.vitmush.qa.selenium_frame.base.SceneManager.Scene.TabHandle;
import org.vitmush.qa.selenium_frame.abstracts.AbstractSite;
import org.vitmush.qa.selenium_frame.types.PageElement.Token.TokenType;
import org.vitmush.qa.selenium_frame.exceptions.*;
import org.vitmush.qa.selenium_frame.exceptions.NoSuchElementException;

import java.sql.SQLException;
import java.text.DateFormatSymbols;
import java.util.*;
import java.util.stream.Collectors;

import static com.blazingseollc.qa.base.ProxyType.AliasCollection.dashboardv2.*;

public class DashboardV2 extends AbstractSite {

    public DashboardV2() throws URLIsImproper {
        this(SceneManager.getDefault().tab("rb_dashboard"));
    }

    public DashboardV2(TabHandle tab) throws URLIsImproper {
        this(tab, Config.env.rebilly.url_dashboard);
    }

    public DashboardV2(TabHandle tab, String customDashboardUrl) throws URLIsImproper {
        this(tab, customDashboardUrl, Config.env.rebilly.url_signup);
    }

    public DashboardV2(TabHandle tab, String customDashboardUrl, String customSignupUrl) throws URLIsImproper {
        this(tab, customDashboardUrl, customSignupUrl, Config.env.rebilly.url_invoice);
    }

    public DashboardV2(TabHandle tab, String customDashboardUrl, String customSignupUrl, String customInvoiceUrl) throws URLIsImproper {
        super("Dashboard_V2", tab, customDashboardUrl);
        signup = new Signup(customSignupUrl);
        invoice = new Invoice(customInvoiceUrl);
    }

    DashboardV2 up() {
        return this;
    }

    public QuickTasks quick = new QuickTasks();
    public Signup signup;
    public Signin signin = new Signin();
    public HomeIncognito incognito = new HomeIncognito();
    public Home home = new Home();
    public Invoice invoice;
    public TFA tfa = new TFA();
    public IPv4 ipv4 = new IPv4();
    public ISPRotating isp_rotating = new ISPRotating();
    public Any any = new Any();
    public Purchase purchase = new Purchase();

    public class QuickTasks {

        public Tools tools = new Tools();

        public QuickTasks signupWithPurchase(ProxyType type) throws PageTimeoutException, ConditionTimeoutException, NoSuchElementException, WrongPageException, URLIsImproper, WebFormErrorException {
            if(type.category == ProxyCategory.ISP_Rotating){
                signupWithPurchase(ISPRotatingPackage.Starter);
            } else {
                signupWithPurchase(type, tools.getSignupQuantity(type));
            }

            return this;
        }

        public QuickTasks signupWithPurchase(ProxyType type, String email) throws PageTimeoutException, ConditionTimeoutException, NoSuchElementException, WrongPageException, URLIsImproper, WebFormErrorException {
            return signupWithPurchase(type, tools.getSignupQuantity(type), email);
        }

        public QuickTasks signupWithPurchase(ProxyType type, int quantity) throws PageTimeoutException, ConditionTimeoutException, NoSuchElementException, WrongPageException, URLIsImproper, WebFormErrorException {
            signupWithPurchase(type, quantity, Helper.generateUniqueEmail());
            return this;
        }

        public QuickTasks signupWithPurchase(ISPRotatingPackage packageType) throws PageTimeoutException, ConditionTimeoutException, NoSuchElementException, WrongPageException, URLIsImproper, WebFormErrorException {
            return signupWithPurchase(packageType, Helper.generateUniqueEmail());
        }

        public QuickTasks signupWithPurchase(ISPRotatingPackage packageType, String email) throws PageTimeoutException, ConditionTimeoutException, NoSuchElementException, WrongPageException, URLIsImproper, WebFormErrorException {
            signup.open().doCreateAccount(packageType, email, Config.env.password);
            makePayment(email);
            Debug.Templates.TestDebug(String.format("Signup with Rotating Residential - %s (%d GB)", packageType.name(), ispRotatingPackageSize.get(packageType)));
            return this;
        }

        public QuickTasks signupWithPurchase(ProxyType type, int quantity, String email) throws PageTimeoutException, ConditionTimeoutException, NoSuchElementException, WrongPageException, URLIsImproper, WebFormErrorException {
            signup.open().doCreateAccount(type, quantity, email, Config.env.password);
            makePayment(email);
            Debug.Templates.TestDebug(String.format("Signup with %s - %s %s", AliasCollection.common.countryName.of(type.country), AliasCollection.common.categoryName.of(type.category), AliasCollection.common.billingCycleName.of(type.billingCycle)));
            return this;
        }

        public QuickTasks signupWithReferral(ISPRotatingPackage packageType, String promoter_id) throws PageTimeoutException, ConditionTimeoutException, NoSuchElementException, WrongPageException, URLIsImproper, WebFormErrorException {
            String email = Helper.generateUniqueEmail();
            openAndWait("%s?fpr=%s".formatted(signup.path, promoter_id), signup.pathAsUrl);
            signup.doCreateAccount(packageType, email, Config.env.password);
            makePayment(email);
            Debug.Templates.TestDebug(String.format("Signup with Rotating Residential - %s (%d GB)", packageType.name(), ispRotatingPackageSize.get(packageType)));
            return this;
        }

        public QuickTasks signupWithReferral(ProxyType type, int quantity, String promoter_id) throws PageTimeoutException, ConditionTimeoutException, NoSuchElementException, WrongPageException, URLIsImproper, WebFormErrorException {
            String email = Helper.generateUniqueEmail();
            openAndWait("%s?fpr=%s".formatted(signup.path, promoter_id), signup.pathAsUrl);
            signup.doCreateAccount(type, quantity, email, Config.env.password);
            makePayment(email);
            Debug.Preserve(String.format("Signup with %s - %s %s", AliasCollection.common.countryName.of(type.country), AliasCollection.common.categoryName.of(type.category), AliasCollection.common.billingCycleName.of(type.billingCycle)), true);
            return this;
        }

        public QuickTasks signupWithArguments(ISPRotatingPackage packageType, String query) throws PageTimeoutException, ConditionTimeoutException, NoSuchElementException, WrongPageException, URLIsImproper, WebFormErrorException {
            String email = Helper.generateUniqueEmail();
            openAndWait("%s?%s".formatted(signup.path, query), signup.pathAsUrl);
            signup.doCreateAccount(packageType, email, Config.env.password);
            makePayment(email);
            Debug.Templates.TestDebug(String.format("Signup with Rotating Residential - %s (%d GB)", packageType.name(), ispRotatingPackageSize.get(packageType)));
            return this;
        }

        public QuickTasks signupWithArguments(ProxyType type, int quantity, String query) throws PageTimeoutException, ConditionTimeoutException, NoSuchElementException, WrongPageException, URLIsImproper, WebFormErrorException {
            String email = Helper.generateUniqueEmail();
            openAndWait("%s?%s".formatted(signup.path, query), signup.pathAsUrl);
            signup.doCreateAccount(type, quantity, email, Config.env.password);
            makePayment(email);
            Debug.Preserve(String.format("Signup with %s - %s %s", AliasCollection.common.countryName.of(type.country), AliasCollection.common.categoryName.of(type.category), AliasCollection.common.billingCycleName.of(type.billingCycle)), true);
            return this;
        }

        public QuickTasks signupWithErrorCardPurchase(ProxyType type) throws PageTimeoutException, ConditionTimeoutException, NoSuchElementException, WrongPageException, URLIsImproper, WebFormErrorException {
            return signupWithErrorCardPurchase(type, tools.getSignupQuantity(type));
        }

        public QuickTasks signupWithErrorCardPurchase(ProxyType type, int quantity) throws PageTimeoutException, ConditionTimeoutException, NoSuchElementException, WrongPageException, URLIsImproper, WebFormErrorException {
            return signupWithErrorCardPurchase(type, quantity, Helper.generateUniqueEmail());
        }

        public QuickTasks signupWithErrorCardPurchase(ProxyType type, int quantity, String email) throws PageTimeoutException, ConditionTimeoutException, NoSuchElementException, WrongPageException, URLIsImproper, WebFormErrorException {
            if(Config.current_env == Config.Environment.PRODUCTION)
                throw new Error("Error card on production is not supported");
            signup.open().doCreateAccount(type, quantity, email, Config.env.password);
            invoice.makeCardPayment("vitaly", "---", email, "4000000000000010", Config.env.rebilly.card_date, Config.env.rebilly.card_cvv);
            Debug.Preserve(String.format("Signup with second attempt error %s - %s %s", AliasCollection.common.countryName.of(type.country), AliasCollection.common.categoryName.of(type.category), AliasCollection.common.billingCycleName.of(type.billingCycle)), true);
            return this;
        }

        private void makePayment(String email) throws PageTimeoutException, ConditionTimeoutException, NoSuchElementException, WrongPageException, URLIsImproper {
            switch(Config.current_env){
                case DEV:
                    invoice.makeCardPayment();
                    break;
                case PRODUCTION:
                    RebillyAdmin a = new RebillyAdmin(SceneManager.getDefault().tab("quick-mark-invoice-paid"));
                    a.quick.makeLastUnpaidInvoicePaid(email);
                    a.tab().close();
                    break;
            }

            Helper.sleep(Config.sleep.rebilly.product_sync_wait);
            t.waitLoaded();
        }

        //TODO low purchaseAdditionalBandwidth to work without email explicitly
        public QuickTasks purchaseAdditionalBandwidth(int amount, String email) throws PageTimeoutException, NoSuchElementException, WrongPageException, ConditionTimeoutException, URLIsImproper {
            try {
                isSubPageOpened("/");
            } catch (WrongPageException e) {
                isp_rotating.home.open();
                if(incognito.isOpened()){
                    signin.open().login(email);
                }
            }
            isp_rotating.buy.open().doBuy(amount);
            switch(Config.current_env){
                case DEV:
                    invoice.makeCardPayment();
                    break;
                case PRODUCTION:
                    RebillyAdmin a = new RebillyAdmin(SceneManager.getDefault().tab("quick-mark-invoice-paid"));
                    a.quick.makeLastUnpaidInvoicePaid(email);
                    a.tab().close();
                    break;
            }
            return this;
        }

        public QuickTasks redirectToDashboardFromSignup() throws ConditionTimeoutException {
            signup.open(); //should be redirected to home page
            return this;
        }

        public QuickTasks confirmTFA() throws JSchException, SQLException, ClassNotFoundException, PageTimeoutException, NoSuchElementException, WrongPageException {
            return confirmTFA(Helper.lastGeneratedUniqueEmail);
        }

        public QuickTasks confirmTFA(String email) throws JSchException, SQLException, ClassNotFoundException, PageTimeoutException, NoSuchElementException, WrongPageException {
            if(tfa.isOpened())
                tfa.confirmTFA(DB.quick.proxy_backend.getTFACode(email));
            return this;
        }

        public QuickTasks waitLocationsRedirect() throws PageTimeoutException {
            int maxWaitMillis = Config.sleep.proxy_backend.ports_get_delivered_timeout * 1000;
            int sleepSecs = 10;
            var start = new Date().getTime();
            while(false == ipv4.locations.isOpened()){
                Helper.sleep(sleepSecs);
                if(new Date().getTime() - start > maxWaitMillis){
                    throw new RuntimeException("Waiting for locations redirect is unsuccessful");
                }
                t.reload();
            }
            return this;
        }

        public QuickTasks setAllLocationsToMixed() throws PageTimeoutException, NoSuchElementException, WrongPageException {
            return setAllLocationsToMixed(false);
        }

        public QuickTasks setAllLocationsToMixed(boolean waitRedirect) throws PageTimeoutException, NoSuchElementException, WrongPageException {
            if (waitRedirect) {
                home.open();
                waitLocationsRedirect();
            }
            if (!ipv4.locations.isOpened())
                return this;
            var locations = ipv4.locations.getPackagesSummary();
            for (var l : locations) {
                if (!l.get("left").equals("0"))
                    ipv4.locations.setLocationsToMixed(l.get("country"), l.get("category"));
            }
            return this;
        }

        public static class Tools {
            Tools(){ }

            public int getSignupQuantity(ProxyType type) {
                int amount = 6;
                if (type.category == ProxyType.ProxyCategory.ISP_Rotating)
                    amount = 4;
                return amount;
            }
        }
    }

    /******
     ****  Pages Implementation
     ******/

    public class Signup extends AbstractPage{

        protected Signup(String url_signup) throws URLIsImproper {
            super("Signup", url_signup, true);
        }

        public enum PurchasePurpose { WebScraping, Testing, Other }

        protected PageElement ipv4_button = new PageElement("ipv4_radio_button", TokenType.XPath, "//label[contains(text(), 'IPv4')]/../div");
        protected PageElement ipv6_button = new PageElement("ipv6_radio_button", TokenType.XPath, "//label[contains(text(), 'IPv6')]/../div");
        protected PageElement proxy_type_selector = new PageElement("proxy_type_selector", TokenType.XPath, "//label[contains(text(), 'Proxy Type')]/../..");
        protected PageElement isp_rotating_package_selector = new PageElement("isp_rotating_package_selector", TokenType.XPath, "//label[contains(text(), 'Package')]/../..");
        protected PageElement country_selector = new PageElement("country_selector", TokenType.XPath, "//label[contains(text(), 'Location')]/../..");
        protected PageElement subscription_selector = new PageElement("subscription_selector",  TokenType.XPath, "//label[contains(text(), 'Subscription Period')]/../..");
        protected PageElement amount_input = new PageElement("amount_input",  TokenType.XPath, "//label[contains(text(), 'Number of Proxies') or contains(text(), 'Traffic in GB')]/../input");
        protected PageElement first_name_input = new PageElement("first_name_input",  TokenType.XPath, "//label[contains(text(), 'First Name')]/../input");
        protected PageElement last_name_input = new PageElement("last_name_input",  TokenType.XPath, "//label[contains(text(), 'Last Name')]/../input");
        protected PageElement email_input = new PageElement("email_input",  TokenType.XPath, "//label[contains(text(), 'Email address')]/../input");
        protected PageElement password_input = new PageElement("password_input",  TokenType.XPath, "//label[contains(text(), 'Password')]/../input");
        protected PageElement phone_countrycode_selector = new PageElement("phone_countrycode_selector",  TokenType.XPath, "//label[contains(text(), 'Phone number')]/../../../../..//div[@class='v-select__slot']");
        protected PageElement phone_number_input = new PageElement("phone_number_input",  TokenType.XPath, "//label[contains(text(), 'Phone number')]/../input");
        protected PageElement company_name_input = new PageElement("company_name_input",  TokenType.XPath, "//label[contains(text(), 'Company Name')]/../input");
        protected PageElement street_input = new PageElement("street_input",  TokenType.XPath, "//label[contains(text(), 'Street address')]/../input");
        protected PageElement street2_input = new PageElement("street2_input",  TokenType.XPath, "//label[contains(text(), 'Street address 2')]/../input");
        protected PageElement city_input = new PageElement("city_input",  TokenType.XPath, "//label[contains(text(), 'City')]/../input");
        protected PageElement state_input = new PageElement("state_input",  TokenType.XPath, "//label[contains(text(), 'State/Territory')]/../input");
        protected PageElement postcode_input = new PageElement("postcode_input",  TokenType.XPath, "//label[contains(text(), 'Postcode')]/../input");
        protected PageElement country_address_selector = new PageElement("country_address_selector",  TokenType.XPath, "//label[contains(text(), 'Country')]/../..");
        protected PageElement birth_day_selector = new PageElement("birth_day_selector",  TokenType.XPath, "//label[contains(text(), 'Day')]/../..");
        protected PageElement birth_month_selector = new PageElement("birth_month_selector",  TokenType.XPath, "//label[contains(text(), 'Month')]/../..");
        protected PageElement birth_year_selector = new PageElement("birth_year_selector",  TokenType.XPath, "//label[contains(text(), 'Year')]/../..");
        protected PageElement usage_selector = new PageElement("usage_selector",  TokenType.XPath, "//label[contains(text(), 'Please select main purpose for using proxies')]/../..");
        protected PageElement usage_custom_input = new PageElement("usage_custom_input",  TokenType.XPath, "//label[contains(text(), 'Your option')]/../input");
        protected PageElement tos_checkbox = new PageElement("tos_checkbox",  TokenType.XPath, "//div[contains(text(), 'I have read and agree')]/../../div[@class='v-input--selection-controls__input']");
        protected PageElement submit_button = new PageElement("submit_button",  TokenType.XPath, "//span[contains(text(), 'BUY NOW')]/parent::button");
        protected PageElement preloader = new PageElement("preloader", TokenType.Class, "v-dialog__content--active");
        protected PageElement enter_alL_required_alert = new PageElement("enter_alL_required_alert",  TokenType.XPath, "//p[contains(text(), 'Please enter all required values')]");
        protected PageElement alert_block = new PageElement("alert_block",  TokenType.XPath, "//div[contains(@class, 'v-toast__item--error')]");
        protected PageElement active_selector_list = new PageElement("active_selector_list", TokenType.XPath, "//div[contains(@class, 'menuable__content__active')]");
        protected PageElement error_messages = new PageElement("error_messages", TokenType.Class, "v-messages__message");
        protected PageElement total_text = new PageElement("total_text", TokenType.XPath, "//div[text() = 'Total']/parent::div[@role='listitem']/div[2]");
        protected GenericPageElement generic_option_selector = new GenericPageElement("generic_option_selector", TokenType.XPath,
                "//div[contains(@class, 'menuable__content__active')]//div[@role='option'][text() = ' %s '] | //div[contains(@class, 'menuable__content__active')]//div[@role='option']//div[@class='v-list-item__title'][contains(text(), '%s')]",
                (String value, Object[] args) -> value.formatted(args[0], args[0]));

        public Signup open() throws ConditionTimeoutException {
            _open();
            waitDisappeared(preloader, true);
            return this;
        }

        public Invoice doCreateAccount(ProxyType proxyType, int amount, String email) throws NoSuchElementException, WrongPageException, PageTimeoutException, ConditionTimeoutException, WebFormErrorException {
            return doCreateAccount(proxyType, amount, email, Config.env.password);
        }

        public Invoice doCreateAccount(ProxyType proxyType, int amount, String email, String password) throws NoSuchElementException, WrongPageException, PageTimeoutException, ConditionTimeoutException, WebFormErrorException {
            assertPageOpened();
            return doFillRequiredFields(proxyType, amount, email, password).clickSubmit();
        }

        public Invoice doCreateAccount(ISPRotatingPackage packageType, String email) throws NoSuchElementException, WrongPageException, PageTimeoutException, ConditionTimeoutException, WebFormErrorException {
            return doCreateAccount(packageType, email, Config.env.password);
        }

        public Invoice doCreateAccount(ISPRotatingPackage packageType, String email, String password) throws NoSuchElementException, WrongPageException, PageTimeoutException, ConditionTimeoutException, WebFormErrorException {
            assertPageOpened();
            return doFillRequiredFields(packageType, email, password).clickSubmit();
        }

        public Signup doFillRequiredFields(ISPRotatingPackage packageType, String email, String password) throws NoSuchElementException, WrongPageException {
            assertPageOpened();

            setISPRotatingProxyType(packageType);

            return doFillRequiredFields(email, password);
        }

        //TODO fix all method that are using this method to work properly with ISP Rotating
        public Signup doFillRequiredFields(ProxyType proxyType, int amount, String email, String password) throws NoSuchElementException, WrongPageException {
            assertPageOpened();

            if(proxyType.category == ProxyType.ProxyCategory.ISP_Rotating) {
                throw new Error("ISP Rotating proxy type not expected in method");
            }

            setProxyType(proxyType).setAmount(amount);

            return doFillRequiredFields(email, password);
        }

        private Signup doFillRequiredFields(String email, String password) throws NoSuchElementException {
            String firstName = email.split("@")[0].replace("+", ".");
            return setFirstName(firstName).setLastName("---").setEmail(email).setPassword(password)
                    .setPhoneNumber("2020").setStreet("street").setCity("city").setState("state")
                    .setPostcode("00000").setPurpose(PurchasePurpose.Other, "[QA Automation]").clickCheckbox();
        }

        public Signup doFillOptionalFields() throws NoSuchElementException, WrongPageException {
            assertPageOpened();

            return setStreet2("street, 2").setPhoneCountryCode("+380").setCompanyName("Sprious").setBirthDay(23).setBirthMonth(10).setBirthYear(1996);
        }

        public Invoice doSubmit() throws PageTimeoutException, ConditionTimeoutException, WebFormErrorException, NoSuchElementException, WrongPageException {
            assertPageOpened();

            return clickSubmit();
        }

        public float getTotal() throws NoSuchElementException, WrongPageException {
            return Float.parseFloat(findOne(total_text).getText().replace("$", ""));
        }

        public Signup clickIPv4() throws NoSuchElementException {
            click(ipv4_button);
            return this;
        }

        public Signup clickIPv6() throws NoSuchElementException {
            click(ipv6_button);
            return this;
        }

        public Signup setISPRotatingProxyType(ISPRotatingPackage plan) throws NoSuchElementException, WrongPageException {
            clickIPv4().setType(ProxyCategory.ISP_Rotating);
            click(isp_rotating_package_selector);
            click(generic_option_selector.context(isp_rotating_package_selector.elementName)
                    .get(plan.name()));
            setPeriod(BillingCycle.Monthly);
            return this;
        }

        public Signup setProxyType(ProxyType proxyType) throws NoSuchElementException, WrongPageException {
            if(proxyType.category == ProxyType.ProxyCategory.ISP_Rotating){
                throw new Error("ISP_Rotating not implemented in setProxyType, use setISPRotatingProxyType method instead");
            }
            if(proxyType.category == ProxyType.ProxyCategory.IPv6){
                clickIPv6();
            } else {
                clickIPv4();
            }
            setType(proxyType.category);
            setLocation(proxyType.country).setPeriod(proxyType.billingCycle);
            return this;
        }

        public Signup setType(ProxyType.ProxyCategory type) throws NoSuchElementException {
            click(proxy_type_selector);
            click(generic_option_selector.context(proxy_type_selector.elementName).get(AliasCollection.cartpageV2.categoryName.of(type)));
            return this;
        }

        public Signup setLocation(ProxyType.ProxyCountry location) throws NoSuchElementException {
            click(country_selector);
            var searchedCountry = generic_option_selector.context(country_selector.elementName).get(AliasCollection.common.countryName.of(location));
            scrollDown(active_selector_list, searchedCountry);
            click(searchedCountry);
            return this;
        }

        public Signup setPeriod(ProxyType.BillingCycle period) throws NoSuchElementException {
            click(subscription_selector);
            click(generic_option_selector.context(subscription_selector.elementName).get(billingCycleName.of(period)));
            return this;
        }

        public Signup setAmount(int amount) throws NoSuchElementException {
            clean(amount_input);
            input(amount_input, String.valueOf(amount));
            return this;
        }

        public Signup setFirstName(String name) throws NoSuchElementException {
            input(first_name_input, name);
            return this;
        }

        public Signup setLastName(String name) throws NoSuchElementException {
            input(last_name_input, name);
            return this;
        }

        public Signup setEmail(String email) throws NoSuchElementException {
            input(email_input, email);
            return this;
        }

        public Signup setPassword(String password) throws NoSuchElementException {
            input(password_input, password);
            return this;
        }

        public Signup setPhoneCountryCode(String countryCode) throws NoSuchElementException {
            click(phone_countrycode_selector);
            var countryCodeOption = generic_option_selector.context(phone_countrycode_selector.elementName).get(countryCode);
            scrollDown(active_selector_list, countryCodeOption);
            click(countryCodeOption);
            return this;
        }

        public Signup setPhoneNumber(String phoneNumber) throws NoSuchElementException {
            input(phone_number_input, phoneNumber);
            return this;
        }

        public Signup setCompanyName(String companyName) throws NoSuchElementException {
            input(company_name_input, companyName);
            return this;
        }

        public Signup setStreet(String street) throws NoSuchElementException {
            input(street_input, street);
            return this;
        }

        public Signup setStreet2(String street2) throws NoSuchElementException {
            input(street2_input, street2);
            return this;
        }

        public Signup setState(String state) throws NoSuchElementException {
            input(state_input, state);
            return this;
        }

        public Signup setCity(String city) throws NoSuchElementException {
            input(city_input, city);
            return this;
        }

        public Signup setPostcode(String postcode) throws NoSuchElementException {
            input(postcode_input, postcode);
            return this;
        }

        public Signup setCountry(String country) throws NoSuchElementException {
            click(country_address_selector);
            var countryOption = generic_option_selector.context(country_address_selector.elementName).get(country);
            scrollDown(active_selector_list, countryOption);
            click(countryOption);
            return this;
        }

        public Signup setBirthDay(int day) throws NoSuchElementException {
            click(birth_day_selector);
            var dayOption = generic_option_selector.context(birth_day_selector.elementName).get(String.valueOf(day));
            scrollDown(active_selector_list, dayOption);
            click(dayOption);
            return this;
        }

        public Signup setBirthMonth(int month) throws NoSuchElementException {
            click(birth_month_selector);
            var monthOption = generic_option_selector.context(birth_month_selector.elementName)
                    .get(new DateFormatSymbols().getMonths()[month - 1 + Calendar.JANUARY]);
            scrollDown(active_selector_list, monthOption);
            click(monthOption);
            return this;
        }

        public Signup setBirthYear(int year) throws NoSuchElementException {
            click(birth_year_selector);
            var yearOption = generic_option_selector.context(birth_year_selector.elementName).get(String.valueOf(year));
            scrollDown(active_selector_list, yearOption);
            click(yearOption);
            return this;
        }

        public Signup setPurpose(PurchasePurpose purpose) throws NoSuchElementException {
            String purposeText = "";
            switch (purpose){
                case WebScraping:
                    purposeText = "Web scraping/Data collection";
                    break;
                case Testing:
                    purposeText = "Web site testing";
                    break;
                case Other:
                    purposeText = "Other";
                    break;
            }
            click(usage_selector);
            click(generic_option_selector.context(usage_selector.elementName).get(purposeText));
            return this;
        }

        public Signup setPurpose(PurchasePurpose purpose, String otherText) throws NoSuchElementException {
            setPurpose(purpose);
            input(usage_custom_input, otherText);
            return this;
        }

        public Signup clickCheckbox() throws NoSuchElementException {
            click(tos_checkbox);
            return this;
        }

        public Signup waitErrorDisappeared() throws ConditionTimeoutException {
            waitDisappeared(alert_block);

            return this;
        }

        public Invoice clickSubmit() throws NoSuchElementException, ConditionTimeoutException, PageTimeoutException, WebFormErrorException {

            String url = tab().a().getCurrentUrl();
            click(submit_button);

            try{
                pageUpdatedSleep();
                //TODO fix this doesn't work on wrong amount input
                String error = findOne(alert_block).getText();
                ArrayList<String> context = new ArrayList<String>();
                for(WebElement message : findAll(error_messages, true)){
                    context.add(message.getText());
                }
                throw new WebFormErrorException(error, getPageName(), context);
            } catch (NoSuchElementException ex){
                //Should not appear, continuing
            }
            waitDisappeared(preloader);

            t.waitPageChanged(url);
            return invoice;
        }

    }

    public class Signin extends AbstractPage{

        protected Signin() throws URLIsImproper {
            super("Signin", Config.env.rebilly.url_signin, true);
        }

        protected PageElement email_input = new PageElement("email_input", TokenType.Id, "email");
        protected PageElement password_input = new PageElement("password_input", TokenType.Id, "password");
        protected PageElement submit_button = new PageElement("submit_button", TokenType.Id, "submit");

        public Signin open(){
            return (Signin) _open();
        }

        public Home login(String email) throws NoSuchElementException, PageTimeoutException, WrongPageException {
            return login(email, Config.env.password);
        }

        public Home login(String email, String password) throws NoSuchElementException, PageTimeoutException, WrongPageException {
            assertPageOpened();

            input(email_input, email);
            input(password_input, password);
            click(submit_button);

            t.waitPageChanged();
            return home;
        }

    }

    public class HomeIncognito extends AbstractPage {
        protected HomeIncognito() throws URLIsImproper {
            super("Home Incognito", "/dashboard");
        }


        public HomeIncognito open(String invoice) {
            return (HomeIncognito) _open();
        }

    }

    public class Home extends AbstractPage {
        protected Home() throws URLIsImproper {
            super("Home", "/dashboard/home", false);
        }
        public Home open() {
            return (Home) _open();
        }

    }



    public class IPv4 {
        protected IPv4() throws URLIsImproper { }

        public IPv4_Dashboard dashboard = new IPv4_Dashboard();
        public IPv4_Export export = new IPv4_Export();
        public IPv4_Locations locations = new IPv4_Locations();
        public IPv4_Replacements replacements = new IPv4_Replacements();
        public IPv4_Settings settings = new IPv4_Settings();
        public IPv4_Packages packages = new IPv4_Packages();

        public class IPv4_Dashboard extends AbstractPage {
            protected IPv4_Dashboard() throws URLIsImproper {
                super("IPv4 Dashboard", "/dashboard/ipv4", true, false);
            }

            protected PageElement inactive_label = new PageElement("inactive_label", TokenType.InnerText, "It looks like you don't have any IPv4 proxies at this time.");
            protected PageElement active_label = new PageElement("active_label", TokenType.InnerText, "Assigned Ports");
            protected PageElement ip_radiobutton = new PageElement("ip_radiobutton", TokenType.XPath, "//input[@id='format_ip']/parent::div");
            protected PageElement pwd_radiobutton = new PageElement("pwd_radiobutton", TokenType.XPath, "//input[@id='format_pw']/parent::div/label");
            protected PageElement ip_input = new PageElement("ip_input", TokenType.Class, "new-ip");
            protected PageElement ip_add_button = new PageElement("ip_add_button", TokenType.Class, "add-new-ip");
            protected PageElement geolocate_ip_button = new PageElement("geolocate_ip_button", TokenType.Class, "use-current-ip");
            protected PageElement export_dropdown = new PageElement("export_dropdown", TokenType.Id, "dropdownMenu1");
            protected PageElement modal_ip_added_confirm_button = new PageElement("modal_ip_added_confirm_button", TokenType.XPath, "//button[@data-bb-handler='ok']");
            /**
             * (1) rotating port id
             */
            protected GenericPageElement rotating_intervals = new GenericPageElement("rotating_intervals", TokenType.CssSelector, "select[data-port-id='%s']");
            //protected GenericPageElement rotating_interval_option = new GenericPageElement("rotating_interval_option", TokenType.CssSelector, "select[data-port-id='%s'] > option");
            
            protected ElementAttribute port_ids = new ElementAttribute("port_ids", TokenType.CssSelector, "tr[data-port-id]", "data-port-id");
            protected ElementAttribute rotating_port_ids = new ElementAttribute("rotating_port_ids", TokenType.CssSelector, "select[data-port-id]", "data-port-id");
            protected ElementAttribute proxy_ips = new ElementAttribute("proxy_ips", TokenType.CssSelector, "tr[data-port-id] > td:nth-of-type(1)", ":text()");
            protected ElementAttribute proxy_ports = new ElementAttribute("proxy_ports", TokenType.CssSelector, "tr[data-port-id] > td:nth-of-type(2)", ":text()");
            protected ElementAttribute proxy_locations = new ElementAttribute("proxy_locations", TokenType.CssSelector, "tr[data-port-id] > td:nth-of-type(3)", ":text()");
            protected ElementAttribute rotation_intervals = new ElementAttribute("rotation_intervals", TokenType.CssSelector, "tr[data-port-id] > td:nth-of-type(4)", ":text()");
            /**
             * (1) rotating port id
             */
            protected GenericElementAttribute selected_rotating_interval = new GenericElementAttribute("selected_rotating_interval", TokenType.CssSelector, "select[data-port-id='%s'] > option:checked", ":text():split(' ')[0]");
            protected ElementAttribute proxy_types = new ElementAttribute("proxy_types", TokenType.CssSelector, "tr[data-port-id] > td:nth-of-type(5)", ":text()");
            protected ElementAttribute assignment_time_column = new ElementAttribute("assignment_time_column", TokenType.CssSelector, "tr[data-port-id] > td:nth-of-type(6)", ":text()");
            /**
             * (1) rotating port id
             */
            protected GenericElementAttribute actual_ip = new GenericElementAttribute("actual_ip", TokenType.XPath, "//select[@data-port-id = '%s']//ancestor::tr/following-sibling::tr//strong", ":text():split(\\:)[1]:trim()");
            /**
             * (1) proxy type code, or 'any'
             */
            protected GenericPageElement export_button = new GenericPageElement("export_button", TokenType.XPath, "//a[contains(@href, '/export/%s')]");
            protected PageElement active_modal_dismiss_button = new PageElement("active_modal_dismiss_button", TokenType.CssSelector, "div[role='dialog'][style*='display: block'] button[data-dismiss='modal']");


            public IPv4_Dashboard open() {
                return (IPv4_Dashboard) _open();
            }


            public boolean isEnabled() throws NoSuchElementException, WrongPageException {
                assertPageOpened();

                try {
                    findOne(inactive_label);
                } catch (NoSuchElementException e) {
                    findOne(active_label);
                    return true;
                }
                return false;
            }

            public boolean isAuthIpAlreadyAdded(String ip) throws WrongPageException {
                assertPageOpened();

                if(ip.isEmpty())
                    throw new RuntimeException("isIpAlreadyAdded expected to receive non empty ip");

                try {
                    findOne(new PageElement("ip_added_check", TokenType.XPath, String.format("//td[contains(text(), '%s')]", ip)));
                    return true;
                } catch (NoSuchElementException e) { /*If caught, then ip is not found*/ }
                return false;
            }

            public IPv4_Export openExportAll() throws WrongPageException, NoSuchElementException, TabAlreadySavedException, URLIsImproper {
                return openExport("all");
            }

            public IPv4_Export openExport(ProxyType type) throws WrongPageException, NoSuchElementException, TabAlreadySavedException, URLIsImproper {
                return openExport("%s-%s".formatted(AliasCollection.common.countryCode.of(type.country), categoryCode.of(type.category)));
            }

            protected IPv4_Export openExport(String type) throws WrongPageException, NoSuchElementException, TabAlreadySavedException, URLIsImproper {
                assertPageOpened();

                click(export_dropdown);
                click(export_button.get(type));

                return new DashboardV2(tab().scene().saveRecentTab("proxy_export_"+type)).ipv4.export;
            }

            public IPv4_Dashboard dismissActiveModals() throws WrongPageException, NoSuchElementException {
                assertPageOpened();
                for(var modal : findAll(active_modal_dismiss_button, true)){
                    modal.click();
                }
                return this;
            }

            public IPv4_Dashboard setAuthIP() throws WrongPageException, NoSuchElementException {
                return setAuthIP("");
            }

            public IPv4_Dashboard setAuthIP(String ip) throws WrongPageException, NoSuchElementException {
                assertPageOpened();

                click(ip_radiobutton);
                try {
                    click(modal_ip_added_confirm_button);
                } catch (NoSuchElementException e){
                    //ignore otherwise, modal may appear if ip auth wasn't selected before, and there's ips to authorize
                }

                if(ip.isEmpty()){
                    click(geolocate_ip_button);
                } else {
                    input(ip_input, ip);
                }


                if(!isAuthIpAlreadyAdded(findOne(ip_input).getAttribute("value"))){
                    click(ip_add_button);
                    click(modal_ip_added_confirm_button);
                }

                return this;
            }

            public IPv4_Dashboard setAuthPWD() throws WrongPageException, NoSuchElementException {
                assertPageOpened();

                click(pwd_radiobutton);

                return this;
            }

            public IPv4_Dashboard setRotationInterval(String portId, int minutes) throws NoSuchElementException {
                select(rotating_intervals.get(portId), "%d mins".formatted(minutes));
                return this;
            }

            /**
             * @return list of map containing keys: id, ip, port, location, rotation(integer, if port rotating, otherwise - text)
             * type(proxy type string), time(assignment time), rotating(boolean true/false), actual_ip (optional, only on rotating ports)
             */
            public List<Map<String, String>> getPortsList() throws NoSuchElementException, WrongPageException, PageTimeoutException {
                assertPageOpened();

                //TODO expand all ips

                var list = new ArrayList<Map<String, String>>();

                List<String> ids;
                try {
                    //simple check if there's at least ids on page
                    ids = findAll(port_ids);
                } catch (NoSuchElementException e){
                    return list; //return empty list, no ips were found
                }
                var ips = findAll(proxy_ips);
                var ipPorts = findAll(proxy_ports);
                var locations = findAll(proxy_locations);
                var rotationIntervals = findAll(rotation_intervals);
                var types = findAll(proxy_types);
                var timeAssignments = findAll(assignment_time_column);
                List<String> rotatingIds;
                try{
                    rotatingIds = findAll(rotating_port_ids);
                } catch(NoSuchElementException e){
                    rotatingIds = List.of();
                }

                for(int i = 0; i < ips.size(); i++){
                    String rotationInterval;
                    String actualIp = null;
                    boolean rotating = false;
                    String portId = ids.get(i);
                    //if current port is rotating:
                    if(rotatingIds.contains(portId)){
                        rotationInterval = findOne(selected_rotating_interval.get(portId));
                        actualIp = findOne(actual_ip.get(portId));
                        rotating = true;
                    } else {
                        rotationInterval = rotationIntervals.get(i);
                    }

                    var map = new HashMap<>(Map.of(
                            "id", portId,
                            "ip", ips.get(i),
                            "port", ipPorts.get(i),
                            "location", locations.get(i),
                            "rotation", rotationInterval,
                            "type", types.get(i),
                            "time", timeAssignments.get(i),
                            "rotating", String.valueOf(rotating)
                    ));
                    if(actualIp != null)
                        map.put("actual_ip", actualIp);

                    list.add(map);
                }
                return list;
            }
        }


        public class IPv4_Packages extends AbstractPage {
            protected IPv4_Packages() throws URLIsImproper {
                super("IPv4_Upgrade", "/checkout/upgrade", false, false);
            }

            PageElement upgrade_button = new PageElement("upgrade_button", TokenType.Class, "upgrade-order");
            PageElement upgrade_confirm_button = new PageElement("upgrade_confirm_button", TokenType.Id, "do-upgrade");

            public class Package {
                ProxyCountry country;
                ProxyCategory category;
                int amount;

                protected Package(String type, String amount) throws WrongNameException {
                    var typeParts = type.trim().split(" ");
                    this.country = ProxyType.findCountry(ProxyType.AliasCollection.dashboardv2.countryShortName, typeParts[0]);
                    this.category = ProxyType.findCategory(typeParts[1]);
                    this.amount = Integer.parseInt(amount.trim());
                }
            }

            public IPv4_Packages open() {
                return (IPv4_Packages) _open();
            }

            public List<Package> getPackages() throws WrongPageException {
                assertPageOpened();

                return new ArrayList<Package>();
            }

            public Invoice clickUpgrade() throws NoSuchElementException, PageTimeoutException, WrongPageException {
                click(upgrade_button);
                click(upgrade_confirm_button);
                tab().waitLoaded();
                invoice.assertPageOpened();

                return invoice;
            }

        }


        public class IPv4_Export extends AbstractPage {
            protected IPv4_Export() throws URLIsImproper {
                super("/dashboard/export", "/", true, false);
            }

            PageElement ip_list = new PageElement("ip_list", TokenType.Tag, "pre");
            
            public IPv4_Export open(String type) {
                return (IPv4_Export) _open();
            }

            public List<String> getIPs() throws WrongPageException, NoSuchElementException {
                assertPageOpened();

                return new ArrayList<String>(List.of(findOne(ip_list).getText().split("\n")));
            }
        }


        public class IPv4_Locations extends AbstractPage {
            protected IPv4_Locations() throws URLIsImproper {
                super("IPv4 Locations", "/dashboard/locations");
            }

            protected GenericPageElement location_amount_input = new GenericPageElement("location_amount_input",
                    "Locations name(like 'Mixed'); country code; category code",
                    TokenType.XPath, "//label[contains(text(), '%s')]/..//input[@data-country='%s' and @data-category='%s']"); //h4[text() = '%s Proxies']/../..//label[contains(text(), '%s')]/..//input");
            protected GenericPageElement save_location_button = new GenericPageElement("save_location_button",
                    "country code; category code;",
                    TokenType.XPath, "//form[contains(@class, '%s_%s')]//input[@type='submit' and @value='Save Location Settings']");//"//h4[text() = '%s Proxies']/../..//input[@type='submit' and @value='Save Location Settings']");
            protected GenericPageElement total_amount_block = new GenericPageElement("total_amount_block",
                    "country code; category code",
                    TokenType.Id, "%s_%s_total"); //"//h4[text() = '%s Proxies']/../..//div[contains(@class, 'location_category')]");
            protected PageElement locations_total_blocks = new PageElement("locations_total_blocks", TokenType.CssSelector, "div[id$='_total']");


            public IPv4_Locations open() {
                return (IPv4_Locations) _open();
            }

            public void setLocationsToMixed(ProxyType type) throws PageTimeoutException, NoSuchElementException, WrongPageException {
                setLocations(type, "Mixed");
            }

            public void setLocationsToMixed(String countryCode, String categoryCode) throws PageTimeoutException, NoSuchElementException, WrongPageException {
                setLocations(countryCode, categoryCode, "Mixed");
            }

            public void setLocationsToMixed(ProxyType type, int amount) throws PageTimeoutException, NoSuchElementException, WrongPageException {
                setLocations(type, "Mixed", amount);
            }

            public void setLocationsToMixed(String countryCode, String categoryCode, int amount) throws PageTimeoutException, NoSuchElementException, WrongPageException {
                setLocations(countryCode, categoryCode, "Mixed", amount);
            }

            public void setLocations(ProxyType type, String location) throws NoSuchElementException, PageTimeoutException, WrongPageException {
                setLocations(AliasCollection.common.countryCode.of(type.country), categoryCode.of(type.category), location);
            }

            public void setLocations(ProxyType type, String location, int amount) throws NoSuchElementException, PageTimeoutException, WrongPageException {
                setLocations(AliasCollection.common.countryCode.of(type.country), categoryCode.of(type.category), location, amount);
            }

            public void setLocations(String countryCode, String categoryCode, String location) throws NoSuchElementException, PageTimeoutException, WrongPageException {
                String availableAmount = findOne(total_amount_block.get(countryCode, categoryCode)).getText();
                int amount = Integer.parseInt(availableAmount);
                setLocations(countryCode, categoryCode, location, amount);
            }

            public void setLocations(String countryCode, String categoryCode, String location, int amount) throws NoSuchElementException, PageTimeoutException, WrongPageException {
                assertPageOpened();

                var location_amount = location_amount_input.get(location, countryCode, categoryCode);
                input(location_amount,
                        Integer.parseInt(findOne(location_amount).getAttribute("value")) + amount);
                click(save_location_button.get(countryCode, categoryCode));

                Helper.sleep(Config.sleep.v2_dashboard.setting_saving_wait);
                t.waitLoaded();
            }

            /**
             * @return list of map containing keys country, category,
             *   total(amount of total locations allowed), left(amount of undistributed locations)
             */
            public List<Map<String, String>> getPackagesSummary() throws WrongPageException, NoSuchElementException {
                assertPageOpened();

                var list = new ArrayList<Map<String, String>>();
                var locations = findAll(locations_total_blocks, false);
                for(WebElement el : locations){
                    String id = el.getAttribute("id");
                    String country = id.split("_")[0];
                    String category = id.split("_")[1];
                    String total = el.getAttribute("data-total");
                    String left = el.getText();
                    list.add(Map.of("country", country, "category", category, "total", total, "left", left));
                }

                return list;
            }

        }


        public class IPv4_Replacements extends AbstractPage {
            protected IPv4_Replacements() throws URLIsImproper {
                super("IPv4 Replacements", "/dashboard/replace", false, false);
            }

            protected PageElement multiple_ips_input = new PageElement("multiple_ips_input", TokenType.Name, "replace");
            protected PageElement multiple_ips_submit = new PageElement("multiple_ips_submit", TokenType.CssSelector, "input[type='submit']");
            /**
             * (1) ip
             */
            protected GenericPageElement single_ip_replace_button = new GenericPageElement("single_ip_replace_button", TokenType.XPath, "//td[text() = '%s']/parent::tr//a[starts-with(@class, 'btn')]");

            public IPv4_Replacements open() {
                return (IPv4_Replacements) _open();
            }

            public IPv4_Replacements replaceMultipleIps(List<String> ips) throws WrongPageException, NoSuchElementException, PageTimeoutException {
                assertPageOpened();
                input(multiple_ips_input, ips.stream().collect(Collectors.joining("\n")));
                click(multiple_ips_submit);
                t.waitLoaded();
                return this;
            }

            public IPv4_Replacements replaceSingleIp(String ip) throws WrongPageException, NoSuchElementException, PageTimeoutException {
                assertPageOpened();
                click(single_ip_replace_button.get(ip));
                t.waitLoaded();
                return this;
            }

        }


        public class IPv4_Settings extends AbstractPage {
            protected IPv4_Settings() throws URLIsImproper {
                super("IPv4_Settings", "/dashboard/settings", false, false);
            }

            protected PageElement dead_rotation_checkbox = new PageElement("dead_rotation_checkbox", TokenType.Id, "rotate_ever");
            protected PageElement monthly_rotation_checkbox = new PageElement("monthly_rotation_checkbox", TokenType.Id, "rotate_30");

            public IPv4_Settings open() {
                return (IPv4_Settings) _open();
            }


            public IPv4_Settings setRotationOnDead(boolean enable) throws NoSuchElementException, WrongPageException {
                assertPageOpened();
                var checkbox = findOne(dead_rotation_checkbox);
                //reverting checkbox status, since it's opposite on page
                if((!checkbox.isSelected()) != enable){
                    checkbox.click();
                    //little sleep until json request is delivered
                    Helper.sleep(Config.sleep.v2_dashboard.setting_saving_wait);
                }
                return this;
            }

            public IPv4_Settings setMonthlyRotation(boolean enable) throws NoSuchElementException, WrongPageException {
                assertPageOpened();
                var checkbox = findOne(monthly_rotation_checkbox);
                if(checkbox.isSelected() != enable){
                    checkbox.click();
                    //little sleep until json request is delivered
                    Helper.sleep(Config.sleep.v2_dashboard.setting_saving_wait);
                }
                return this;
            }

            public boolean getRotationOnDeadStatus() throws NoSuchElementException, WrongPageException {
                assertPageOpened();
                //reverting status, since checkbox means opposite
                return ! findOne(dead_rotation_checkbox).isSelected();
            }

            public boolean getMonthlyRotationStatus() throws NoSuchElementException, WrongPageException {
                assertPageOpened();
                return findOne(monthly_rotation_checkbox).isSelected();
            }
        }
    }



    public class ISPRotating {
        protected ISPRotating() throws URLIsImproper { }

        public ISPRotating_Home home = new ISPRotating_Home();
        public ISPRotating_Buy buy = new ISPRotating_Buy();

        public class ISPRotating_Home extends AbstractPage{
            protected ISPRotating_Home() throws URLIsImproper {
                super("Rotating Residential Home", "/dashboard/residential-proxy");
            }

            protected PageElement common_creds = new PageElement("common-creds", TokenType.XPath, "//div[contains(text(), 'Your proxy access is:')]/span");
            protected PageElement special_creds = new PageElement("special-creds", TokenType.XPath, "//div[contains(text(), 'Want a higher success rate')]/span");
            protected ElementAttribute subscription_balance = new ElementAttribute("subscription_balance", TokenType.XPath, "//*[contains(text(), 'Monthly balance:')]", ":text():split(\\:)[1]:split( )[0]");
            protected ElementAttribute additional_balance = new ElementAttribute("additional_balance", TokenType.XPath, "//*[contains(text(), 'Additional balance:')]", ":text():split(\\:)[1]:split( )[0]");


            public ISPRotating_Home open(){
                return (ISPRotating_Home) _open();
            }

            public String getCommonCreds() throws WrongPageException, NoSuchElementException {
                assertPageOpened();
                return findOne(common_creds).getText();
            }

            public String getSpecialCreds() throws WrongPageException, NoSuchElementException {
                assertPageOpened();
                return findOne(special_creds).getText();
            }

            @Obsolete
            public float getBalance() throws WrongPageException, NoSuchElementException {
                return Math.round(getMonthlyBalance() / 1024f * 100f) / 100f;
            }

            public float getMonthlyBalance() throws WrongPageException, NoSuchElementException {
                assertPageOpened();
                return Math.round(Float.parseFloat(findOne(subscription_balance)) * 100) / 100.0f;
            }

            public float getAdditionalBalance() throws WrongPageException, NoSuchElementException {
                assertPageOpened();
                return Math.round(Float.parseFloat(findOne(additional_balance)) * 100) / 100.0f;
            }

        }

        public class ISPRotating_Buy extends AbstractPage {
            ISPRotating_Buy() throws URLIsImproper {
                super("Rotating Residential Buy", "/dashboard/buy-bandwidth");
            }

            protected PageElement traffic_amount_input = new PageElement("traffic_amount_input", TokenType.Tag, "input");
            protected PageElement buy_button = new PageElement("buy_button", TokenType.Tag, "button");

            public ISPRotating_Buy open(){
                return (ISPRotating_Buy) _open();
            }

            public Invoice doBuy() throws NoSuchElementException, PageTimeoutException, WrongPageException {
                return doBuy(1);
            }

            public Invoice doBuy(int amount) throws NoSuchElementException, PageTimeoutException, WrongPageException {
                assertPageOpened();

                input(traffic_amount_input, amount);
                click(buy_button);
                t.waitLoaded();
                return invoice;
            }
        }
    }

    public class TFA extends AbstractPage {
        protected TFA() throws URLIsImproper {
            super("TFA", "/dashboard/tfa");
        }

        protected PageElement tfa_code_input = new PageElement("tfa_code_input", TokenType.CssSelector, "input[name='code']");
        protected PageElement submit_button = new PageElement("submit_button", TokenType.CssSelector, "input[type='submit']");

        public TFA open(){
            return (TFA) _open();
        }

        public Home confirmTFA(String tfa) throws NoSuchElementException, WrongPageException, PageTimeoutException {
            assertPageOpened();

            input(tfa_code_input, tfa);
            click(submit_button);

            t.waitPageChanged(getUrl());
            return home;
        }
    }

    public class Invoice extends AbstractPage {

        protected Invoice(String url_invoice) throws URLIsImproper {
            super("Invoice", url_invoice, true, true);
        }

        private class old {
            protected PageElement cardholder_input = new PageElement("cardholder_input", TokenType.CssSelector, "input[data-rebilly='fullName']");
            protected PageElement pay_button = new PageElement("pay_button", TokenType.Class, "r-button-type-primary");
            protected PageElement pay_button_loading = new PageElement("pay_button_loading", TokenType.CssSelector, "button.r-button-type-primary[title='Waiting for response']");
        }

        protected PageElement info_iframe = new PageElement("info_iframe", TokenType.Name, "payment-card");
        protected PageElement confirmation_iframe = new PageElement("confirmation_iframe", TokenType.Name, "rebilly-instruments-confirmation");
        protected PageElement first_name_input = new PageElement("first_name_input", TokenType.CssSelector, "input[data-rebilly='firstName']");
        protected PageElement last_name_input = new PageElement("last_name_input", TokenType.CssSelector, "input[data-rebilly='lastName']");
        protected PageElement email_input = new PageElement("email_input", TokenType.CssSelector, "input[data-rebilly='emails']");
        protected PageElement card_iframe = new PageElement("card_iframe", TokenType.Id, "card");
        protected PageElement card_number_input = new PageElement("card_number_input", TokenType.Class, "rebilly-framepay-input-card-number");
        protected PageElement card_date_input = new PageElement("card_date_input", TokenType.Class, "rebilly-framepay-input-expiration-date");
        protected PageElement card_cvv_input = new PageElement("card_cvv_input", TokenType.Class, "rebilly-framepay-input-cvv");
        protected PageElement pay_button = new PageElement("pay_button", TokenType.XPath, "//button[@class = 'rebilly-instruments-button']");
        protected PageElement form_preloader = new PageElement("confirm_payment_loader", TokenType.CssSelector, "rebilly-instruments-loader-spinner");
        protected PageElement merchant_link = new PageElement("", TokenType.Class, "merchant-link");
        protected PageElement total_text = new PageElement("total_text", TokenType.XPath, "//span[contains(text(), 'Pay $')]");
        protected PageElement existing_card_toggle = new PageElement("existing_card_toggle", TokenType.Class, "rebilly-instruments-form-field-radio");
        protected PageElement existing_card_iframe = new PageElement("existing_card_iframe", TokenType.Id, "cardCvv");
        protected PageElement preloader = new PageElement("preloader", TokenType.Class, "r-loader-icon-wrapper");

        //TODO add method to force card

        public Home makeCardPayment() throws WrongPageException, NoSuchElementException, PageTimeoutException, ConditionTimeoutException, URLIsImproper {
            return makeCardPayment(Config.env.rebilly.card_firstName, Config.env.rebilly.card_firstName,
                    Config.env.rebilly.card_email, Config.env.rebilly.card_number,
                    Config.env.rebilly.card_date, Config.env.rebilly.card_cvv);
        }

        public Home makeCardPayment(String firstName, String lastName, String holderEmail, String card, String expDate, String cvv) throws WrongPageException, NoSuchElementException, PageTimeoutException, ConditionTimeoutException, URLIsImproper {
            assertPageOpened();

            waitDisappeared(preloader);
            sleep(org.vitmush.qa.selenium_frame.base.Config.run.waitSleep);
            waitDisappeared(form_preloader);
            sleep(org.vitmush.qa.selenium_frame.base.Config.run.waitSleep);

            if(isCardAlreadyExists()){
                t.frameFocus(findOne(info_iframe));
                t.frameFocus(findOne(existing_card_iframe));
                input(card_cvv_input, Config.env.rebilly.card_cvv);
                t.frameLose();
            } else {
                t.frameFocus(findOne(info_iframe));
                //input(cardholder_input, holderName);
                findOne(first_name_input);
                input(first_name_input, firstName);
                input(last_name_input, lastName);
                input(email_input, holderEmail);
                //t.frameLose();

                t.frameFocus(findOne(card_iframe));
                var el = findOne(card_number_input);
                input(card_number_input, card);
                input(card_date_input, expDate);
                input(card_cvv_input, cvv);
                t.frameLose();
                sleep(org.vitmush.qa.selenium_frame.base.Config.run.waitSleep);
            }

            t.frameFocus(findOne(info_iframe));
            click(pay_button);
            t.frameLose();
            waitDisappeared(form_preloader);

            String url = t.a().getCurrentUrl();
            //confirm payment
            t.frameFocus(findOne(confirmation_iframe));
            click(pay_button);
            t.frameLose();

            waitDisappeared(form_preloader, true);
            t.waitPageChanged(url   );

            return home;
        }

        public boolean isCardAlreadyExists(){
            try{
                t.frameFocus(findOne(info_iframe));
                findOne(existing_card_toggle, true);
                return true;
            } catch(NoSuchElementException ex){
                return false;
            } finally {
                t.frameLose();
            }
        }

        public float getTotal() throws NoSuchElementException {
            return Float.parseFloat(findOne(total_text).getText().replaceAll("[a-zA-Z$ ]+", ""));
        }

        @Deprecated
        public Home clickReturnLink() throws NoSuchElementException, PageTimeoutException {
            click(merchant_link);
            t.waitPageChanged();

            return home;
        }
    }



    public class Purchase extends AbstractPage {
        protected Purchase() throws URLIsImproper {
            super("Purchase", "/checkout/buy", true, false);
        }

        PageElement lose_focus = new PageElement("lose_focus", TokenType.Class, "content-heading");
        PageElement preloader = new PageElement("preloader", TokenType.Class, "spinner-bars");
        PageElement country_select = new PageElement("country_select", TokenType.Name, "plan[country]");
        PageElement category_select = new PageElement("category_select", TokenType.Name, "plan[category]");
        PageElement amount_field = new PageElement("amount_field", TokenType.Name, "plan[quantity]");
        GenericPageElement billing_cycle_radiobutton = new GenericPageElement("billing_cycle_radiobutton", TokenType.XPath, "//input[@id = 'radios-%s']");
        PageElement promo_field = new PageElement("promo_field", TokenType.Name, "details[promocode]");
        PageElement promo_apply = new PageElement("promo_apply", TokenType.LinkText, "Apply Promo Code");
        PageElement promo_discount = new PageElement("promo_discount", TokenType.Class, "discount-total");
        ElementAttribute total = new ElementAttribute("total", TokenType.Id, "total", ":text()");
        PageElement tos_checkbox = new PageElement("terms_of_service_checkbox", TokenType.Id, "checkbox-tos");
        PageElement aup_checkbox = new PageElement("acceptable_use_policy_checkbox", TokenType.Id, "checkbox-aup");
        PageElement pp_checkbox= new PageElement("privacy_policy_checkbox", TokenType.Id, "checkbox-pp");
        PageElement complete_button = new PageElement("complete_button", TokenType.XPath, "//button[@type = 'submit' and text() = 'Checkout']");
        PageElement complete_button_disabled_state =
                new PageElement("complete_button_disabled_state", TokenType.CssSelector, "[tyPageElement=\"submit\"].disabled");
        PageElement complete_button_enabled_state =
                new PageElement("complete_button_enabled_state", TokenType.CssSelector, "[tyPageElement=\"submit\"]:not(.disabled)");

        public Purchase open() {
            return (Purchase) _open();
        }

        public Invoice purchase(ProxyType type, int amount) throws NoSuchElementException, ConditionTimeoutException, WrongPageException, PageTimeoutException {
            return fillForm(type, amount).fillTOS().submit();
        }

        public Purchase fillForm(ProxyType type, int amount) throws NoSuchElementException, WrongPageException, ConditionTimeoutException {
            assertPageOpened();
            select(country_select, AliasCollection.dashboardv2.countryCode.of(type.country), true);
            select(category_select, AliasCollection.dashboardv2.categoryCode.of(type.category), true);
            input(amount_field, amount);
            click(lose_focus);
            waitDisappeared(preloader);
            click(billing_cycle_radiobutton.get(billingCycleCode.of(type.billingCycle)), true);

            return this;
        }

        public Purchase fillTOS() throws NoSuchElementException {
            click(tos_checkbox, true);
            click(aup_checkbox, true);
            click(pp_checkbox, true);

            return this;
        }

        public Invoice submit() throws NoSuchElementException, PageTimeoutException {
            var url = getUrl();
            click(complete_button);

            tab().waitPageChanged(url);
            return invoice;
        }

        public double total() throws NoSuchElementException, WrongPageException {
            assertPageOpened();
            return Helper.getPrice(findOne(total));
        }



    }



    public class Any extends AbstractPage {
        protected Any() throws URLIsImproper {
            super("Any", "/dashboard", true, false);
        }

        PageElement quit_button = new PageElement("quit_button", TokenType.XPath, "//*[@class='icon-logout']/parent::a");

        public Any open() {
            return (Any) _open();
        }

        public HomeIncognito quit() throws WrongPageException, NoSuchElementException, PageTimeoutException {
            assertPageOpened();

            String currentUrl = t.a().getCurrentUrl();
            click(quit_button);

            t.waitPageChanged(currentUrl);
            return incognito;
        }

    }

}
