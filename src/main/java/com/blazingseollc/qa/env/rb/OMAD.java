package com.blazingseollc.qa.env.rb;

import com.blazingseollc.qa.base.Config;
import com.blazingseollc.qa.tools.Helper;
import org.openqa.selenium.Keys;
import org.vitmush.qa.selenium_frame.base.SceneManager;
import org.vitmush.qa.selenium_frame.abstracts.AbstractSite;
import org.vitmush.qa.selenium_frame.exceptions.*;
import org.vitmush.qa.selenium_frame.base.SceneManager.Scene.TabHandle;
import org.vitmush.qa.selenium_frame.types.PageElement.Token.TokenType;

import java.text.SimpleDateFormat;
import java.util.Calendar;

//TODO validate for 404 page opened in AbstractPage._open

public class OMAD extends AbstractSite {
    public OMAD() throws URLIsImproper {
        this(SceneManager.getDefault().tab("omad"));
    }

    public OMAD(TabHandle tab) throws URLIsImproper {
        super("OMAD", tab, Config.env.rebilly.omad.url);
    }

    public QuickTasks quick = new QuickTasks();
    public Login login = new Login();
    public Home home = new Home();
    public Trial trial = new Trial();
    public Users users = new Users();

    public static class defaults {
        public static float trialTestAmount = 1.0f;
        public static int trialPlanAmount = 2;
        public static int trialDays = 1;
    }

    public class QuickTasks {

        public QuickTasks login() throws PageTimeoutException, NoSuchElementException, WrongPageException {

            try {
                isSubPageOpened("/");
            } catch (WrongPageException e){
                login.open();
            }

            if(login.isOpened())
                login.doLogin();

            return this;
        }

        public QuickTasks createTrial() throws PageTimeoutException, NoSuchElementException, WrongPageException {
            return createTrial(Helper.generateUniqueEmail(), defaults.trialTestAmount, defaults.trialPlanAmount, defaults.trialDays);
        }

        public QuickTasks createTrial(String email) throws PageTimeoutException, NoSuchElementException, WrongPageException {
            return createTrial(email, defaults.trialPlanAmount, defaults.trialDays);
        }

        public QuickTasks createTrial(String email, int planAmount, int days) throws PageTimeoutException, NoSuchElementException, WrongPageException {
            return createTrial(email, defaults.trialTestAmount, planAmount, days);
        }

        public QuickTasks createTrial(String email, float trialAmount, int planAmount, int days) throws PageTimeoutException, NoSuchElementException, WrongPageException {
            if(!trial.create.isOpened()) {
                this.login();
                trial.create.open();
            }
            trial.create.createTrial(email, Config.env.password, trialAmount, planAmount, days);
            Helper.sleep(Config.sleep.omad.trial_being_delivered_wait);
            return this;
        }

        public DashboardV2 loginAsClient(String email) throws PageTimeoutException, NoSuchElementException, WrongPageException, TabAlreadySavedException, URLIsImproper {
            this.login();
            return users.open().loginAsUser(email);
        }
    }


    public class Login extends AbstractPage {
        protected Login() throws URLIsImproper {
            super("Signin", "/login", false, false);
        }

        protected PageElement email_input = new PageElement("email_input", TokenType.Id, "email");
        protected PageElement password_input = new PageElement("password_input", TokenType.Id, "password");
        protected PageElement submit_button = new PageElement("submit_button", TokenType.CssSelector, "button[type='submit']");

        public Login open() {
            return (Login) _open();
        }

        public Home doLogin() throws PageTimeoutException, NoSuchElementException, WrongPageException {
            return doLogin(Config.env.rebilly.omad.email, Config.env.rebilly.omad.password);
        }

        public Home doLogin(String email, String password) throws NoSuchElementException, WrongPageException, PageTimeoutException {
            assertPageOpened();
            input(email_input, email);
            input(password_input, password);
            click(submit_button);
            t.waitLoaded();
            return home;
        }

    }

    public class Home extends AbstractPage {
        protected Home() throws URLIsImproper {
            super("Home", "/dashboard", false, false);
        }

        public Home open() {
            return (Home) _open();
        }

    }
    
    public class Trial {
        public Trial() throws URLIsImproper {
        }
        
        public Trial_Create create = new Trial_Create();
        public Trial_Index index = new Trial_Index();

        public class Trial_Index extends AbstractPage {
            protected Trial_Index() throws URLIsImproper {
                super("Trial_Index", "/trials", false, false);
            }


            public Trial_Index open() {
                return (Trial_Index) _open();
            }

            
        }

        public class Trial_Create extends AbstractPage {
            protected Trial_Create() throws URLIsImproper {
                super("Trial_Create", "/trials/create", false, false);
            }

            protected PageElement email_input = new PageElement("email_input", TokenType.Id, "email");
            protected PageElement password_input = new PageElement("password_input", TokenType.Id, "password");
            protected PageElement amount_input = new PageElement("amount_input", TokenType.Id, "quantity");
            protected PageElement trial_amount_input = new PageElement("trial_amount_input", TokenType.Id, "trial_balance");
            protected PageElement due_date_input = new PageElement("due_date_input", TokenType.Id, "dueDate");
            protected PageElement submit_button = new PageElement("submit_button", TokenType.Id, "submit-form");
            protected ElementAttribute warning_text = new ElementAttribute("warning_text", TokenType.XPath, "//strong[text() = 'Warning!']/parent::div", ":text():split(! )[1]:split(\n)[0]:trim()");
            protected ElementAttribute info_text = new ElementAttribute("info_text", TokenType.XPath, "//strong[text() = 'Info!']/parent::div", ":text():split(! )[1]:split(\n)[0]:trim()");

            public Trial_Create open() {
                return (Trial_Create) _open();
            }

            public boolean isTrialCreated() throws WrongPageException {
                assertPageOpened();
                try {
                    if(findOne(info_text).contains("Trial package was created"))
                        return true;
                } catch (NoSuchElementException e){
                    return false;
                }
                return false;
            }

            public Trial_Create createTrial(String email, String password, int amount, int days) throws NoSuchElementException, PageTimeoutException, WrongPageException {
                return createTrial(email, password, 1, amount, days);
            }

            public Trial_Create createTrial(String email, String password, float trialAmount, int amount, int days) throws NoSuchElementException, PageTimeoutException, WrongPageException {
                assertPageOpened();
                return setEmail(email).setPassword(password).setTrialAmount(trialAmount)
                        .setAmount(amount).setDays(days).pressSubmit();
            }

            public Trial_Create assertFieldsRequired() throws NoSuchElementException {
                assert(findOne(email_input).getAttribute("required").equals("true"));
                assert(findOne(due_date_input).getAttribute("required").equals("true"));
                return this;
            }
            
            public String getError() throws NoSuchElementException {
                return findOne(warning_text);
            }

            public Trial_Create setEmail(String email) throws NoSuchElementException {
                input(email_input, email);
                return this;
            }

            public Trial_Create setPassword(String password) throws NoSuchElementException {
                input(password_input, password);
                return this;
            }

            public Trial_Create setTrialAmount(float amount) throws NoSuchElementException {
                input(trial_amount_input, amount);
                return this;
            }

            public Trial_Create setAmount(int amount) throws NoSuchElementException {
                input(amount_input, amount);
                return this;
            }

            public Trial_Create setDays(int days) throws NoSuchElementException {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DATE, days);
                String duedate = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
                input(due_date_input, duedate.replace("-", Keys.RIGHT));
                return this;
            }

            public Trial_Create pressSubmit() throws NoSuchElementException, PageTimeoutException {
                click(submit_button);
                t.waitLoaded();
                return this;
            }
        }
    }

    public class Users extends AbstractPage {
        protected Users() throws URLIsImproper {
            super("Users", "/users", false, false);
        }

        protected PageElement search_input = new PageElement("search_input", TokenType.CssSelector, "input[type='search']");
        protected PageElement login_button = new PageElement("login_button", TokenType.Class, "edit-user");
        /**
         * (1) email
         */
        protected GenericPageElement user_login_button = new GenericPageElement("user_login_button", TokenType.XPath, "//td[@class='sorting_1' and text()='%s']//following::a[contains(@class, 'edit-user')]");

        public Users open() {
            return (Users) _open();
        }

        public DashboardV2 loginAsUser(String email) throws WrongPageException, NoSuchElementException, TabAlreadySavedException, URLIsImproper, PageTimeoutException {
            assertPageOpened();
            var user_login = user_login_button.get(email);
            try {
                findOne(user_login, true).click();
            } catch (NoSuchElementException e){
                input(search_input, email + Keys.ENTER);
                findOne(user_login, Config.sleep.omad.user_being_searched_timeout * 1000).click();
            }

            var dashboard = new DashboardV2(tab().scene().saveRecentTab());
            dashboard.tab().waitLoaded();
            return dashboard;
        }

    }
}