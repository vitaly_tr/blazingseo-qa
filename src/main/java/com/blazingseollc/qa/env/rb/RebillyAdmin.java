package com.blazingseollc.qa.env.rb;

import com.blazingseollc.qa.base.Config;
import com.blazingseollc.qa.tools.Helper;
import com.blazingseollc.qa.base.ProxyType;
import com.blazingseollc.qa.tools.Wait;
import org.vitmush.qa.selenium_frame.base.Debug;
import org.vitmush.qa.selenium_frame.base.SceneManager;
import org.vitmush.qa.selenium_frame.abstracts.AbstractSite;
import org.vitmush.qa.selenium_frame.types.PageElement.Token.TokenType;
import org.vitmush.qa.selenium_frame.exceptions.*;
import org.vitmush.qa.selenium_frame.exceptions.NoSuchElementException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.*;

import static com.blazingseollc.qa.base.ProxyType.AliasCollection.rebilly.categoryName;

public class RebillyAdmin extends AbstractSite {

    public RebillyAdmin() throws URLIsImproper {
        this(SceneManager.getDefault().tab("rb_admin"));
    }

    public RebillyAdmin(SceneManager.Scene.TabHandle tab) throws URLIsImproper {
        super("Rebilly Admin", tab, Config.env.rebilly.admin.url);
    }

    public Signin signin = new Signin();
    public Home home = new Home();
    public User user = new User();
    public Subscription subscription = new Subscription();
    public Invoice invoice = new Invoice();
    public Any any = new Any();
    public QuickTasks quick = new QuickTasks();

    public class QuickTasks {
        public QuickTasks makeLastUnpaidInvoicePaid(String userEmail) throws PageTimeoutException, ConditionTimeoutException, NoSuchElementException, WrongPageException {
            var invoices = getInvoices(userEmail);
            if(invoices.size() == 0)
                throw new RuntimeException("User has no invoices.");
            makeInvoicePaid(invoices.get(0).get("id"), true);
            Helper.sleep(Config.sleep.rebilly.product_sync_wait);
            return this;
        }

        public QuickTasks makeAllUnpaidInvoicesPaid(String userEmail) throws PageTimeoutException, NoSuchElementException, WrongPageException, ConditionTimeoutException {
            var invoices = getInvoices(userEmail);
            for(var invoice : invoices){
                makeInvoicePaid(invoice.get("id"), true);
            }
            Helper.sleep(Config.sleep.rebilly.product_sync_wait);
            return this;
        }

        public List<User.SubscriptionInfo> getActiveSubscriptions(String userEmail) throws PageTimeoutException, ConditionTimeoutException, NoSuchElementException, WrongPageException, ParseException, URLIsImproper {
            return openUser(userEmail).getActiveSubscriptions();
        }

        public QuickTasks login() throws ConditionTimeoutException, PageTimeoutException, NoSuchElementException, WrongPageException {
            home.open();
            if(signin.isOpened())
                signin.login();
            return this;
        }

        public User openUser(String userEmail) throws PageTimeoutException, NoSuchElementException, WrongPageException, ConditionTimeoutException {
            login();
            return home.findUserByEmail(userEmail);
        }

        public Wait makeTrialSubscriptionRenewalDateToSuspendInAMinute(String subscriptionId) throws PageTimeoutException, ConditionTimeoutException, NoSuchElementException, WrongPageException {
            return makeTrialSubscriptionRenewalDateInAMinute(subscriptionId, Duration.ofHours(2));
        }

        public Wait makeTrialSubscriptionRenewalDateInAMinute(String subscriptionId) throws PageTimeoutException, ConditionTimeoutException, NoSuchElementException, WrongPageException {
            return makeTrialSubscriptionRenewalDateInAMinute(subscriptionId, Duration.ofSeconds(0));
        }
 
        private Wait makeTrialSubscriptionRenewalDateInAMinute(String subscriptionId, Duration dateShift) throws PageTimeoutException, ConditionTimeoutException, NoSuchElementException, WrongPageException {
            login();
            subscription.open(subscriptionId);
            //date that be in a minute; if it's less than 30 seconds until next minute, then it will be minute after next minute
            int currentSecond = LocalDateTime.now().getSecond();
            var inAMinute = new Date(new Date().getTime() + (currentSecond > 30 ? 120 - currentSecond : 60 - currentSecond) * 1000);
            var inAMinuteWithShift = new Date(inAMinute.getTime() + dateShift.toMillis());
            subscription.changeTrialRenewalDate(inAMinuteWithShift);

            return new Wait(new Date(inAMinute.getTime() + (long) Config.sleep.rebilly.product_update_on_cron_wait * 1000));
        }


        private List<Map<String, String>> getInvoices(String userEmail) throws PageTimeoutException, NoSuchElementException, WrongPageException, ConditionTimeoutException {
            return openUser(userEmail).getUnpaidInvoices();
        }

        private QuickTasks makeInvoicePaid(String invoiceId) throws PageTimeoutException, ConditionTimeoutException, NoSuchElementException, WrongPageException {
            return makeInvoicePaid(invoiceId, false);
        }

        private QuickTasks makeInvoicePaid(String invoiceId, boolean doNotWaitUntilPaid) throws PageTimeoutException, ConditionTimeoutException, NoSuchElementException, WrongPageException {
            //invoice.open(invoiceId).makeFreeAndPaid();
            invoice.open(invoiceId).addMinimalPayment();
            if(false == doNotWaitUntilPaid)
                Helper.sleep(Config.sleep.rebilly.product_sync_wait);
            return this;
        }

    }

    /******
     ****  Pages Implementation
     ******/

    public class Signin extends AbstractPage{
        protected Signin() throws URLIsImproper {
            super("Signin", "/login");
        }

        protected PageElement email_input = new PageElement("email_input", TokenType.XPath, "//input[contains(@placeholder, 'yours@example.com')]");
        protected PageElement password_input = new PageElement("password_input", TokenType.XPath, "//input[contains(@placeholder, 'your password')]");
        protected PageElement submit_button = new PageElement("submit_button", TokenType.XPath, "//span[contains(text(), 'Log In')]/parent::button");

        public Signin open(){
            return (Signin) _open();
        }

        @Override
        public void assertPageOpened() throws WrongPageException {
            isSubPageOpened(Config.env.rebilly.admin.authUrl, true);
        }

        public RebillyAdmin.Home login() throws PageTimeoutException, NoSuchElementException, WrongPageException, ConditionTimeoutException {
            return login(Config.env.rebilly.admin.email, Config.env.rebilly.admin.password);
        }

        public RebillyAdmin.Home login(String email, String password) throws NoSuchElementException, PageTimeoutException, WrongPageException, ConditionTimeoutException {
            assertPageOpened();

            input(email_input, email);
            input(password_input, password);
            click(submit_button);

            t.waitPageChanged();
            var authorizationInProgressUrl = getPathUrl("/authorized", false);
            if(t.a().getCurrentUrl().equals(authorizationInProgressUrl)){
                t.waitPageChanged(authorizationInProgressUrl);
            }
            any.waitRebillyLoading();
            return home;
        }

    }

    public class Home extends AbstractPage {
        protected Home() throws URLIsImproper {
            super("KPIs", "/", true, false);
        }

        protected PageElement search_input = new PageElement("search_input", TokenType.XPath, "//input[contains(@placeholder, 'Search by name, organization or id')]");
        protected PageElement search_submit_button = new PageElement("search_submit_button", TokenType.XPath, "//div[contains(text(), 'Search')]/parent::button");
        protected PageElement first_search_result_link = new PageElement("first_search_result_link", TokenType.CssSelector, ".isHighLight-customers-0 a");
        protected PageElement search_spinner = new PageElement("search_spinner", TokenType.CssSelector, "svg.r-is-spinning");

        public Home open() throws ConditionTimeoutException, PageTimeoutException {
            _open();
            any.waitRebillyLoading();
            return this;
        }

        public User findUserByEmail(String email) throws PageTimeoutException, NoSuchElementException, ConditionTimeoutException, WrongPageException {
            assertPageOpened();

            input(search_input, email);
            click(search_submit_button);
            click(first_search_result_link);

            t.waitPageChanged();
            any.waitRebillyLoading();
            any.waitPreloader();
            return user;
        }
    }

    public class User extends AbstractPage {
        protected User() throws URLIsImproper {
            super("User", "/", true, false);
        }

        public static class SubscriptionInfo {
            public String id;
            public String plan;
            public String country;
            public String category;
            public LocalDateTime start;
            public LocalDateTime renewal;
            public int cycle;
            public int days;
            public int quantity;

            protected SubscriptionInfo(String id, String plan, String country, String category, LocalDateTime start, LocalDateTime renewal, int cycle, int days, int quantity){
                this.id = id;
                this.plan = plan;
                this.country = country;
                this.category = category;
                this.start = start;
                this.renewal = renewal;
                this.cycle = cycle;
                this.days = days;
                this.quantity = quantity;
            }
        }

        protected PageElement segments_switch = new PageElement("inactive_segments_switch", TokenType.CssSelector, "span.field-switch-handle"); //""input.field-switch[value='false']");
        protected ElementAttribute segments_switch_status = new ElementAttribute("segments_switch_status", TokenType.CssSelector, "input.field-switch", "value");
        protected PageElement billing_section = new PageElement("billing_section", TokenType.XPath, "//strong[contains(text(), 'Billing')]");
        protected PageElement unpaid_invoices_subsection = new PageElement("unpaid_invoices_subsection", TokenType.XPath, "//span[contains(text(), 'Unpaid Invoices')]");
        protected PageElement active_subscriptions_subsection = new PageElement("unpaid_invoices_subsection", TokenType.XPath, "//span[contains(text(), 'Active Subscriptions')]");
        protected PageElement segments_table = new PageElement("segments_table", TokenType.XPath, "//div[contains(@class, 'segments-grid')]//table");
        /**
         * (1) N column
         */
        protected GenericElementAttribute segments_table_column = new GenericElementAttribute("segments_table_column", TokenType.XPath, "//div[contains(@class, 'segments-grid')]//table//tr/td[%d]/div", ":text()");

        @Override
        public void assertPageOpened() throws WrongPageException {
            super.assertPageOpened();

            String currentUrl = t.a().getCurrentUrl();
            if(! currentUrl.contains("/customers/"))
                throw new WrongPageException(currentUrl, getUrl() + "*/customers/*", getPageName());
        }

        /**
         *
         * @return list of map {id: invoice id, amount: 1.00(price), plan: US Rotating Proxies, country: US, category: Rotating}
         */
        public List<Map<String,String>> getUnpaidInvoices() throws WrongPageException, NoSuchElementException, ConditionTimeoutException, PageTimeoutException {
            //TODO create InvoiceInfo class
            assertPageOpened();

            //TODO return list of invoice id instead

            if(findOne(segments_switch_status).equals("false")) {
                click(segments_switch);
                any.waitRebillyLoading();
                any.waitPreloader();
            }
            click(billing_section);
            any.waitPreloader();
            click(unpaid_invoices_subsection);
            any.waitPreloader();

            var invoices = new ArrayList<Map<String, String>>();

            try {
                var ids = findAll(segments_table_column.get(2), true);
                var plans = findAll(segments_table_column.get(4), true);
                var amounts = findAll(segments_table_column.get(6), true);
                for (var i = 0; i < ids.size(); i++) {
                    String plan = plans.get(i).replaceFirst(" \\(.*", "");
                    var planPart = plan.split(" ");
                    boolean containsCountry = planPart.length > 2;
                    invoices.add(Map.of(
                            "id", ids.get(i),
                            "amount", amounts.get(i).replace("$", ""),
                            "plan", plan,
                            "country", containsCountry ? planPart[0] : "",
                            "category", containsCountry ? planPart[1] : planPart[0]
                    ));
                }
                /*List<WebElement> invoices = findAll(unpaid_invoices);
                var urls = new ArrayList<String>();
                invoices.forEach(n -> urls.add(n.getAttribute("href")));
                return urls;*/
            } catch (NoSuchElementException e) {
                Debug.Warning("Unpaid invoices not found");
            }

            return invoices;
        }

        public List<SubscriptionInfo> getActiveSubscriptions() throws WrongPageException, ConditionTimeoutException, ParseException, PageTimeoutException, NoSuchElementException, URLIsImproper {
            //TODO cycle if fetched as difference between 'created' and 'renewal' dates, fix this, check https://app.rebilly.com/4086bbf4-bfdd-49cd-ad47-2311ace34a26/customers/0ca80695-8b8d-47a9-bce5-8c84d6e9cf2d
            assertPageOpened();

            var list = new ArrayList<SubscriptionInfo>();

            if(findOne(segments_switch_status).equals("false")) {
                click(segments_switch);
                any.waitRebillyLoading();
                any.waitPreloader();
            }
            click(billing_section);
            any.waitPreloader();
            click(active_subscriptions_subsection);
            any.waitPreloader();

            //will be used to get plan name from subscription page
            RebillyAdmin admin = new RebillyAdmin(SceneManager.getDefault().tab("Admin_getActiveSubscriptions"));

            try {
                findOne(segments_table);
                var ids = findAll(segments_table_column.get(1), true);
                var start_timestamps = findAll(segments_table_column.get(5));
                var renewal_timestamps = findAll(segments_table_column.get(7));
                var quantities = findAll(segments_table_column.get(9));
                for (var i = 0; i < ids.size(); i++) {
                    String id = ids.get(i);
                    String start_date_raw = start_timestamps.get(i);
                    String renewal_date_raw = renewal_timestamps.get(i);
                    String quantity = quantities.get(i);
                    String plan_raw = admin.subscription.open(id).getPlanName();

                    //get difference between created and renewal dates, this gives cycle parameter
                    var dates = new HashMap<String, String>(Map.of("created", start_date_raw, "renewal", renewal_date_raw));
                    //TODO include time of start and renewal as well (for extended tests)
                    dates.forEach((k, v) -> {
                        dates.replace(k, v.replaceAll("(th|[nrd]{1,2}|st), ", " ").replaceAll(" at", ""));
                    });
                    SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd yyyy hh:mm aa");
                    var created_date = dateFormat.parse(dates.get("created")).toInstant().atZone(ZoneId.systemDefault())
                            .toLocalDateTime();
                    var renewal_date = dateFormat.parse(dates.get("renewal")).toInstant().atZone(ZoneId.systemDefault())
                            .toLocalDateTime();
                    //TODO it's better to check product configuration to find out cycle
                    var cycle = (int) Period.between(created_date.toLocalDate(), renewal_date.toLocalDate()).toTotalMonths();
                    var days = (int) ChronoUnit.DAYS.between(created_date, renewal_date);

                    //Plan text parsed - 'US Rotating Proxies (e3f3eb80-1ee6-4701-bf49-e838105acff1)' - to expected view
                    String plan = plan_raw.replaceFirst(" \\(.*", "");
                    var planPart = plan.split(" ");
                    boolean containsCountry = planPart.length > 2;

                    //Just another way to check cycle, but I think it's not the best way to verify if cycle is proper
                    /*var cycle = ProxyType.AliasCollection.common.billingCycleMonthCount.of(
                            ProxyType.findBillingCycle(ProxyType.AliasCollection.rebilly.planBillingCycleName,
                            planPart[planPart.length - 1])
                    );*/

                    list.add(new SubscriptionInfo(id, plan,
                            containsCountry ? planPart[0] : "", containsCountry ? planPart[1] : planPart[0],
                            created_date, renewal_date, cycle, days, Integer.parseInt(quantity)));
                }
            } catch (NoSuchElementException e){
                Debug.Warning("Active subscriptions not found");
            }

            admin.tab().close();

            return list;

            /*var subscriptions = findAll(active_subscription_blocks);
            var list = new ArrayList<Map<String, String>>();

            for(var s : subscriptions){
                //get text of web elements
                String created = s.findElement(created_date_child.by()).getText();
                String renewal = s.findElement(renewal_date_child.by()).getText();
                String plan_raw = s.findElement(plan_raw_name_child.by()).getText();

                //get difference between created and renewal dates, this gives cycle parameter
                var dates = new HashMap<String, String>(Map.of("created", created, "renewal", renewal));
                dates.forEach((k, v) -> {
                    dates.replace(k, v.replaceAll("(th|d|st), ", " ").replaceAll(" at.*", ""));
                });
                SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd yyyy");
                long cycle = Period.between(
                        dateFormat.parse(dates.get("created")).toInstant().atZone(ZoneId.systemDefault()).toLocalDate(),
                        dateFormat.parse(dates.get("renewal")).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
                        .toTotalMonths();

                //Plan text parsed - 'US Rotating Proxies (e3f3eb80-1ee6-4701-bf49-e838105acff1)' - to expected view
                String plan = plan_raw.replaceFirst(" \\(.*", "");
                var planPart = plan.split(" ");
                list.add(Map.of(
                        "plan", plan,
                        "country", planPart[0],
                        "category", planPart[1],
                        "cycle", String.valueOf(cycle)));
            }

            return list;*/
        }

        public void assertHasSubscription(ProxyType type) throws ConditionTimeoutException, ParseException, NoSuchElementException, WrongPageException, PageTimeoutException, URLIsImproper {
            if(type.category == ProxyType.ProxyCategory.ISP_Rotating){
                //TODO Add validation for ISP Rotating
                Debug.Warning("Can't validate product with no subscription");
                return;
            }
            boolean subscriptionFound = false;
            for(var subscription : getActiveSubscriptions()){
                if(subscription.country.toLowerCase().equals(ProxyType.AliasCollection.common.countryCode.of(type.country))
                        && subscription.category.equals(categoryName.of(type.category))
                        && String.valueOf(subscription.cycle).equals(ProxyType.AliasCollection.common.billingCycleMonthCount.of(type.billingCycle)))
                    subscriptionFound = true;
            }
            if(!subscriptionFound){
                throw new RuntimeException(String.format("Subscription not found in Rebilly, expected subscription: %s %s %s",
                        ProxyType.AliasCollection.common.countryCode.of(type.country),
                        categoryName.of(type.category),
                        ProxyType.AliasCollection.common.billingCycleMonthCount.of(type.billingCycle)));
            }
        }
    }

    public class Subscription extends AbstractPage {
        protected Subscription() throws URLIsImproper {
            super("Subscription", "/", true, false);
        }

        protected String calendar_date_format = "MM/dd/yyyy KK:mm aa";

        protected ElementAttribute plan_name = new ElementAttribute("plan_name", TokenType.XPath, "//th[text() = 'Description']/parent::tr/following-sibling::tr//p[1]", ":text()");
        protected PageElement cancel_order_button = new PageElement("cancel_order_button", TokenType.XPath, "//span[text()='Cancel Order']/parent::div/parent::button[@role='button']");
        protected PageElement now_radiobutton = new PageElement("now_radiobutton", TokenType.XPath, "//label[contains(text(), 'Now')]/following-sibling::input[@type='radio']");
        protected PageElement specific_date_radiobutton = new PageElement("specific_date_radiobutton", TokenType.XPath, "//label[contains(text(), ' Specific Date')]");
        protected PageElement cancel_order_confrim_button = new PageElement("cancel_order_confrim_button", TokenType.XPath, "//div[text()='Cancel Order']/parent::button[@role='button']");
        protected PageElement change_trial_end_button = new PageElement("change_trial_end_button", TokenType.XPath, "//div[text()='Change trial end date']/parent::button");
        protected PageElement trial_end_date_input = new PageElement("trial_end_date_input", TokenType.XPath, "//label[text()='Trial end']/parent::div//span//input");
        protected PageElement trial_end_confirm_button = new PageElement("trial_end_confirm_button", TokenType.XPath, "//div[@class='r-modal-actions']//div[@role='button'][contains(text(),'Change')]");
        protected PageElement change_renewal_date_button = new PageElement("change_renewal_date_button", TokenType.XPath, "//div[text()='Update Renewal']/parent::button");
        protected PageElement change_renewal_date_input = new PageElement("change_renewal_date_input", TokenType.XPath, "//label[contains(text(), ' Specific Date')]/parent::div/following-sibling::div//div[@class='calendar r-date-input']/span//input");
        protected PageElement change_renewal_confirm_button = new PageElement("change_renewal_confirm_button", TokenType.XPath, "//div[text()='Update']/parent::button");


        public Subscription open(String subscriptionId) throws PageTimeoutException, ConditionTimeoutException {
            openAndWait("/orders/%s/upcoming-invoice".formatted(subscriptionId));
            any.waitRebillyLoading();
            return this;
        }

        @Override
        public void assertPageOpened() throws WrongPageException {
            super.assertPageOpened();

            String currentUrl = t.a().getCurrentUrl();
            if (!currentUrl.contains("/orders/") && !currentUrl.contains("/upcoming-invoice"))
                throw new WrongPageException(currentUrl, getUrl() + "*/orders/*", getPageName());
        }

        public String getPlanName() throws WrongPageException, NoSuchElementException {
            assertPageOpened();
            return findOne(plan_name);
        }

        public Subscription cancelOrder() throws WrongPageException, NoSuchElementException, ConditionTimeoutException, PageTimeoutException {
            assertPageOpened();
            click(cancel_order_button);
            click(now_radiobutton, true);
            click(cancel_order_confrim_button);

            any.waitPreloader();;
            t.waitLoaded();
            any.waitRebillyLoading();
            return this;
        }

        public Subscription changeTrialRenewalDate(Date date) throws WrongPageException, NoSuchElementException, ConditionTimeoutException, PageTimeoutException {
            assertPageOpened();

            var dateFormat = new SimpleDateFormat();
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            var time = dateFormat.format(date);
            click(change_trial_end_button);
            input(trial_end_date_input, time);
            click(trial_end_confirm_button);

            any.waitPreloader();
            t.waitLoaded();
            any.waitRebillyLoading();
            return this;
        }

        public Subscription changeRenewalDate(Date date) throws WrongPageException, NoSuchElementException {
            assertPageOpened();
            var dateFormat = new SimpleDateFormat("MM/dd/yyyy KK:mm aa");
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            var time = dateFormat.format(date);

            click(change_renewal_date_button);
            click(specific_date_radiobutton);
            input(change_renewal_date_input, time);
            click(change_renewal_date_button);

            return this;
        }

    }

    public class Invoice extends AbstractPage {
        protected Invoice () throws URLIsImproper {
            super("Invoice", "/", true, false);
        }

        PageElement price_edit_button = new PageElement("price_edit_button", TokenType.XPath, "//th[text()='Unit Price']/parent::tr/parent::tbody//*[local-name()='use' and @*='#icon-edit-s']/parent::*/parent::div/parent::button");
        PageElement unit_price_input = new PageElement("unit_price_input", TokenType.XPath, "//label[text()='Unit price']/parent::div/input");
        PageElement confirm_update_button = new PageElement("confirm_update_button", TokenType.XPath, "//div[contains(text(), 'Update invoice item')]/parent::button");
        PageElement client_invoice_link_button = new PageElement("invoice_client_link", TokenType.XPath, "//div[contains(text(), 'Invoice Link')]/parent::button");
        ElementAttribute client_invoice_url_text = new ElementAttribute("client_invoice_url_text", TokenType.XPath, "//p[@class='break-word']", ":text()");
        PageElement pay_invoice_button = new PageElement("pay_invoice_button", TokenType.XPath, "//div[contains(text(), 'Pay Invoice')]/parent::button");
        PageElement pay_invoice_option_outside_radiobutton = new PageElement("pay_invoice_option_outside_radiobutton", TokenType.XPath, "//input[@value='paid-outside']/parent::div//label");
        PageElement pay_invoice_select = new PageElement("pay_invoice_select", TokenType.XPath, "//label[contains(text(), 'Payment Method ')]/parent::div//div");
        PageElement pay_invoice_advcash_select = new PageElement("pay_invoice_advcash_select", TokenType.XPath, "//span[text()='AdvCash']");
        PageElement pay_invoice_submit_button = new PageElement("pay_invoice_submit_button", TokenType.XPath, "//div[contains(text(), 'Submit')]/parent::button");

        public Invoice open(String invoiceId) throws ConditionTimeoutException, PageTimeoutException {
            openAndWait("/invoices/"+invoiceId);
            any.waitRebillyLoading();
            return this;
        }

        @Override
        public void assertPageOpened() throws WrongPageException {
            super.assertPageOpened();

            String currentUrl = t.a().getCurrentUrl();
            if(! currentUrl.contains("/invoices/"))
                throw new WrongPageException(currentUrl, getUrl() + "*/invoices/*", getPageName());
        }

        public Invoice addMinimalPayment() throws WrongPageException, NoSuchElementException, PageTimeoutException, ConditionTimeoutException {
            assertPageOpened();

            click(price_edit_button);
            input(unit_price_input, String.valueOf(0.01));
            click(confirm_update_button);

            any.waitPreloader();

            click(pay_invoice_button);
            any.waitPreloader();
            click(pay_invoice_option_outside_radiobutton);
            click(pay_invoice_select);
            click(pay_invoice_advcash_select, true);
            click(pay_invoice_submit_button);

            any.waitPreloader();
            return this;
        }

        public Invoice makeFreeAndPaid() throws WrongPageException, NoSuchElementException, PageTimeoutException, ConditionTimeoutException {
            assertPageOpened();

            click(price_edit_button);
            input(unit_price_input, String.valueOf(0));
            click(confirm_update_button);

            t.waitLoaded();
            any.waitRebillyLoading();
            any.waitPreloader();
            return this;
        }

        public DashboardV2.Invoice openInvoiceAsClient() throws WrongPageException, URLIsImproper, NoSuchElementException, PageTimeoutException {
            var url = getInvoiceClientUrl();
            var d = new DashboardV2(tab().scene().tab("RebillyAdmin.openInvoiceAsClient"));
            d.tab().get(url).waitLoaded();
            return d.invoice;
        }

        public String getInvoiceClientUrl() throws WrongPageException, NoSuchElementException {
            assertPageOpened();

            click(client_invoice_link_button);
            return findOne(client_invoice_url_text, true);
        }

    }

    public class Any extends AbstractPage {
        Any() throws URLIsImproper {
            super("", "/", true, false);
        }

        PageElement loading_tiles = new PageElement("loading_tiles", TokenType.Class, "loading-details-tile");
        PageElement preloader = new PageElement("preloader", TokenType.Class, "r-loader-icon-wrapper");

        public void waitRebillyLoading() throws ConditionTimeoutException, PageTimeoutException {
            t.waitLoaded();
            waitDisappeared(loading_tiles, true);
        }

        public void waitPreloader() throws ConditionTimeoutException {
            waitDisappeared(preloader, true);
        }

    }

}
