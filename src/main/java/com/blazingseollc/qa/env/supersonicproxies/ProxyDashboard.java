package com.blazingseollc.qa.env.supersonicproxies;

import org.openqa.selenium.NoSuchElementException;
import org.vitmush.qa.selenium_frame.base.SceneManager.Scene;
import org.vitmush.qa.selenium_frame.types.PageElement;
import org.vitmush.qa.selenium_frame.types.PageElement.Token.TokenType;
import org.vitmush.qa.selenium_frame.exceptions.ConditionTimeoutException;

import com.blazingseollc.qa.base.Config;

public class ProxyDashboard extends com.blazingseollc.qa.env.lightproxies.ProxyDashboard {
	
	public ProxyDashboard(Scene.TabHandle tab) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException{
		super(tab, Config.env.url.ssp_dashboard);
		_post_init();
	}
	
	public ProxyDashboard(Scene.TabHandle tab, String baseUrl) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException{
		super(tab, baseUrl);
		_post_init();
	}

	private void _post_init() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		home = new Home();
		login = new Login();
		signup = new Signup();
		locations = new Locations();
		dashboard = new Dashboard();
		sidebar = new Sidebar();
		checkout= new Checkout();
	}
	
	public class Home extends com.blazingseollc.qa.env.lightproxies.ProxyDashboard.Home {
		protected Home() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException{
			path = "/panel";
			signin_button.change(TokenType.CssSelector, "a.btn-primary.btn-pill-left.btn-lg");
			signup_button.change(TokenType.CssSelector, "a.btn-success.btn-pill-right.btn-lg");
		}
	}

	public class Login extends com.blazingseollc.qa.env.lightproxies.ProxyDashboard.Login {
		protected Login() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException{
			path =  "/panel/auth/sign-in.html";
		}	
	}
	
	
	public class Signup extends com.blazingseollc.qa.env.lightproxies.ProxyDashboard.Signup {
		protected Signup() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException{
			promo_apply.change(TokenType.CssSelector, "a.appprpmo");
		}	
	}
	
	
	public class Locations extends com.blazingseollc.qa.env.lightproxies.ProxyDashboard.Locations {
		protected Locations() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException{
			path = "/panel/update-locations.html";
		}	
	}
	
	public class Dashboard extends com.blazingseollc.qa.env.lightproxies.ProxyDashboard.Dashboard {
		protected Dashboard() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException{
			path = "/panel/overview.html";
			
			export = new Export();
		}	
		
		public final PageElement proxies_tab_closed_state = new PageElement("dashboard-proxies_tab-closed", TokenType.CssSelector, "a[href='#proxies'][aria-expanded=false]");
		public final PageElement auth_tab_closed_state = new PageElement("dashboard-auth-tab-closed", TokenType.CssSelector, "a[href='#authtype'][aria-expanded=false]");
		
		@Override
		public Dashboard openProxiesTab() throws org.vitmush.qa.selenium_frame.exceptions.NoSuchElementException, ConditionTimeoutException {
			try {
				findOne(proxies_tab_closed_state);
			} catch (NoSuchElementException ex) {
				return this;
			}
			click(proxies_tab);
			return this;
		}
		
		@Override
		public Dashboard openAuthTab() throws org.vitmush.qa.selenium_frame.exceptions.NoSuchElementException, ConditionTimeoutException {
			try {
				findOne(auth_tab_closed_state);
			} catch (NoSuchElementException ex) {
				return this;
			}
			click(auth_tab);
			return this;
		}
		
		
		public class Export extends com.blazingseollc.qa.env.lightproxies.ProxyDashboard.Dashboard.Export {
			protected Export() {
				path = "/panel/export";
			}
		}
	}
	

	public class Sidebar extends com.blazingseollc.qa.env.lightproxies.ProxyDashboard.Sidebar {
		protected Sidebar() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException{
			checkout_link.change(TokenType.CssSelector, ".sidebar-nav > .nav > li:nth-child(2) > a");
		}
	}
	
	
	public class Checkout extends com.blazingseollc.qa.env.lightproxies.ProxyDashboard.Checkout {
		protected Checkout() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException{
			path = "/panel/checkout.html";
			promo_apply.change(TokenType.CssSelector, ".promocode > div > a.btn");
		}
	}
	
}
