package com.blazingseollc.qa.base;

import org.apache.http.annotation.Obsolete;
import org.vitmush.qa.selenium_frame.base.Debug;
import org.vitmush.qa.selenium_frame.base.SceneManager;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.*;
import java.lang.reflect.Field;
import java.security.NoSuchAlgorithmException;
import java.util.Properties;

public class Config {

	static class Password {
		public static void decrypt(String password) throws NoSuchPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException {
        /*String secretMessage = "Baeldung secret message";
        Cipher encryptCipher = Cipher.getInstance("RSA");
        encryptCipher.init(Cipher.ENCRYPT_MODE, publicKey);
        byte[] secretMessageBytes = secretMessage.getBytes(StandardCharsets.UTF_8);)
        byte[] encryptedMessageBytes = encryptCipher.doFinal(secretMessageBytes);
        String encodedMessage = Base64.getEncoder().encodeToString(encryptedMessageBytes);

        Cipher decryptCipher = Cipher.getInstance("RSA");
        decryptCipher.init(Cipher.DECRYPT_MODE, privateKey);
        byte[] decryptedMessageBytes = decryptCipher.doFinal(encryptedMessageBytes);
        String decryptedMessage = new String(decryptedMessageBytes, StandardCharsets.UTF_8);
        assertEquals(secretMessage, decryptedMessage);*/
		}
	}

	public enum Environment { DEV, STAGING, PRODUCTION };
	public static Environment current_env;

	private static final String[] envPropertiesFilename = { "dev.properties", "staging.properties", "prod.properties" };
	private static final String generalPropertiesFilename = "general.properties" ;
	private static final String examplePropertiesFilename = "example.properties" ;

	//invoke static block by calling method
	public static void invoke_static(){	}
	//static block to be invoked
	static {
		//Initiate environment var
		String env_var = System.getenv("env");
		if(env_var != null){
			if(env_var.equals("dev"))
				current_env = Environment.DEV;
			else if(env_var.equals("staging"))
				current_env = Environment.STAGING;
			else if(env_var.equals("prod"))
				current_env = Environment.PRODUCTION;
			else { Debug.Error("Invalid ENVIRONMENT specified through environment variables"); }
		} else {
			switch(getGeneralProperty("env")){
				case "dev":
					current_env = Environment.DEV;
					break;
				case "staging":
				case "stage":
					current_env = Environment.STAGING;
					break;
				case "production":
				case "prod":
					current_env = Environment.PRODUCTION;
					break;
				default:
					Debug.Error("Invalid ENVIRONMENT specified through properties");
			}
		}

		String headless_var = System.getenv("headless");
		if(headless_var != null){
			if(headless_var.equals("false"))
				SceneManager.forceHeadless = false;
			else if(headless_var.equals("true") || headless_var.isEmpty())
				SceneManager.forceHeadless = true;
			else { Debug.Error("Invalid HEADLESS parameter is specified through environment variables"); }
		} else {
			switch (getGeneralProperty("headless")) {
				case "true" -> SceneManager.forceHeadless = true;
				case "false" -> SceneManager.forceHeadless = false;
			}
		}

		initPropertiesRecursively(getProperties(), Config.class, "");
	}

	public static class sleep {

		public static class common {

			public static int email_is_being_delivered_timeout = 3 * 60;

			static {
				invoke_static();
			}
		}

		public static class proxy_backend {
			public static int ports_get_delivered_timeout = 2 * 60;
			public static int package_is_added_timeout = 2 * 60;
			public static int ips_get_assigned_timeout = 60 * 60;
			public static int tfa_gets_saved_wait = 3;

			static {
				invoke_static();
			}
		}

		public static class v2_dashboard {
			public static int isp_rotating_authorization_wait = 3 * 60;
			public static int balance_updated_wait = 1 * 60;
			public static int setting_saving_wait = 6;

			static {
				invoke_static();
			}
		}

		public static class omad {
			public static int user_being_searched_timeout = 30;
			public static int trial_being_delivered_wait = 10;

			static {
				invoke_static();
			}
		}

		public static class rebilly {
			public static int product_update_on_cron_wait = 30;
			public static int product_sync_wait = 15;

			static {
				invoke_static();
			}
		}
	}

	public static class env {

		@Obsolete
		public static String email;
		public static String email_tokenFormat;
		public static String password;
		//TODO MEDIUM configure SceneManager to open with headless through Config

		static {
			invoke_static();
		}
		
		public static class url {
			public static String lp_whmcs;
			public static String lp_dashboard;
			public static String ssp_whmcs;
			public static String ssp_dashboard;

			static {
				invoke_static();
			}
		}

		public static class whmcs{

			public static String url_bdr;
			public static String url_whmcs;
			public static String promo_100;

			static {
				invoke_static();
			}

			public static class admin {
				public static String username;
				public static String password;

				static {
					invoke_static();
				}
			}
		}

		public static class rebilly{
			public static String url_signup;
			public static String url_signin;
			public static String url_dashboard;
			public static String url_invoice;
			public static String card_firstName;
			public static String card_lastName;
			public static String card_email;
			public static String card_number;
			public static String card_date;
			public static String card_cvv;

			static {
				invoke_static();
			}

			public static class admin {

				public static String authUrl;
				public static String url;
				public static String email;
				public static String password;

				static {
					invoke_static();
				}
			}

			public static class omad {
				public static String url;
				public static String email;
				public static String password;

				static {
					invoke_static();
				}
			}
		}

		public static class rayobyte {
			public static class api {
				public static class proxy_manager {

					public static String url;
					public static String path;
					public static String staticAdminToken;
					public static String searchMethod;
					public static String ipsMethod;

					static {
						invoke_static();
					}
				}

				public static class proxy_creds {

					public static String url;
					public static String path;
					public static String authToken;
					public static String collectionSearchByIDMethod;
					public static String collectionSearchMethod;
					public static String collectionAddMethod;
					public static String collectionUpdateMethod;
					public static String collectionRemoveMethod;
					public static String collectionRemoveByIDMethod;
					public static String credentialAddMethod;
					public static String credentialUpdateMethod;
					public static String credentialRemoveMethod;
					public static String credentialRemoveByIDMethod;

					static {
						invoke_static();
					}
				}

				public static class proxy_slots {

					public static String url;
					public static String path;
					public static String authToken;
					public static String getMethod;
					public static String createMethod;
					public static String updateMethod;
					public static String deleteSingleMethod;
					public static String deleteMultipleMethod;

					static {
						invoke_static();
					}
				}
			}
		}
	}

	public static class misc {
		public static class proxy_checker {

			public static String mbyte_file_url_1;
			public static String mbyte_file_url_2;
			public static String mbyte_file_url_5;
			public static String mbyte_file_url_10;
			public static String mbyte_file_url_20;
			public static String mbyte_file_url_50;
			public static String mbyte_file_url_100;
			public static String mbyte_file_url_200;
			public static String mbyte_file_url_500;
			public static int retries_on_fail;
			public static int download_threads;

			static {
				invoke_static();
			}
		}
	}

	public static class ssh {

		public static String ssh_creds_folder;

		static {
			invoke_static();
		}

		public static class proxy_backend {

			public static String host;
			public static String username;
			public static String privateKey;

			static {
				invoke_static();
			}
		}

		public static class whmcs {

			public static String host;
			public static String username;
			public static String privateKey;

			static {
				invoke_static();
			}
		}

		public static class v2_backend {

			public static String host;
			public static String username;
			public static String privateKey;

			static {
				invoke_static();
			}
		}

		public static class api {

			public static String host;
			public static String username;
			public static String privateKey;

			static {
				invoke_static();
			}
		}
	}

	public static class db {
		public static class proxy_backend {

			public static String dbName;
			public static String dbUsername;
			public static String dbPassword;
			public static String forwardingDestination;

			static {
				invoke_static();
			}
		}

		public static class whmcs {

			public static String dbName;
			public static String dbUsername;
			public static String dbPassword;
			public static String forwardingDestination;

			static {
				invoke_static();
			}
		}
		public static class v2_backend {

			public static String dbName;
			public static String dbUsername;
			public static String dbPassword;
			public static String forwardingDestination;

			static {
				invoke_static();
			}
		}

		public static class api {
			public static class proxy_creds {

				public static String dbName;
				public static String dbUsername;
				public static String dbPassword;
				public static String forwardingDestination;

				static {
					invoke_static();
				}
			}

			public static class proxy_slots {

				public static String dbName;
				public static String dbUsername;
				public static String dbPassword;
				public static String forwardingDestination;

				static {
					invoke_static();
				}
			}
		}
	}


	private static OutputStream propertiesListExample;
	private static void initPropertiesRecursively(Properties prop, Class cls, String composite){
		boolean debug = Boolean.parseBoolean(getGeneralProperty("debug_config_init"));

		Field[] fields = cls.getDeclaredFields();

		//if base level
		if(composite.isEmpty()) {
			fields = new Field[0];
			try {
				propertiesListExample = new FileOutputStream(examplePropertiesFilename);
			} catch (FileNotFoundException e) { e.printStackTrace(); }
		}

		for(Field f : fields){
			try {
				String propertyName = composite + '.' + f.getName();
				String property = prop.getProperty(propertyName);
				try {
					propertiesListExample.write(String.format("%s=\n", propertyName).getBytes());
				} catch (IOException e) { e.printStackTrace(); }
				if(property == null)
					continue;
				boolean nonValidType = false;
				switch (f.getGenericType().getTypeName()){
					case "boolean":
						f.set(null, Boolean.valueOf(property));
						break;
					case "java.lang.String":
						f.set(null, property);
						break;
					case "int":
						f.set(null, Integer.parseInt(property));
						break;
					default:
						Debug.Error(String.format("Unknown type of property (%s - %s)", propertyName, f.getGenericType().getTypeName()));
						nonValidType = true;
						break;
				}
				if(debug && !nonValidType)
					Debug.Log(String.format("Field %s initiated with '%s' (%s)", f.getName(), prop.getProperty(propertyName), propertyName));
			} catch (IllegalAccessException e) {
				//e.printStackTrace();
			}
		}
		if(fields.length > 0){
			try {
				propertiesListExample.write("\n".getBytes());
			} catch (IOException e) { e.printStackTrace(); }
		}

		Class[] childrens = cls.getClasses();
		if(childrens.length > 0) {
			for(Class child : childrens){
				if(child.isEnum())
					continue;
				if(debug)
					Debug.Log(String.format("Descend to %s", child.getSimpleName()));
				initPropertiesRecursively(prop, child, composite + (composite.isEmpty() ? "" : '.') + child.getSimpleName());
			}
		}

		//if base level
		if(composite.isEmpty()) {
			try {
				propertiesListExample.close();
			} catch (IOException e) { e.printStackTrace(); }
		}
	}

	private static Properties getProperties() {
		Properties prop = new Properties();
		//Load environment specific properties
		try {
			prop = loadProperties(envPropertiesFilename[current_env.ordinal()]);
		} catch (IOException e){ e.printStackTrace(); }

		//Add general properties
		try {
			Properties general = loadProperties(generalPropertiesFilename);
			for (Object k : general.keySet()){
				if(!prop.containsKey(k))
					prop.setProperty((String) k, general.getProperty((String) k));
			}
		} catch (IOException e){ e.printStackTrace(); }

		/*fillProperty(prop, "dev.password");
		fillProperty(prop, "prod.password");

		try {
			saveProperties(prop);
		} catch (IOException e) {
			e.printStackTrace();
		}*/

		return prop;
	}

	private static String getGeneralProperty(String key) {
		Properties prop = new Properties();
		try {
			prop = loadProperties(generalPropertiesFilename);
		} catch (IOException e){ e.printStackTrace(); }

		return prop.getProperty(key);
	}

	private static Properties loadProperties(String filename) throws IOException {
		Properties prop = new Properties();
		InputStream input = new FileInputStream(filename);
		prop.load(input);
		input.close();

		return prop;
	}

	/*private static void saveProperties (Properties prop) throws IOException {
		OutputStream output = new FileOutputStream(propName[current_env.ordinal()]);
		prop.store(output, null);
	}*/

	/*private static void fillProperty(Properties prop, String key){
		fillProperty(prop, key, "");
	}

	private static void fillProperty(Properties prop, String key, String customValue){
		if(!prop.containsKey(key)){
			prop.setProperty(key, customValue);
		}
	}*/

}
