package com.blazingseollc.qa.base;

public class ProxyType {

    public enum ProxyCountry { Argentina, Australia, Belgium, Brazil, Canada, China, Colombia, Egypt, France, Germany,
        India, Indonesia, Italy, Japan, Mexico, Netherlands, Pakistan, Philippines, Poland, Russia,
        Saudi_Arabia, Seychelles, Singapore, South_Africa, South_Korea, Spain,
        Taiwan, Thailand, UK, USA, Vietnam }
    //TODO rename ISP_Rotating
    public enum ProxyCategory { Dedicated, Semi, Rotating, Sneaker, Shopify, ISP, ISP_Rotating, Sneaker_ISP, Semi_ISP, IPv6 }
    public enum BillingCycle { Monthly, Quarterly, Semi_Annually, Annually }
    public enum ISPRotatingPackage { Starter, Personal, Consumer, Professional, Corporate, Enterprise }

    public ProxyCountry country;
    public ProxyCategory category;
    public BillingCycle billingCycle;

    public ProxyType(ProxyCategory category){
        this(category, ProxyCountry.USA, BillingCycle.Monthly);
    }
    public ProxyType(ProxyCategory category, ProxyCountry country){
        this(category, country, BillingCycle.Monthly);
    }
    public ProxyType(ProxyCategory category, ProxyCountry country, BillingCycle cycle){
        this.country = country;
        this.category = category;
        this.billingCycle = cycle;
    }

    public static class WrongNameException extends Exception {
        protected WrongNameException(String field, String name){
            super("Specified %s - '%s' - wasn't found in provided alias".formatted(field, name));
        }
    }

    public static ProxyCountry findCountry(String countryName) throws WrongNameException {
        return findCountry(AliasCollection.common.countryName, countryName);
    }

    public static ProxyCountry findCountry(Alias<ProxyCountry, String> alias, String country) throws WrongNameException {
        var prepared = country.trim();
        for(var k = 0; k < alias.keys.length; k++){
            if(alias.keys[k].equals(prepared))
                return ProxyCountry.values()[k];
        }
        throw new WrongNameException("country", country);
    }

    public static ProxyCategory findCategory(String categoryName) throws WrongNameException {
        return findCategory(AliasCollection.common.categoryName, categoryName);
    }

    public static ProxyCategory findCategory(Alias<ProxyCategory, String> alias, String category) throws WrongNameException {
        var prepared = category.trim();
        for(var k = 0; k < alias.keys.length; k++){
            if(alias.keys[k].equals(prepared))
                return ProxyCategory.values()[k];
        }
        throw new WrongNameException("category", category);
    }

    public static BillingCycle findBillingCycle(String billingCycleName) throws WrongNameException {
        return findBillingCycle(AliasCollection.common.billingCycleName, billingCycleName);
    }

    public static <T> BillingCycle findBillingCycle(Alias<BillingCycle, T> alias, T billingCycle) throws WrongNameException {
        //var prepared = billingCycle.trim();
        for(var k = 0; k < alias.keys.length; k++){
            if(alias.keys[k].equals(billingCycle))
                return BillingCycle.values()[k];
        }
        throw new WrongNameException("billingCycle", String.valueOf(billingCycle));
    }

    public String toString(){
        return "%s - %s (%s)".formatted(AliasCollection.common.countryName.of(country),
                AliasCollection.common.categoryName.of(category),
                AliasCollection.common.billingCycleName.of(billingCycle));
    }

    public static class Alias<T extends Enum<T>, V> {
        private final Object[] keys;

        private Alias(int enumLength){
            keys = new Object[enumLength];
        }
        public Alias(Alias<T, V> right){
            this.keys = right.keys.clone();
        }

        public V get(T origin){ return (V) keys[origin.ordinal()]; }

        public V of(T origin){
            return get(origin);
        }

        public void set(T origin, V value){
            keys[origin.ordinal()] = value;
        }
    }


    public static class AliasCollection {
        public static class common {
            public static Alias<ProxyCountry, String> countryCode = new Alias<>(ProxyCountry.values().length);
            public static Alias<ProxyCategory, String> categoryCode = new Alias<>(ProxyCategory.values().length);
            public static Alias<ProxyCategory, String> categoryName = new Alias<>(ProxyCategory.values().length);
            public static Alias<ProxyCountry, String> countryName = new Alias<>(ProxyCountry.values().length);
            public static Alias<BillingCycle, String> billingCycleName = new Alias<>(BillingCycle.values().length);
            public static Alias<BillingCycle, Integer> billingCycleMonthCount = new Alias<>(BillingCycle.values().length);

            static {
                countryCode.set(ProxyCountry.Argentina, "ar");
                countryCode.set(ProxyCountry.Australia, "au");
                countryCode.set(ProxyCountry.Belgium, "be");
                countryCode.set(ProxyCountry.Brazil, "br");
                countryCode.set(ProxyCountry.Canada, "ca");
                countryCode.set(ProxyCountry.China, "cn");
                countryCode.set(ProxyCountry.Colombia, "co");
                countryCode.set(ProxyCountry.Egypt, "eg");
                countryCode.set(ProxyCountry.France, "fr");
                countryCode.set(ProxyCountry.Germany, "de");
                countryCode.set(ProxyCountry.India, "in");
                countryCode.set(ProxyCountry.Indonesia, "id");
                countryCode.set(ProxyCountry.Italy, "it");
                countryCode.set(ProxyCountry.Japan, "jp");
                countryCode.set(ProxyCountry.Mexico, "mx");
                countryCode.set(ProxyCountry.Netherlands, "nl");
                countryCode.set(ProxyCountry.Pakistan, "pk");
                countryCode.set(ProxyCountry.Philippines, "ph");
                countryCode.set(ProxyCountry.Poland, "pl");
                countryCode.set(ProxyCountry.Russia, "ru");
                countryCode.set(ProxyCountry.Saudi_Arabia, "sa");
                countryCode.set(ProxyCountry.Seychelles, "sc");
                countryCode.set(ProxyCountry.Singapore, "sg");
                countryCode.set(ProxyCountry.South_Africa, "za");
                countryCode.set(ProxyCountry.South_Korea, "kr");
                countryCode.set(ProxyCountry.Spain, "es");
                countryCode.set(ProxyCountry.Taiwan, "tw");
                countryCode.set(ProxyCountry.Thailand, "th");
                countryCode.set(ProxyCountry.UK, "gb");
                countryCode.set(ProxyCountry.USA, "us");
                countryCode.set(ProxyCountry.Vietnam, "vn");

                categoryCode.set(ProxyCategory.Dedicated, "static");
                categoryCode.set(ProxyCategory.Semi, "semi-3");
                categoryCode.set(ProxyCategory.Rotating, "rotate");
                categoryCode.set(ProxyCategory.Sneaker, "sneaker");
                categoryCode.set(ProxyCategory.Shopify, "shopify");
                categoryCode.set(ProxyCategory.ISP, "static-res");
                categoryCode.set(ProxyCategory.Sneaker_ISP, "sneaker-static-res");
                categoryCode.set(ProxyCategory.Semi_ISP, "semi-res");
                //categoryCode.set(ProxyCategory.ISP_Rotating, "");
                //categoryCode.set(ProxyCategory.IPv6, "");

                categoryName.set(ProxyCategory.Dedicated, "Dedicated");
                categoryName.set(ProxyCategory.Semi, "Semi-Dedicated");
                categoryName.set(ProxyCategory.Rotating, "Rotating");
                categoryName.set(ProxyCategory.Sneaker, "Sneaker");
                categoryName.set(ProxyCategory.Shopify, "Shopify");
                categoryName.set(ProxyCategory.ISP, "ISP");
                categoryName.set(ProxyCategory.ISP_Rotating, "Rotating Residential");
                categoryName.set(ProxyCategory.Sneaker_ISP, "Sneaker ISP Proxies");
                categoryName.set(ProxyCategory.Semi_ISP, "Semi-Dedicated ISP");
                categoryName.set(ProxyCategory.IPv6, "IPv6");

                for (ProxyCountry country : ProxyCountry.values()) {
                    countryName.set(country, country.name());
                }
                countryName.set(ProxyCountry.Saudi_Arabia, "Saudi Arabia");
                countryName.set(ProxyCountry.South_Africa, "South Africa");
                countryName.set(ProxyCountry.South_Korea, "South Korea");
                countryName.set(ProxyCountry.UK, "United Kingdom");
                countryName.set(ProxyCountry.USA, "United States");

                for (BillingCycle cycle : BillingCycle.values()) {
                    billingCycleName.set(cycle, cycle.name());
                }
                billingCycleName.set(BillingCycle.Semi_Annually, "Semi-Annually");

                billingCycleMonthCount.set(BillingCycle.Monthly, 1);
                billingCycleMonthCount.set(BillingCycle.Quarterly, 3);
                billingCycleMonthCount.set(BillingCycle.Semi_Annually, 6);
                billingCycleMonthCount.set(BillingCycle.Annually, 12);
            }
        }

        public static class bdr {
            public static Alias<ProxyCategory, String> categoryName = new Alias<>(common.categoryName);
            public static Alias<BillingCycle, String> billingCycleName = new Alias<>(common.billingCycleName);

            static {
                categoryName.set(ProxyType.ProxyCategory.ISP, "ISP proxies");
                categoryName.set(ProxyType.ProxyCategory.Sneaker, "Sneakers");

                billingCycleName.set(ProxyType.BillingCycle.Monthly, "1 Month");
                billingCycleName.set(ProxyType.BillingCycle.Quarterly, "3 Months");
                billingCycleName.set(ProxyType.BillingCycle.Semi_Annually, "6 Months");
                billingCycleName.set(ProxyType.BillingCycle.Annually, "12 Months");
            }
        }

        public static class dashboardv2 {
            public static Alias<ProxyCountry, String> countryShortName = new Alias<>(common.countryName);
            public static Alias<ProxyCategory, String> categoryName = new Alias<>(common.categoryName);
            public static Alias<ProxyCountry, String> countryCode = new Alias<>(common.countryCode);
            public static Alias<ProxyCategory, String> categoryCode = new Alias<>(common.categoryCode);
            public static Alias<BillingCycle, String> billingCycleName = new Alias<>(common.billingCycleName);
            public static Alias<BillingCycle, String> billingCycleCode = new Alias<>(common.billingCycleName);
            public static Alias<ISPRotatingPackage, Integer> ispRotatingPackageSize = new Alias<>(ISPRotatingPackage.values().length);

            static {
                countryShortName.set(ProxyCountry.USA, "USA");
                countryShortName.set(ProxyCountry.Germany, "German");
                countryShortName.set(ProxyCountry.UK, "UK");
                countryShortName.set(ProxyCountry.France, "French");
                countryShortName.set(ProxyCountry.India, "Indian");
                countryShortName.set(ProxyCountry.Canada, "Canadian");
                countryShortName.set(ProxyCountry.Japan, "Japanese");
                countryShortName.set(ProxyCountry.Australia, "Australian");
                countryShortName.set(ProxyCountry.Vietnam, "Vietnamese");
                countryShortName.set(ProxyCountry.Spain, "Spanish");
                countryShortName.set(ProxyCountry.Italy, "Italian");
                countryShortName.set(ProxyCountry.Poland, "Polish");
                countryShortName.set(ProxyCountry.South_Africa, "South African");
                countryShortName.set(ProxyCountry.South_Korea, "South Korea");
                countryShortName.set(ProxyCountry.Russia, "Chinese");

                categoryName.set(ProxyType.ProxyCategory.IPv6, "Dedicated");

                for(var country : ProxyCountry.values()) {
                    countryCode.set(country, countryCode.get(country).toUpperCase());
                }

                categoryCode.set(ProxyType.ProxyCategory.Dedicated, "dedicated");
                //categoryCodeAlias.set(ProxyType.ProxyCategory.Semi, "semi-dedicated");
                categoryCode.set(ProxyType.ProxyCategory.Rotating, "rotating");

                billingCycleName.set(ProxyType.BillingCycle.Monthly, "1 month");
                billingCycleName.set(ProxyType.BillingCycle.Quarterly, "3 months");
                billingCycleName.set(ProxyType.BillingCycle.Semi_Annually, "6 months");
                billingCycleName.set(ProxyType.BillingCycle.Annually, "1 year");

                billingCycleCode.set(BillingCycle.Monthly, "monthly");
                billingCycleCode.set(BillingCycle.Quarterly, "quarterly");
                billingCycleCode.set(BillingCycle.Semi_Annually, "semiannually");
                billingCycleCode.set(BillingCycle.Annually, "annually");

                ispRotatingPackageSize.set(ISPRotatingPackage.Starter, 4);
                ispRotatingPackageSize.set(ISPRotatingPackage.Personal, 16);
                ispRotatingPackageSize.set(ISPRotatingPackage.Consumer, 50);
                ispRotatingPackageSize.set(ISPRotatingPackage.Professional, 100);
                ispRotatingPackageSize.set(ISPRotatingPackage.Corporate, 250);
                ispRotatingPackageSize.set(ISPRotatingPackage.Enterprise, 1000);
            }
        }

        public static class cartpageV2 {
            public static Alias<ProxyCategory, String> categoryName = new Alias<>(common.categoryName);

            static {
                categoryName.set(ProxyType.ProxyCategory.Dedicated, "DC Proxies - Dedicated");
                categoryName.set(ProxyCategory.Semi, "DC Proxies - Semi-Dedicated");
                categoryName.set(ProxyType.ProxyCategory.Rotating, "DC Proxies - Rotating");
                categoryName.set(ProxyType.ProxyCategory.ISP, "ISP Proxies - Dedicated");
                categoryName.set(ProxyCategory.Semi_ISP, "ISP Proxies - Semi-Dedicated");
                categoryName.set(ProxyCategory.ISP_Rotating, "Rotating-Residential");
                categoryName.set(ProxyType.ProxyCategory.IPv6, "DC Proxies - IPv6");
            }
        }


        public static class rebilly{
            public static Alias<ProxyCountry, String> countryName = new Alias<>(common.countryCode);
            public static Alias<ProxyCategory, String> categoryName = new Alias<>(common.categoryName);
            public static Alias<BillingCycle, String> planBillingCycleName = new Alias<>(dashboardv2.billingCycleCode);

            static {
                for(var country : ProxyCountry.values()) {
                    countryName.set(country, countryName.get(country).toUpperCase());
                }

                //categoryName.set(ProxyCategory.ISP, "ISP Proxies");
            }
        }

    }

}
