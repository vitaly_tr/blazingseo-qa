package com.blazingseollc.qa.exceptions;

import com.blazingseollc.qa.base.ProxyType;

public class NotAllowedProxyTypeError extends Error {

    public NotAllowedProxyTypeError(ProxyType.ProxyCategory category){
        this(category, "");
    }

    public NotAllowedProxyTypeError(ProxyType.ProxyCategory category, String info){
        super("Category '%s' is not allowed".formatted(category, info.isEmpty() ? "" : " (%s)".formatted(info)));
    }

}
