package qa.monitoring.supersonicproxies.dashboard;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import org.vitmush.qa.selenium_frame.base.SceneManager;
import org.vitmush.qa.selenium_frame.base.SceneManager.Scene;
import org.vitmush.qa.selenium_frame.exceptions.WrongPageException;
import org.vitmush.qa.selenium_frame.exceptions.WrongValueException;

import com.blazingseollc.qa.base.Config;

public class Cleanup {

	public void sleep() throws InterruptedException {
		sleep(5);
	}
	
	
	public void sleep(float secs) throws InterruptedException {
		Thread.sleep((long) (secs * 1000));
	}
  
	@Before
	public void setUp() {

	}
  
	@After
	public void tearDown() {
		SceneManager.quit();
	}
  
	@Test
	public void test() throws Exception {
		Scene scene = SceneManager.add("chrome0", "chrome");

		com.blazingseollc.qa.env.supersonicproxies.Whmcs whmcs = new com.blazingseollc.qa.env.supersonicproxies.
				Whmcs(scene.tab("main"));
		
		int userid = whmcs.admin.authorize().clients.open().isOpened().findClient(Config.env.email).getId();
		whmcs.admin.client.summary.openOrders().waitLoaded();
		
		try {
			int ordersTotal = whmcs.admin.client.orders.isOpened().getTotalOrders();
			if(ordersTotal > 15)
				throw new Exception("too much orders -"+ordersTotal+"- I'm not gonna delete it; needs additional validation step");
			whmcs.admin.client.orders.deleteAll().waitLoaded();
		} catch (WrongPageException ex) {
			whmcs.admin.client.order.delete().waitLoaded();
		}
		
		whmcs.admin.client.profile.open(userid)
		.updateState().updateEmail(Config.env.email.replaceFirst("@", "_"+String.valueOf(userid)+"@"));

		whmcs.admin.client.summary.open(userid).pressLoginAsClient().waitLoaded();
		whmcs.client.home.isOpened().pressLoginProxyDashboard().waitLoaded();
		try {
			whmcs.oauth.isOpened().confirm().waitLoaded();
		} catch(WrongPageException ex) {
			//then, it was already accepted
		}
		
		whmcs.admin.client.summary.open(userid).deleteAccount().waitLoaded();
		
		try {
			whmcs.admin.clients.open().findClient(Config.env.email);
		} catch (WrongValueException ex) {
			// All correct; User not found, thus removed 
		}
	}
}
