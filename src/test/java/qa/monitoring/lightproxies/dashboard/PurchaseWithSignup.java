package qa.monitoring.lightproxies.dashboard;


import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNot.not;

import org.vitmush.qa.selenium_frame.base.SceneManager;
import org.vitmush.qa.selenium_frame.base.SceneManager.Scene;
import org.vitmush.qa.selenium_frame.exceptions.WrongPageException;

import com.blazingseollc.qa.base.Config;

public class PurchaseWithSignup {
  
	public void sleep() throws InterruptedException {
		sleep(5);
	}
	
	
	public void sleep(float secs) throws InterruptedException {
		Thread.sleep((long) (secs * 1000));
	}
  
  @Before
  public void setUp() {

  }
  
  @After
  public void tearDown() {
    SceneManager.quit();
  }
  
  @Test
  public void test() throws Exception {
	  Scene scene = SceneManager.add("chrome0", "chrome");
		
		com.blazingseollc.qa.env.lightproxies.ProxyDashboard dash = new com.blazingseollc.qa.env.lightproxies.
				ProxyDashboard(scene.tab("main"));
		
		dash.home.open().isOpened().pressSignup().isOpened().signUp(
				dash.synonyms.country.usa.listName, dash.synonyms.category.dedicated.listName, "10", Config.env.whmcs.promo_100,
				Config.env.email, Config.env.password, Config.env.password, 
				"VTest", "--", "00000", "BlazingSEO", "mein address", "mein city",
				"mein state", "000", "France");
		
		dash.tab().waitLoaded();
		
		for(int i = 0; i < 20; i++) {
			try {
				dash.locations.isOpened();
			} catch(WrongPageException ex) {
				//locations not opened yet
				dash.tab().reload();
				sleep(30);
				continue;
			}
			break;
		}
		
		if(! dash.locations.saveAllMixedOnFirst().up().checkout.open().isOpened()
			.hasPackage(dash.synonyms.country.usa.robotName, dash.synonyms.category.dedicated.robotName))
			throw new Exception("Purchased package not found");
  }
}
