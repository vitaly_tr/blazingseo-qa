package qa.monitoring.lightproxies.dashboard;

import com.blazingseollc.qa.tools.ProxyChecker;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import org.vitmush.qa.selenium_frame.base.SceneManager;
import org.vitmush.qa.selenium_frame.base.SceneManager.Scene;

import com.blazingseollc.qa.base.Config;

public class ProxiesConnectValidation {
  
	public void sleep() throws InterruptedException {
		sleep(5);
	}
	
	
	public void sleep(float secs) throws InterruptedException {
		Thread.sleep((long) (secs * 1000));
	}
  
	@Before
	public void setUp() {

	}
  
	@After
	public void tearDown() {
		SceneManager.quit();
	}
  
	@Test
	public void test() throws Exception {
		Scene scene = SceneManager.add("chrome0", "chrome");
		
		com.blazingseollc.qa.env.lightproxies.ProxyDashboard dash = new com.blazingseollc.qa.env.lightproxies
				.ProxyDashboard(scene.tab("main"));
		
		dash.login.open().signIn(Config.env.email, Config.env.password).waitLoaded();
		dash.dashboard.isOpened().openExportPackage(dash.synonyms.country.usa.packageName, dash.synonyms.category.dedicated.packageName);
		
		com.blazingseollc.qa.env.lightproxies.ProxyDashboard exp = new com.blazingseollc.qa.env.lightproxies
				.ProxyDashboard(scene.tab());
		
		String proxies = exp.dashboard.export.getList();
		int amount = 0;
		for(String s : proxies.split("\n")) {
			amount++;
		}
		int i = Math.round((float) Math.random() * (amount - 1) + 1);

		ProxyChecker.testIPv4Connection(proxies.split("\n")[i]);
		exp.tab().close();
		
		
		
		dash.dashboard.openExportPackage(dash.synonyms.country.usa.packageName, dash.synonyms.category.rotate.packageName);
		
		exp = new com.blazingseollc.qa.env.lightproxies.ProxyDashboard(scene.tab());
		
		proxies = exp.dashboard.export.getList();
		amount = 0;
		for(String s : proxies.split("\n")) {
			amount++;
		}
		i = Math.round((float) Math.random() * (amount - 1) + 1);

		ProxyChecker.testIPv4Connection(proxies.split("\n")[i]);
		exp.tab().close();
	}
}
