package qa.monitoring.lightproxies.dashboard;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import org.vitmush.qa.selenium_frame.base.SceneManager;
import org.vitmush.qa.selenium_frame.base.SceneManager.Scene;
import org.vitmush.qa.selenium_frame.exceptions.WrongPageException;

import com.blazingseollc.qa.base.Config;

public class LoginAndPurchase {

	public void sleep() throws InterruptedException {
		sleep(5);
	}
	
	
	public void sleep(float secs) throws InterruptedException {
		Thread.sleep((long) (secs * 1000));
	}
  
	@Before
	public void setUp() {

	}
  
	@After
	public void tearDown() {
		SceneManager.quit();
	}
  
	@Test
	public void test() throws Exception {
		Scene scene = SceneManager.add("chrome0", "chrome");

		com.blazingseollc.qa.env.lightproxies.ProxyDashboard dash = new com.blazingseollc.qa.env.lightproxies.
				ProxyDashboard(scene.tab("main"));
		
		dash.home.open().pressSignin().signIn(Config.env.email, Config.env.password);
		dash.tab().waitLoaded();
		boolean signed = false;
		try {
			dash.login.isOpened();
		} catch (WrongPageException ex) {
			signed = true;
		}
		if(!signed)
			throw new Exception("Couldn't sign in");
		
		com.blazingseollc.qa.env.lightproxies.Whmcs whmcs = new com.blazingseollc.qa.env.lightproxies.
				Whmcs(scene.tab("main"));
		try {
			whmcs.oauth.isOpened().confirm();
			whmcs.tab().waitLoaded();
		} catch(WrongPageException ex) {
			//then oauth was already confirmed
		}
		
		dash.sidebar.isFound().pressCheckout().isOpened().purchasePackage(
				dash.synonyms.country.usa.listName, dash.synonyms.category.rotate.listName, "5", Config.env.whmcs.promo_100);
		dash.tab().waitLoaded();
		dash.checkout.open();
		
		int i = 0;
		while(true) {
			if(dash.checkout.hasPackage(dash.synonyms.country.usa.robotName, dash.synonyms.category.rotate.robotName)) {
				break;
			} else if (i++ == 20) {
				throw new Exception("Purchased package not found");
			} else {
				dash.tab().reload();
				sleep(5);
			}
		}
		
		dash.dashboard.open().isOpened().setCurrentAuthIP().validateIpAdded();
	}
}
