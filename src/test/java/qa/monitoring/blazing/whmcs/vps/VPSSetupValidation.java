package qa.monitoring.blazing.whmcs.vps;


import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNot.not;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import java.util.*;

public class VPSSetupValidation {
  private WebDriver driver;
  private Map<String, Object> vars;
  JavascriptExecutor js;
  
	public void sleep() throws InterruptedException {
		sleep(5);
	}
	
	
	public void sleep(float secs) throws InterruptedException {
		Thread.sleep((long) (secs * 1000));
	}
  
  @Before
  public void setUp() {
    driver = new ChromeDriver();
    js = (JavascriptExecutor) driver;
    vars = new HashMap<String, Object>();
  }
  
  @After
  public void tearDown() {
    driver.quit();
  }
  
  public void test(String email) throws Exception {
	  driver.get("https://billing.blazingseollc.com/hosting/index.php");
	  sleep(5);
	  driver.findElement(By.linkText("Login")).click();
	  sleep(5);
	  driver.findElement(By.id("inputEmail")).sendKeys(email);
	  driver.findElement(By.id("inputPassword")).sendKeys("tester12");
	  driver.findElement(By.id("inputPassword")).sendKeys(Keys.ENTER);
	  sleep(10);
	  {
		  WebElement element = driver.findElement(By.linkText("Portal Home"));
		  Actions builder = new Actions(driver);
		  builder.moveToElement(element).perform();
		  }
	  driver.findElement(By.cssSelector(".col-sm-3:nth-child(1) .stat")).click();
	  sleep(5);
	  driver.findElement(By.cssSelector(".sorting_1")).click();
	  sleep(20);
	  driver.switchTo().frame("virtualizor_manager");
	  assertThat(driver.findElement(By.id("current_status_text")).getText(), is("Online"));
	  driver.switchTo().defaultContent();
	  driver.findElement(By.cssSelector("a.btn-danger")).click();
	  sleep(5);
	  driver.findElement(By.id("cancellationreason")).sendKeys("reason");
	  driver.findElement(By.cssSelector(".btn-danger")).click();
	  sleep(5);
	  driver.findElement(By.linkText("« Back")).click();
	  sleep(5);
	  {
	      List<WebElement> elements = driver.findElements(By.id("alertPendingCancellation"));
	      assert(elements.size() > 0);
	    }
  }
  
  @Test
  public void test1() throws Exception {
	  test("vtest_autoqa1@grr.la");
  }
  
  @Test
  public void test2() throws Exception {
	  test("vtest_autoqa2@grr.la");
  }
}
