package qa.monitoring.blazing.whmcs.vps;


import com.blazingseollc.qa.env.blazing.old.Whmcs;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import org.vitmush.qa.selenium_frame.base.SceneManager;
import org.vitmush.qa.selenium_frame.base.SceneManager.Scene;
import org.vitmush.qa.selenium_frame.exceptions.WrongPageException;
import org.vitmush.qa.selenium_frame.exceptions.WrongValueException;

import com.blazingseollc.qa.base.Config;

public class Cleanup {
  
	public void sleep() throws InterruptedException {
		sleep(5);
	}
	
	
	public void sleep(float secs) throws InterruptedException {
		Thread.sleep((long) (secs * 1000));
	}
  
	@BeforeAll
	public void setUp() {
	  
	}
  
	@AfterAll
	public void tearDown() {
		SceneManager.quit();
	}
  
	public void clean(String email) throws Exception {
		Scene scene = SceneManager.add("chrome0", "chrome");

		Whmcs whmcs = new Whmcs(scene.tab("main"), Config.env.whmcs.url_whmcs);
		
		int userid = whmcs.admin.authorize().clients.open().isOpened().findClient(email).getId();
		whmcs.admin.client.summary.openOrders().waitLoaded();
		
		try {
			int ordersTotal = whmcs.admin.client.orders.isOpened().getTotalOrders();
			if(ordersTotal > 15)
				throw new Exception("too much orders -"+ordersTotal+"- I'm not gonna delete it; needs additional validation step");
			whmcs.admin.client.orders.deleteAll().waitLoaded();
		} catch (WrongPageException ex) {
			whmcs.admin.client.order.delete().waitLoaded();
		}
		
		whmcs.admin.client.summary.open(userid).deleteAccount().waitLoaded();
		
		try {
			whmcs.admin.clients.open().findClient(email);
		} catch (WrongValueException ex) {
			// All correct; User not found, thus removed 
		}
	}
  
	@Test
	public void test1() throws Exception {
		clean("vtest_autoqa1@grr.la");
	}
  
	@Test
	public void test2() throws Exception {
		clean("vtest_autoqa2@grr.la");
	}
}
