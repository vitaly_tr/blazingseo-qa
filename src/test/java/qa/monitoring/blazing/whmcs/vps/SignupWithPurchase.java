package qa.monitoring.blazing.whmcs.vps;


import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNot.not;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.JavascriptExecutor;

import java.util.*;

public class SignupWithPurchase {
  private WebDriver driver;
  private Map<String, Object> vars;
  JavascriptExecutor js;
  
	public void sleep() throws InterruptedException {
		sleep(5);
	}
	
	
	public void sleep(float secs) throws InterruptedException {
		Thread.sleep((long) (secs * 1000));
	}
  
  @Before
  public void setUp() {
    driver = new ChromeDriver();
    js = (JavascriptExecutor) driver;
    vars = new HashMap<String, Object>();
  }
  
  @After
  public void tearDown() {
    driver.quit();
  }
  
  @Test
  public void test1() throws Exception {
    driver.get("https://billing.blazingseollc.com/hosting");
    driver.findElement(By.linkText("Store")).click();
    driver.findElement(By.linkText("Los Angeles VPS")).click();
    driver.findElement(By.id("product2-order-button")).click();
    driver.findElement(By.id("inputHostname")).sendKeys("autoqa_testhost");
    driver.findElement(By.cssSelector(".col-md-8")).click();
    driver.findElement(By.cssSelector(".row:nth-child(1) > .col-sm-6:nth-child(2) > .form-group")).click();
    driver.findElement(By.id("inputRootpw")).click();
    driver.findElement(By.id("inputRootpw")).sendKeys("AqWeTeStEr12");
    sleep(5);
    driver.findElement(By.id("btnCompleteProductConfig")).click();
    sleep(5);
    driver.findElement(By.id("inputPromotionCode")).sendKeys("RIMIUSPROMIUS100");
    driver.findElement(By.name("validatepromo")).click();
    sleep(5);
    driver.findElement(By.id("checkout")).click();
    sleep(5);
    driver.findElement(By.id("inputFirstName")).sendKeys("VTest");
    driver.findElement(By.id("inputLastName")).sendKeys("--");
    driver.findElement(By.id("inputEmail")).sendKeys("vtest_autoqa1@grr.la");
    driver.findElement(By.id("inputPhone")).sendKeys("456456");
    driver.findElement(By.id("inputAddress1")).sendKeys("asdgasdf");
    (new Select(driver.findElement(By.id("stateselect")))).selectByIndex(1);
    driver.findElement(By.id("inputCity")).sendKeys("asdgasdf");
    driver.findElement(By.id("inputPostcode")).sendKeys("456456");
    driver.findElement(By.id("customfield158569")).sendKeys("2000-10-10");
    driver.findElement(By.id("inputNewPassword1")).sendKeys("tester12");
    driver.findElement(By.id("inputNewPassword2")).sendKeys("tester12");
    (new Select(driver.findElement(By.id("inputSecurityQId")))).selectByIndex(1);
    driver.findElement(By.id("inputSecurityQAns")).sendKeys("tester12");
    for(WebElement el : driver.findElements(By.cssSelector("label.checkbox"))){
  		el.click();
	}
    driver.findElement(By.id("btnCompleteOrder")).click();
    sleep(10);
    driver.get("https://billing.blazingseollc.com/hosting");
    sleep(10);
    driver.findElement(By.cssSelector("#Primary_Navbar-Services > a")).click();
    sleep(1);
    driver.findElement(By.cssSelector("#Primary_Navbar-Services-My_Services > a")).click();
    sleep(5);
    assertThat(driver.findElement(By.cssSelector(".label")).getText(), is("Active"));
  }
}
