package qa.manual;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Nested;
import org.vitmush.qa.selenium_frame.base.Debug;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestClassOrder(ClassOrderer.OrderAnnotation.class)
public class TestTest {

    @BeforeAll
    public static void beforeAll(){
        Debug.Log("BEFORE ALL");
    }

    @AfterAll
    public static void afterAll(){
        Debug.Log("AFTER ALL");
    }

    @BeforeEach
    public void beforeEach(){
        Debug.Log("<");
    }

    @AfterEach
    public void afterEach(){
        Debug.Log(">\n");
    }

    @Test
    @Order(1)
    public void test1(){
        Debug.Log("#1");
    }

    @Test
    @Order(2)
    public void test2(){
        Debug.Log("#2");
    }

    @Nested
    @Order(1)
    @TestMethodOrder(MethodOrderer.OrderAnnotation.class)
    public class NestedTest0{

        @BeforeAll
        public static void beforeAll(){
            Debug.Log("NestedTest0::BEFORE ALL");
            value = 666;
        }

        @AfterAll
        public static void afterAll(){
            Debug.Log("NestedTest0::AFTER ALL");
        }

        @BeforeEach
        public void beforeEach(){
            Debug.Log("NestedTest0::<");
        }

        @AfterEach
        public void afterEach(){
            Debug.Log("NestedTest0::>");
        }

        static int value = 0;

        @Test
        @Order(1)
        public void test3(){
            Debug.Log("#0_3");
            Debug.Log("value: %d".formatted(value));
            value = 3;
        }

        @Test
        @Order(2)
        public void test4(){
            Debug.Log("#0_4");
            Debug.Log("value: %d".formatted(value));
            value = 4;
        }
    }

    @Nested
    @Order(3)
    public class NestedTest{
        @Test
        @Order(3)
        public void test3(){
            Debug.Log("#3");
        }

        @Test
        @Order(4)
        public void test4(){
            Debug.Log("#4");
        }
    }



    @Test
    @Order(5)
    public void test5(){
        Debug.Log("#5");
    }

}
