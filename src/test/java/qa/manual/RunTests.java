package qa.manual;

import com.blazingseollc.qa.abstracts.AbstractRebillyTest;
import com.blazingseollc.qa.base.Config;
import com.blazingseollc.qa.env.blazing.BDR;
import com.blazingseollc.qa.env.blazing.old.Whmcs;
import com.blazingseollc.qa.tools.DB;
import com.blazingseollc.qa.tools.Helper;
import com.blazingseollc.qa.base.ProxyType;
import com.jcraft.jsch.JSchException;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.vitmush.qa.selenium_frame.exceptions.*;
import qa.rebilly.functionality.ResidentialProxyTests;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import java.util.stream.IntStream;

public class RunTests extends AbstractRebillyTest {


    //TODO create ability to specify card and additional parameters on account creation

    @Nested
    public class Dummy {
        @Test
        public void CreateDummyIPv4Account() {
            test(() -> {
                var email = Helper.generateUniqueEmail();
                Dashboard.signup.open().doCreateAccount(new ProxyType(
                        ProxyType.ProxyCategory.Dedicated, ProxyType.ProxyCountry.USA,
                                ProxyType.BillingCycle.Quarterly), 10, email);
            });
        }

        @Test
        public void CreateDummyISPRotatingAccount() throws ConditionTimeoutException, PageTimeoutException, WebFormErrorException, NoSuchElementException, WrongPageException {
            test(() -> {
                Dashboard.signup.open().doCreateAccount(
                        ProxyType.ISPRotatingPackage.Starter, Helper.generateUniqueEmail());
            });
        }
    }



    @Nested
    public class CreatePackage {

        @Test
        public void CreateIPv4Package() {
            test(() -> {
                Dashboard.quick.signupWithPurchase(
                        new ProxyType(ProxyType.ProxyCategory.Semi, ProxyType.ProxyCountry.USA,
                                ProxyType.BillingCycle.Monthly), 10, "vitaly.troshuk+20220531_172355_2@sprious.com");
            });
        }

        @Test
        public void CreateIPv6Package() {
            test(() -> {
                Dashboard.quick.signupWithPurchase(
                        new ProxyType(ProxyType.ProxyCategory.IPv6, ProxyType.ProxyCountry.USA,
                                ProxyType.BillingCycle.Semi_Annually),
                        10);
            });
        }

        @Test
        public void CreateNIPv4Packages() throws InvocationTargetException, URLIsImproper, NoSuchMethodException, InstantiationException, IllegalAccessException {
            int amountToCreate = 2;
            for(var i : IntStream.range(0, amountToCreate).toArray()){
                CreateIPv4Package();
                refreshScene();
            }
        }

        @Test
        public void CreateISPRotatingPackage() {
            test(() -> {
                Dashboard.quick.signupWithPurchase(ProxyType.ISPRotatingPackage.Starter);
            });
        }


        @Test
        public void CreateISPRotatingTrial(){
            test(() -> {
                String email = "vitaly.troshuk+20220426_220708@sprious.com";
                if(email.isEmpty())
                    email = Helper.generateUniqueEmail();
                Omad.quick.createTrial(email, 0.5f, 2, 3);
            });
        }

    }

    @Nested
    public class CreateWithQuery{
        @Test
        public void CreateIPv4WithReferral() {
            test(() -> {
                Dashboard.quick.signupWithReferral(
                        new ProxyType(ProxyType.ProxyCategory.Semi, ProxyType.ProxyCountry.USA,
                                ProxyType.BillingCycle.Quarterly),
                        5, "blz-d3523");
            });
        }

        @Test
        public void CreateISPRotatingWithReferral() {
            test(() -> {
                Dashboard.quick.signupWithReferral(ProxyType.ISPRotatingPackage.Starter, "blz-d1420");
            });
        }


        @Test
        public void CreateSneakerStaticResidential() {
            test(() -> {
                Dashboard.quick.signupWithArguments(
                        new ProxyType(ProxyType.ProxyCategory.Sneaker_ISP, ProxyType.ProxyCountry.USA,
                                ProxyType.BillingCycle.Monthly),
                        5, "proposal=best");
            });
        }
    }

    @Nested
    public class DashboardV1 {

        @Test
        public void CreateDummyAccount(){
            test(() -> {
                String email = Helper.generateUniqueEmail();
                new BDR().quick.createAccount(email, new ProxyType(ProxyType.ProxyCategory.Dedicated , ProxyType.ProxyCountry.USA));
            });
        }

        @Test
        public void CreateIPv4PackageOnBDR(){
            test(() -> {
                String email = Helper.generateUniqueEmail();
                new BDR().quick.createAccountWithProduct(
                        new ProxyType(ProxyType.ProxyCategory.Dedicated , ProxyType.ProxyCountry.USA), 10, email);
            });
        }

        @Test
        public void CreateSneakerStaticResidentialOnBDR() {
            test(() -> {
                new BDR().quick.signupWithArguments(
                        new ProxyType(ProxyType.ProxyCategory.Sneaker_ISP, ProxyType.ProxyCountry.USA,
                                ProxyType.BillingCycle.Monthly),
                        5, "proposal=best");
            });
        }

    }


    @Nested
    public class CreateWithFailCard {
        @Test
        public void CreateIPv4PackageWithFailCard() {
            test(() -> {
                var type = new ProxyType(ProxyType.ProxyCategory.Dedicated, ProxyType.ProxyCountry.USA,
                        ProxyType.BillingCycle.Monthly);
                Dashboard.signup.open().doCreateAccount(type, 10, Helper.generateUniqueEmail())
                        .makeCardPayment("Vitaly", "---", "vitaly.troshuk@sprious.com",
                                "4000000000000010", "1070", "123");
            });
        }

        @Test
        public void CreateISPRotatingPackageWithFailCard() {
            test(() -> {
                Dashboard.signup.open().doCreateAccount(ProxyType.ISPRotatingPackage.Starter, Helper.generateUniqueEmail())
                        .makeCardPayment("Vitaly", "---", "vitaly.troshuk@sprious.com",
                                "4000000000000010", "1070", "123");
            });
        }
    }


    @Test
    public void CreateBunchOfIPv4PackageOnBDR(){
        var types = List.of(
                new ProxyType(ProxyType.ProxyCategory.ISP, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Monthly),
                new ProxyType(ProxyType.ProxyCategory.ISP, ProxyType.ProxyCountry.Germany, ProxyType.BillingCycle.Monthly),
                new ProxyType(ProxyType.ProxyCategory.ISP, ProxyType.ProxyCountry.UK, ProxyType.BillingCycle.Monthly)
        );

        step("Create ISP packages on BDR");

        for(var type : types) {
            test("Signup with %s".formatted(type.toString()), () -> {
                String email = Helper.generateUniqueEmail();
                new BDR().quick.createAccountWithProduct(type, 7, email);
                refreshScene();
            }, true);
        }

        step("Create ISP packages on V2");

        for(var type : types) {
            test("Signup with %s".formatted(type.toString()), () -> {
                Dashboard.quick.signupWithPurchase(type, 7);
                refreshScene();
            }, true);
        }

    }


    @Test
    public void MakeInvoicePaid(){
        test(() -> {
            String invoiceUrl = "https://sandbox-portal.secure-payments.app/payment/?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJqdGkiOiIwZDMwN2IyNi1mOTk5LTQ5NWEtOGRmNS1jMTU1YTZhMTBlYjUiLCJleHAiOjE2Nzg5MTIzMzQsImlhdCI6MTY0NzM3NjMzNS42MTcwOSwiYWNsIjpbeyJzY29wZSI6eyJvcmdhbml6YXRpb25JZCI6WyI0MDg2YmJmNC1iZmRkLTQ5Y2QtYWQ0Ny0yMzExYWNlMzRhMjYiXSwiaW52b2ljZUlkIjpbIjA0MDVmMWQ2LTdkYTAtNDBjZC1iYjEyLWI2Y2NjOGMxY2ZjYSJdLCJjdXN0b21GaWVsZE5hbWUiOlsiU0lOTGFzdDQiXX0sInBlcm1pc3Npb25zIjpbMjg0LDQxNCw0MTUsNDM0LDQxMiw0MTMsNDI0LDQyNSw0MjYsNDI3LDQyOCw0MjksNDA5LDQxMCw0MDEsNDAyLDQzM119XSwiY2xhaW1zIjp7IndlYnNpdGVJZCI6ImRldi1zcHJpb3VzIiwiaW52b2ljZUlkIjoiMDQwNWYxZDYtN2RhMC00MGNkLWJiMTItYjZjY2M4YzFjZmNhIiwicGF5bWVudE1ldGhvZHMiOltdfSwibWVyY2hhbnQiOiI0MDg2YmJmNC1iZmRkLTQ5Y2QtYWQ0Ny0yMzExYWNlMzRhMjYiLCJjdXN0b21lciI6eyJpZCI6ImFhODhhMzVmLTkyMWUtNDk3YS1iNWRhLThhMWViODY3MjkxNCIsIm5hbWUiOiJ2aXRhbHkudHJvc2h1ay4yMDIyMDMxNV8xOTI1MTEgLS0tIiwiY3JlYXRlZFRpbWUiOiIyMDIyLTAzLTE1VDE3OjI1OjUyKzAwOjAwIn19.PUCG__1Yru9LiXhLaldjuQwm70DpQGCLEziTcADyCjqpMBa9CPwokL-jUj_ReC4tkRimQ1wLf6_UXBKLubOv5eeDzprlhT47inlHMoovtnFvsGeatUFh7Y7dthFbcczzZxeAprU27JWzuk1cjsR8b3E6iZUc8aW5CyAg1kNMdvpgk00TTKPWglPQU4U-8ooJnTo1JUesf3I7UL14Jm3RkEhmuLxRRx8V0eAxE5Nm-QYO5WmYtIeuKVNrbRLt1DuuW-sQf0HBxroamsqJtC3gMCZHaBx3FF2Z0xMVPIU72u4aIVLY_hTZ79q-E507f0W6ZB7plGl0IuVpabWTyOWbbA";
            Dashboard.tab().get(invoiceUrl);
            Dashboard.invoice.makeCardPayment();
        });
    }



    @Test
    public void test(){
        var countries = List.of(ProxyType.ProxyCountry.USA,  ProxyType.ProxyCountry.UK, ProxyType.ProxyCountry.Germany);

        assertEquals("Plan name", "US ISP Proxies", "%s %s".formatted(ProxyType.AliasCollection.rebilly.countryName.of(ProxyType.ProxyCountry.USA), ProxyType.AliasCollection.rebilly.categoryName.of(ProxyType.ProxyCategory.ISP)));
        for(var country : countries) {
            test(() -> {
                step("Purchase package");
                int amount = 6;
                Dashboard.quick.signupWithPurchase(new ProxyType(ProxyType.ProxyCategory.ISP, country), amount);
                String email = Helper.lastGeneratedUniqueEmail;
                var subscription = Rebilly.quick.getActiveSubscriptions(email).get(0);
                step("Validate proper package purchased");
                assertEquals("Plan name", subscription.plan, "%s %s".formatted(ProxyType.AliasCollection.rebilly.countryName.of(country), ProxyType.AliasCollection.rebilly.categoryName.of(ProxyType.ProxyCategory.ISP)));
                assertEquals("Amount", subscription.quantity, amount);
                refreshScene();
            }, true);
        }
    }

    @Test
    public void TestMultiple() {

        test(() -> {
            Dashboard.signin.open().login("vitaly.troshuk+20220131_212711@sprious.com");
            Dashboard.quick.confirmTFA();
            Dashboard.purchase.open().fillForm(new ProxyType(ProxyType.ProxyCategory.Rotating, ProxyType.ProxyCountry.Germany, ProxyType.BillingCycle.Semi_Annually), 10);
            Dashboard.waitUserAction();
        });
    }

    @Test
    public void MultiplePurchaseForHubspotDowngradeTestCase() {
        int amount = 10;
        var types = List.of(
                new ProxyType(ProxyType.ProxyCategory.Semi, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Quarterly),
                new ProxyType(ProxyType.ProxyCategory.Rotating, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Semi_Annually)/*,
                new ProxyType(ProxyType.ProxyCategory.Shopify, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Annually),
                new ProxyType(ProxyType.ProxyCategory.ISP, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Semi_Annually),
                new ProxyType(ProxyType.ProxyCategory.Dedicated, ProxyType.ProxyCountry.Germany, ProxyType.BillingCycle.Annually),
                new ProxyType(ProxyType.ProxyCategory.Rotating, ProxyType.ProxyCountry.Brazil, ProxyType.BillingCycle.Quarterly),
                new ProxyType(ProxyType.ProxyCategory.Semi, ProxyType.ProxyCountry.Germany, ProxyType.BillingCycle.Semi_Annually)*/
        );

        test("Create account", () -> {
            Dashboard.quick.signupWithPurchase(
                    new ProxyType(ProxyType.ProxyCategory.Dedicated), amount);
            Dashboard.quick.redirectToDashboardFromSignup().confirmTFA();
            //Dashboard.quick.setAllLocationsToMixed(true);
        });

        for(var type : types){
            test("Purchase %s:".formatted(type.toString()), () -> {

                Dashboard.purchase.open().purchase(type, amount).makeCardPayment();
                //Dashboard.quick.setAllLocationsToMixed(true);
            }, true);
        }
    }


    @Test
    @Order(1)
    public void TestProperPackagesCreationMultiple() {
        int amount = 6;
        var types = List.of(
                new ProxyType(ProxyType.ProxyCategory.Shopify, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Semi_Annually)
                /*new ProxyType(ProxyType.ProxyCategory.ISP, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Monthly),
                new ProxyType(ProxyType.ProxyCategory.Rotating, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Annually),
                new ProxyType(ProxyType.ProxyCategory.Dedicated, ProxyType.ProxyCountry.Brazil, ProxyType.BillingCycle.Semi_Annually),
                new ProxyType(ProxyType.ProxyCategory.Dedicated, ProxyType.ProxyCountry.Germany, ProxyType.BillingCycle.Quarterly),
                new ProxyType(ProxyType.ProxyCategory.Rotating, ProxyType.ProxyCountry.France, ProxyType.BillingCycle.Quarterly),
                new ProxyType(ProxyType.ProxyCategory.Rotating, ProxyType.ProxyCountry.Brazil, ProxyType.BillingCycle.Monthly),
                new ProxyType(ProxyType.ProxyCategory.Rotating, ProxyType.ProxyCountry.Germany, ProxyType.BillingCycle.Monthly),
                new ProxyType(ProxyType.ProxyCategory.Semi, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Annually),
                new ProxyType(ProxyType.ProxyCategory.Semi, ProxyType.ProxyCountry.Brazil, ProxyType.BillingCycle.Quarterly),
                new ProxyType(ProxyType.ProxyCategory.Semi, ProxyType.ProxyCountry.Germany, ProxyType.BillingCycle.Semi_Annually),
                new ProxyType(ProxyType.ProxyCategory.Semi, ProxyType.ProxyCountry.France, ProxyType.BillingCycle.Monthly),
                new ProxyType(ProxyType.ProxyCategory.Semi, ProxyType.ProxyCountry.UK, ProxyType.BillingCycle.Monthly),
                new ProxyType(ProxyType.ProxyCategory.Dedicated, ProxyType.ProxyCountry.Japan, ProxyType.BillingCycle.Monthly),
                new ProxyType(ProxyType.ProxyCategory.Dedicated, ProxyType.ProxyCountry.Canada, ProxyType.BillingCycle.Quarterly),
                new ProxyType(ProxyType.ProxyCategory.Dedicated, ProxyType.ProxyCountry.Poland, ProxyType.BillingCycle.Semi_Annually),
                new ProxyType(ProxyType.ProxyCategory.Dedicated, ProxyType.ProxyCountry.Mexico, ProxyType.BillingCycle.Monthly),
                new ProxyType(ProxyType.ProxyCategory.Dedicated, ProxyType.ProxyCountry.Belgium, ProxyType.BillingCycle.Quarterly),
                new ProxyType(ProxyType.ProxyCategory.Dedicated, ProxyType.ProxyCountry.India, ProxyType.BillingCycle.Quarterly)*/
                );

        for(var type : types) {
            var country = ProxyType.AliasCollection.rebilly.countryName.of(type.country);
            var category = ProxyType.AliasCollection.rebilly.categoryName.of(type.category);
            var billingCycle = ProxyType.AliasCollection.rebilly.planBillingCycleName.of(type.billingCycle);
            var billingCycleMonths = ProxyType.AliasCollection.common.billingCycleMonthCount.of(type.billingCycle);
            test(("%s %s %s").formatted(country, category, billingCycle), () -> {
                var email = Helper.generateUniqueEmail();
                var password = Config.env.password;
                if(type.category == ProxyType.ProxyCategory.ISP_Rotating)
                    Dashboard.signup.open().doFillRequiredFields(ProxyType.ISPRotatingPackage.Starter, email, password);
                else
                    Dashboard.signup.open().doFillRequiredFields(type, amount, email, password);
                var total = Dashboard.signup.getTotal();
                Dashboard.signup.clickSubmit();

                var invoice = Rebilly.quick.openUser(email).getUnpaidInvoices().get(0);
                assertEquals("Country of invoice is expected", invoice.get("country"), country);
                assertEquals("Category of invoice is expected", invoice.get("category"), category);
                assertEquals("Total of invoice is expected", Float.parseFloat(invoice.get("amount")), total);

                Rebilly.invoice.open(invoice.get("id")).addMinimalPayment();

                var subscription = Rebilly.quick.getActiveSubscriptions(email).get(0);
                assertEquals("BillingCycle of subscription is expected", subscription.cycle, billingCycleMonths);


                var p = DB.quick.proxy_backend.getIPv4Packages(email).get(0);
                assertEquals("Country of package is expected",
                        p.get("country"), ProxyType.AliasCollection.common.countryCode.of(type.country));
                assertEquals("Category of package is expected",
                        p.get("category"), ProxyType.AliasCollection.common.categoryCode.of(type.category));
                assertEquals("Amount of package is expected",
                        Integer.valueOf(p.get("amount")), amount);

                Rebilly.subscription.open(subscription.id).cancelOrder();

                refreshScene();
            }, true);

        }
    }

    @Test
    @Order(2)
    public void test2() throws PageTimeoutException, TabAlreadySavedException, InterruptedException, NoSuchElementException, URLIsImproper, WrongPageException, JSchException, SQLException, ConditionTimeoutException, ParseException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        //AdminTrialTests.single().AccessToTrialProxiesGotSuspendedOnDownload();
        //AdminTrialTests.single().AccessToTrialProxiesKeepsSuspendedAfterRenewalInvoiceGenerated();
        //AdminTrialTests.single().AccessToTrialProxiesGotRestoredOnPaymentAfterDownloadSuspend();
    }

    @Test
    @Order(3)
    public void test3() throws PageTimeoutException, InterruptedException, NoSuchElementException, URLIsImproper, WrongPageException, JSchException, SQLException, ConditionTimeoutException, ParseException, ClassNotFoundException, WebFormErrorException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        //AdminTrialTests.single().TestCommonProductsCreated();
    }

    @Test
    @Order(4)
    public void test4() throws PageTimeoutException, InterruptedException, NoSuchElementException, URLIsImproper, WrongPageException, JSchException, SQLException, ConditionTimeoutException, ClassNotFoundException, WebFormErrorException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        ResidentialProxyTests.single().AdditionalBandwidthOnSignupCreatesAccessToProxies();
    }


    @Test
    public void testPurchase() throws PageTimeoutException, NoSuchElementException, WrongPageException, ConditionTimeoutException, URLIsImproper {
        Dashboard.signin.open().login("vitaly.troshuk+20220416_214546@sprious.com");
        Dashboard.purchase.open().purchase(
                new ProxyType(ProxyType.ProxyCategory.Semi, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Monthly),
                10).makeCardPayment();
        Dashboard.quick.setAllLocationsToMixed(true);

    }

    @Test
    public void testErrorOnUpgradeInvoiceGeneration2(){
        testErrorOnUpgradeInvoiceGeneration();
    }

    @Test
    public void testErrorOnUpgradeInvoiceGeneration(){
        test("Generate user", () -> {
           new CreatePackage().CreateIPv4Package();
           Dashboard.quick.confirmTFA();
        });

        String email = Helper.lastGeneratedUniqueEmail;

        int iterationsLeft = 10;
        while(iterationsLeft-- > 0){
            test("Fill Locations", () -> {
                DB.getSession(DB.Host.Proxy_Backend).update(
                        "UPDATE user_ports up join proxy_users pu on pu.id = up.user_id "
                        + "set region_id = 1 "
                        + "where email = '%s' ".formatted(email));
                //Dashboard.quick.setAllLocationsToMixed(true);
            });

            test("Test Upgrade (left - %d)".formatted(iterationsLeft), () -> {
                Dashboard.ipv4.packages.open().clickUpgrade().assertPageOpened();
            });

            test("Cancel subscription", () -> {
                var subsriptionId = Rebilly.quick.openUser(email)
                        .getActiveSubscriptions().get(0).id;
                Rebilly.subscription.open(subsriptionId).cancelOrder();
            });

            test("Purchase new test package", () -> {
                Dashboard.purchase.open().purchase(
                        new ProxyType(ProxyType.ProxyCategory.Dedicated, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Monthly),
                        10).makeCardPayment();
            });
        }
    }

}
