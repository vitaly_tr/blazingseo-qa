package qa.api.proxy_manager;

import com.blazingseollc.qa.abstracts.AbstractAnyTest;
import com.blazingseollc.qa.env.rayobyte.api.ProxyManager;
import com.blazingseollc.qa.tools.DB;
import com.jcraft.jsch.JSchException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.vitmush.qa.selenium_frame.base.Debug;

import java.sql.SQLException;
import java.util.List;
import java.util.stream.Stream;

import static org.hamcrest.Matchers.equalTo;

public class IPs extends AbstractAnyTest {

    //TODO check that all IDs are passed as Integer into api methods (except some validation tests)
    //TODO add more comments
    //TODO add better description, you should be able to understand the issue when it happens from reading logs

    //TODO test for empty input

    @Test
    @Order(1)
    @DisplayName("Single IP allow")
    void singleIPAllow() throws JSchException, SQLException, ClassNotFoundException {
        var id = DB.quick.proxy_backend.getRandomProxies(1).get(0);
        Debug.Preserve("Testing 'assign' on ip: %s(%s)".formatted(id.get("id"), id.get("assign")), true);
        //Prepare row in DB, since IP will be allowed, right now I need to to make ip not assigned
        if(id.get("assign").equals("1")){
            DB.quick.proxy_backend.updateProxies("assign = 0", List.of(id.get("id")));
        }

        test("Call API with assignable = true", () -> {
            ProxyManager.ips(List.of(id.get("id")), true).
                    then().assertThat().
                    spec(ProxyManager.successIPsSpec).
                    body("data.updatedCount", equalTo(1));

            assertEquals("Validate that 'assign' property is changed in DBB",
                    DB.quick.proxy_backend.getProxyInfo(List.of(id.get("id"))).get(0).get("assign"), "1");
        }, true);

        test("Fallback DB", () -> {
            DB.quick.proxy_backend.updateProxies("assign = %s".formatted(id.get("assign")), List.of(id.get("id")));
        });
    }

    @Test
    @Order(1)
    @DisplayName("Multiple IPs allow")
    void multipleIPsAllow() throws JSchException, SQLException, ClassNotFoundException {
        var ids = DB.quick.proxy_backend.getRandomProxies(5);
        var idsList = ids.stream().map((var id) -> Integer.valueOf(id.get("id"))).toList();
        Debug.Preserve("Testing 'assign' on ips: " +
                        ids.stream().map((var m) -> "%s(%s)".formatted(m.get("id"), m.get("assign"))).toList().toString(),
                true);
        //Prepare row in DB, since IP will be allowed, right now I need to to make ip not assigned
        DB.quick.proxy_backend.updateProxies("assign = 0", idsList);

        test("Call API with assignable = true", () -> {
            ProxyManager.ips(idsList, true).
                    then().assertThat().
                    spec(ProxyManager.successIPsSpec).
                    body("data.updatedCount", equalTo(5));

            for(var id : idsList) {
                assertEquals("Validate that 'assign' property is changed in DB (id - %s)".formatted(id),
                        DB.quick.proxy_backend.getProxyInfo(List.of(id)).get(0).get("assign"), "1");
            }
        }, true);

        test("Fallback DB", () -> {
            for(var id : ids) {
                DB.quick.proxy_backend.updateProxies("assign = %s".formatted(id.get("assign")), List.of(id.get("id")));
            }
        });
    }

    @Test
    @Order(1)
    @DisplayName("Multiple IPs allow with invalid IPs in request")
    void multipleIPsAllowWithInvalidInRequest() throws JSchException, SQLException, ClassNotFoundException {
        var ids = DB.quick.proxy_backend.getRandomProxies(5);
        var idsList = ids.stream().map((var id) -> Integer.valueOf(id.get("id"))).toList();
        var idsListToRequest = Stream.concat(
                Stream.concat(List.of(2147483645).stream(), idsList.stream()),
                List.of(2147483646, 2147483647).stream()).toList();

        Debug.Preserve("Testing 'assign' on ips: " +
                        ids.stream().map((var m) -> "%s(%s)".formatted(m.get("id"), m.get("assign"))).toList().toString(),
                true);
        //Prepare row in DB, since IP will be allowed, right now I need to to make ip not assigned
        DB.quick.proxy_backend.updateProxies("assign = 0", idsList);

        test("Call API with assignable = true", () -> {
            ProxyManager.ips(idsListToRequest, true).
                    then().assertThat().
                    spec(ProxyManager.successIPsSpec).
                    body("data.updatedCount", equalTo(5));

            for(var id : idsList) {
                assertEquals("Validate that 'assign' property is changed in DB (id - %s)".formatted(id),
                        DB.quick.proxy_backend.getProxyInfo(List.of(id)).get(0).get("assign"), "1");
            }
        }, true);

        test("Fallback DB", () -> {
            for(var id : ids) {
                DB.quick.proxy_backend.updateProxies("assign = %s".formatted(id.get("assign")), List.of(id.get("id")));
            }
        });
    }

    @Test
    @Order(1)
    @DisplayName("Single IP disallow")
    void singleIPsDisallow() throws JSchException, SQLException, ClassNotFoundException {
        var id = DB.quick.proxy_backend.getRandomProxies(1).get(0);
        Debug.Preserve("Testing 'assign' on ip: %s(%s)".formatted(id.get("id"), id.get("assign")), true);
        //Prepare row in DB, since IP will be allowed, right now I need to to make ip not assigned
        if(id.get("assign").equals("0")){
            DB.quick.proxy_backend.updateProxies("assign = 1", List.of(id.get("id")));
        }

        test("Call API with assignable = false", () -> {
            ProxyManager.ips(List.of(id.get("id")), false).
                    then().assertThat().
                    spec(ProxyManager.successIPsSpec).
                    body("data.updatedCount", equalTo(1));

            assertEquals("Validate that 'assign' property is changed in DB",
                    DB.quick.proxy_backend.getProxyInfo(List.of(id.get("id"))).get(0).get("assign"), "0");
        }, true);

        test("Fallback DB", () -> {
            DB.quick.proxy_backend.updateProxies("assign = %s".formatted(id.get("assign")), List.of(id.get("id")));
        });
    }

    @Test
    @Order(1)
    @DisplayName("Multiple IPs Disallow")
    void multipleIPsDisallow() throws JSchException, SQLException, ClassNotFoundException {
        var ids = DB.quick.proxy_backend.getRandomProxies(5);
        var idsList = ids.stream().map((var id) -> Integer.valueOf(id.get("id"))).toList();
        Debug.Preserve("Testing 'assign' on ips: " +
                        ids.stream().map((var m) -> "%s(%s)".formatted(m.get("id"), m.get("assign"))).toList().toString(),
                true);
        //Prepare row in DB, since IP will be allowed, right now I need to to make ip not assigned
        DB.quick.proxy_backend.updateProxies("assign = 1", idsList);

        test("Call API with assignable = false", () -> {
            ProxyManager.ips(idsList, false).
                    then().assertThat().
                    spec(ProxyManager.successIPsSpec).
                    body("data.updatedCount", equalTo(5));

            for(var id : idsList) {
                assertEquals("Validate that 'assign' property is changed in DB (id - %s)".formatted(id),
                        DB.quick.proxy_backend.getProxyInfo(List.of(id)).get(0).get("assign"), "0");
            }
        }, true);

        test("Fallback DB", () -> {
            for(var id : ids) {
                DB.quick.proxy_backend.updateProxies("assign = %s".formatted(id.get("assign")), List.of(id.get("id")));
            }
        });
    }

    @Test
    @Order(1)
    @DisplayName("Multiple IPs Disallow with invalid IPs in request")
    void multipleIPsDisallowWithInvalidInRequest() throws JSchException, SQLException, ClassNotFoundException {
        var ids = DB.quick.proxy_backend.getRandomProxies(5);
        var idsList = ids.stream().map((var id) -> Integer.valueOf(id.get("id"))).toList();
        var idsListToRequest = Stream.concat(
                Stream.concat(List.of(2147483645).stream(), idsList.stream()),
                List.of(2147483646, 2147483647).stream()).toList();

        Debug.Preserve("Testing 'assign' on ips: " +
                        ids.stream().map((var m) -> "%s(%s)".formatted(m.get("id"), m.get("assign"))).toList().toString(),
                true);
        //Prepare row in DB, since IP will be allowed, right now I need to to make ip not assigned
        DB.quick.proxy_backend.updateProxies("assign = 1", idsList);

        test("Call API with assignable = false", () -> {
            ProxyManager.ips(idsListToRequest, false).
                    then().assertThat().
                    spec(ProxyManager.successIPsSpec).
                    body("data.updatedCount", equalTo(5));

            for(var id : idsList) {
                assertEquals("Validate that 'assign' property is changed in DB (id - %s)".formatted(id),
                        DB.quick.proxy_backend.getProxyInfo(List.of(id)).get(0).get("assign"), "0");
            }
        }, true);

        test("Fallback DB", () -> {
            for(var id : ids) {
                DB.quick.proxy_backend.updateProxies("assign = %s".formatted(id.get("assign")), List.of(id.get("id")));
            }
        });
    }


    @Nested
    @Order(2)
    class ValidationTests {
        @Test
        @DisplayName("Invalid input on single ID Allow tests")
        void invalidInputOnSingleIDAllowTests () {

            test("Try to set assignable = true for ID that does not exist in DB", () -> {
                //Search int max, definitely there's no such id in DB
                int id = 2147483647;
                ProxyManager.ips(List.of(id), true).then().assertThat().
                        spec(ProxyManager.invalidInputSpec("id"));
            }, true);

            test("Try to set assignable = true for ID = 0", () -> {
                int id = 0;
                ProxyManager.ips(List.of(id), true).then().assertThat().
                        spec(ProxyManager.invalidInputSpec("id"));
            }, true);

            test("Try to set assignable = true for ID = -1", () -> {
                int id = -1;
                ProxyManager.ips(List.of(id), true).then().assertThat().
                        spec(ProxyManager.invalidInputSpec("id"));
            }, true);

            test("Try to set assignable = true for ID = 'hello'", () -> {
                String id = "hello";
                ProxyManager.ips(List.of(id), true).then().assertThat().
                        spec(ProxyManager.invalidInputSpec("id")).
                        body("data.id.size()", equalTo(2));
            }, true);
        }

        @Test
        @DisplayName("Invalid input on multiple IDs Allow tests")
        void invalidInputOnMultipleIDAllowTests () throws JSchException, SQLException, ClassNotFoundException {

            var id = DB.quick.proxy_backend.getRandomProxies(1).get(0);
            int validID = Integer.parseInt(id.get("id"));
            Debug.Preserve("Testing 'assign' on ip: %s(%s)".formatted(id.get("id"), id.get("assign")), true);
            //Prepare row in DB, since IP will be allowed, right now I need to to make ip not assigned
            if(id.get("assign").equals("1")){
                DB.quick.proxy_backend.updateProxies("assign = 0", List.of(id.get("id")));
            }

            test("Set assignable = true for 2 invalid IDs = [-1, -2]", () -> {
                var ids = List.of(-1, -2);
                ProxyManager.ips(ids, true).then().assertThat().
                        spec(ProxyManager.invalidInputSpec("id"));
            }, true);

            test("Set assignable = true for valid ID and ID that does not exist in DB", () -> {
                //Search valid ID and ID with int max, definitely there's no such id in DB
                var ids = List.of(validID, 2147483647);
                ProxyManager.ips(ids, true).then().assertThat().
                        spec(ProxyManager.successIPsSpec).
                        body("data.updatedCount", equalTo(1));
            }, true);

            test("Set assignable = true for valid ID and ID = 0", () -> {
                var ids = List.of(validID, 0);
                ProxyManager.ips(ids, true).then().assertThat().
                        spec(ProxyManager.successIPsSpec).
                        body("data.updatedCount", equalTo(1));
            }, true);

            test("Set assignable = true for valid ID and ID = -1", () -> {
                var ids = List.of(validID, -1);
                ProxyManager.ips(ids, true).then().assertThat().
                        spec(ProxyManager.successIPsSpec).
                        body("data.updatedCount", equalTo(1));
            }, true);

            test("Set assignable = true for valid ID and ID = 'hello'", () -> {
                var ids = List.of(String.valueOf(validID), "hello");
                ProxyManager.ips(ids, true).then().assertThat().
                        spec(ProxyManager.invalidInputSpec("id"));
            }, true);

            test("Fallback DB", () -> {
                DB.quick.proxy_backend.updateProxies("assign = %s".formatted(id.get("assign")), List.of(id.get("id")));
            });
        }

        @Test
        @DisplayName("Invalid input on multiple IDs Disallow tests")
        void invalidInputOnMultipleIDDisallowTests () throws JSchException, SQLException, ClassNotFoundException {

            var id = DB.quick.proxy_backend.getRandomProxies(1).get(0);
            int validID = Integer.parseInt(id.get("id"));
            Debug.Preserve("Testing 'assign' on ip: %s(%s)".formatted(id.get("id"), id.get("assign")), true);
            //Prepare row in DB, since IP will be allowed, right now I need to to make ip not assigned
            if(id.get("assign").equals("1")){
                DB.quick.proxy_backend.updateProxies("assign = 0", List.of(id.get("id")));
            }

            test("Set assignable = false for 2 invalid IDs = [-1, -2]", () -> {
                var ids = List.of(-1, -2);
                ProxyManager.ips(ids, false).then().assertThat().
                        log().body().
                        spec(ProxyManager.invalidInputSpec("id"));
            }, true);

            test("Set assignable = false for valid ID and ID that does not exist in DB", () -> {
                //Search valid ID and ID with int max, definitely there's no such id in DB
                var ids = List.of(validID, 2147483647);
                ProxyManager.ips(ids, false).then().assertThat().
                        spec(ProxyManager.successIPsSpec).
                        body("data.updatedCount", equalTo(1));
            }, true);

            test("Set assignable = false for valid ID and ID = 0", () -> {
                var ids = List.of(validID, 0);
                ProxyManager.ips(ids, false).then().assertThat().
                        spec(ProxyManager.successIPsSpec).
                        body("data.updatedCount", equalTo(1));
            }, true);

            test("Set assignable = false for valid ID and ID = -1", () -> {
                var ids = List.of(validID, -1);
                ProxyManager.ips(ids, false).then().assertThat().
                        spec(ProxyManager.successIPsSpec).
                        body("data.updatedCount", equalTo(1));
            }, true);

            test("Set assignable = false for valid ID and ID = 'hello'", () -> {
                var ids = List.of(String.valueOf(validID), "hello");
                ProxyManager.ips(ids, false).then().assertThat().
                        spec(ProxyManager.invalidInputSpec("id"));
            }, true);

            test("Fallback DB", () -> {
                DB.quick.proxy_backend.updateProxies("assign = %s".formatted(id.get("assign")), List.of(id.get("id")));
            });
        }
    }

    @Nested
    @Order(3)
    class LargeInputTests{

        @Test
        @DisplayName("10k IPs Allow")
        void multipleIPsAllow() throws JSchException, SQLException, ClassNotFoundException {
            var ids = DB.quick.proxy_backend.getRandomProxies(10000, "assign = 1");
            var idsList = ids.stream().map((var id) -> Integer.valueOf(id.get("id"))).toList();
            Debug.Preserve("Testing 'assign' on ips: " +
                            ids.stream().map((var m) -> "%s(%s)".formatted(m.get("id"), m.get("assign"))).toList().toString(),
                    true);
            //Prepare row in DB, since IP will be allowed, right now I need to to make ip not assigned
            DB.quick.proxy_backend.updateProxies("assign = 0", idsList);

            test("Call API with assignable = true", () -> {
                ProxyManager.ips(idsList, true).
                        then().assertThat().
                        spec(ProxyManager.successIPsSpec).
                        body("data.updatedCount", equalTo(10000));

                var idsChanged = DB.quick.proxy_backend.getProxyInfo(idsList);
                for(var id : idsChanged) {
                    assertEquals("Validate that 'assign' property is changed in DB (id - %s)".formatted(id), id.get("assign"), "1");
                }
            }, true);

            test("Fallback DB", () -> {
                DB.quick.proxy_backend.updateProxies("assign = 1", idsList);
            });
        }

        @Test
        @DisplayName("10k IPs Disallow")
        void multipleIPsDisallow() throws JSchException, SQLException, ClassNotFoundException {
            var ids = DB.quick.proxy_backend.getRandomProxies(10000, "assign = 1");
            var idsList = ids.stream().map((var id) -> Integer.valueOf(id.get("id"))).toList();
            Debug.Preserve("Testing 'assign' on ips: " +
                            ids.stream().map((var m) -> "%s(%s)".formatted(m.get("id"), m.get("assign"))).toList().toString(),
                    true);
            //Prepare row in DB, since IP will be allowed, right now I need to to make ip not assigned
            DB.quick.proxy_backend.updateProxies("assign = 1", idsList);

            test("Call API with assignable = false", () -> {
                ProxyManager.ips(idsList, false).
                        then().assertThat().
                        spec(ProxyManager.successIPsSpec).
                        body("data.updatedCount", equalTo(10000));

                var idsChanged = DB.quick.proxy_backend.getProxyInfo(idsList);
                for(var id : idsChanged) {
                    assertEquals("Validate that 'assign' property is changed in DB (id - %s)".formatted(id), id.get("assign"), "0");
                }
            }, true);

            test("Fallback DB", () -> {
                DB.quick.proxy_backend.updateProxies("assign = 1", idsList);
            });
        }
    }


}
