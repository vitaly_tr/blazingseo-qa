package qa.api.proxy_manager;

import com.blazingseollc.qa.abstracts.AbstractAnyTest;
import com.blazingseollc.qa.env.rayobyte.api.ProxyManager;
import com.blazingseollc.qa.tools.DB;
import com.jcraft.jsch.JSchException;
import org.junit.jupiter.api.*;
import org.vitmush.qa.selenium_frame.base.Debug;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.IntStream;

import static org.hamcrest.Matchers.*;

public class Search extends AbstractAnyTest {

    //TODO check that all IDs are passed as Integer into api methods (except some validation tests)
    //TODO add more comments
    //TODO add better description, you should be able to understand the issue when it happens from reading logs
    //TODO add test for region_id = -1

    @Test
    @Order(1)
    @DisplayName("Common output without any input test and data validity")
    void noInputParamAndDataValidityTest() throws JSchException, SQLException, ClassNotFoundException {
        var response = ProxyManager.search();
        response.then().assertThat().
                spec(ProxyManager.successSearchSpec).
                body("$", hasKey("limit")).
                body("$", hasKey("offset")).
                body("limit", equalTo(100)).
                body("offset", equalTo(0)).
                body("data.size()", equalTo(100));

        assertTrue("[limit] field in output is Integer", response.then().extract().jsonPath().get("limit") instanceof Integer);
        assertTrue("[offset] field in output is Integer", response.then().extract().jsonPath().get("offset") instanceof Integer);
        var validateFields = List.of(
                "id", "static", "status.assignable", "status.active", "status.active", "status.new", "status.dead",
                "proxy_server.id", "region.id");

        //Test data validity through 10 random rows
        for(var randomIndex : new Random().ints(0, 100).limit(20).toArray()){
            int proxyId = response.body().jsonPath().getInt("data[%d].id".formatted(randomIndex));
            //Debug.Log(String.valueOf(randomIndex) + " . " + String.valueOf(proxyId));
            var proxyInfo = DB.quick.proxy_backend.getProxyInfo(List.of(proxyId)).get(0);
            var sourceInfo = DB.quick.proxy_backend.getSourceInfo(proxyInfo.get("source_id"));

            boolean mixedRegionProxy = proxyInfo.get("region_id").equals("-1");

            Map<String, String> regionInfo = null;
            if(false == mixedRegionProxy)
                regionInfo = DB.quick.proxy_backend.getRegionInfo(proxyInfo.get("region_id"));

            for(var fieldName : validateFields){
                var field = response.then().extract().jsonPath().get("data[%d].%s".formatted(randomIndex, fieldName));
                assertTrue("[data.%s] field in output is Integer".formatted(fieldName),
                         mixedRegionProxy && fieldName.equals("region.id") ? field == null : field instanceof Integer);
            }

            response.then().
                    rootPath("data[%d]".formatted(randomIndex)).

                    body("id", equalTo(Integer.parseInt(proxyInfo.get("id")))).
                    body("ip", equalTo(proxyInfo.get("ip"))).
                    body("block", equalTo(proxyInfo.get("block"))).
                    body("static", equalTo(Integer.parseInt(proxyInfo.get("static")))).

                    body("status.assignable", equalTo(Integer.parseInt(proxyInfo.get("assign")))).
                    body("status.active", equalTo(Integer.parseInt(proxyInfo.get("active")))).
                    body("status.new", equalTo(Integer.parseInt(proxyInfo.get("new")))).
                    body("status.dead", equalTo(Integer.parseInt(proxyInfo.get("dead")))).

                    body("meta.host", equalTo(proxyInfo.get("host"))).
                    body("meta.organization", equalTo(proxyInfo.get("organization"))).
                    body("meta.host_loc", equalTo(proxyInfo.get("host_loc"))).

                    body("proxy_server.id", equalTo(Integer.parseInt(sourceInfo.get("id")))).
                    body("proxy_server.name", equalTo(sourceInfo.get("name"))).
                    body("proxy_server.ip", equalTo(sourceInfo.get("ip"))).
                    body("proxy_server.data_created", equalTo(sourceInfo.get("created")));

                    if(mixedRegionProxy) {
                        response.then().rootPath("data[%d]".formatted(randomIndex)).
                                body("region.id", nullValue());
                    } else {
                        response.then().rootPath("data[%d]".formatted(randomIndex)).
                                body("region.id", equalTo(Integer.parseInt(regionInfo.get("id")))).
                                body("region.country", equalTo(regionInfo.get("country"))).
                                body("region.region", equalTo(regionInfo.get("region"))).
                                body("region.state", equalTo(regionInfo.get("state")));
                    }
        }
    }

    @Nested
    @Order(2)
    class IDAndIPInputTests {

        @Test
        @DisplayName("Search by single ID")
        void searchBySingleId() throws JSchException, SQLException, ClassNotFoundException {
            int id = Integer.parseInt(DB.quick.proxy_backend.getRandomProxies(1).get(0).get("id"));
            ProxyManager.search(List.of(id), null).then().assertThat().
                    spec(ProxyManager.successSearchSpec).
                    body("data.size()", equalTo(1)).
                    body("data[0].id", equalTo(id));
        }

        @Test
        @DisplayName("Search by multiple IDs")
        void searchByMultipleIds() throws JSchException, SQLException, ClassNotFoundException {
            //get 3 multiple random ids from DB
            var ids = DB.quick.proxy_backend.getRandomProxies(3).stream().map((Map<String, String> v) -> {return Integer.parseInt(v.get("id"));}).toList();
            ProxyManager.search(ids, null).then().assertThat().
                    spec(ProxyManager.successSearchSpec).
                    body("data.size()", equalTo(3)).
                    body("data.collect { it.id }", hasItems(ids.get(0), ids.get(1), ids.get(2)));
        }


        @Test
        @DisplayName("Search by single IP")
        void searchBySingleIP() throws JSchException, SQLException, ClassNotFoundException {
            String ip = DB.quick.proxy_backend.getRandomProxies(1).get(0).get("ip");
            ProxyManager.search(null, List.of(ip)).then().assertThat().
                    spec(ProxyManager.successSearchSpec).
                    body("data.size()", equalTo(1)).
                    body("data[0].ip", equalTo(ip));
        }

        @Test
        @DisplayName("Search by multiple IPs")
        void searchByMultipleIps() throws JSchException, SQLException, ClassNotFoundException {
            //get 3 multiple random ips from DB
            var ips = DB.quick.proxy_backend.getRandomProxies(3).stream().map((Map<String, String> v) -> {return v.get("ip");}).toList();
            ProxyManager.search(null, ips).then().assertThat().
                    spec(ProxyManager.successSearchSpec).
                    body("data.size()", equalTo(3)).
                    body("data.collect { it.ip }", hasItems(ips.get(0), ips.get(1), ips.get(2)));
        }

        @Test
        @DisplayName("Search by ID and IP simultaneously")
        void searchByIDAndIPAtSameTime() throws JSchException, SQLException, ClassNotFoundException {
            var proxies = DB.quick.proxy_backend.getRandomProxies(15);
            var idsToCheck = proxies.stream().limit(10).map(v -> v.get("id")).toList();
            var ipsToCheck = proxies.stream().skip(5).limit(10).map(v -> v.get("ip")).toList();

            List<Integer> responseIds = ProxyManager.search(idsToCheck, ipsToCheck).then().assertThat().
                    spec(ProxyManager.successSearchSpec).
                    body("data.size()", equalTo(5)).
                    extract().jsonPath().getList("data.collect { it.id }");

            assertEquals("Only matching ips returned in response while pushing 10 ids and 10 ips (which has 5 common ips)",
                    (int) proxies.stream().skip(5).limit(5).map(v -> Integer.parseInt(v.get("id"))).filter(responseIds::contains).count(), 5);
        }

    }

    @Nested
    @Order(2)
    class LimitAndOffsetTests {

        @Test
        @DisplayName("Limit Without Specifying ips")
        void limitWithoutSpecifyingIPs() {
            ProxyManager.search(null, null, 10, null).then().assertThat().
                    spec(ProxyManager.successSearchSpec).
                    body("data.size()", equalTo(10));
        }

        @Test
        @DisplayName("Limit with specified IDs")
        void limitWithSpecifiedIDs() throws JSchException, SQLException, ClassNotFoundException {
            var ids = DB.quick.proxy_backend.getRandomProxies(10).stream().map((var m) -> Integer.valueOf(m.get("id"))).toList();

            List<Integer> responseIds = ProxyManager.search(ids, null, 5, null).then().assertThat().
                    spec(ProxyManager.successSearchSpec).
                    body("data.size()", equalTo(5)).
                    extract().jsonPath().getList("data.collect { it.id }");

            assertEquals("Returned ips are only that specified in request body",
                    (int) responseIds.stream().filter(ids::contains).count(), 5);
        }

        @Test
        @DisplayName("Limit with specified IPs")
        void limitWithSpecifiedIPs() throws JSchException, SQLException, ClassNotFoundException {
            var ips = DB.quick.proxy_backend.getRandomProxies(10).stream().map((var m) -> m.get("ip")).toList();

            List<String> responseIds = ProxyManager.search(null, ips, 5, null).then().assertThat().
                    spec(ProxyManager.successSearchSpec).
                    body("data.size()", equalTo(5)).
                    extract().jsonPath().getList("data.collect { it.ip }");

            assertEquals("Returned ips are only that specified in request body",
                    (int) responseIds.stream().filter(ips::contains).count(), 5);
        }


        @Test
        @DisplayName("Limit and Offset test with specifying IDs")
        void limitAndOffsetWithSpecifyingIDs() throws JSchException, SQLException, ClassNotFoundException {
            var ids = DB.quick.proxy_backend.getRandomProxies(10).stream().map((var m) -> Integer.valueOf(m.get("id"))).toList();
            List<Integer> responseIds = ProxyManager.search(ids, null, null, null).then().assertThat().
                    spec(ProxyManager.successSearchSpec).
                    body("data.size()", equalTo(10)).
                    extract().jsonPath().getList("data.collect { it.id }");

            List<Integer> offsetReponseIds = ProxyManager.search(ids, null, 3, 5).then().assertThat().
                    spec(ProxyManager.successSearchSpec).
                    body("data.size()", equalTo(3)).
                    extract().jsonPath().getList("data.collect { it.id }");

            assertEquals("Response with offset 5 and limit 3 must contain 3 matching ips comparing to request with no offset and 10 IDs limit",
                    (int) offsetReponseIds.stream().filter(responseIds.stream().skip(5).limit(3).toList()::contains).count(), 3);
        }

        @Test
        @DisplayName("Offset over specified IDs")
        void offsetOverSpecifiedIDs() throws JSchException, SQLException, ClassNotFoundException {
            var ids = DB.quick.proxy_backend.getRandomProxies(10).stream().map((var m) -> Integer.valueOf(m.get("id"))).toList();
            ProxyManager.search(ids, null, null, 10).then().assertThat().
                    spec(ProxyManager.successSearchSpec).
                    body("data", empty());
        }

        @Test
        @DisplayName("Push 'limit' param over max value")
        void limitMaxValue() {
            ProxyManager.search(null, null, 2001, null).then().assertThat().
                    spec(ProxyManager.invalidInputSpec("limit"));
        }

        @Test
        @DisplayName("Offset without specifying IPs")
        void offsetWithoutSpecifyingIPs(){
            List<Integer> responseIds = ProxyManager.search(null, null, null, null).then().assertThat().
                    spec(ProxyManager.successSearchSpec).
                    body("data.size()", equalTo(100)).
                    extract().jsonPath().getList("data.collect { it.id }");

            List<Integer> offsetReponseIds = ProxyManager.search(null, null, null, 10).then().assertThat().
                    spec(ProxyManager.successSearchSpec).
                    body("data.size()", equalTo(100)).
                    extract().jsonPath().getList("data.collect { it.id }");

            assertEquals("Response with offset 10 must contain 90 matching ips comparing to request without offset",
                    (int) offsetReponseIds.stream().filter(responseIds::contains).count(), 90);
        }

        @Test
        @DisplayName("Offset with specifying IDs")
        void offsetWithSpecifyingIDs() throws JSchException, SQLException, ClassNotFoundException {
            var ids = DB.quick.proxy_backend.getRandomProxies(10).stream().map((var m) -> m.get("id")).toList();
            List<Integer> responseIds = ProxyManager.search(ids, null, null, null).then().assertThat().
                    spec(ProxyManager.successSearchSpec).
                    body("data.size()", equalTo(10)).
                    extract().jsonPath().getList("data.collect { it.id }");

            List<Integer> offsetReponseIds = ProxyManager.search(ids, null, null, 5).then().assertThat().
                    spec(ProxyManager.successSearchSpec).
                    body("data.size()", equalTo(5)).
                    extract().jsonPath().getList("data.collect { it.id }");

            assertEquals("Response with offset 5 must contain 5 matching ips comparing to request with no offset and 10 IDs specified",
                    (int) offsetReponseIds.stream().filter(responseIds::contains).count(), 5);
        }

        @Test
        @DisplayName("Offset with specifying IPs")
        void offsetWithSpecifyingIPs() throws JSchException, SQLException, ClassNotFoundException {
            var ips = DB.quick.proxy_backend.getRandomProxies(15).stream().map((var m) -> m.get("ip")).toList();
            List<String> responseIds = ProxyManager.search(null, ips, null, null).then().assertThat().
                    spec(ProxyManager.successSearchSpec).
                    body("data.size()", equalTo(15)).
                    extract().jsonPath().getList("data.collect { it.ip }");

            List<String> offsetReponseIds = ProxyManager.search(null, ips, null, 5).then().assertThat().
                    spec(ProxyManager.successSearchSpec).
                    body("data.size()", equalTo(10)).
                    extract().jsonPath().getList("data.collect { it.ip }");

            assertEquals("Response with offset 5 must contain 10 matching ips comparing to request with no offset and 15 IPs specified",
                    (int) offsetReponseIds.stream().filter(responseIds::contains).count(), 10);
        }


        @Test
        @DisplayName("Limit and Offset test with specifying IPs")
        void limitAndOffsetWithSpecifyingIPs() throws JSchException, SQLException, ClassNotFoundException {
            var ips = DB.quick.proxy_backend.getRandomProxies(10).stream().map((var m) -> m.get("ip")).toList();
            List<String> responseIds = ProxyManager.search(null, ips, null, null).then().assertThat().
                    spec(ProxyManager.successSearchSpec).
                    body("data.size()", equalTo(10)).
                    extract().jsonPath().getList("data.collect { it.ip }");

            List<String> offsetReponseIds = ProxyManager.search(null, ips, 3, 5).then().assertThat().
                    spec(ProxyManager.successSearchSpec).
                    body("data.size()", equalTo(3)).
                    extract().jsonPath().getList("data.collect { it.ip }");

            assertEquals("Response with offset 5 and limit 3 must contain 3 matching ips comparing to request with no offset and 10 IPs limit",
                    (int) offsetReponseIds.stream().filter(responseIds.stream().skip(5).limit(3).toList()::contains).count(), 3);
        }

        @Test
        @DisplayName("Offset over specified IPs")
        void offsetOverSpecifiedIPs() throws JSchException, SQLException, ClassNotFoundException {
            var ips = DB.quick.proxy_backend.getRandomProxies(10).stream().map((var m) -> m.get("ip")).toList();
            ProxyManager.search(null, ips, null, 10).then().assertThat().
                    spec(ProxyManager.successSearchSpec).
                    body("data", empty());
        }

        @Test
        @DisplayName("Push 'offset' param over max value")
        void offsetMaxValue() {
            ProxyManager.search(null, null, null, 100000).then().assertThat().
                    spec(ProxyManager.successSearchSpec);
        }

        @Test
        @DisplayName("Limit with Offset functionality")
        void limitWithOffsetTest(){
            List<Integer> responseIds = ProxyManager.search(null, null, 50, null).then().assertThat().
                    spec(ProxyManager.successSearchSpec).
                    body("data.size()", equalTo(50)).
                    extract().jsonPath().getList("data.collect { it.id }");

            List<Integer> offsetReponseIds = ProxyManager.search(null, null, 50, 20).then().assertThat().
                    spec(ProxyManager.successSearchSpec).
                    body("data.size()", equalTo(50)).
                    extract().jsonPath().getList("data.collect { it.id }");

            assertEquals("Response with offset 20 must contain 30 matching ips comparing to request without offset (and custom limit - 50)",
                    (int) offsetReponseIds.stream().filter(responseIds::contains).count(), 30);
        }

    }

    @Nested
    @Order(3)
    class ValidationTests{

        @Test
        @DisplayName("Invalid input on single ID search tests")
        void invalidInputOnSingleIDSearchTests () {

            test("Search for ID that does not exist in DB", () -> {
                //Search int max, definitely there's no such id in DB
                int id = 2147483647;
                ProxyManager.search(List.of(id), null).then().assertThat().
                        spec(ProxyManager.successSearchSpec).
                        body("data", empty());
            }, true);

            test("Search for ID = 0", () -> {
                int id = 0;
                ProxyManager.search(List.of(id), null).then().assertThat().
                        spec(ProxyManager.successSearchSpec).
                        body("data", empty());
            }, true);

            test("Search for ID = -1", () -> {
                int id = -1;
                ProxyManager.search(List.of(id), null).then().assertThat().
                        spec(ProxyManager.successSearchSpec).
                        body("data", empty());
            }, true);

            test("Search for ID = 'hello'", () -> {
                String id = "hello";
                ProxyManager.search(List.of(id), null).then().assertThat().
                        spec(ProxyManager.invalidInputSpec("id"));
            }, true);
        }

        @Test
        @DisplayName("Invalid input on multiple IDs search tests")
        void invalidInputOnMultipleIDSearchTests () throws JSchException, SQLException, ClassNotFoundException {

            int validID = Integer.parseInt(DB.quick.proxy_backend.getRandomProxies(1).get(0).get("id"));

            test("Search fo 2 invalid IDs = [-1, -2]", () -> {
                var ids = List.of(-1, -2);
                ProxyManager.search(ids, null).then().assertThat().
                        spec(ProxyManager.successSearchSpec).
                        body("data.size()", equalTo(0));
            }, true);

            test("Search for valid ID and ID that does not exist in DB", () -> {
                //Search valid ID and ID with int max, definitely there's no such id in DB
                var ids = List.of(validID, 2147483647);
                ProxyManager.search(ids, null).then().assertThat().
                        spec(ProxyManager.successSearchSpec).
                        body("data.size()", equalTo(1));
            }, true);

            test("Search for valid ID and ID = 0", () -> {
                var ids = List.of(validID, 0);
                ProxyManager.search(ids, null).then().assertThat().
                        spec(ProxyManager.successSearchSpec).
                        body("data.size()", equalTo(1));
            }, true);

            test("Search for valid ID and ID = -1", () -> {
                var ids = List.of(validID, -1);
                ProxyManager.search(ids, null).then().assertThat().
                        spec(ProxyManager.successSearchSpec).
                        body("data.size()", equalTo(1));
            }, true);

            test("Search for valid ID and ID = 'hello'", () -> {
                var ids = List.of(String.valueOf(validID), "hello");
                ProxyManager.search(ids, null).then().assertThat().
                        spec(ProxyManager.invalidInputSpec("id"));
            }, true);
        }


        @Test
        @DisplayName("Invalid input on single IP search tests")
        void invalidInputOnSingleIPSearchTests () {

            test("Search for IP - '127.1.1.1' (does not exist in DB)", () -> {
                String ip = "127.1.1.1";
                ProxyManager.search(null, List.of(ip)).then().assertThat().
                        spec(ProxyManager.successSearchSpec).
                        body("data", empty());
            }, true);

            test("Search for IP = '172' (string, improper input)", () -> {
                String ip = "172";
                ProxyManager.search(null, List.of(ip)).then().assertThat().
                        spec(ProxyManager.invalidInputSpec("ip"));
            }, true);

            test("Search for IP = '172' (integer, improper input)", () -> {
                int ip = 172;
                ProxyManager.search(null, List.of(ip)).then().assertThat().
                        spec(ProxyManager.invalidInputSpec("ip"));
            }, true);

            test("Search for IP = 'hello'", () -> {
                String ip = "hello";
                ProxyManager.search(null, List.of(ip)).then().assertThat().
                        spec(ProxyManager.invalidInputSpec("ip"));
            }, true);
        }

        @Test
        @DisplayName("Invalid input on multiple IPs search tests")
        void invalidInputOnMultipleIPSearchTests () throws JSchException, SQLException, ClassNotFoundException {

            String validIP = DB.quick.proxy_backend.getRandomProxies(1).get(0).get("ip");

            test("Search for valid IP and IP that does not exist in DB", () -> {
                var ips = List.of(validIP, "127.1.1.1");
                ProxyManager.search(null, ips).then().assertThat().
                        spec(ProxyManager.successSearchSpec).
                        body("data.size()", equalTo(1));
            }, true);

            test("Search for valid IP and IP = '172.'", () -> {
                var ips = List.of(validIP, "172.");
                ProxyManager.search(null, ips).then().assertThat().
                        spec(ProxyManager.invalidInputSpec("ip"));
            }, true);

            test("Search for valid IP and IP = '' (empty)", () -> {
                var ips = List.of(validIP, "");
                ProxyManager.search(null, ips).then().assertThat().
                        spec(ProxyManager.invalidInputSpec("ip"));
            }, true);

            test("Search for valid IP and IP = 'hello'", () -> {
                var ips = List.of(validIP, "hello");
                ProxyManager.search(null, ips).then().assertThat().
                        spec(ProxyManager.invalidInputSpec("ip"));
            }, true);
        }

        @Test
        @DisplayName("Invalid input on limit and offset param")
        void invalidInputOnLimitAndOffsetTests(){
            test("Limit equal 0", () -> {
                ProxyManager.search(null, null, 0, null).then().assertThat().
                        spec(ProxyManager.successSearchSpec).
                        body("data.size()", equalTo(100));
            }, true);

            test("Limit equal -1", () -> {
                ProxyManager.search(null, null, -1, null).then().assertThat().
                        spec(ProxyManager.invalidInputSpec("limit"));
            }, true);

            test("Limit equal 2000 (max allowed value)", () -> {
                ProxyManager.search(null, null, 2000, null).then().assertThat().
                        spec(ProxyManager.successSearchSpec).
                        body("data.size()", equalTo(2000));
            }, true);

            test("Limit equal 2001 (max value)", () -> {
                ProxyManager.search(null, null, 2001, null).then().assertThat().
                        spec(ProxyManager.invalidInputSpec("limit"));
            }, true);

            test("Offset equal 0", () -> {
                ProxyManager.search(null, null, null, 0).then().assertThat().
                        spec(ProxyManager.successSearchSpec).
                        body("data.size()", equalTo(100));
            }, true);

            test("Offset equal -1", () -> {
                ProxyManager.search(null, null, null, -1).then().assertThat().
                        spec(ProxyManager.invalidInputSpec("offset"));
            }, true);

            test("Offset equal 2000", () -> {
                ProxyManager.search(null, null, null, 2000).then().assertThat().
                        spec(ProxyManager.successSearchSpec).
                        body("data.size()", equalTo(100));
            }, true);

            test("Offset equal 2001", () -> {
                ProxyManager.search(null, null, null, 2001).then().assertThat().
                        spec(ProxyManager.successSearchSpec).
                        body("data.size()", equalTo(100));
            }, true);

            test("Limit equal 2001, offset equal -1", () -> {
                ProxyManager.search(null, null, 2001, -1).then().assertThat().
                        spec(ProxyManager.invalidInputSpec("limit")).
                        spec(ProxyManager.invalidInputSpec("offset"));
            }, true);

            test("Limit equal 2, offset equal -1", () -> {
                ProxyManager.search(null, null, 2, -1).then().assertThat().
                        spec(ProxyManager.invalidInputSpec("offset"));
            }, true);
        }

    }

    @Nested
    @Order(4)
    class StressTests{
        @Test
        @DisplayName("Input of 10k IDs")
        void input10kIDs() throws JSchException, SQLException, ClassNotFoundException {
            var ids = DB.quick.proxy_backend.getRandomProxies(10000).stream()
                    .map((Map<String, String> v) -> Integer.parseInt(v.get("id"))).toList();
            ProxyManager.search(ids, null).then().assertThat().
                    spec(ProxyManager.successSearchSpec).
                    body("data.size()", equalTo(100));
        }

        @Test
        @DisplayName("Input of 10k IPs")
        void input10kIPs() throws JSchException, SQLException, ClassNotFoundException {
            var ips = DB.quick.proxy_backend.getRandomProxies(10000).stream()
                    .map((Map<String, String> v) -> v.get("ip")).toList();
            ProxyManager.search(null, ips).then().assertThat().
                    spec(ProxyManager.successSearchSpec).
                    body("data.size()", equalTo(100));
        }

        @Test
        @Disabled
        @DisplayName("Search IDs stress test single thread")
        void singleThreadMultipleRequestsMultipleIDsSearch() throws JSchException, SQLException, ClassNotFoundException {
            var ids = DB.quick.proxy_backend.getRandomProxies(5).stream().map((Map<String, String> v) -> Integer.parseInt(v.get("id"))).toList();
            assertTrue("5 ids is returned", ids.size() == 5);

            for(var i : IntStream.range(0, 300).toArray()) {
                test("Attempt #%d".formatted(i+1), () -> {
                    ProxyManager.search(ids, null).then().assertThat().
                            spec(ProxyManager.successSearchSpec).
                            body("data.size()", equalTo(5)).
                            body("data.collect { it.id }", hasItems(ids.get(0), ids.get(1), ids.get(2), ids.get(3), ids.get(4)));
                }, true);
            }
        }

        @Test
        @Disabled
        @DisplayName("Repeat Search IDs test multiple times")
        void repeatMultipleIDsSearch() {
            for(var i : IntStream.range(0, 300).toArray()) {
                test("Attempt #%d".formatted(i+1), () -> {
                    var ids = DB.quick.proxy_backend.getRandomProxies(5).stream().map((Map<String, String> v) -> Integer.parseInt(v.get("id"))).toList();
                    assertTrue("5 ids is returned", ids.size() == 5);
                    ProxyManager.search(ids, null).then().assertThat().
                            spec(ProxyManager.successSearchSpec).
                            body("data.size()", equalTo(5)).
                            body("data.collect { it.id }", hasItems(ids.get(0), ids.get(1), ids.get(2), ids.get(3), ids.get(4)));
                }, true);
            }
        }
    }

}
