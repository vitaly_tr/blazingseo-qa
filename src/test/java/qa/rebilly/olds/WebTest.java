package qa.rebilly.olds;

import java.net.MalformedURLException;
import java.net.URL;

import com.blazingseollc.qa.tools.ProxyChecker;
import org.vitmush.qa.selenium_frame.base.SceneManager;
import org.vitmush.qa.selenium_frame.base.SceneManager.Scene;
import org.vitmush.qa.selenium_frame.exceptions.WrongPageException;
import org.vitmush.qa.selenium_frame.exceptions.WrongValueException;

import com.blazingseollc.qa.base.Config;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class WebTest extends TestCase {

	
	public WebTest(String string) { 
		super(string); 
	}
	

	protected void setUp() throws Exception {
		super.setUp();
	}

	
	protected void tearDown() throws Exception {
		sleep();
		SceneManager.quit();
		super.tearDown();
	}
	
	
	public void sleep() throws InterruptedException {
		sleep(5);
	}
	
	
	public void sleep(float secs) throws InterruptedException {
		Thread.sleep((long) (secs * 1000));
	}
	
	
	public void test0() throws MalformedURLException {
		URL url = new URL("https://panel.lightproxies.com/update-locations.html#");
		System.out.println(url.getHost());
		System.out.println(url.getPath());
		
		System.out.println(String.format("hello %s!", ",world"));
		
		int userid = 420;
		System.out.println(Config.env.email.replaceFirst("@", "_"+String.valueOf(userid)+"@"));
	}
	
	
	public void test1() throws Exception {
		Scene scene = SceneManager.add("chrome0", "chrome");
		
		com.blazingseollc.qa.env.lightproxies.ProxyDashboard dash = new com.blazingseollc.qa.env.lightproxies.
				ProxyDashboard(scene.tab("main"));
		
		dash.home.open().isOpened().pressSignup().isOpened().signUp(
				dash.synonyms.country.usa.listName, dash.synonyms.category.dedicated.listName, "10", Config.env.whmcs.promo_100,
				Config.env.email, Config.env.password, Config.env.password, 
				"VTest", "--", "00000", "BlazingSEO", "mein address", "mein city",
				"mein state", "000", "France");
		
		dash.tab().waitLoaded();
		
		for(int i = 0; i < 20; i++) {
			try {
				dash.locations.isOpened();
			} catch(WrongPageException ex) {
				//locations not opened yet
				dash.tab().reload();
				sleep(30);
				continue;
			}
			break;
		}
		
		if(! dash.locations.saveAllMixedOnFirst().up().checkout.open().isOpened()
			.hasPackage(dash.synonyms.country.usa.robotName, dash.synonyms.category.dedicated.robotName))
			throw new Exception("Purchased package not found");
		
		
		/*dash.home.open().isOpened().pressSignin().isOpened().open().isOpened().signIn(Config.env.email, Config.env.password);
		
		tab.waitLoaded();
		
		com.blazingseollc.qa.automation.env.lightproxies.Whmcs whmcs = new com.blazingseollc.qa.automation.env.lightproxies.
				Whmcs(tab, Config.env.url.lp_whmcs);
		try {
			whmcs.oauth.isOpened().confirm();
			tab.waitLoaded();
		} catch(WrongPageException ex) {
			//then oauth was already confirmed
		}*/
		
		//dash.locations.open().isOpened().saveAllMixedOnFirst().setAuthType(AuthType.PWD).openExportAll();
		/*dash.dashboard.open().isOpened().setAuthType(AuthType.PWD).openExportAll();
		
		com.blazingseollc.qa.automation.env.lightproxies.ProxyDashboard export = new com.blazingseollc.qa.automation.env.lightproxies.
				ProxyDashboard(scene.t(), dash.getBaseUrl());
		String proxies = export.dashboard.export.isOpened().getList();
		System.out.println(proxies);
		
		export.Tab().close();*/
		
		
		//sleep(30/2);
	}
	
	
	public void test2() throws Exception {
		Scene scene = SceneManager.add("chrome0", "chrome");

		com.blazingseollc.qa.env.lightproxies.ProxyDashboard dash = new com.blazingseollc.qa.env.lightproxies.
				ProxyDashboard(scene.tab("main"));
		
		dash.home.open().pressSignin().signIn(Config.env.email, Config.env.password);
		dash.tab().waitLoaded();
		boolean signed = false;
		try {
			dash.login.isOpened();
		} catch (WrongPageException ex) {
			signed = true;
		}
		if(!signed)
			throw new Exception("Couldn't sign in");
		
		com.blazingseollc.qa.env.lightproxies.Whmcs whmcs = new com.blazingseollc.qa.env.lightproxies.
				Whmcs(scene.tab("main"));
		try {
			whmcs.oauth.isOpened().confirm();
			whmcs.tab().waitLoaded();
		} catch(WrongPageException ex) {
			//then oauth was already confirmed
		}
		
		
		dash.sidebar.isFound().pressCheckout().isOpened().purchasePackage(
				dash.synonyms.country.usa.listName, dash.synonyms.category.rotate.listName, "5", Config.env.whmcs.promo_100);
		dash.tab().waitLoaded();
		dash.checkout.open();
		
		int i = 0;
		while(true) {
			if(dash.checkout.hasPackage(dash.synonyms.country.usa.robotName, dash.synonyms.category.rotate.robotName)) {
				break;
			} else if (i++ == 20) {
				throw new Exception("Purchased package not found");
			} else {
				dash.tab().reload();
				sleep(5);
			}
		}
	}
	
	
	public void test3() throws Exception {
		Scene scene = SceneManager.add("chrome0", "chrome");

		com.blazingseollc.qa.env.lightproxies.Whmcs whmcs = new com.blazingseollc.qa.env.lightproxies.
				Whmcs(scene.tab("main"));
		
		int userid = whmcs.admin.authorize().clients.open().isOpened().findClient(Config.env.email).getId();
		whmcs.admin.client.summary.openOrders().waitLoaded();
		
		try {
			int ordersTotal = whmcs.admin.client.orders.isOpened().getTotalOrders();
			if(ordersTotal > 15)
				throw new Exception("too much orders -"+ordersTotal+"- I'm not gonna delete it; needs additional validation step");
			whmcs.admin.client.orders.deleteAll().waitLoaded();
		} catch (WrongPageException ex) {
			whmcs.admin.client.order.delete().waitLoaded();
		}
		
		whmcs.admin.client.profile.open(userid)
		.updateState().updateEmail(Config.env.email.replaceFirst("@", "_"+String.valueOf(userid)+"@"));

		whmcs.admin.client.summary.open(userid).pressLoginAsClient().waitLoaded();
		whmcs.client.home.isOpened().pressLoginProxyDashboard().waitLoaded();
		try {
			whmcs.oauth.isOpened().confirm().waitLoaded();
		} catch(WrongPageException ex) {
			//then, it was already accepted
		}
		
		whmcs.admin.client.summary.open(userid).deleteAccount().waitLoaded();
		
		try {
			whmcs.admin.clients.open().findClient(Config.env.email);
		} catch (WrongValueException ex) {
			// All correct; User not found, thus removed 
		}
	}
	
	
	public void test4() throws Exception {
		Scene scene = SceneManager.add("chrome0", "chrome");
		
		com.blazingseollc.qa.env.supersonicproxies.ProxyDashboard dash = new com.blazingseollc.qa.env.supersonicproxies.
				ProxyDashboard(scene.tab("main"));
		
		dash.home.open().isOpened().pressSignup().isOpened().signUp(
				dash.synonyms.country.usa.listName, dash.synonyms.category.dedicated.listName, "10", Config.env.whmcs.promo_100,
				Config.env.email, Config.env.password, Config.env.password, 
				"VTest", "--", "00000", "BlazingSEO", "mein address", "mein city",
				"mein state", "000", "France").waitLoaded();
		
		com.blazingseollc.qa.env.supersonicproxies.Whmcs whmcs = new com.blazingseollc.qa.env.supersonicproxies.
				Whmcs(scene.tab("main"));
		
		try {
			whmcs.client.invoice.isOpened().pressGetProxies().waitLoaded();
		} catch (WrongPageException ex) {
			//then it wasn't redirected to invoice page
		}
		
		dash.isOpened();
		
		for(int i = 0; i < 20; i++) {
			try {
				dash.locations.isOpened();
			} catch(WrongPageException ex) {
				//locations not opened yet
				dash.tab().reload();
				sleep(30);
				continue;
			}
			break;
		}
		
		if(! dash.locations.saveAllMixedOnFirst().up().checkout.open().isOpened()
			.hasPackage(dash.synonyms.country.usa.robotName, dash.synonyms.category.dedicated.robotName))
			throw new Exception("Purchased package not found");
	}
	
	
	public void test5() throws Exception {
		Scene scene = SceneManager.add("chrome0", "chrome");

		com.blazingseollc.qa.env.supersonicproxies.ProxyDashboard dash = new com.blazingseollc.qa.env.supersonicproxies.
				ProxyDashboard(scene.tab("main"));
		
		dash.home.open().pressSignin().signIn(Config.env.email, Config.env.password);
		dash.tab().waitLoaded();
		boolean signed = false;
		try {
			dash.login.isOpened();
		} catch (WrongPageException ex) {
			signed = true;
		}
		if(!signed)
			throw new Exception("Couldn't sign in");
		
		com.blazingseollc.qa.env.supersonicproxies.Whmcs whmcs = new com.blazingseollc.qa.env.supersonicproxies.
				Whmcs(scene.tab("main"));
		try {
			whmcs.oauth.isOpened().confirm();
			whmcs.tab().waitLoaded();
		} catch(WrongPageException ex) {
			//then oauth was already confirmed
		}
		
		
		dash.sidebar.isFound().pressCheckout().isOpened().purchasePackage(
				dash.synonyms.country.usa.listName, dash.synonyms.category.rotate.listName, "5", Config.env.whmcs.promo_100);
		dash.tab().waitLoaded();
		dash.checkout.open();
		
		int i = 0;
		while(true) {
			if(dash.checkout.hasPackage(dash.synonyms.country.usa.robotName, dash.synonyms.category.rotate.robotName)) {
				break;
			} else if (i++ == 20) {
				throw new Exception("Purchased package not found");
			} else {
				dash.tab().reload();
				sleep(5);
			}
		}
	}
	
	
	public void test6() throws Exception {
		Scene scene = SceneManager.add("chrome0", "chrome");

		com.blazingseollc.qa.env.supersonicproxies.Whmcs whmcs = new com.blazingseollc.qa.env.supersonicproxies.
				Whmcs(scene.tab("main"));
		
		int userid = whmcs.admin.authorize().clients.open().isOpened().findClient(Config.env.email).getId();
		whmcs.admin.client.summary.openOrders().waitLoaded();
		
		try {
			int ordersTotal = whmcs.admin.client.orders.isOpened().getTotalOrders();
			if(ordersTotal > 15)
				throw new Exception("too much orders -"+ordersTotal+"- I'm not gonna delete it; needs additional validation step");
			whmcs.admin.client.orders.deleteAll().waitLoaded();
		} catch (WrongPageException ex) {
			whmcs.admin.client.order.delete().waitLoaded();
		}
		
		whmcs.admin.client.profile.open(userid)
		.updateState().updateEmail(Config.env.email.replaceFirst("@", "_"+String.valueOf(userid)+"@"));

		whmcs.admin.client.summary.open(userid).pressLoginAsClient().waitLoaded();
		whmcs.client.home.isOpened().pressLoginProxyDashboard().waitLoaded();
		try {
			whmcs.oauth.isOpened().confirm().waitLoaded();
		} catch(WrongPageException ex) {
			//then, it was already accepted
		}
		
		whmcs.admin.client.summary.open(userid).deleteAccount().waitLoaded();
		
		try {
			whmcs.admin.clients.open().findClient(Config.env.email);
		} catch (WrongValueException ex) {
			// All correct; User not found, thus removed 
		}
	}
		
	
	public void testProxiesLP () throws Exception {
		Scene scene = SceneManager.add("chrome0", "chrome");
		
		com.blazingseollc.qa.env.lightproxies.ProxyDashboard dash = new com.blazingseollc.qa.env.lightproxies
				.ProxyDashboard(scene.tab("main"));
		
		dash.login.open().signIn(Config.env.email, Config.env.password).waitLoaded();
		dash.dashboard.isOpened().openExportPackage(dash.synonyms.country.usa.packageName, dash.synonyms.category.dedicated.packageName);
		
		com.blazingseollc.qa.env.lightproxies.ProxyDashboard exp = new com.blazingseollc.qa.env.lightproxies
				.ProxyDashboard(scene.tab());
		
		String proxies = exp.dashboard.export.getList();
		int amount = 0;
		for(String s : proxies.split("\n")) {
			amount++;
		}
		int i = Math.round((float) Math.random() * (amount - 1) + 1);

		ProxyChecker.testIPv4Connection(proxies.split("\n")[i]);
		exp.tab().close();
		
		
		
		dash.dashboard.openExportPackage(dash.synonyms.country.usa.packageName, dash.synonyms.category.rotate.packageName);
		
		exp = new com.blazingseollc.qa.env.lightproxies.ProxyDashboard(scene.tab());
		
		proxies = exp.dashboard.export.getList();
		amount = 0;
		for(String s : proxies.split("\n")) {
			amount++;
		}
		i = Math.round((float) Math.random() * (amount - 1) + 1);

		ProxyChecker.testIPv4Connection(proxies.split("\n")[i]);
		exp.tab().close();
	}
	
	
	public void testProxiesSSP () throws Exception {
		Scene scene = SceneManager.add("chrome0", "chrome");
		
		com.blazingseollc.qa.env.supersonicproxies.ProxyDashboard dash = new com.blazingseollc.qa.env.supersonicproxies
				.ProxyDashboard(scene.tab("main"));
		
		dash.login.open().signIn(Config.env.email, Config.env.password).waitLoaded();
		dash.dashboard.isOpened().openExportPackage(dash.synonyms.country.usa.packageName, dash.synonyms.category.dedicated.packageName);
		
		com.blazingseollc.qa.env.supersonicproxies.ProxyDashboard exp = new com.blazingseollc.qa.env.supersonicproxies
				.ProxyDashboard(scene.tab());
		
		String proxies = exp.dashboard.export.getList();
		int amount = 0;
		for(String s : proxies.split("\n")) {
			amount++;
		}
		int i = Math.round((float) Math.random() * (amount - 1) + 1);

		ProxyChecker.testIPv4Connection(proxies.split("\n")[i]);
		exp.tab().close();
		
		
		
		dash.dashboard.openExportPackage(dash.synonyms.country.usa.packageName, dash.synonyms.category.rotate.packageName);
		
		exp = new com.blazingseollc.qa.env.supersonicproxies.ProxyDashboard(scene.tab());
		
		proxies = exp.dashboard.export.getList();
		amount = 0;
		for(String s : proxies.split("\n")) {
			amount++;
		}
		i = Math.round((float) Math.random() * (amount - 1) + 1);

		ProxyChecker.testIPv4Connection(proxies.split("\n")[i]);
		exp.tab().close();
	}
	

	public static Test suite() {
		TestSuite tests = new TestSuite();
		//tests.addTest(new qa.rebilly.olds.WebTest("test0"));
		
		//tests.addTest(new qa.rebilly.olds.WebTest("testProxiesLP"));
		//tests.addTest(new qa.rebilly.olds.WebTest("testProxiesSSP"));
		
			//tests.addTest(new qa.rebilly.olds.WebTest("test1"));
			//tests.addTest(new qa.rebilly.olds.WebTest("test2"));
		//tests.addTest(new qa.rebilly.olds.WebTest("setAuthLP"));
		//tests.addTest(new qa.rebilly.olds.WebTest("test3"));
		
		//tests.addTest(new qa.rebilly.olds.WebTest("test4"));
		//tests.addTest(new qa.rebilly.olds.WebTest("test5"));
		//tests.addTest(new qa.rebilly.olds.WebTest("setAuthSSP"));
		//tests.addTest(new qa.rebilly.olds.WebTest("test6"));
	
		return tests;
	}
	
}
