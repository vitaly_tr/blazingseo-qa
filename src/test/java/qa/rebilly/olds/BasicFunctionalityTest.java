package qa.rebilly.olds;

import com.blazingseollc.qa.base.Config;
import com.blazingseollc.qa.env.blazing.BDR;
import com.blazingseollc.qa.env.rb.RebillyAdmin;
import com.blazingseollc.qa.env.rb.DashboardV2;
import com.blazingseollc.qa.tools.DB;
import com.blazingseollc.qa.tools.Helper;
import com.blazingseollc.qa.tools.ProxyChecker;
import com.blazingseollc.qa.base.ProxyType;
import com.jcraft.jsch.JSchException;
import org.junit.jupiter.api.*;
import org.vitmush.qa.selenium_frame.base.Debug;
import org.vitmush.qa.selenium_frame.base.SceneManager;
import org.vitmush.qa.selenium_frame.abstracts.AbstractTest;
import org.vitmush.qa.selenium_frame.exceptions.*;

import java.io.IOException;
import java.sql.*;
import java.text.ParseException;
import java.time.Duration;

public class BasicFunctionalityTest extends AbstractTest {

    @Test
    @Order(1)
    public void SignupWithRotatingRes() throws NoSuchElementException, WrongPageException, PageTimeoutException, ConditionTimeoutException, JSchException, SQLException, ClassNotFoundException, URLIsImproper, WebFormErrorException {
        DashboardV2 d = new DashboardV2(SceneManager.add().tab());
        String email = Helper.generateUniqueEmail();
        Debug.Log(email);
        d.signup.open()
                .doCreateAccount(new ProxyType(ProxyType.ProxyCategory.ISP_Rotating), 55, email, Config.env.password)
                .makeCardPayment()/*.clickReturnLink()*/.isOpened();
    }

    @Test
    @Order(1)
    public void SignupWithFrenchRotating() throws NoSuchElementException, WrongPageException, PageTimeoutException, ConditionTimeoutException, URLIsImproper, WebFormErrorException, ParseException, JSchException, SQLException, ClassNotFoundException, InterruptedException {
        Object[][] cases = new Object[][] {
                //{ new ProxyType(ProxyType.ProxyCategory.Dedicated, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Monthly) },
                { new ProxyType(ProxyType.ProxyCategory.Rotating, ProxyType.ProxyCountry.Germany, ProxyType.BillingCycle.Monthly) },
                //{ new ProxyType(ProxyType.ProxyCategory.Semi, ProxyType.ProxyCountry.Brazil, ProxyType.BillingCycle.Monthly) },
                //{ new ProxyType(ProxyType.ProxyCategory.Rotating, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Quarterly) },
                { new ProxyType(ProxyType.ProxyCategory.Dedicated, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Semi_Annually) },
                //{ new ProxyType(ProxyType.ProxyCategory.ISP, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Annually) },
                //{ new ProxyType(ProxyType.ProxyCategory.Shopify, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Monthly) },
                //{ new ProxyType(ProxyType.ProxyCategory.Sneaker, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Monthly) },
                { new ProxyType(ProxyType.ProxyCategory.ISP_Rotating, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Monthly) }
        };

        for(Object[] c : cases){
            DashboardV2 d = new DashboardV2();
            ProxyType type = (ProxyType) c[0];
            if(c.length > 1)
                d.quick.signupWithPurchase(type, (int) c[1]);
            else d.quick.signupWithPurchase(type);

            int amount = (c.length > 1) ? (int) c[1] : d.quick.tools.getSignupQuantity((ProxyType) c[0]);

            if(type.category == ProxyType.ProxyCategory.ISP_Rotating){
                d.signup.open();
                //should happen redirect to dashboard home instead
                if(d.tfa.isOpened())
                    d.tfa.confirmTFA(DB.quick.proxy_backend.getTFACode(Helper.lastGeneratedUniqueEmail));
                Helper.sleep(60);
                String creds = d.isp_rotating.home.open().getCommonCreds();
                assert(amount == d.isp_rotating.home.getBalance());
                ProxyChecker.downloadWithISPRotating(creds, 5);
            } else {
                new RebillyAdmin().quick.openUser(Helper.lastGeneratedUniqueEmail).assertHasSubscription(type);
                var backend_package = DB.quick.proxy_backend.getIPv4Packages(Helper.lastGeneratedUniqueEmail).get(0);

                assert(backend_package.get("country").equals(ProxyType.AliasCollection.common.countryCode.of(type.country)));
                assert(backend_package.get("category").equals(ProxyType.AliasCollection.common.categoryCode.of(type.category)));
                assert(backend_package.get("amount").equals(String.valueOf(amount)));
            }

            SceneManager.quit();
        }

        Debug.FlushPreserved();
    }

    @Test
    public void TestISPRotatingConnection() throws PageTimeoutException, NoSuchElementException, WrongPageException, InterruptedException, URLIsImproper, JSchException, SQLException, ClassNotFoundException {
        DashboardV2 d = new DashboardV2();
        String email = "vitaly.troshuk+104@sprious.com";
        d.signin.open().login(email);
        if(d.tfa.isOpened())
            d.tfa.confirmTFA(DB.quick.proxy_backend.getTFACode(email));
        String creds = d.isp_rotating.home.open().getSpecialCreds(); //.getCommonCreds();
        Debug.Log(String.format("|%s|", creds));
        float prevBalance = d.isp_rotating.home.getBalance();
        float downloadedMB = ProxyChecker.downloadWithISPRotating(creds, 100);
        org.vitmush.qa.selenium_frame.base.Helper.sleep(3 * 60 * 1000);
        d.tab().reload();
        float newBalance = d.isp_rotating.home.getBalance();
        Debug.Log(String.format("Downloaded: %fmb  (Balance diff: %fmb  old: %fgb, new: %fgb)", downloadedMB, (newBalance - prevBalance) * 1024, prevBalance, newBalance));
    }

    @Test
    public void SignupConcurenncyTesting() throws JSchException, PageTimeoutException, SQLException, ConditionTimeoutException, ClassNotFoundException, NoSuchElementException, WrongPageException, URLIsImproper, ParseException, WebFormErrorException {
        ProxyType[][] types = new ProxyType[][] {
                {
                        new ProxyType(ProxyType.ProxyCategory.Dedicated, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Monthly),
                        new ProxyType(ProxyType.ProxyCategory.Semi, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Quarterly)
                },
                /*{
                        new ProxyType(ProxyType.ProxyCategory.Dedicated, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Monthly),
                        new ProxyType(ProxyType.ProxyCategory.Dedicated, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Monthly)
                },*/
                {
                        new ProxyType(ProxyType.ProxyCategory.Dedicated, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Monthly),
                        new ProxyType(ProxyType.ProxyCategory.ISP_Rotating, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Monthly)
                }
        };

        for(var type : types){
            new BDR().quick.createAccountWithProduct(type[0]);

            var d = new DashboardV2();
            d.quick.signupWithPurchase(type[1], Helper.lastGeneratedUniqueEmail);
            //should redirect to home
            d.signup.open();
            if(d.tfa.isOpened())
                d.tfa.confirmTFA(DB.quick.proxy_backend.getTFACode(Helper.lastGeneratedUniqueEmail));
            //TODO fill Locations if exists
            Helper.sleep(Duration.ofMinutes(1));
            d.isp_rotating.home.open();
            d.waitUserAction(String.format("Check balance is %f", new DashboardV2().isp_rotating.home.getBalance()));
            d.any.quit();
        }

    }

    @Test
    public void test() throws PageTimeoutException, ConditionTimeoutException, NoSuchElementException, WrongPageException, NoSuchFieldException, JSchException, SQLException, ClassNotFoundException, IOException, InterruptedException, IllegalAccessException, URLIsImproper, ParseException, TabAlreadySavedException, WrongValueException {
        /*Dashboard d = new Dashboard();
        d.signin.open().login("vitaly.troshuk+20211015_175320@sprious.com", "tester");
        d.ipv4.dashboard.open().isEnabled();
        d.ipv4.dashboard.setAuthPWD().openExport( new ProxyType(ProxyType.ProxyCategory.Dedicated, ProxyType.ProxyCountry.USA)).getIPs().forEach(Debug::Log);
*/
        ProxyChecker.testIPv4Connection("107.152.198.124:3128");
        ProxyChecker.testIPv4Connection("107.173.177.50:4444:d04ae319b5:fTI4eKLl");
        ProxyChecker.testIPv4Connection("207.229.93.67:1028", "154.16.6.234");
    }

    @Test
    public void testIPv4Purchase() throws PageTimeoutException, ConditionTimeoutException, NoSuchElementException, WrongPageException, NoSuchFieldException, JSchException, SQLException, ClassNotFoundException, IOException, InterruptedException, IllegalAccessException, URLIsImproper, WebFormErrorException {
        var d = new DashboardV2();
        d.quick.signupWithPurchase(new ProxyType(ProxyType.ProxyCategory.Dedicated, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Monthly));
        org.vitmush.qa.selenium_frame.base.Helper.sleep(10000);
        var list = DB.quick.proxy_backend.getIPv4Packages(Helper.lastGeneratedUniqueEmail);
        list.forEach(n -> Debug.Log(n.get("country") + "-" + n.get("category")));
    }

    @Test
    public void TestISPRotatingDownload() throws PageTimeoutException, ConditionTimeoutException, NoSuchElementException, WrongPageException, NoSuchFieldException, JSchException, SQLException, ClassNotFoundException, IOException, InterruptedException, IllegalAccessException, URLIsImproper {
        ProxyChecker.downloadWithISPRotating("vitalytroshuk20211027223420spriouscom:Hu1eD2DiqOJV@152.44.97.155:8000", 2);
    }

    @Test
    public void db_test() throws JSchException, SQLException, ClassNotFoundException {
        DB db = DB.getSession(DB.Host.Proxy_Backend);
        DB.SelectResult result = db.select("SELECT * FROM user_mta_otp;");
        System.out.println("Row is fetched. Sample code: " + result.set.getString("code"));
    }

    @Test
    @Order(2)
    public void BDRTest() throws PageTimeoutException, ConditionTimeoutException, NoSuchElementException, WrongPageException, JSchException, SQLException, ClassNotFoundException, URLIsImproper {
        BDR bdr = new BDR();
        //TODO write some BDR test
        /*bdr.quick.createAccount(
                new ProxyType(ProxyType.ProxyCategory.Dedicated, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Monthly), 10
        );
        bdr.quick.createAccount(
                new ProxyType(ProxyType.ProxyCategory.Dedicated, ProxyType.ProxyCountry.France, ProxyType.BillingCycle.Annually), 10
        );
        bdr.quick.createAccount(
                new ProxyType(ProxyType.ProxyCategory.Semi, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Quarterly), 10
        );
        bdr.quick.createAccount(
                new ProxyType(ProxyType.ProxyCategory.Semi, ProxyType.ProxyCountry.UK, ProxyType.BillingCycle.Semi_Annually), 10
        );
        bdr.quick.createAccount(
                new ProxyType(ProxyType.ProxyCategory.Rotating, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Monthly), 10
        );
        bdr.quick.createAccount(
                new ProxyType(ProxyType.ProxyCategory.ISP, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Monthly), 10
        );
        bdr.quick.createAccount(
                new ProxyType(ProxyType.ProxyCategory.IPv6, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Quarterly), 25
        );*/
    }

}
