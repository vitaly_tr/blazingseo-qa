package qa.rebilly.functionality;

import com.blazingseollc.qa.base.Config;
import com.blazingseollc.qa.base.ProxyType;
import com.blazingseollc.qa.env.rb.DashboardV2;
import com.blazingseollc.qa.env.rb.OMAD;
import com.blazingseollc.qa.abstracts.AbstractRebillyTest;
import com.blazingseollc.qa.tools.*;
import com.blazingseollc.qa.base.ProxyType.*;
import com.jcraft.jsch.JSchException;
import org.junit.jupiter.api.*;
import org.vitmush.qa.selenium_frame.base.Debug;
import org.vitmush.qa.selenium_frame.base.SceneManager;
import org.vitmush.qa.selenium_frame.exceptions.*;
import org.vitmush.qa.selenium_frame.exceptions.NoSuchElementException;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.text.ParseException;
import java.time.*;
import java.util.*;

public class AdminTrialTests extends AbstractRebillyTest {

    private AdminTrialTests() {
        super(true);
    }

    static public AdminTrialTests single() throws InvocationTargetException, URLIsImproper, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return (AdminTrialTests) new AdminTrialTests().getSingle();
    }

    public void SuspendMake(){
        var time = LocalTime.now().plusMinutes(58 - LocalTime.now().getMinute());
        time = time.minusSeconds(time.getSecond() - 40);
        //Debug.Log(time.toString());
        var timestamp = new Date(time.toEpochSecond(LocalDate.now(), OffsetDateTime.now().getOffset()) * 1000);
        Debug.Log("Wait until: " + String.valueOf(timestamp));
        new Wait(timestamp).waitUntil();
        //TestProxyConnectionaIsAdded();
    }

    //TODO Download through proxy checker much less traffic
    //TODO proxies got suspended and resuspended (proxies got terminated after invoice paid case)

    //TODO Medium:
    //TODO test proxy connection on trial created for same user
    //TODO repeatable test to validate generated prices after trial is finished (compare with signup prices)
    //TODO test that balance is exactly 0 GB on cancelling trial
    //TODO middle multiple trial packages tests
    // test trial created after previous trial terminated
    // test trial created after previous subscription terminated
    //TODO test email messages content (on new user and existing user)
    //TODO cleanup

    //TODO low test search on trial index (currently it's broken)
    //TODO proceed simplify test cases code
    //TODO trial keeps terminated on after renewal invoice generated and order got cancelled


    @Nested
    @Order(1)
    public class CreationFormTests {

        @Test
        public void FieldsAreRequiredOnTrialCreationForm() throws PageTimeoutException, NoSuchElementException, WrongPageException {
            Omad.quick.login();
            var Create = Omad.trial.create;
            //validate that some fields are required by default
            Create.open().assertFieldsRequired().setEmail(Helper.generateUniqueEmail()).setDays(1);
            assert(!Create.pressSubmit().getError().isEmpty());
            //validate password cannot be empty
            assert(!Create.setAmount(1).pressSubmit().getError().isEmpty());
            //validate amount cannot be 0
            assert(!Create.setAmount(0).setPassword(Config.env.password).pressSubmit().getError().isEmpty());
            //validate trial created
            assert(Create.setAmount(1).pressSubmit().isTrialCreated());
        }

        @Test
        public void CredentialsAreProperOnTrialCreationForNewAndExistingUser() throws URLIsImproper, PageTimeoutException, NoSuchElementException, WrongPageException, JSchException, SQLException, ClassNotFoundException {
            //create first trial
            Omad.quick.login().createTrial();
            var email = Helper.lastGeneratedUniqueEmail;

            //validate login after trial created
            SceneManager.quit();
            Dashboard = new DashboardV2();
            Dashboard.signin.open().login(email, Config.env.password);
            Dashboard.quick.confirmTFA();
            Dashboard.home.assertPageOpened();

            //create second trial with different password
            SceneManager.quit();
            Omad = new OMAD();
            Omad.quick.login();
            Omad.trial.create.open().createTrial(email, "some_improper_password", 1, 1);

            //test that password didn't change
            SceneManager.quit();
            Dashboard = new DashboardV2();
            Dashboard.signin.open().login(email, Config.env.password);
            Dashboard.home.assertPageOpened();
        }
    }



    @Nested
    @Order(2)
    @TestMethodOrder(MethodOrderer.OrderAnnotation.class)
    public class TrialPackageStatusBundle {

        static String Email = null;
        final int Bandwidth_Amount = 3;

        @Test
        @Order(1)
        public void TrialCreatedForNewUser() throws URLIsImproper, PageTimeoutException, NoSuchElementException, WrongPageException, JSchException, SQLException, ClassNotFoundException, ConditionTimeoutException, ParseException, TabAlreadySavedException, InterruptedException {
            String email = Helper.generateUniqueEmail();
            var amount = Bandwidth_Amount;
            var days = 2;

            step("creating trial from omad");
            Omad.quick.login().createTrial(email, amount,days);
            assertTrue("Trial created message appeared", Omad.trial.create.isTrialCreated());

            //initiate wait, do some preparations, validation, then wait until authorization completed
            var isp_rotating_authorization = new Wait(Config.sleep.v2_dashboard.isp_rotating_authorization_wait);
            Dashboard = Omad.users.open().loginAsUser(email);

            String proxyCreds = Dashboard.isp_rotating.home.open().getCommonCreds();
            assertTrue("Rotating Residential proxy credentials are generated"
                    , proxyCreds.startsWith(email.replaceAll("[\\.\\+@_-]", "")));
            assertTrue("Balance is added, and equals to 1 GB", Dashboard.isp_rotating.home.getBalance() == 1);

            var subscriptions = Rebilly.quick.openUser(email).getActiveSubscriptions();
            assert(subscriptions.size() == 1);
            assert(subscriptions.get(0).days == days);
            assert(subscriptions.get(0).quantity == amount);
            assert(subscriptions.get(0).plan.equals("Starter"));

            isp_rotating_authorization.waitUntil();

            //validate proxy authorization
            assert(ProxyChecker.downloadWithISPRotating(proxyCreds, 10) >= 10.0);

            Email = email;
        }

        @Test
        @Order(2)
        public void AccessToProxiesGotSuspendedOnRenewalDate() throws URLIsImproper, PageTimeoutException, NoSuchElementException, WrongPageException, JSchException, SQLException, ClassNotFoundException, ConditionTimeoutException, ParseException, InterruptedException, TabAlreadySavedException {

            assert(Email != null);
            String email = Email;

            var subscriptions = Rebilly.quick.openUser(email).getActiveSubscriptions();
            //date that be in a minute; if it's less than 30 seconds until next minute, then it will be minute after next minute
            int currentSecond = LocalDateTime.now().getSecond();
            var inAMinute = new Date(new Date().getTime() + (currentSecond > 30 ? 120 - currentSecond : 60 - currentSecond) * 1000);

            //change renewal date
            Rebilly.subscription.open(subscriptions.get(0).id).changeTrialRenewalDate(inAMinute);
            //wait until package is updated from rebilly
            new Wait(inAMinute).waitUntil();
            Helper.sleep(Config.sleep.rebilly.product_update_on_cron_wait);

            //initiate proxy authorization wait
            Wait isp_rotating_authorization = new Wait(Config.sleep.v2_dashboard.isp_rotating_authorization_wait);

            //validate that subscription is terminated on backend
            var result = DB.quick.v2_backend.getSubscriptionsBandwidth(email);
            assert(result.size() != 0);
            assert(result.size() == 1);
            assert(result.get(0).get("status").equals("suspended"));
            assert(result.get(0).get("type").equals("traffic"));

            //get proxy auth creds
            Dashboard = Omad.quick.login().loginAsClient(email);
            String proxyCreds = Dashboard.isp_rotating.home.open().getCommonCreds();
            assert(Dashboard.isp_rotating.home.getBalance() == 0);

            //wait until proxies authorized
            isp_rotating_authorization.waitUntil();

            try {
                assert(ProxyChecker.downloadWithISPRotating(proxyCreds, 5) == 0);
            } catch(RuntimeException e){
                //expected, connection should be suspended
            }
        }

        @Test
        @Order(3)
        public void AccessToProxiesRegainedAfterInvoicePaid() {

            assert(Email != null);

            test("Make invoice generated on trial renewal date as paid", () -> {
                var invoice = Rebilly.quick.openUser(Email).getUnpaidInvoices().get(0);
                assertNotEquals("Invoice price is not $0", Float.parseFloat(invoice.get("amount")), 0f);
                Rebilly.invoice.open(invoice.get("id")).makeFreeAndPaid();
                Helper.sleep(Config.sleep.rebilly.product_sync_wait);
            });

            var proxy_authorization = new Wait(Config.sleep.v2_dashboard.isp_rotating_authorization_wait);

            test("Validate renewed subscription data", () -> {
                var subscription = Rebilly.quick.getActiveSubscriptions(Email).get(0);
                assertEquals("Plan name is Starter", subscription.plan, "Starter");
                assertEquals("Subscription has 1 month cycle", subscription.cycle, 1);
                assertEquals("Subscription has expected GB amount", subscription.quantity, Bandwidth_Amount);
                //TODO needs some way to test subscription date if start date and renewal days are differ
                // (like, 30 days is added to subscription after renewal, and not after start date
                assertEquals("Subscription has certain amount of days", subscription.days, 30);
                //TODO local date will not work with UTC
                assertTrue("Subscription start date is today", subscription.start.toLocalDate().isEqual(LocalDate.now()));
            }, true);

            test("Validate that proxy connection is restored", () -> {
                Dashboard = Omad.quick.loginAsClient(Email);
                var balance = Dashboard.isp_rotating.home.open().getBalance();
                assertEquals("Balance of trial is expected (proper to amount specified on trial creation)",
                        balance, Bandwidth_Amount);
                var proxyCreds = Dashboard.isp_rotating.home.getCommonCreds();
                proxy_authorization.waitUntil();
                assertTrue("At least 5mb downloaded through Rotating Residential proxy",
                        ProxyChecker.downloadWithISPRotating(proxyCreds, 5) >= 5.0);
            }, true);
        }

    }



    @Nested
    @Order(3)
    @TestMethodOrder(MethodOrderer.OrderAnnotation.class)
    public class ProxyConnectionBundle {

        static String Email = null;
        final int Bandwidth_Amount = 2;

        @Test
        @Order(1)
        public void AccessToTrialProxiesGotSuspendedOnDownload() {
            String email = Helper.generateUniqueEmail();
            var get = new Object() { String proxyCreds; };

            test("Create rotating residential trial from OMAD", () -> {
                Dashboard = Omad.quick.login().createTrial(email, Bandwidth_Amount, 1).loginAsClient(email);

                step("Get Rotating Residential Credentials");
                var proxyAuhtorized = new Wait(Config.sleep.v2_dashboard.isp_rotating_authorization_wait);
                get.proxyCreds = Dashboard.isp_rotating.home.open().getCommonCreds();

                step("Add 1020mb into DB bandwidth directly");
                DB.quick.v2_backend.addBandwidth(email, 1020);

                step("Test proxy connection works, download last 4mb+ to get to 1GB");
                proxyAuhtorized.waitUntil();
                assertTrue("At least 4mb was downloaded",
                        ProxyChecker.downloadWithISPRotating(get.proxyCreds, 10) >= 4);

            });

            test("Validate that bandwith balance is <= 0", () -> {
                Helper.sleep(Config.sleep.v2_dashboard.balance_updated_wait);
                assertTrue("Balance is <= 0", Dashboard.isp_rotating.home.open().getBalance() <= 0);
            }, true);

            test("Test that proxies cannot be connected anymore", () -> {
                Helper.sleep(Config.sleep.v2_dashboard.isp_rotating_authorization_wait);
                try{
                    assertTrue("Proxy suspended, and nothing downloaded",
                            ProxyChecker.downloadWithISPRotating(get.proxyCreds, 2) == 0);
                } catch(RuntimeException e){
                    //expected result
                }
            });

            Email = email;
        }

        @Test
        @Order(2)
        public void AccessToTrialProxiesKeepsSuspendedAfterRenewalInvoiceGenerated() throws URLIsImproper, PageTimeoutException, ConditionTimeoutException, NoSuchElementException, WrongPageException, JSchException, SQLException, ClassNotFoundException, TabAlreadySavedException, InterruptedException, ParseException {

            assert(Email != null);
            String email = Email;

            var subscriptions = Rebilly.quick.openUser(email).getActiveSubscriptions();
            //date that be in a minute; if it's less than 30 seconds until next minute, then it will be minute after next minute
            int currentSecond = LocalDateTime.now().getSecond();
            var inAMinute = new Date(new Date().getTime() + (currentSecond > 30 ? 120 - currentSecond : 60 - currentSecond) * 1000);

            //change renewal date
            Rebilly.subscription.open(subscriptions.get(0).id).changeTrialRenewalDate(inAMinute);
            //wait until package is updated from rebilly
            new Wait(inAMinute).waitUntil();
            Helper.sleep(Config.sleep.rebilly.product_update_on_cron_wait);

            Wait isp_rotating_authorization = new Wait(Config.sleep.v2_dashboard.isp_rotating_authorization_wait);

            var result = DB.quick.v2_backend.getSubscriptionsBandwidth(email);
            assert(result.size() != 0);
            assert(result.size() == 1);
            assert(result.get(0).get("status").equals("suspended"));
            assert(result.get(0).get("type").equals("traffic"));

            //check balance, and get creds for proxy checker test
            Omad.quick.login();
            Dashboard = Omad.users.open().loginAsUser(email);
            String proxyCreds = Dashboard.isp_rotating.home.open().getCommonCreds();
            assert(Dashboard.isp_rotating.home.getBalance() == 0);

            isp_rotating_authorization.waitUntil();

            try {
                assert(ProxyChecker.downloadWithISPRotating(proxyCreds, 5) == 0);
            } catch(RuntimeException e){
                //expected, connection should be suspended
            }
        }

        @Test
        @Order(3)
        public void AccessToTrialProxiesGotRestoredOnPaymentAfterDownloadSuspend() throws URLIsImproper, PageTimeoutException, NoSuchElementException, WrongPageException, TabAlreadySavedException, InterruptedException, ConditionTimeoutException {

            assert(Email != null);
            String email = Email;

            var invoice = Rebilly.quick.login().openUser(email).getUnpaidInvoices().get(0);
            assert(Float.parseFloat(invoice.get("amount")) != 0);
            Rebilly.invoice.open(invoice.get("id")).makeFreeAndPaid();
            Helper.sleep(Config.sleep.rebilly.product_sync_wait);

            var proxy_authorization = new Wait(Config.sleep.v2_dashboard.isp_rotating_authorization_wait);

            Omad.quick.login();
            Dashboard = Omad.users.open().loginAsUser(email);
            var balance = Dashboard.isp_rotating.home.open().getBalance();
            assert(balance == Bandwidth_Amount);
            var proxyCreds = Dashboard.isp_rotating.home.getCommonCreds();

            proxy_authorization.waitUntil();

            assert(ProxyChecker.downloadWithISPRotating(proxyCreds, 5) >= 5.0);
        }
    }



    @Nested
    @Order(4)
    public class PurchaseAndCancellationTests {

        @Test
        public void ProxiesGotSuspendedOnTrialCancellation() throws URLIsImproper, PageTimeoutException, ConditionTimeoutException, NoSuchElementException, WrongPageException, ParseException, JSchException, SQLException, ClassNotFoundException, TabAlreadySavedException, InterruptedException {
            String email = Helper.generateUniqueEmail();
            Omad.quick.login().createTrial(email, 1, 1);

            //get proxy credentials, wait until authorization completed, and test proxy
            var isp_rotating_authorization = new Wait(Config.sleep.v2_dashboard.isp_rotating_authorization_wait);
            Dashboard = Omad.users.open().loginAsUser(email);
            String proxyCreds = Dashboard.isp_rotating.home.open().getCommonCreds();
            isp_rotating_authorization.waitUntil();
            assert(ProxyChecker.downloadWithISPRotating(proxyCreds, 5) >= 5.0);
            Wait balance_updated = new Wait(Config.sleep.v2_dashboard.balance_updated_wait);

            //cancel order
            var subscriptions = Rebilly.quick.openUser(email).getActiveSubscriptions();
            Rebilly.subscription.open(subscriptions.get(0).id).cancelOrder();
            //wait until package is updated from rebilly
            Helper.sleep(Config.sleep.rebilly.product_sync_wait);

            Wait isp_rotating_suspend = new Wait(Config.sleep.v2_dashboard.isp_rotating_authorization_wait);

            var result = DB.quick.v2_backend.getSubscriptionsBandwidth(email);
            assert(result.size() != 0);
            assert(result.size() == 1);
            assert(result.get(0).get("status").equals("terminated"));
            assert(result.get(0).get("type").equals("traffic"));

            //validate balance reset
            balance_updated.waitUntil();
            assert(Dashboard.isp_rotating.home.open().getBalance() == 0);

            isp_rotating_suspend.waitUntil();

            try {
                assert(ProxyChecker.downloadWithISPRotating(proxyCreds, 5) == 0);
            } catch(RuntimeException | InterruptedException e){
                //expected, connection should be suspended
            }
        }

        @Test
        public void ProxiesGotSuspendedOnTrialSuspend() throws URLIsImproper, PageTimeoutException, ConditionTimeoutException, NoSuchElementException, WrongPageException, ParseException, JSchException, SQLException, ClassNotFoundException, TabAlreadySavedException, InterruptedException {
            String email = Helper.generateUniqueEmail();
            var get = new Object() { String proxyCreds; };

            test("Create trial", () -> {
                Omad.quick.login().createTrial(email, 1, 1);

                var isp_rotating_authorization = new Wait(Config.sleep.v2_dashboard.isp_rotating_authorization_wait);
                Dashboard = Omad.quick.loginAsClient(email);
                get.proxyCreds = Dashboard.isp_rotating.home.open().getCommonCreds();
                assertEquals("Balance equals to 1gb", Dashboard.isp_rotating.home.getBalance(), 1f);
                isp_rotating_authorization.waitUntil();
            });

            test("Test proxy connection", () -> {
                assertTrue("Proxy connection works", ProxyChecker.downloadWithISPRotating(get.proxyCreds, 1) >= 1.0);
            }, true);

            test("Suspend trial order", () -> {
                Rebilly.quick.makeTrialSubscriptionRenewalDateToSuspendInAMinute(
                        Rebilly.quick.openUser(email).getActiveSubscriptions().get(0).id).waitUntil();
            });

            test("Test proxy connection is suspended", () -> {
                Wait isp_rotating_suspend = new Wait(Config.sleep.v2_dashboard.isp_rotating_authorization_wait);

                assertEquals("Balance is 0", Dashboard.isp_rotating.home.open().getBalance(), 0f);

                isp_rotating_suspend.waitUntil();

                try {
                    assertEquals("Nothing downloaded by proxy checker",
                            ProxyChecker.downloadWithISPRotating(get.proxyCreds, 1), 0f);
                } catch(RuntimeException | InterruptedException e){
                    //expected, connection should be suspended
                }
            });
        }

        @Test
        public void TestBalanceChangesOnOrderCancellation() {
            String email = Helper.generateUniqueEmail();

            test(() -> {
                step("Create account with unpaid product");
                Dashboard.signup.open().doCreateAccount(new ProxyType(ProxyCategory.Semi), 5, email);
                Dashboard.quick.redirectToDashboardFromSignup().confirmTFA();

                step("Purchase one time bandwidth twice");
                Dashboard.quick.purchaseAdditionalBandwidth(1, email);
                Dashboard.quick.purchaseAdditionalBandwidth(1, email);
                Helper.sleep(Config.sleep.rebilly.product_sync_wait);
                assertEquals("Balance equals to expected", Dashboard.isp_rotating.home.open().getBalance(), 2f);
                Dashboard.any.open().quit();

                step("Create trial from OMAD");
                Omad.quick.login().createTrial(email, 3, 1);

                step("Cancel trial order");
                Rebilly.subscription.open(Rebilly.quick.openUser(email).getActiveSubscriptions().get(0).id).cancelOrder();
                Helper.sleep(Config.sleep.rebilly.product_sync_wait);

                step("Validate balance is 0");
                assertEquals("Balance is equals to expected",
                        Omad.quick.loginAsClient(email).isp_rotating.home.open().getBalance(), 0f);
            });
        }

        @Test
        public void BandwidthPurchaseAfterTrialCancellation() {
            String email = Helper.generateUniqueEmail();

            test(() -> {
                step("Create trial from OMAD");
                Omad.quick.login().createTrial(email, 3, 1);

                Dashboard = Omad.users.open().loginAsUser(email);
                assertEquals("Balance equals to 1", Dashboard.isp_rotating.home.open().getBalance(), 1f);

                step("Cancel trial order");
                Rebilly.subscription.open(Rebilly.quick.openUser(email).getActiveSubscriptions().get(0).id).cancelOrder();
                Helper.sleep(Config.sleep.rebilly.product_sync_wait);
                assertEquals("Balance equals to 0", Dashboard.isp_rotating.home.open().getBalance(), 0f);

                step("Purchase one time bandwidth plans twice");
                Dashboard.quick.purchaseAdditionalBandwidth(1, email);
                Dashboard.quick.purchaseAdditionalBandwidth(1, email);
                Helper.sleep(Config.sleep.rebilly.product_sync_wait);

                step("Validate that bandwidth is added");
                assertEquals("Balance equals to expected", Dashboard.isp_rotating.home.open().getBalance(), 2f);
            });
        }

        @Test
        public void BalanceOverriddenOnTrialEnds() {
            String email = Helper.generateUniqueEmail();
            int amount = 3;

            test(() -> {
                step("Create trial from OMAD for %dGB".formatted(amount));
                Omad.quick.login().createTrial(email, amount, 1);

                Dashboard = Omad.users.open().loginAsUser(email);
                assertEquals("Balance equals to 1", Dashboard.isp_rotating.home.open().getBalance(), 1f);

                step("Buy one time bandwidth twice");
                Dashboard.quick.purchaseAdditionalBandwidth(1, email);
                Dashboard.quick.purchaseAdditionalBandwidth(1, email);
                assertEquals("Balance equals to expected", Dashboard.isp_rotating.home.open().getBalance(), 3f);

                step("Wait until trial period ends");
                Rebilly.quick.makeTrialSubscriptionRenewalDateInAMinute(Rebilly.quick.openUser(email).getActiveSubscriptions().get(0).id)
                        .waitUntil();

                step("Validate that balance is set to 0");
                assertEquals("Balance equals to 0", Dashboard.isp_rotating.home.open().getBalance(), 0f);
            });
        }

        @Test
        public void BalanceOverriddenOnTrialSubscriptionProlonged() {
            String email = Helper.generateUniqueEmail();
            int amount = 3;

            test(() -> {
                step("Create trial from OMAD for %dGB".formatted(amount));
                Omad.quick.login().createTrial(email, amount, 1);

                Dashboard = Omad.users.open().loginAsUser(email);
                assertEquals("Balance equals to 1", Dashboard.isp_rotating.home.open().getBalance(), 1f);

                step("Wait until trial period ends");
                Rebilly.quick.makeTrialSubscriptionRenewalDateInAMinute(Rebilly.quick.openUser(email).getActiveSubscriptions().get(0).id)
                        .waitUntil();
                assertEquals("Balance equals to 0", Dashboard.isp_rotating.home.open().getBalance(), 0f);

                step("Buy one time bandwidth twice");
                Dashboard.quick.purchaseAdditionalBandwidth(1, email);
                Dashboard.quick.purchaseAdditionalBandwidth(1, email);
                assertEquals("Balance equals to expected", Dashboard.isp_rotating.home.open().getBalance(), 2f);

                step("Make subscription order (generated on trial) paid");
                Rebilly.quick.makeLastUnpaidInvoicePaid(email);;
                assertEquals("Balance equals to expected",
                        Dashboard.isp_rotating.home.open().getBalance(), (float) amount + 2f);
            });
        }

        @Test
        public void TrialCreatedAndRenewedOnExistingSubscription() {
            String email = Helper.generateUniqueEmail();
            int amount = 3;

            test(() -> {
                step("Signup with Rotating Residential 50GB purchase");
                Dashboard.quick.signupWithPurchase(new ProxyType(ProxyCategory.ISP_Rotating), 50, email);
                Dashboard = Omad.quick.loginAsClient(email);
                assertEquals("Balance equals to expected", Dashboard.isp_rotating.home.open().getBalance(), 50f);

                step("Create trial for same customer for %dGB".formatted(amount));
                Omad.quick.login().createTrial(email, amount, 1);
                assertEquals("Balance equals to expected", Dashboard.isp_rotating.home.open().getBalance(), 51f);

                step("Wait trial renewal date");
                Rebilly.quick.makeTrialSubscriptionRenewalDateInAMinute(Rebilly.quick.openUser(email).getActiveSubscriptions().get(0).id)
                        .waitUntil();
                assertEquals("Balance equals to expected", Dashboard.isp_rotating.home.open().getBalance(), 0f);

                step("Make trial paid");
                Rebilly.quick.makeLastUnpaidInvoicePaid(email);
                assertEquals("Balance equals to expected", Dashboard.isp_rotating.home.open().getBalance(), (float) amount);
            });
        }

        @Test
        public void TrialCreatedAndRenewedWhileAnotherTrialAlreadyExists() {
            String email = Helper.generateUniqueEmail();
            int amount = 3;

            test(() -> {
                step("Create trial for new customer for %dGB".formatted(amount));
                Omad.quick.createTrial(email, amount, 1);
                Dashboard = Omad.quick.loginAsClient(email);
                assertEquals("Balance equals to expected", Dashboard.isp_rotating.home.open().getBalance(), 1f);

                step("Create trial for same customer for %dGB".formatted(amount));
                Omad.quick.createTrial(email, amount, 2);

                var subscriptions = Rebilly.quick.openUser(email).getActiveSubscriptions();

                step("Wait until first trial's renewal date");
                Rebilly.quick.makeTrialSubscriptionRenewalDateInAMinute(subscriptions.get(1).id).waitUntil();
                assertEquals("Balance equals to expected", Dashboard.isp_rotating.home.open().getBalance(), 0f);

                step("Make trial paid");
                Rebilly.quick.makeLastUnpaidInvoicePaid(email);
                assertEquals("Balance equals to expected", Dashboard.isp_rotating.home.open().getBalance(), (float) amount);

                step("Cancel second trial");
                Rebilly.subscription.open(subscriptions.get(0).id).cancelOrder();
                assertEquals("Balance equals to expected", Dashboard.isp_rotating.home.open().getBalance(), 0f);
            });
        }

        @Test
        public void TrialCreatedSecondTimeAfterPreviousTrialIsCancelled() {
            String email = Helper.generateUniqueEmail();
            int amount = 3;

            test(() -> {
                step("Create trial for new customer for %dGB".formatted(amount));
                Omad.quick.createTrial(email, amount, 1);
                Dashboard = Omad.quick.loginAsClient(email);
                assertEquals("Balance equals to expected", Dashboard.isp_rotating.home.open().getBalance(), 1f);

                step("Cancel trial order");
                Rebilly.subscription.open(Rebilly.quick.openUser(email).getActiveSubscriptions().get(0).id).cancelOrder();
                assertEquals("Balance equals to expected", Dashboard.isp_rotating.home.open().getBalance(), 0f);

                step("Create trial for same customer for %dGB".formatted(amount));
                Omad.quick.createTrial(email, amount, 2);
                assertEquals("Balance equals to expected", Dashboard.isp_rotating.home.open().getBalance(), 1f);

                step("Wait until first trial's renewal date");
                Rebilly.quick.makeTrialSubscriptionRenewalDateInAMinute(
                                Rebilly.quick.openUser(email).getActiveSubscriptions().get(1).id)
                        .waitUntil();
                assertEquals("Balance equals to expected", Dashboard.isp_rotating.home.open().getBalance(), 0f);

                step("Make trial paid");
                Rebilly.quick.makeLastUnpaidInvoicePaid(email);
                assertEquals("Balance equals to expected", Dashboard.isp_rotating.home.open().getBalance(), (float) amount);
            });
        }
    }



    @Nested
    @Order(5)
    public class ProxyConnectionTests {

        @Test
        public void ConnectionRestoredOnAdditionalBandwidthAfterOutOfBalance(){
            String email = Helper.generateUniqueEmail();
            test("Create trial for new account", () -> {
                Omad.quick.createTrial(email, 1, 1);

                step("Reduce balance by 1020mb through DB directly, download remaining MB to get proxies suspended");
                var proxyAuthorized = new Wait(Config.sleep.v2_dashboard.isp_rotating_authorization_wait);
                Dashboard = Omad.quick.loginAsClient(email);
                String creds = Dashboard.isp_rotating.home.open().getCommonCreds();
                DB.quick.v2_backend.addBandwidth(email, 1020);
                proxyAuthorized.waitUntil();
                assertTrue("At least 4mb downloaded through Residential Proxy",
                        ProxyChecker.downloadWithISPRotating(creds, 10) >= 4f);

                step("Test connection is suspended");
                Helper.sleep(Config.sleep.v2_dashboard.isp_rotating_authorization_wait);
                try{
                    assertEquals("Nothing downloaded through proxies",
                            ProxyChecker.downloadWithISPRotating(creds, 10), 0f);
                } catch (Exception e){
                    //expected
                }

                step("Purchase additional bandwidth");
                Dashboard.isp_rotating.buy.open().doBuy(1);
                Rebilly.quick.makeLastUnpaidInvoicePaid(email);

                step("Test connection is unsuspended");
                Helper.sleep(Config.sleep.v2_dashboard.isp_rotating_authorization_wait);
                assertTrue("10mb downloaded through Residential Proxy",
                        ProxyChecker.downloadWithISPRotating(creds, 10) >= 10f);
            });
        }

        @Test
        public void ConnectionRestoredOnAdditionalBandwidthAfterCancellation(){
            String email = Helper.generateUniqueEmail();
            test("Create trial for new account", () -> {
                Omad.quick.createTrial(email, 1, 1);
                var proxiesAuthorized = new Wait(Config.sleep.v2_dashboard.isp_rotating_authorization_wait);
                Dashboard = Omad.quick.loginAsClient(email);
                String creds = Dashboard.isp_rotating.home.open().getCommonCreds();
                proxiesAuthorized.waitUntil();
                assertTrue("Download through Rotating Residential works",
                        ProxyChecker.downloadWithISPRotating(creds, 1) >= 1f);

                step("Suspend proxies by cancelling trial order");
                Rebilly.subscription.open(Rebilly.quick.openUser(email).getActiveSubscriptions().get(0).id).cancelOrder();

                step("Test connection is suspended");
                Helper.sleep(Config.sleep.v2_dashboard.isp_rotating_authorization_wait);
                try{
                    assertEquals("Nothing downloaded through proxies",
                            ProxyChecker.downloadWithISPRotating(creds, 10), 0f);
                } catch (Exception e){
                    //expected
                }

                step("Purchase additional bandwidth");
                Dashboard.isp_rotating.buy.open().doBuy(1);
                Rebilly.quick.makeLastUnpaidInvoicePaid(email);

                step("Test connection is unsuspended");
                Helper.sleep(Config.sleep.v2_dashboard.isp_rotating_authorization_wait);
                assertTrue("10mb downloaded through Residential Proxy",
                        ProxyChecker.downloadWithISPRotating(creds, 1) >= 1f);
            });
        }

        @Test
        public void ConnectionRestoredOnAdditionalBandwidthAfterRenewal(){
            String email = Helper.generateUniqueEmail();
            test("Create trial for new account", () -> {
                Omad.quick.createTrial(email, 1, 1);
                var proxiesAuthorized = new Wait(Config.sleep.v2_dashboard.isp_rotating_authorization_wait);
                Dashboard = Omad.quick.loginAsClient(email);
                String creds = Dashboard.isp_rotating.home.open().getCommonCreds();
                proxiesAuthorized.waitUntil();
                assertTrue("Download through Rotating Residential works",
                        ProxyChecker.downloadWithISPRotating(creds, 5) >= 5f);

                step("Suspend proxies by waiting until trial is ended");
                Rebilly.quick.makeTrialSubscriptionRenewalDateInAMinute(Rebilly.quick.openUser(email).getActiveSubscriptions().get(0).id)
                        .waitUntil();

                step("Test connection is suspended");
                Helper.sleep(Config.sleep.v2_dashboard.isp_rotating_authorization_wait);
                try{
                    assertEquals("Nothing downloaded through proxies",
                            ProxyChecker.downloadWithISPRotating(creds, 10), 0f);
                } catch (Exception e){
                    //expected
                }

                step("Purchase additional bandwidth");
                Dashboard.isp_rotating.buy.open().doBuy(1);
                Rebilly.quick.makeLastUnpaidInvoicePaid(email);

                step("Test connection is unsuspended");
                Helper.sleep(Config.sleep.v2_dashboard.isp_rotating_authorization_wait);
                assertTrue("10mb downloaded through Residential Proxy",
                        ProxyChecker.downloadWithISPRotating(creds, 10) >= 10f);
            });
        }
    }

    @Nested
    @Order(5)
    public class BalanceRenewalTests {

        @Test
        public void BalanceAddedAndRenewedOnTrialEnd() {
            test(() -> {
               step("Create trial package");
               String email = Helper.generateUniqueEmail();
               float trialAmount = 0.5f;
               int amount = 1;
               Omad.quick.login();
               Omad.trial.create.open().createTrial(email, Config.env.password, trialAmount, amount, 2);

               var user = Omad.quick.loginAsClient(email);
               assertEquals("Trial balance is updated", user.isp_rotating.home.open().getMonthlyBalance(), trialAmount * 1024);
               assertEquals("No additional balance", user.isp_rotating.home.getAdditionalBalance(), 0);

               step("Make renewal invoice paid");
               Rebilly.quick.makeLastUnpaidInvoicePaid(email);
               assertEquals("Balance updated to subscription balance", user.isp_rotating.home.open().getMonthlyBalance(), amount * 1024);
            });
        }

    }


    @Nested
    @Order(6)
    public class MiscellaneousTests {

        @Test
        public void EmailMessagesReceivedOnTrialCreationForNewAndExistingUser() {
            String email = Helper.generateUniqueEmail();
            test("Create new account with Rotating Residential trial", () -> {
                Omad.quick.createTrial(email, 1, 1);
            });

            test("Check emails on trial creation for new account", () -> {
                waitUntil("3 signup emails are received", () -> MailChecker.getMailList(email).size() >= 3,
                        Config.sleep.common.email_is_being_delivered_timeout, true);
                var subjects = MailChecker.getMailList(email).stream().map(e -> e.subject).toList();
                assertEquals("3 email messages are received", subjects.size(), 3);
                assertTrue("Welcome email received", subjects.contains("Welcome"));
                assertTrue("Order email received", subjects.contains("Order Confirmation"));
                assertTrue("Invoice email received", subjects.contains("Trial created"));
            }, true);

            var timestamp = new Date();

            test("Create Rotating Residential trial for same account", () -> {
                Omad.quick.createTrial(email, 1, 2);
            });

            test("Check emails on trial creation for existing account", () -> {
                waitUntil("3 signup emails are received", () -> MailChecker.getMailList(email, timestamp).size() >= 2,
                        Config.sleep.common.email_is_being_delivered_timeout, true);
                var subjects = MailChecker.getMailList(email, timestamp).stream().map(e -> e.subject).toList();
                assertEquals("2 email messages are received", subjects.size(), 2);
                assertTrue("Order email received", subjects.contains("Order Confirmation"));
                assertTrue("Invoice email received", subjects.contains("Trial created"));
            }, true);
        }
    }



    @Nested
    @Order(9)
    public class RepeatableTests{

        @Test
        public void AmountOfBandwidthGotDeliveredProperlyToRequested() throws URLIsImproper, PageTimeoutException, NoSuchElementException, WrongPageException, JSchException, SQLException, ClassNotFoundException, ConditionTimeoutException, ParseException {

            //Authorize OMAD trial create page, authorize Rebilly admin
            Omad.login.open().doLogin();
            Omad.trial.create.open();

            Rebilly.quick.login();

            int repeats = 50;
            var random = new Random();

            //until tries left, create trials for new user with random amount and period
            while(repeats-- > 0){
                String email = Helper.generateUniqueEmail();
                int amount = random.nextInt(15) + 1;
                int days = random.nextInt(3) + 1;

                //create trial, validate backend that package is delivered properly
                Omad.quick.createTrial(email, amount, days);
                var result = DB.quick.v2_backend.getSubscriptionsBandwidth(email);
                assert(result.size() != 0);
                assert(result.size() == 1);
                assert(result.get(0).get("status").equals("in_trial"));
                assert(result.get(0).get("type").equals("traffic"));
                assert(Float.parseFloat(result.get(0).get("value")) == 1);

                //validate rebilly that package is delivered properly
                var subscriptions = Rebilly.quick.openUser(email).getActiveSubscriptions();
                assert(subscriptions.size() == 1);
                assert(subscriptions.get(0).days == days);
                assert(subscriptions.get(0).quantity == amount);
                assert(subscriptions.get(0).plan.equals("Starter"));
            }
        }

        @Test
        public void TestProxyConnectionaIsAdded(){
            test("Prepare test", () -> {
                int repeats = 30;

                class TestConnection implements Runnable {

                    String email;
                    Wait authorized;
                    String creds;
                    String error;
                    Thread thread;
                    float downloaded = 0;
                    boolean success = false;

                    public TestConnection(String email, Wait proxyAuthorized){
                        this.email = email;
                        this.authorized = proxyAuthorized;
                    }

                    public TestConnection(String email, String creds, Wait proxyAuthorized){
                        this.email = email;
                        this.authorized = proxyAuthorized;
                        this.creds = creds;
                    }

                    @Override
                    public void run() {
                        authorized.waitUntil();
                        try {
                            downloaded = ProxyChecker.downloadWithISPRotating(creds, 2);
                            success = downloaded >= 2f;
                        } catch (RuntimeException | InterruptedException e) {
                            error = e.getMessage();
                        }
                    }
                }

                var tests = new ArrayList<TestConnection>();
                Omad.quick.login();
                Omad.trial.create.open();
                var Omad_login = new OMAD(SceneManager.getDefault().tab("omad-login-as-client"));
                Omad_login.quick.login();
                Omad_login.users.open();

                step("Generate %d accounts with trial packages".formatted(repeats));
                while(repeats-- > 0){
                    String email = Helper.generateUniqueEmail();
                    Omad.trial.create.createTrial(email, Config.env.password,1, 1);
                    /*Dashboard.tab().close();
                    Dashboard = Omad_login.users.loginAsUser(email);
                    String creds = Dashboard.isp_rotating.home.open().getCommonCreds();
                    var test = new TestConnection(email, creds, new Wait(Config.sleep.v2_dashboard.isp_rotating_authorization_wait));
                    test.thread = new Thread(test);
                    test.thread.start();
                    tests.add(test);*/
                    tests.add(new TestConnection(email, new Wait(Config.sleep.v2_dashboard.isp_rotating_authorization_wait)));
                }

                step("Start ProxyChecker connection tests");
                for(var test : tests){
                    Dashboard.tab().close();
                    Dashboard = Omad_login.users.loginAsUser(test.email);
                    test.creds = Dashboard.isp_rotating.home.open().getCommonCreds();
                    test.thread = new Thread(test);
                    test.thread.start();
                }

                step("Wait until all accounts tested");
                for(var test : tests){
                    test.thread.join();
                    if(test.success == false){
                        error("Proxy Check download failed on account %s (mb downloaded: %f)".formatted(test.email, test.downloaded)
                                + (test.error.isEmpty() ? "" : " - " + test.error));
                    }
                }
            });
        }
    }



    @Nested
    @Order(10)
    public class RegressionTests{

        @Test
        public void BandwidthDoesNotGetAddedWithoutPaymentOnSignup() throws PageTimeoutException, ConditionTimeoutException, WebFormErrorException, NoSuchElementException, URLIsImproper, WrongPageException, JSchException, SQLException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
            SignupTests.single().BandwidthDoesNotGetAddedWithoutPaymentCheck();
        }

        @Test
        public void AdditionalBandwidthOnSignupCreatesAccessToProxies() throws JSchException, PageTimeoutException, SQLException, ConditionTimeoutException, WebFormErrorException, ClassNotFoundException, InterruptedException, NoSuchElementException, WrongPageException, InvocationTargetException, URLIsImproper, NoSuchMethodException, InstantiationException, IllegalAccessException {
            ResidentialProxyTests.single().AdditionalBandwidthOnSignupCreatesAccessToProxies();
        }

        @Test
        public void TestCommonProductsCreated() throws JSchException, PageTimeoutException, SQLException, ConditionTimeoutException, WebFormErrorException, ParseException, ClassNotFoundException, InterruptedException, NoSuchElementException, WrongPageException, URLIsImproper, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
            SignupTests.single().MultipleProxyTypesSignupTest();
        }
    }

}
