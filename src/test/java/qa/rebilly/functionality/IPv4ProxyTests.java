package qa.rebilly.functionality;

import com.blazingseollc.qa.base.Config;
import com.blazingseollc.qa.env.rb.DashboardV2;
import com.blazingseollc.qa.tools.DB;
import com.blazingseollc.qa.tools.Helper;
import com.blazingseollc.qa.tools.ProxyChecker;
import com.blazingseollc.qa.base.ProxyType;
import com.blazingseollc.qa.base.ProxyType.ProxyCategory;
import com.blazingseollc.qa.base.ProxyType.ProxyCountry;
import com.jcraft.jsch.JSchException;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.vitmush.qa.selenium_frame.base.Debug;
import org.vitmush.qa.selenium_frame.abstracts.AbstractTest;
import org.vitmush.qa.selenium_frame.exceptions.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class IPv4ProxyTests extends AbstractTest {

    //TODO High
    //TODO auth type change test
    //TODO check for errors on replacement page
    //TODO test Rotation time change
    //TODO locations redirect
    //TODO auth ip is addable
    //TODO skip ipv4 auth test ond dev
    //TODo emails received
    //TODO ip export (partially)
    //TODO api connection (in incognito)

    //TODO Medium
    //TODO locations first set and update
    //TODO auth ip validation on existing/wrong ip
    //TODO api connection and changeability(no CF cache)
    //TODO all api methods are tested

    static List<String> testProxyConnection_Emails;
    static Date testProxyConnection_PreparedTimestamp;
    static List<String> proxyReplacementTest_Emails;
    static Date proxyReplacementTest_PreparedTimestamp;

    @Test
    @Order(0)
    public void PrepareAccountsForTests() throws URLIsImproper, PageTimeoutException, ConditionTimeoutException, WebFormErrorException, NoSuchElementException, WrongPageException, IOException, InterruptedException, JSchException, SQLException, ClassNotFoundException {

        //create three accounts with Dedicated, Rotating and Semi-Dedicated proxies, configure PWD, IP auth type,
        // rotation time for rotating proxies;
        // Save email, save timestamp, accounts will be used to check proxy connection later;
        if(Config.current_env == Config.Environment.PRODUCTION) {
            testProxyConnection_Emails = new ArrayList<>();
            String myIP = ProxyChecker.getConnectionIp();
            var d = new DashboardV2();

            d.quick.signupWithPurchase(new ProxyType(ProxyCategory.Dedicated))
                    .redirectToDashboardFromSignup().confirmTFA().setAllLocationsToMixed(true);
            d.ipv4.dashboard.open().dismissActiveModals().setAuthPWD();
            d.any.quit();
            testProxyConnection_Emails.add(Helper.lastGeneratedUniqueEmail);

            d.quick.signupWithPurchase(new ProxyType(ProxyCategory.Semi))
                    .redirectToDashboardFromSignup().confirmTFA().setAllLocationsToMixed(true);
            d.ipv4.dashboard.open().dismissActiveModals().setAuthIP(myIP);
            d.any.quit();
            testProxyConnection_Emails.add(Helper.lastGeneratedUniqueEmail);

            d.quick.signupWithPurchase(new ProxyType(ProxyCategory.Rotating)).redirectToDashboardFromSignup().confirmTFA();
            d.ipv4.dashboard.open().dismissActiveModals().setAuthIP(myIP);
            var portIds = d.ipv4.dashboard.getPortsList().stream()
                    .filter(p -> Boolean.parseBoolean(p.get("rotating"))).map(p -> p.get("id")).toList();
            for (var id : portIds) {
                d.ipv4.dashboard.setRotationInterval(id, 100);
            }
            d.any.quit();
            testProxyConnection_Emails.add(Helper.lastGeneratedUniqueEmail);
            testProxyConnection_PreparedTimestamp = new Date();
        }

        var d = new DashboardV2();
        // Prepare another account for ProxyReplacementTest
        d.quick.signupWithPurchase(new ProxyType(ProxyCategory.Dedicated))
                .redirectToDashboardFromSignup().confirmTFA().setAllLocationsToMixed(true);
        d.any.quit();
        proxyReplacementTest_Emails = List.of(Helper.lastGeneratedUniqueEmail);
        proxyReplacementTest_PreparedTimestamp = new Date();

    }

    @Test
    @Order(1)
    public void SettingsChangeTest() throws URLIsImproper, PageTimeoutException, ConditionTimeoutException, WebFormErrorException, NoSuchElementException, WrongPageException, JSchException, SQLException, ClassNotFoundException {
        var d = new DashboardV2();
        String email = Helper.generateUniqueEmail();
        d.signup.open().doCreateAccount(new ProxyType(ProxyCategory.Dedicated), 5, email);
        d.quick.redirectToDashboardFromSignup().confirmTFA();

        //set settings to true, check if checkboxes changed status after reload, check values on proxy backend then
        d.ipv4.settings.open().setRotationOnDead(true).setMonthlyRotation(true);
        d.tab().reload();
        assert(d.ipv4.settings.getMonthlyRotationStatus() == true);
        assert(d.ipv4.settings.getRotationOnDeadStatus() == true);
        var result = DB.quick.proxy_backend.getUserInfo(email);
        assert(result.get("rotate_ever").equals("0"));
        assert(result.get("rotate_30").equals("1"));

        //same check, but check that checkboxes are disabled
        d.ipv4.settings.open().setRotationOnDead(false).setMonthlyRotation(false);
        d.tab().reload();
        assert(d.ipv4.settings.getMonthlyRotationStatus() == false);
        assert(d.ipv4.settings.getRotationOnDeadStatus() == false);
        result = DB.quick.proxy_backend.getUserInfo(email);
        assert(result.get("rotate_ever").equals("1"));
        assert(result.get("rotate_30").equals("0"));
    }

    @Test
    @Order(10)
    public void MultipleTypePackagesDeliveredCheck() throws URLIsImproper, PageTimeoutException, ConditionTimeoutException, WebFormErrorException, NoSuchElementException, WrongPageException, JSchException, SQLException, ClassNotFoundException {
        ProxyType[] testingTypes = new ProxyType[] {
                new ProxyType(ProxyCategory.Dedicated),
                new ProxyType(ProxyCategory.Rotating),
                new ProxyType(ProxyCategory.Semi),
                new ProxyType(ProxyCategory.ISP),
                new ProxyType(ProxyCategory.Sneaker),
                new ProxyType(ProxyCategory.Shopify),
                new ProxyType(ProxyCategory.Dedicated, ProxyCountry.Brazil),
                new ProxyType(ProxyCategory.Dedicated, ProxyCountry.Japan),
                new ProxyType(ProxyCategory.Semi, ProxyCountry.Germany),
                new ProxyType(ProxyCategory.Dedicated, ProxyCountry.UK),
                new ProxyType(ProxyCategory.Dedicated, ProxyCountry.Canada),
                new ProxyType(ProxyCategory.Rotating, ProxyCountry.Germany)
        };

        for(var type : testingTypes){
            var d = new DashboardV2();
            int amount = d.quick.tools.getSignupQuantity(type);

            //TODO fix exception 'Url 'https://app.rebilly.com/4086bbf4-bfdd-49cd-ad47-2311ace34a26/customers/803b1c53-741e-473f-bba1-75f32b59fe58' not page model of urlRebilly Admin-Invoice expected url - 'https://app.rebilly.com/*/customers/*'':
            // ProxyChecker.java:215
            d.quick.signupWithPurchase(type).redirectToDashboardFromSignup().confirmTFA();

            //validate that delivered package is expected
            var packages_backend = DB.quick.proxy_backend.getIPv4Packages(Helper.lastGeneratedUniqueEmail);
            var start = new Date().getTime();
            var timeoutMillis = Config.sleep.proxy_backend.package_is_added_timeout * 1000;
            while(packages_backend.size() == 0) {
                Helper.sleep(10);
                if(new Date().getTime() - start > timeoutMillis)
                    throw new RuntimeException("Package was not added into account after %f minutes".formatted(timeoutMillis / 1000 / 60.0));
                packages_backend = DB.quick.proxy_backend.getIPv4Packages(Helper.lastGeneratedUniqueEmail);
            }
            assert(packages_backend.size() == 1);
            assert(packages_backend.get(0).get("country").equals(ProxyType.AliasCollection.common.countryCode.of(type.country)));
            assert(packages_backend.get(0).get("category").equals(ProxyType.AliasCollection.common.categoryCode.of(type.category)));
            assert(Integer.parseInt(packages_backend.get(0).get("amount")) == amount);

            //check that locations are proper to package, set all to Mixed
            if(List.of(ProxyCategory.Dedicated, ProxyCategory.Semi).contains(type.category)) {
                d.quick.waitLocationsRedirect();
                var packages_location = d.ipv4.locations.getPackagesSummary();
                assert(packages_location.size() == 1);
                assert(packages_location.get(0).get("country").equals(ProxyType.AliasCollection.common.countryCode.of(type.country)));
                assert(packages_location.get(0).get("category").equals(ProxyType.AliasCollection.dashboardv2.categoryCode.of(type.category)));
                assert(Integer.parseInt(packages_location.get(0).get("total")) == amount);
                d.ipv4.locations.setLocationsToMixed(type);
                d.home.assertPageOpened();
            }

            //validate that proper ports are shown on ipv4 dashboard
            var ports = d.ipv4.dashboard.open().dismissActiveModals().getPortsList();
            assert(ports.size() == amount);
            assert(ports.get(0).get("type").startsWith(
                    "%s %s".formatted(ProxyType.AliasCollection.dashboardv2.countryShortName.of(type.country), ProxyType.AliasCollection.common.categoryName.of(type.category))));

            d.any.quit();
        }
    }

    @Test
    @Order(20)
    public void TestProxyConnection() throws URLIsImproper, PageTimeoutException, NoSuchElementException, WrongPageException, TabAlreadySavedException, IOException, InterruptedException, WrongValueException {
        if(Config.current_env != Config.Environment.PRODUCTION)
            return;

        //sleep using timestamp from PrepareAccountsForAuthTest until time for authorization fulfilled
        int authorizationAwaitSeconds = 20 * 60;
        long secondsPassed = (new Date().getTime() - testProxyConnection_PreparedTimestamp.getTime()) / 1000;
        if(secondsPassed < authorizationAwaitSeconds){
            int waitSeconds = (int) (authorizationAwaitSeconds - secondsPassed);
            Debug.Log("Waiting %f minutes until proxies authorized".formatted(waitSeconds / 60.0));
            Helper.sleep(waitSeconds);
        }

        //check proxy connections on prepared accounts
        var d = new DashboardV2();
        for(var email : testProxyConnection_Emails){
            d.signin.open().login(email);
            var ips = d.ipv4.dashboard.open().openExportAll().getIPs().stream().skip(2).toList();
            var rotatingPorts = d.ipv4.dashboard.getPortsList().stream()
                    .filter(p -> Boolean.parseBoolean(p.get("rotating")))
                    .map(p -> Map.of("%s:%s".formatted(p.get("ip"), p.get("port")), p.get("actual_ip")))
                    .flatMap(m -> m.entrySet().stream()).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
            for(var ip : ips){
                if(rotatingPorts.isEmpty() == false){
                    String actualIp = rotatingPorts.get(ip);
                    //TODO fix exception 'Response Status Code: 407 Proxy Authentication Required'
                    // ProxyChecker.java:215
                    ProxyChecker.testIPv4Connection(ip, actualIp);
                } else {
                    ProxyChecker.testIPv4Connection(ip);
                }
            }
            d.any.quit();
        }
    }

    @Test
    @Order(21)
    public void ProxyReplacementTest() throws URLIsImproper, PageTimeoutException, NoSuchElementException, WrongPageException, JSchException, SQLException, ClassNotFoundException {
        var d = new DashboardV2();
        //amountOfIPsToReplace should not be more than prepared package
        final int amountOfIPsToReplace = 3;

        d.signin.open().login(proxyReplacementTest_Emails.get(0));

        //wait for ips to be delivered (at least required quantity of amountOfIPsToReplace)
        List<Map<String, String>> ports;
        int maxWaitMillis = Config.sleep.proxy_backend.ips_get_assigned_timeout * 1000;
        //Subtract already passed time
        var start = proxyReplacementTest_PreparedTimestamp.getTime();
        boolean skipFirstSleep = true;
        do {
            if(skipFirstSleep)
                skipFirstSleep = false;
            else Helper.sleep(60);

            if(new Date().getTime() - start > maxWaitMillis)
                throw new RuntimeException("IPs weren't delivered in %f minutes".formatted(maxWaitMillis / 1000 / 60.0));

            ports = d.ipv4.dashboard.open().dismissActiveModals().getPortsList()
                    .stream().filter(p -> !p.get("ip").isEmpty()).toList();
        } while (ports.size() < amountOfIPsToReplace);

        //replace ips with single ip and multiple ips method
        var replace = ports.stream().limit(amountOfIPsToReplace).toList();
        d.ipv4.replacements.open().replaceSingleIp(replace.stream().findFirst().get().get("ip"));
        d.ipv4.replacements.replaceMultipleIps(replace.stream().map(n -> n.get("ip")).skip(1).limit(amountOfIPsToReplace - 1).toList());

        //check if ports currently are in replacement
        String portIds = replace.stream().map(p -> p.get("id")).collect(Collectors.joining("', '", "'", "'"));
        assert(amountOfIPsToReplace == Integer.parseInt(DB.getSession(DB.Host.Proxy_Backend)
                .select("select sum(pending_replace) sum from user_ports where id in (%s)".formatted(portIds))
                .fetchFirst().get("sum")));
    }
}
