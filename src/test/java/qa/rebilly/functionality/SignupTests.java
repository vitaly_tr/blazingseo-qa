package qa.rebilly.functionality;

import com.blazingseollc.qa.base.Config;
import com.blazingseollc.qa.base.ProxyType;
import com.blazingseollc.qa.env.blazing.BDR;
import com.blazingseollc.qa.env.rb.RebillyAdmin;
import com.blazingseollc.qa.env.rb.DashboardV2;
import com.blazingseollc.qa.abstracts.AbstractRebillyTest;
import com.blazingseollc.qa.tools.*;
import com.blazingseollc.qa.base.ProxyType.*;
import com.jcraft.jsch.JSchException;
import org.junit.jupiter.api.*;
import org.vitmush.qa.selenium_frame.base.Debug;
import org.vitmush.qa.selenium_frame.base.SceneManager;
import org.vitmush.qa.selenium_frame.exceptions.*;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.text.ParseException;
import java.time.Year;
import java.util.Date;

class SignupTests extends AbstractRebillyTest {

    private SignupTests() {  }

    static public SignupTests single() throws InvocationTargetException, URLIsImproper, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return (SignupTests) new SignupTests().getSingle();
    }

    //TODO High
    //TODO check link on invoice after it's paid
    //TODO validate errors are seen by user
    //TODO validate emails body in CheckThatEmailsAreReceived
    //TODO validate all pricing tiers appear on dashboard
    //TODO validate all checks are working properly

    //TODO Medium
    //TODO compare validations fields on rebilly



    public void test1() throws ParseException, URLIsImproper, NoSuchElementException, PageTimeoutException, ConditionTimeoutException, WrongPageException, JSchException, SQLException, ClassNotFoundException {

        DashboardV2 d = new DashboardV2(SceneManager.add().tab());
        d.signup.open().doFillRequiredFields(new ProxyType(ProxyType.ProxyCategory.ISP_Rotating), 50,
                Helper.generateUniqueEmail(), "tester");

        d.waitUserAction();
    }


    public void test2() throws URLIsImproper, NoSuchElementException, PageTimeoutException, ConditionTimeoutException, WrongPageException, JSchException, SQLException, ClassNotFoundException, WebFormErrorException {

        String email = Helper.generateUniqueEmail();

        BDR b = new BDR();
        b.quick.createAccount(email);

        DashboardV2 d = new DashboardV2();
        d.signup.open().doFillRequiredFields(new ProxyType(ProxyType.ProxyCategory.Semi), 10,
                email, "tester").doSubmit();
        RebillyAdmin a = new RebillyAdmin();
        a.quick.makeLastUnpaidInvoicePaid(email);
        d.signup.open();

        d.waitUserAction();
    }

    public void test3() throws URLIsImproper, NoSuchElementException, PageTimeoutException, ConditionTimeoutException, WrongPageException, JSchException, SQLException, ClassNotFoundException, WebFormErrorException {

        String email = Helper.generateUniqueEmail();
        DashboardV2 d = new DashboardV2();
        d.signup.open().doFillRequiredFields(new ProxyType(ProxyType.ProxyCategory.ISP_Rotating), 53,
                email, "tester").doSubmit().makeCardPayment();

        d.signup.open();
        if(d.tfa.isOpened())
            d.tfa.confirmTFA(DB.quick.proxy_backend.getTFACode(email));

        float balance = d.isp_rotating.home.open().getBalance();
        d.isp_rotating.buy.open().doBuy(2).makeCardPayment();
        Helper.sleep(20);
        Debug.Log("Prev Balance: %f, New Balance: %f".formatted(balance, d.isp_rotating.home.open().getBalance()));

        d.waitUserAction();
    }

    public void DoSignup() throws URLIsImproper, PageTimeoutException, ConditionTimeoutException, NoSuchElementException, WrongPageException, WebFormErrorException, JSchException, SQLException, ClassNotFoundException {
        DashboardV2 d = new DashboardV2(SceneManager.add().tab());
        String email = Helper.generateUniqueEmail();
        Debug.Log(email);
        d.signup.open()
                .doFillRequiredFields(new ProxyType(ProxyType.ProxyCategory.ISP_Rotating), 50, email, Config.env.password);

        d.signup.doFillOptionalFields().doSubmit(); //.setCountry("Albania").setCompanyName("Sprious"); //.doSubmit();
        d.invoice.assertPageOpened();
        var db_info = DB.quick.v2_backend.getClientInfo(email);

    }

    @Test
    @Order(1)
    public void DoSignupWithInformationFieldsCheck() throws URLIsImproper, PageTimeoutException, ConditionTimeoutException, NoSuchElementException, WrongPageException, WebFormErrorException, JSchException, SQLException, ClassNotFoundException {
        var proxyType = new ProxyType(ProxyType.ProxyCategory.ISP_Rotating);
        String email = Helper.generateUniqueEmail();
        String firstName = email.split("@")[0].replace("+", ".");
        String lastName = "---";
        String phoneCountryCode = "+380";
        String phoneNumber = "2020";
        String street = "street";
        String street2 = "street2";
        String city = "city";
        String state = "state";
        String postcode = "00000";
        String purpose = "[QA Automation]";
        String company = "Sprious";
        String birthday = "1996-10-23";

        DashboardV2 d = new DashboardV2();
        d.signup.open().setProxyType(proxyType).setAmount(50).setFirstName(firstName).setLastName(lastName)
                .setEmail(email).setPassword(Config.env.password)
                .setPhoneCountryCode(phoneCountryCode).setPhoneNumber(phoneNumber).setCompanyName(company)
                .setStreet(street).setStreet2(street2).setCity(city).setState(state)
                .setPostcode(postcode).setBirthDay(23).setBirthMonth(10).setBirthYear(1996)
                .setPurpose(DashboardV2.Signup.PurchasePurpose.Other, purpose)
                .clickCheckbox().clickSubmit();
        d.invoice.assertPageOpened();

        var db_info = DB.quick.v2_backend.getClientInfo(email);
        String[][] correctValues = new String[][] {
                {"first_name", firstName}, {"last_name", lastName}, {"company_name", company}, {"birthday", birthday},
                {"address", street}, {"address2", street2}, {"state", state}, {"city", city}, {"post_code", postcode},
                {"country", "UA"}, {"phone_number",phoneCountryCode + phoneNumber}, {"purpose", purpose}
        };
        for(var check : correctValues){
            assertEquals("Client info saved with exact value from signup (%s)".formatted(check[0]), db_info.get(check[0]), check[1]);
        }
    }

    @Test
    @Order(1)
    public void DoSignupWithGenericPurposeFieldCheck() throws URLIsImproper, NoSuchElementException, WrongPageException, PageTimeoutException, ConditionTimeoutException, WebFormErrorException, JSchException, SQLException, ClassNotFoundException {
        var d = new DashboardV2();
        d.signup.open().doFillRequiredFields(new ProxyType(ProxyType.ProxyCategory.Dedicated), 5
                , Helper.generateUniqueEmail(), Config.env.password).setPurpose(DashboardV2.Signup.PurchasePurpose.WebScraping).doSubmit();
        assertEquals("Purpose saved properly to selected option",
                DB.quick.v2_backend.getClientInfo(Helper.lastGeneratedUniqueEmail).get("purpose"), "Web scraping/Data collection");
    }

    @Test
    @Order(1)
    public void BandwidthDoesNotGetAddedWithoutPaymentCheck() throws URLIsImproper, NoSuchElementException, WrongPageException, PageTimeoutException, ConditionTimeoutException, WebFormErrorException, JSchException, SQLException, ClassNotFoundException {
        var d = new DashboardV2();
        d.signup.open().doCreateAccount(new ProxyType(ProxyCategory.ISP), 50, Helper.generateUniqueEmail());
        d.quick.redirectToDashboardFromSignup().confirmTFA();
        Helper.sleep(Config.sleep.v2_dashboard.balance_updated_wait);
        assertEquals("Balance is 0", d.isp_rotating.home.open().getBalance(), 0f);
    }

    @Test
    @Order(1)
    public void CheckThatEmailsAreReceivedOnSignup(){
        String email = Helper.generateUniqueEmail();
        test("Create account with unpaid product", () -> {
            Dashboard.signup.open().doCreateAccount(new ProxyType(ProxyCategory.Semi), 5, email);
        });

        test("Check that signup emails are received", () -> {
            waitUntil("3 signup emails are received", () -> MailChecker.getMailList(email).size() >= 3,
                    Config.sleep.common.email_is_being_delivered_timeout, true);
            var subjects = MailChecker.getMailList(email).stream().map(e -> e.subject).toList();
            assertEquals("3 email messages are received", subjects.size(), 3);
            assertTrue("Welcome email received", subjects.contains("Welcome"));
            assertTrue("Order email received", subjects.contains("Order Confirmation"));
            assertTrue("Invoice email received", subjects.contains("An Invoice Has Been Generated"));
        }, true);

        test("Check OTP confirmation email is received", () -> {
            var emailTimestamp = new Date();
            Dashboard.quick.redirectToDashboardFromSignup();
            if(false == Dashboard.tfa.isOpened())
                exit("TFA page is not opened, not able to continue test");
            waitUntil("TFA email is received", () -> MailChecker.getMailList(email, emailTimestamp).size() > 0,
                    Config.sleep.common.email_is_being_delivered_timeout);
            var emails = MailChecker.getMailList(email, emailTimestamp);
            assertEquals("Only 1 email received recently", emails.size(), 1);
            assertEquals("TFA message is received", orEmpty(() -> emails.get(0).subject), "Please confirm your account");
        }, true);
    }

    @Test
    @Order(2)
    public void CheckThatFieldsAreRequired() throws URLIsImproper, NoSuchElementException, WrongPageException, PageTimeoutException, ConditionTimeoutException, WebFormErrorException {

        ProxyType proxyType = new ProxyType(ProxyType.ProxyCategory.Dedicated);
        String email = Helper.generateUniqueEmail();
        String password = Config.env.password;
        int totalErrors = 11;

        var d = new DashboardV2();
        d.signup.open().setProxyType(proxyType).setAmount(5);
        try {
            d.signup.doSubmit();
            throw new RuntimeException("Submit expected to fail");
        } catch (WebFormErrorException e) {
            assertEquals("All errors are shown (amount check)", e.context.size(), totalErrors);
        }

        d.signup.setStreet("street").setCity("city").setEmail(email).setPassword(password);
        try {
            d.signup.doSubmit();
            throw new RuntimeException("Submit expected to fail");
        } catch (WebFormErrorException e) {
            assertEquals("4 errors less are shown after street, city, email and password filled",
                    e.context.size(), totalErrors - 4);
        }

        d.signup.doFillRequiredFields(proxyType, 5, email, password);
        d.signup.clickCheckbox();
        try {
            d.signup.doSubmit();
            throw new RuntimeException("Submit expected to fail");
        } catch (WebFormErrorException e) {
            assertEquals("Only 1 - tos/aup related - error is shown", e.context.size(), 1);
            assertEquals("Error message is the one expected",
                    e.context.get(0).trim(), "You must agree to our terms to continue with a purchase");
        }
        d.signup.waitErrorDisappeared().clickCheckbox().clickSubmit();
        d.invoice.assertPageOpened();

    }

    @Test
    @Order(2)
    public void CheckSignupAgeRestriction() throws URLIsImproper, NoSuchElementException, WrongPageException, PageTimeoutException, ConditionTimeoutException, WebFormErrorException {
        var d = new DashboardV2();
        d.signup.open().doFillRequiredFields(new ProxyType(ProxyType.ProxyCategory.Dedicated), 7,
                        Helper.generateUniqueEmail(), Config.env.password)
                .setBirthYear(Year.now().getValue() - 10).setBirthMonth(9);
        try {
            d.signup.doSubmit();
            throw new RuntimeException("Error expected on submitting form with setting birth year to just 10 year ago (18 years old restriction didn't work)");
        } catch (WebFormErrorException ex){
            //Debug.Log(ex.toString());
            //Debug.Log(ex.context.stream().collect(Collectors.joining("\n")));
            assertEquals("Age restriction error is shown", ex.context.size(), 1);
            assertEquals("Expected error message is shown",
                    ex.context.get(0).trim(), "You must be 18 years or older to purchase.");
        }

        try {
            d.signup.setBirthYear(Year.now().getValue() - 17).doSubmit();
            throw new RuntimeException("Error expected on submitting form with setting birth year to just 17 year ago (18 years old restriction didn't work)");
        } catch (WebFormErrorException ex){
            assertEquals("Age restriction error is shown", ex.context.size(), 1);
            assertEquals("Expected error message is shown",
                    ex.context.get(0).trim(), "You must be 18 years or older to purchase.");
        }

        d.signup.setBirthYear(Year.now().getValue() - 18);
        d.signup.waitErrorDisappeared().doSubmit();
        d.invoice.assertPageOpened();
    }

    @Test
    @Order(100)
    public void ValidateSignupToInvoicePricesEquality() throws URLIsImproper, NoSuchElementException, WrongPageException, PageTimeoutException, ConditionTimeoutException, WebFormErrorException {
        Object[][] cases = new Object[][] {
                { new ProxyType(ProxyType.ProxyCategory.Dedicated, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Monthly), 12, 24 },
                { new ProxyType(ProxyType.ProxyCategory.Rotating, ProxyType.ProxyCountry.Germany, ProxyType.BillingCycle.Monthly), 108, 344.52 },
                { new ProxyType(ProxyType.ProxyCategory.Semi, ProxyType.ProxyCountry.Brazil, ProxyType.BillingCycle.Monthly), 501, 425.85 },
                { new ProxyType(ProxyType.ProxyCategory.Semi, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Monthly), 1002, 601.2 },
                { new ProxyType(ProxyType.ProxyCategory.Rotating, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Quarterly), 51, 436.05 },
                { new ProxyType(ProxyType.ProxyCategory.Dedicated, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Semi_Annually), 35, 378 },
                { new ProxyType(ProxyType.ProxyCategory.ISP, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Annually), 11, 561 },
                { new ProxyType(ProxyType.ProxyCategory.ISP, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Monthly), 6, 30 },
                { new ProxyType(ProxyType.ProxyCategory.Shopify, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Monthly), 12, 27.6 },
                { new ProxyType(ProxyType.ProxyCategory.Sneaker, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Monthly), 6, 15 },
                { new ProxyType(ProxyType.ProxyCategory.ISP_Rotating, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Monthly), 51, 102 },
                { new ProxyType(ProxyType.ProxyCategory.ISP_Rotating, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Monthly), 150, 300 }
        };

        for(var c : cases) {
            test(() -> {
                ProxyType type = (ProxyType) c[0];
                var d = new DashboardV2();
                d.signup.open().doFillRequiredFields(type,
                        c.length > 1 ? (int) c[1] : d.quick.tools.getSignupQuantity(type),
                        Helper.generateUniqueEmail(), Config.env.password);
                float signupTotal = d.signup.getTotal();
                assertNotEquals("Signup total doesn't equal to 0", signupTotal, 0);
                //compare with last known price
                if(c.length > 2) {
                    float knownPrice;
                    if(c[2] instanceof Double)
                        knownPrice = (float) (double) c[2];
                    else if(c[2] instanceof Integer)
                        knownPrice = (float) (int) c[2];
                    else knownPrice = (float) c[2];
                    if(signupTotal != knownPrice)
                        Debug.Warning(String.format("Wrong price for '%s': %f, expected price: %f", type.toString(), signupTotal, knownPrice));
                }
                d.signup.doSubmit();
                d.invoice.assertPageOpened();
                float invoiceTotal = d.invoice.getTotal();
                assertEquals("Price on signup(actual) equals to price on invoice", signupTotal, invoiceTotal);
            }, true);

            SceneManager.quit();
        }
    }

    @Test
    @Order(101)
    public void MultipleProxyTypesSignupTest() throws NoSuchElementException, WrongPageException, PageTimeoutException, ConditionTimeoutException, URLIsImproper, WebFormErrorException, ParseException, JSchException, SQLException, ClassNotFoundException, InterruptedException {
        Object[][] cases = new Object[][] {
                { new ProxyType(ProxyType.ProxyCategory.ISP_Rotating, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Monthly) },
                { new ProxyType(ProxyType.ProxyCategory.Dedicated, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Monthly) },
                { new ProxyType(ProxyType.ProxyCategory.Rotating, ProxyType.ProxyCountry.Germany, ProxyType.BillingCycle.Monthly) },
                { new ProxyType(ProxyType.ProxyCategory.Semi, ProxyType.ProxyCountry.Brazil, ProxyType.BillingCycle.Monthly) },
                { new ProxyType(ProxyType.ProxyCategory.Rotating, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Quarterly) },
                { new ProxyType(ProxyType.ProxyCategory.Dedicated, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Semi_Annually) },
                { new ProxyType(ProxyType.ProxyCategory.ISP, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Annually) },
                { new ProxyType(ProxyType.ProxyCategory.Shopify, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Monthly) },
                { new ProxyType(ProxyType.ProxyCategory.Sneaker, ProxyType.ProxyCountry.USA, ProxyType.BillingCycle.Monthly) }
        };

        for(Object[] c : cases){
            test(() -> {

                var d = new DashboardV2();
                ProxyType type = (ProxyType) c[0];

                step("Signup with: %s".formatted(type.toString()));

                if(c.length > 1)
                    d.quick.signupWithPurchase(type, (int) c[1]);
                else d.quick.signupWithPurchase(type);

                int amount = (c.length > 1) ? (int) c[1] : d.quick.tools.getSignupQuantity((ProxyType) c[0]);

                if(type.category == ProxyType.ProxyCategory.ISP_Rotating){
                    d.signup.open();
                    //TODO check ISP Rotating product name
                    //should happen redirect to dashboard home instead
                    if(d.tfa.isOpened()) {
                        Helper.sleep(Config.sleep.proxy_backend.tfa_gets_saved_wait);
                        d.tfa.confirmTFA(DB.quick.proxy_backend.getTFACode(Helper.lastGeneratedUniqueEmail));
                    }
                    Helper.sleep(Config.sleep.v2_dashboard.isp_rotating_authorization_wait);
                    String creds = d.isp_rotating.home.open().getCommonCreds();
                    assertEquals("Proper balance is added", d.isp_rotating.home.getBalance(), (float) amount);
                    assertTrue("Proxy connection works", ProxyChecker.downloadWithISPRotating(creds, 5) >= 5);
                } else {
                    new RebillyAdmin().quick.openUser(Helper.lastGeneratedUniqueEmail).assertHasSubscription(type);
                    var backend_package = DB.quick.proxy_backend.getIPv4Packages(Helper.lastGeneratedUniqueEmail).get(0);

                    assertEquals("Country of package is proper", backend_package.get("country"), AliasCollection.common.countryCode.of(type.country));
                    assertEquals("Category of package is proper", backend_package.get("category"), AliasCollection.common.categoryCode.of(type.category));
                    assertEquals("Amount in package is proper",backend_package.get("amount"), String.valueOf(amount));
                }
            }, true);

            SceneManager.quit();
        }
    }

}
