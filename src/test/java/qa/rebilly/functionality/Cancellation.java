package qa.rebilly.functionality;

import com.blazingseollc.qa.abstracts.AbstractRebillyTest;
import com.blazingseollc.qa.env.blazing.ProxyDashboard;
import com.blazingseollc.qa.env.rb.DashboardV2;
import com.blazingseollc.qa.env.rb.RebillyAdmin;
import com.blazingseollc.qa.tools.Helper;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class Cancellation extends AbstractRebillyTest {

    @Nested
    class StaticResidentialTrial {

        @Test
        void CancelOrderWithOverdueInvoice (){
            test(() -> {
                String email = Helper.generateUniqueEmail();
                Omad.quick.createTrial(email);
                step("Confirm trial created");
                DashboardV2 d = Omad.quick.loginAsClient(email);
                assertTrue("Balance is added", d.isp_rotating.home.open().getMonthlyBalance() > 0,
                        true);

                step("Overdue trial and make invoice paid");
                RebillyAdmin.User.SubscriptionInfo s = Rebilly.quick.getActiveSubscriptions(email).get(0);
                Rebilly.quick.makeTrialSubscriptionRenewalDateInAMinute(s.id).waitUntil();
                Rebilly.quick.makeLastUnpaidInvoicePaid(email);

                step("Cancel order");
                Rebilly.subscription.open(s.id).cancelOrder();
                assertEquals("Balance is 0", Dashboard.isp_rotating.home.open().getMonthlyBalance(), 0);
            });
        }

        @Test
        void CancelOrderWithUnpaidInvoice(){

        }

        @Test
        void CancelOrderAfterFirstPaid(){

        }

    }


}
