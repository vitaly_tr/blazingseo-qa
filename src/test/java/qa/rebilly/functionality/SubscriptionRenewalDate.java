package qa.rebilly.functionality;

import com.blazingseollc.qa.abstracts.AbstractRebillyTest;
import com.blazingseollc.qa.base.Config;
import com.blazingseollc.qa.tools.DB;
import com.blazingseollc.qa.tools.Helper;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class SubscriptionRenewalDate extends AbstractRebillyTest {

    //TODO trial
    // Changing renewal date on trial doesn't change date_from, but changes date_to
    // ^ same case but if trial changed without invoice generated
    // Making payment on renewal invoice on trial updates date_from and date_to
    //TODO
    // Changing renewal date on common  subscription changes only date_to
    // Making payment on renewal invoice of common product changes date_from and date_to
    // Failing payment on renewal invoice of common product doesn't change anything
    // Do not making payment on renewal invoice of common product doesn't change anything
    //TODO Low
    // One time subscription should not have date_from and date_to
    // Making payment on upgrade invoice doesn't change anything
    // Making downgrade - doesn't change anything

    @Nested
    class Trial {

        @Test
        public void ChangingRenewalOnTrialWithoutInvoiceTest() {
            test("Create trial", () -> {
                var email = Helper.generateUniqueEmail();
                Omad.quick.createTrial(email, 1, 15);

                var db = DB.getSession(DB.Host.v2_Backend);
                var query = (
                        "select s.status, s.date_from, s.date_to " +
                                "from subscriptions s " +
                                "join users u on u.id = s.user_id " +
                                "where u.email = '%s'").formatted(email);


                var dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                var result = db.select(query).fetchFirst();
                var date_from_original = dateFormat.parse(result.get("date_from"));
                var date_to_original = dateFormat.parse(result.get("date_to"));
                var date_to_expected = DateUtils.addDays(date_to_original, 2);

                step("Change trial renewal date");
                var subscription = Rebilly.quick.openUser(email).getActiveSubscriptions().get(0);
                Rebilly.subscription.open(subscription.id).changeTrialRenewalDate(date_to_expected);
                Helper.sleep(Config.sleep.rebilly.product_sync_wait);

                step("Test how date_from and date_to changed");
                result = db.select(query).fetchFirst();
                assertEquals("date_from didn't change", result.get("date_from"),
                        dateFormat.format(date_from_original.getTime()));
                assertEquals("date_to is changed", result.get("date_to"),
                        dateFormat.format(date_to_expected.getTime()));
            });
        }

        @Test
        public void ChangingRenewalOnTrialWithInvoiceTest() {
            test("Create trial", () -> {
                Omad.quick.createTrial();

                var email = Helper.lastGeneratedUniqueEmail;
                var db = DB.getSession(DB.Host.v2_Backend);
                var query = (
                        "select s.status, s.date_from, s.date_to " +
                                "from subscriptions s " +
                                "join users u on u.id = s.user_id " +
                                "where u.email = '%s'").formatted(email);


                var dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                var result = db.select(query).fetchFirst();
                var date_from_original = dateFormat.parse(result.get("date_from"));
                var date_to_original = dateFormat.parse(result.get("date_to"));
                var date_to_expected = DateUtils.addDays(date_to_original, 2);

                step("Change trial renewal date");
                var subscription = Rebilly.quick.openUser(email).getActiveSubscriptions().get(0);
                Rebilly.subscription.open(subscription.id).changeTrialRenewalDate(date_to_expected);
                Helper.sleep(Config.sleep.rebilly.product_sync_wait);

                step("Test how date_from and date_to changed");
                result = db.select(query).fetchFirst();
                assertEquals("date_from didn't change", result.get("date_from"),
                        dateFormat.format(date_from_original.getTime()));
                assertEquals("date_to is changed", result.get("date_to"),
                        dateFormat.format(date_to_expected.getTime()));
            });
        }

        @Test
        public void ChangingRenewalOnTrialPaymentTest() {
            test("Create trial", () -> {
                Omad.quick.createTrial();

                var email = Helper.lastGeneratedUniqueEmail;
                var db = DB.getSession(DB.Host.v2_Backend);
                var query = (
                        "select s.status, s.date_from, s.date_to " +
                                "from subscriptions s " +
                                "join users u on u.id = s.user_id " +
                                "where u.email = '%s'").formatted(email);

                var dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

                step("Make invoice paid");
                Rebilly.invoice.open(Rebilly.quick.openUser(email).getUnpaidInvoices().get(0).get("id"))
                        .openInvoiceAsClient().makeCardPayment();
                Rebilly.quick.makeTrialSubscriptionRenewalDateInAMinute(
                        Rebilly.quick.openUser(email).getActiveSubscriptions().get(0).id).waitUntil();

                step("Test how date_from and date_to changed");
                var subscription = Rebilly.quick.openUser(email).getActiveSubscriptions().get(0);
                var result = db.select(query).fetchFirst();
                assertEquals("date_from is changed", result.get("date_from"),
                        dateFormat.format(subscription.start));
                assertEquals("date_to is changed", result.get("date_to"),
                        dateFormat.format(subscription.renewal));
            });
        }

    }


    @Nested
    class CommonSubscription {

    }

}
