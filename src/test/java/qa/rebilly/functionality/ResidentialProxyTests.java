package qa.rebilly.functionality;

import com.blazingseollc.qa.base.Config;
import com.blazingseollc.qa.abstracts.AbstractRebillyTest;
import com.blazingseollc.qa.base.ProxyType;
import com.blazingseollc.qa.tools.Helper;
import com.blazingseollc.qa.tools.*;
import com.blazingseollc.qa.base.ProxyType.*;
import com.jcraft.jsch.JSchException;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.vitmush.qa.selenium_frame.exceptions.*;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;

public class ResidentialProxyTests extends AbstractRebillyTest {

    private ResidentialProxyTests() {  }

    static public ResidentialProxyTests single() throws InvocationTargetException, URLIsImproper, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return (ResidentialProxyTests) new ResidentialProxyTests().getSingle();
    }

    //TODO High
    //TODO proxy suspends
    //TODO proxy unsuspend
    //TODO proxy additional purchases
    //TODO proxy connection
    //TODO emails received

    //TODO Medium
    //TODO page open flow
    //TODO balance is seen and can be updated
    //TODO basic chart updates

    @Test
    @Order(1)
    public void SignupWithRotatingResidential() throws PageTimeoutException, ConditionTimeoutException, WebFormErrorException, NoSuchElementException, WrongPageException, URLIsImproper, InterruptedException, JSchException, SQLException, ClassNotFoundException {
        int amount = 52;
        Dashboard.quick.signupWithPurchase(new ProxyType(ProxyCategory.ISP_Rotating), amount);

        Dashboard.signup.open();
        //TODO check ISP Rotating product name
        //should happen redirect to dashboard home instead
        if (Dashboard.tfa.isOpened()) {
            Helper.sleep(Config.sleep.proxy_backend.tfa_gets_saved_wait);
            Dashboard.tfa.confirmTFA(DB.quick.proxy_backend.getTFACode(Helper.lastGeneratedUniqueEmail));
        }
        Helper.sleep(Config.sleep.v2_dashboard.isp_rotating_authorization_wait);
        String creds = Dashboard.isp_rotating.home.open().getCommonCreds();
        assertEquals("Balance equals to amount purchase", Dashboard.isp_rotating.home.getBalance(), amount);
        assertTrue("At least 1mb is downloaded", ProxyChecker.downloadWithISPRotating(creds, 1) >= 1);
    }


    @Test
    public void AdditionalBandwidthOnSignupCreatesAccessToProxies() throws PageTimeoutException, NoSuchElementException, WrongPageException, ConditionTimeoutException, WebFormErrorException, JSchException, SQLException, ClassNotFoundException, InterruptedException, URLIsImproper {
        var email = Helper.generateUniqueEmail();
        //create account without paying invoice
        Dashboard.signup.open().doCreateAccount(new ProxyType(ProxyCategory.Semi), 5, email);
        Dashboard.quick.redirectToDashboardFromSignup().confirmTFA();

        //buy additional bandwith, wait proxies to authorize
        Dashboard.quick.purchaseAdditionalBandwidth(1, email);
        Helper.sleep(Config.sleep.v2_dashboard.isp_rotating_authorization_wait);

        //validate balance, validate connection
        assertEquals("Balance equals to amount purchased", Dashboard.isp_rotating.home.open().getBalance(), 1);
        String creds = Dashboard.isp_rotating.home.getCommonCreds();
        assertTrue("At least 1mb is downloaded", ProxyChecker.downloadWithISPRotating(creds, 1) >= 1);
    }

}
